"use strict";

require("core-js/modules/es.object.define-property.js");
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _express = _interopRequireDefault(require("express"));
var _controllers = require("./controllers");
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
var routes = (0, _express["default"])();
var provincePath = "/ref/province";
var cityPath = "/ref/city";
var facilityTypePath = "/ref/facilitytype";
var statusPath = "/ref/status";
var ownershipPath = "/ref/ownership";
var fasyankesPath = "/ref/fasyankes";
routes.route(provincePath).get(_controllers.Controllers_References.getProvince);
routes.route(cityPath).get(_controllers.Controllers_References.getCity);
routes.route(facilityTypePath).get(_controllers.Controllers_References.getFacility);
routes.route(statusPath).get(_controllers.Controllers_References.getStatus);
routes.route(ownershipPath).get(_controllers.Controllers_References.getOwnerShip);
routes.route(fasyankesPath).get(_controllers.Controllers_References.getFasyankes);
var _default = exports["default"] = routes;