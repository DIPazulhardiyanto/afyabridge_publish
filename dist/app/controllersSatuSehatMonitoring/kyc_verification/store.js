"use strict";

require("core-js/modules/es.symbol.js");
require("core-js/modules/es.symbol.description.js");
require("core-js/modules/es.symbol.iterator.js");
require("core-js/modules/es.array.iterator.js");
require("core-js/modules/es.object.define-property.js");
require("core-js/modules/es.object.to-string.js");
require("core-js/modules/es.string.iterator.js");
require("core-js/modules/web.dom-collections.iterator.js");
require("core-js/modules/es.symbol.to-primitive.js");
require("core-js/modules/es.date.now.js");
require("core-js/modules/es.date.to-primitive.js");
require("core-js/modules/es.number.constructor.js");
require("core-js/modules/es.parse-int.js");
function _typeof(o) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) { return typeof o; } : function (o) { return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o; }, _typeof(o); }
function _classCallCheck(a, n) { if (!(a instanceof n)) throw new TypeError("Cannot call a class as a function"); }
function _defineProperties(e, r) { for (var t = 0; t < r.length; t++) { var o = r[t]; o.enumerable = o.enumerable || !1, o.configurable = !0, "value" in o && (o.writable = !0), Object.defineProperty(e, _toPropertyKey(o.key), o); } }
function _createClass(e, r, t) { return r && _defineProperties(e.prototype, r), t && _defineProperties(e, t), Object.defineProperty(e, "prototype", { writable: !1 }), e; }
function _defineProperty(e, r, t) { return (r = _toPropertyKey(r)) in e ? Object.defineProperty(e, r, { value: t, enumerable: !0, configurable: !0, writable: !0 }) : e[r] = t, e; }
function _toPropertyKey(t) { var i = _toPrimitive(t, "string"); return "symbol" == _typeof(i) ? i : i + ""; }
function _toPrimitive(t, r) { if ("object" != _typeof(t) || !t) return t; var e = t[Symbol.toPrimitive]; if (void 0 !== e) { var i = e.call(t, r || "default"); if ("object" != _typeof(i)) return i; throw new TypeError("@@toPrimitive must return a primitive value."); } return ("string" === r ? String : Number)(t); }
var authDetail;
var DefaultAuthStore = /*#__PURE__*/function () {
  function DefaultAuthStore() {
    _classCallCheck(this, DefaultAuthStore);
    _defineProperty(this, "ANTICIPATION", 300);
  }
  return _createClass(DefaultAuthStore, [{
    key: "get",
    value:
    // seconds
    function get() {
      if (authDetail) {
        var _authDetail = authDetail,
          issued_at = _authDetail.issued_at,
          expires_in = _authDetail.expires_in;
        var issuedAt = parseInt(issued_at, 10);
        var expiresIn = parseInt(expires_in, 10);
        if (!issuedAt || !expiresIn) {
          authDetail = undefined;
          return;
        }
        // expiration time in milliseconds
        var expirationTime = issuedAt + expiresIn * 1000;
        // time when the token is considered about to expire
        var aboutToExpireTime = expirationTime - this.ANTICIPATION * 1000;
        // compare with the current time, if expired set detail to undefined
        if (aboutToExpireTime <= Date.now()) {
          authDetail = undefined;
        }
      }
      return authDetail;
    }
  }, {
    key: "set",
    value: function set(detail) {
      authDetail = detail;
    }
  }]);
}();
module.exports = {
  DefaultAuthStore: DefaultAuthStore
};