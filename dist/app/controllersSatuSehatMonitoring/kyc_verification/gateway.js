"use strict";

require("core-js/modules/es.symbol.js");
require("core-js/modules/es.symbol.description.js");
require("core-js/modules/es.symbol.async-iterator.js");
require("core-js/modules/es.symbol.iterator.js");
require("core-js/modules/es.symbol.to-primitive.js");
require("core-js/modules/es.symbol.to-string-tag.js");
require("core-js/modules/es.array.filter.js");
require("core-js/modules/es.array.for-each.js");
require("core-js/modules/es.array.reverse.js");
require("core-js/modules/es.array.slice.js");
require("core-js/modules/es.date.to-primitive.js");
require("core-js/modules/es.function.name.js");
require("core-js/modules/es.json.to-string-tag.js");
require("core-js/modules/es.math.to-string-tag.js");
require("core-js/modules/es.number.constructor.js");
require("core-js/modules/es.object.create.js");
require("core-js/modules/es.object.define-properties.js");
require("core-js/modules/es.object.define-property.js");
require("core-js/modules/es.object.get-own-property-descriptor.js");
require("core-js/modules/es.object.get-own-property-descriptors.js");
require("core-js/modules/es.object.get-prototype-of.js");
require("core-js/modules/es.object.set-prototype-of.js");
require("core-js/modules/web.dom-collections.for-each.js");
function _typeof(o) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) { return typeof o; } : function (o) { return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o; }, _typeof(o); }
require("core-js/modules/es.array.find.js");
require("core-js/modules/es.array.includes.js");
require("core-js/modules/es.array.iterator.js");
require("core-js/modules/es.date.now.js");
require("core-js/modules/es.date.to-string.js");
require("core-js/modules/es.object.keys.js");
require("core-js/modules/es.object.to-string.js");
require("core-js/modules/es.promise.js");
require("core-js/modules/es.regexp.exec.js");
require("core-js/modules/es.regexp.to-string.js");
require("core-js/modules/es.string.includes.js");
require("core-js/modules/es.string.iterator.js");
require("core-js/modules/es.string.search.js");
require("core-js/modules/web.dom-collections.iterator.js");
require("core-js/modules/web.url.js");
require("core-js/modules/web.url.to-json.js");
require("core-js/modules/web.url-search-params.js");
var _excluded = ["headers", "type", "path", "searchParams"];
function _regeneratorRuntime() { "use strict"; /*! regenerator-runtime -- Copyright (c) 2014-present, Facebook, Inc. -- license (MIT): https://github.com/facebook/regenerator/blob/main/LICENSE */ _regeneratorRuntime = function _regeneratorRuntime() { return e; }; var t, e = {}, r = Object.prototype, n = r.hasOwnProperty, o = Object.defineProperty || function (t, e, r) { t[e] = r.value; }, i = "function" == typeof Symbol ? Symbol : {}, a = i.iterator || "@@iterator", c = i.asyncIterator || "@@asyncIterator", u = i.toStringTag || "@@toStringTag"; function define(t, e, r) { return Object.defineProperty(t, e, { value: r, enumerable: !0, configurable: !0, writable: !0 }), t[e]; } try { define({}, ""); } catch (t) { define = function define(t, e, r) { return t[e] = r; }; } function wrap(t, e, r, n) { var i = e && e.prototype instanceof Generator ? e : Generator, a = Object.create(i.prototype), c = new Context(n || []); return o(a, "_invoke", { value: makeInvokeMethod(t, r, c) }), a; } function tryCatch(t, e, r) { try { return { type: "normal", arg: t.call(e, r) }; } catch (t) { return { type: "throw", arg: t }; } } e.wrap = wrap; var h = "suspendedStart", l = "suspendedYield", f = "executing", s = "completed", y = {}; function Generator() {} function GeneratorFunction() {} function GeneratorFunctionPrototype() {} var p = {}; define(p, a, function () { return this; }); var d = Object.getPrototypeOf, v = d && d(d(values([]))); v && v !== r && n.call(v, a) && (p = v); var g = GeneratorFunctionPrototype.prototype = Generator.prototype = Object.create(p); function defineIteratorMethods(t) { ["next", "throw", "return"].forEach(function (e) { define(t, e, function (t) { return this._invoke(e, t); }); }); } function AsyncIterator(t, e) { function invoke(r, o, i, a) { var c = tryCatch(t[r], t, o); if ("throw" !== c.type) { var u = c.arg, h = u.value; return h && "object" == _typeof(h) && n.call(h, "__await") ? e.resolve(h.__await).then(function (t) { invoke("next", t, i, a); }, function (t) { invoke("throw", t, i, a); }) : e.resolve(h).then(function (t) { u.value = t, i(u); }, function (t) { return invoke("throw", t, i, a); }); } a(c.arg); } var r; o(this, "_invoke", { value: function value(t, n) { function callInvokeWithMethodAndArg() { return new e(function (e, r) { invoke(t, n, e, r); }); } return r = r ? r.then(callInvokeWithMethodAndArg, callInvokeWithMethodAndArg) : callInvokeWithMethodAndArg(); } }); } function makeInvokeMethod(e, r, n) { var o = h; return function (i, a) { if (o === f) throw Error("Generator is already running"); if (o === s) { if ("throw" === i) throw a; return { value: t, done: !0 }; } for (n.method = i, n.arg = a;;) { var c = n.delegate; if (c) { var u = maybeInvokeDelegate(c, n); if (u) { if (u === y) continue; return u; } } if ("next" === n.method) n.sent = n._sent = n.arg;else if ("throw" === n.method) { if (o === h) throw o = s, n.arg; n.dispatchException(n.arg); } else "return" === n.method && n.abrupt("return", n.arg); o = f; var p = tryCatch(e, r, n); if ("normal" === p.type) { if (o = n.done ? s : l, p.arg === y) continue; return { value: p.arg, done: n.done }; } "throw" === p.type && (o = s, n.method = "throw", n.arg = p.arg); } }; } function maybeInvokeDelegate(e, r) { var n = r.method, o = e.iterator[n]; if (o === t) return r.delegate = null, "throw" === n && e.iterator["return"] && (r.method = "return", r.arg = t, maybeInvokeDelegate(e, r), "throw" === r.method) || "return" !== n && (r.method = "throw", r.arg = new TypeError("The iterator does not provide a '" + n + "' method")), y; var i = tryCatch(o, e.iterator, r.arg); if ("throw" === i.type) return r.method = "throw", r.arg = i.arg, r.delegate = null, y; var a = i.arg; return a ? a.done ? (r[e.resultName] = a.value, r.next = e.nextLoc, "return" !== r.method && (r.method = "next", r.arg = t), r.delegate = null, y) : a : (r.method = "throw", r.arg = new TypeError("iterator result is not an object"), r.delegate = null, y); } function pushTryEntry(t) { var e = { tryLoc: t[0] }; 1 in t && (e.catchLoc = t[1]), 2 in t && (e.finallyLoc = t[2], e.afterLoc = t[3]), this.tryEntries.push(e); } function resetTryEntry(t) { var e = t.completion || {}; e.type = "normal", delete e.arg, t.completion = e; } function Context(t) { this.tryEntries = [{ tryLoc: "root" }], t.forEach(pushTryEntry, this), this.reset(!0); } function values(e) { if (e || "" === e) { var r = e[a]; if (r) return r.call(e); if ("function" == typeof e.next) return e; if (!isNaN(e.length)) { var o = -1, i = function next() { for (; ++o < e.length;) if (n.call(e, o)) return next.value = e[o], next.done = !1, next; return next.value = t, next.done = !0, next; }; return i.next = i; } } throw new TypeError(_typeof(e) + " is not iterable"); } return GeneratorFunction.prototype = GeneratorFunctionPrototype, o(g, "constructor", { value: GeneratorFunctionPrototype, configurable: !0 }), o(GeneratorFunctionPrototype, "constructor", { value: GeneratorFunction, configurable: !0 }), GeneratorFunction.displayName = define(GeneratorFunctionPrototype, u, "GeneratorFunction"), e.isGeneratorFunction = function (t) { var e = "function" == typeof t && t.constructor; return !!e && (e === GeneratorFunction || "GeneratorFunction" === (e.displayName || e.name)); }, e.mark = function (t) { return Object.setPrototypeOf ? Object.setPrototypeOf(t, GeneratorFunctionPrototype) : (t.__proto__ = GeneratorFunctionPrototype, define(t, u, "GeneratorFunction")), t.prototype = Object.create(g), t; }, e.awrap = function (t) { return { __await: t }; }, defineIteratorMethods(AsyncIterator.prototype), define(AsyncIterator.prototype, c, function () { return this; }), e.AsyncIterator = AsyncIterator, e.async = function (t, r, n, o, i) { void 0 === i && (i = Promise); var a = new AsyncIterator(wrap(t, r, n, o), i); return e.isGeneratorFunction(r) ? a : a.next().then(function (t) { return t.done ? t.value : a.next(); }); }, defineIteratorMethods(g), define(g, u, "Generator"), define(g, a, function () { return this; }), define(g, "toString", function () { return "[object Generator]"; }), e.keys = function (t) { var e = Object(t), r = []; for (var n in e) r.push(n); return r.reverse(), function next() { for (; r.length;) { var t = r.pop(); if (t in e) return next.value = t, next.done = !1, next; } return next.done = !0, next; }; }, e.values = values, Context.prototype = { constructor: Context, reset: function reset(e) { if (this.prev = 0, this.next = 0, this.sent = this._sent = t, this.done = !1, this.delegate = null, this.method = "next", this.arg = t, this.tryEntries.forEach(resetTryEntry), !e) for (var r in this) "t" === r.charAt(0) && n.call(this, r) && !isNaN(+r.slice(1)) && (this[r] = t); }, stop: function stop() { this.done = !0; var t = this.tryEntries[0].completion; if ("throw" === t.type) throw t.arg; return this.rval; }, dispatchException: function dispatchException(e) { if (this.done) throw e; var r = this; function handle(n, o) { return a.type = "throw", a.arg = e, r.next = n, o && (r.method = "next", r.arg = t), !!o; } for (var o = this.tryEntries.length - 1; o >= 0; --o) { var i = this.tryEntries[o], a = i.completion; if ("root" === i.tryLoc) return handle("end"); if (i.tryLoc <= this.prev) { var c = n.call(i, "catchLoc"), u = n.call(i, "finallyLoc"); if (c && u) { if (this.prev < i.catchLoc) return handle(i.catchLoc, !0); if (this.prev < i.finallyLoc) return handle(i.finallyLoc); } else if (c) { if (this.prev < i.catchLoc) return handle(i.catchLoc, !0); } else { if (!u) throw Error("try statement without catch or finally"); if (this.prev < i.finallyLoc) return handle(i.finallyLoc); } } } }, abrupt: function abrupt(t, e) { for (var r = this.tryEntries.length - 1; r >= 0; --r) { var o = this.tryEntries[r]; if (o.tryLoc <= this.prev && n.call(o, "finallyLoc") && this.prev < o.finallyLoc) { var i = o; break; } } i && ("break" === t || "continue" === t) && i.tryLoc <= e && e <= i.finallyLoc && (i = null); var a = i ? i.completion : {}; return a.type = t, a.arg = e, i ? (this.method = "next", this.next = i.finallyLoc, y) : this.complete(a); }, complete: function complete(t, e) { if ("throw" === t.type) throw t.arg; return "break" === t.type || "continue" === t.type ? this.next = t.arg : "return" === t.type ? (this.rval = this.arg = t.arg, this.method = "return", this.next = "end") : "normal" === t.type && e && (this.next = e), y; }, finish: function finish(t) { for (var e = this.tryEntries.length - 1; e >= 0; --e) { var r = this.tryEntries[e]; if (r.finallyLoc === t) return this.complete(r.completion, r.afterLoc), resetTryEntry(r), y; } }, "catch": function _catch(t) { for (var e = this.tryEntries.length - 1; e >= 0; --e) { var r = this.tryEntries[e]; if (r.tryLoc === t) { var n = r.completion; if ("throw" === n.type) { var o = n.arg; resetTryEntry(r); } return o; } } throw Error("illegal catch attempt"); }, delegateYield: function delegateYield(e, r, n) { return this.delegate = { iterator: values(e), resultName: r, nextLoc: n }, "next" === this.method && (this.arg = t), y; } }, e; }
function ownKeys(e, r) { var t = Object.keys(e); if (Object.getOwnPropertySymbols) { var o = Object.getOwnPropertySymbols(e); r && (o = o.filter(function (r) { return Object.getOwnPropertyDescriptor(e, r).enumerable; })), t.push.apply(t, o); } return t; }
function _objectSpread(e) { for (var r = 1; r < arguments.length; r++) { var t = null != arguments[r] ? arguments[r] : {}; r % 2 ? ownKeys(Object(t), !0).forEach(function (r) { _defineProperty(e, r, t[r]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys(Object(t)).forEach(function (r) { Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r)); }); } return e; }
function _defineProperty(e, r, t) { return (r = _toPropertyKey(r)) in e ? Object.defineProperty(e, r, { value: t, enumerable: !0, configurable: !0, writable: !0 }) : e[r] = t, e; }
function _objectWithoutProperties(e, t) { if (null == e) return {}; var o, r, i = _objectWithoutPropertiesLoose(e, t); if (Object.getOwnPropertySymbols) { var s = Object.getOwnPropertySymbols(e); for (r = 0; r < s.length; r++) o = s[r], t.includes(o) || {}.propertyIsEnumerable.call(e, o) && (i[o] = e[o]); } return i; }
function _objectWithoutPropertiesLoose(r, e) { if (null == r) return {}; var t = {}; for (var n in r) if ({}.hasOwnProperty.call(r, n)) { if (e.includes(n)) continue; t[n] = r[n]; } return t; }
function asyncGeneratorStep(n, t, e, r, o, a, c) { try { var i = n[a](c), u = i.value; } catch (n) { return void e(n); } i.done ? t(u) : Promise.resolve(u).then(r, o); }
function _asyncToGenerator(n) { return function () { var t = this, e = arguments; return new Promise(function (r, o) { var a = n.apply(t, e); function _next(n) { asyncGeneratorStep(a, r, o, _next, _throw, "next", n); } function _throw(n) { asyncGeneratorStep(a, r, o, _next, _throw, "throw", n); } _next(void 0); }); }; }
function _classCallCheck(a, n) { if (!(a instanceof n)) throw new TypeError("Cannot call a class as a function"); }
function _defineProperties(e, r) { for (var t = 0; t < r.length; t++) { var o = r[t]; o.enumerable = o.enumerable || !1, o.configurable = !0, "value" in o && (o.writable = !0), Object.defineProperty(e, _toPropertyKey(o.key), o); } }
function _createClass(e, r, t) { return r && _defineProperties(e.prototype, r), t && _defineProperties(e, t), Object.defineProperty(e, "prototype", { writable: !1 }), e; }
function _toPropertyKey(t) { var i = _toPrimitive(t, "string"); return "symbol" == _typeof(i) ? i : i + ""; }
function _toPrimitive(t, r) { if ("object" != _typeof(t) || !t) return t; var e = t[Symbol.toPrimitive]; if (void 0 !== e) { var i = e.call(t, r || "default"); if ("object" != _typeof(i)) return i; throw new TypeError("@@toPrimitive must return a primitive value."); } return ("string" === r ? String : Number)(t); }
var _require = require("../../controllers/callApi/A_call_auth_access"),
  callTheAuthAccess = _require.callTheAuthAccess;
var _require2 = require("../../controllers/callApi/query"),
  sql_getEnvironmentAccess = _require2.sql_getEnvironmentAccess;
var _require3 = require("../../controllers/callApi/repository"),
  callApiRepository = _require3["default"];
var _require4 = require("../../controllers/requestResponse/repository"),
  requestResponseRepository = _require4["default"];
var _require5 = require("../../libs/utils/callQuery"),
  queryExecute = _require5.queryExecute;
var _require6 = require("../../libs/utils/logActivity"),
  logActivity = _require6.logActivity,
  logOutcome = _require6.logOutcome;
var _require7 = require("../../libs/utils/responseApi"),
  result = _require7.result,
  statusHttpCode = _require7.statusHttpCode,
  findStatusHttpByCode = _require7.findStatusHttpByCode;
var _require8 = require("./store"),
  DefaultAuthStore = _require8.DefaultAuthStore;
var defaultBaseUrls = {
  development: {
    auth: "https://api-satusehat-dev.dto.kemkes.go.id/oauth2/v1",
    fhir: "https://api-satusehat-dev.dto.kemkes.go.id/fhir-r4/v1",
    consent: "https://api-satusehat-dev.dto.kemkes.go.id/consent/v1",
    kyc: "https://api-satusehat-dev.dto.kemkes.go.id/kyc/v1"
  },
  staging: {
    auth: "https://api-satusehat-stg.dto.kemkes.go.id/oauth2/v1",
    fhir: "https://api-satusehat-stg.dto.kemkes.go.id/fhir-r4/v1",
    consent: "https://api-satusehat-stg.dto.kemkes.go.id/consent/v1",
    kyc: "https://api-satusehat-stg.dto.kemkes.go.id/kyc/v1"
  },
  production: {
    auth: "https://api-satusehat.kemkes.go.id/oauth2/v1",
    fhir: "https://api-satusehat.kemkes.go.id/fhir-r4/v1",
    consent: "https://api-satusehat.kemkes.go.id/consent/v1",
    kyc: "https://api-satusehat.kemkes.go.id/kyc/v1"
  }
};
var ApiGateWay = /*#__PURE__*/function () {
  function ApiGateWay() {
    _classCallCheck(this, ApiGateWay);
  }
  return _createClass(ApiGateWay, [{
    key: "request",
    value: function () {
      var _request = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee(parameter) {
        var start, _parameter$headers, headers, type, path, searchParams, init, getConfig, _getConfig$results, idEnv, idUser, variable, paramsAuth, isToken, url, response, statusCode, findStatusCode, resError, authCall, _resError, contentType, resultResponse;
        return _regeneratorRuntime().wrap(function _callee$(_context) {
          while (1) switch (_context.prev = _context.next) {
            case 0:
              start = Date.now();
              _parameter$headers = parameter.headers, headers = _parameter$headers === void 0 ? {} : _parameter$headers, type = parameter.type, path = parameter.path, searchParams = parameter.searchParams, init = _objectWithoutProperties(parameter, _excluded); // Step 1: Configuration Check
              _context.next = 4;
              return this.getConfig(parameter.request);
            case 4:
              getConfig = _context.sent;
              if (!(getConfig.code === 400)) {
                _context.next = 9;
                break;
              }
              _context.next = 8;
              return logOutcome(parameter.request, getConfig, Date.now() - start, {
                path: path,
                method: init.method
              });
            case 8:
              return _context.abrupt("return", getConfig);
            case 9:
              _getConfig$results = getConfig.results, idEnv = _getConfig$results.idEnv, idUser = _getConfig$results.idUser, variable = _getConfig$results.variable;
              paramsAuth = {
                params: {
                  id: idEnv
                },
                query: {
                  idUser: idUser,
                  env: variable.varName
                }
              }; // Step 2: Authorization Check
              isToken = variable.code.find(function (x) {
                return x.key === "token";
              });
              if (isToken) {
                _context.next = 14;
                break;
              }
              return _context.abrupt("return", result({
                statusCode: statusHttpCode.badRequest400,
                message: "KYC requires a token to access!"
              }));
            case 14:
              // Step 3: URL Construction
              url = new URL(defaultBaseUrls["production"]["kyc"] + path);
              url.search = searchParams ? new URLSearchParams(searchParams).toString() : "";

              // Step 4: Set Headers
              init.headers = _objectSpread({
                Authorization: "".concat(isToken.value)
              }, headers);
              _context.prev = 17;
              _context.next = 20;
              return fetch(url, init);
            case 20:
              response = _context.sent;
              statusCode = response.status;
              findStatusCode = findStatusHttpByCode(statusCode); // Step 6: Token Expiry Handling
              if (!(findStatusCode.code === 401)) {
                _context.next = 37;
                break;
              }
              resError = {
                status: findStatusCode.code,
                message: response ? response.statusText : "Failed to fetch"
              };
              _context.next = 27;
              return logOutcome(init, resError, Date.now() - start, {
                path: url.href,
                method: init.method,
                resourceName: "Outbond-SatuSehat",
                groupName: url.pathname,
                idUser: parameter.request.decoded ? parameter.request.decoded.id : "PUBLIC"
              });
            case 27:
              _context.next = 29;
              return callTheAuthAccess(paramsAuth);
            case 29:
              authCall = _context.sent;
              if (!(authCall.resultCall.code === 200)) {
                _context.next = 36;
                break;
              }
              _context.next = 33;
              return this.request(parameter);
            case 33:
              return _context.abrupt("return", _context.sent);
            case 36:
              return _context.abrupt("return", authCall.resultCall);
            case 37:
              if (!(!response || !response.ok)) {
                _context.next = 42;
                break;
              }
              _resError = {
                status: findStatusCode.code,
                message: response ? response.statusText : "Failed to fetch"
              };
              _context.next = 41;
              return logOutcome(init, _resError, Date.now() - start, {
                path: url.href,
                method: init.method,
                resourceName: "Outbond-SatuSehat",
                groupName: url.pathname,
                idUser: parameter.request.decoded ? parameter.request.decoded.id : "PUBLIC"
              });
            case 41:
              return _context.abrupt("return", result({
                statusCode: statusHttpCode[findStatusCode.keyStatusHttpCode],
                message: response ? response.statusText : "Failed to fetch"
              }));
            case 42:
              contentType = response.headers.get("Content-Type") || "";
              resultResponse = {
                status: response.status,
                message: response.ok ? "ok" : "Request failed"
              };
              if (!contentType.includes("text/plain")) {
                _context.next = 50;
                break;
              }
              _context.next = 47;
              return response.text();
            case 47:
              resultResponse.data = _context.sent;
              _context.next = 59;
              break;
            case 50:
              if (!contentType.includes("application/json")) {
                _context.next = 56;
                break;
              }
              _context.next = 53;
              return response.json();
            case 53:
              resultResponse.data = _context.sent;
              _context.next = 59;
              break;
            case 56:
              _context.next = 58;
              return response.blob();
            case 58:
              resultResponse.data = _context.sent;
            case 59:
              _context.next = 61;
              return logOutcome(init, resultResponse, Date.now() - start, {
                path: url.href,
                method: init.method,
                resourceName: "Outbond-SatuSehat",
                groupName: url.pathname,
                idUser: parameter.request.decoded ? parameter.request.decoded.id : "PUBLIC"
              });
            case 61:
              return _context.abrupt("return", result({
                statusCode: response ? statusHttpCode.ok200 : statusHttpCode.badRequest400,
                message: response ? response.statusText : "ok",
                data: resultResponse.data
              }));
            case 64:
              _context.prev = 64;
              _context.t0 = _context["catch"](17);
              return _context.abrupt("return", result({
                statusCode: statusHttpCode.internalServerError500,
                message: _context.t0.message || "An unexpected error occurred"
              }));
            case 67:
            case "end":
              return _context.stop();
          }
        }, _callee, this, [[17, 64]]);
      }));
      function request(_x) {
        return _request.apply(this, arguments);
      }
      return request;
    }()
  }, {
    key: "getConfig",
    value: function () {
      var _getConfig = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee2(req) {
        var id, getEnvironmentAccess, execGetEnvAccess, getVariable, envByParams;
        return _regeneratorRuntime().wrap(function _callee2$(_context2) {
          while (1) switch (_context2.prev = _context2.next) {
            case 0:
              id = req.decoded.id;
              getEnvironmentAccess = sql_getEnvironmentAccess({
                userId: id,
                idEnv: "1684297477024" //dihardcode untuk satusehat
              });
              _context2.next = 4;
              return queryExecute({
                query: getEnvironmentAccess,
                getOneObject: true
              });
            case 4:
              execGetEnvAccess = _context2.sent;
              if (execGetEnvAccess) {
                _context2.next = 7;
                break;
              }
              return _context2.abrupt("return", result({
                statusCode: statusHttpCode.badRequest400,
                message: "Environment cant access, please call administrator !"
              }));
            case 7:
              if (execGetEnvAccess.status) {
                _context2.next = 9;
                break;
              }
              return _context2.abrupt("return", result({
                statusCode: statusHttpCode.badRequest400,
                message: "Environment status cant active, please call administrator !"
              }));
            case 9:
              getVariable = JSON.parse(execGetEnvAccess.variable);
              if (!(getVariable.length == 0)) {
                _context2.next = 12;
                break;
              }
              return _context2.abrupt("return", result({
                statusCode: statusHttpCode.badRequest400,
                message: "Environment not found !"
              }));
            case 12:
              envByParams = getVariable.find(function (el) {
                return el.varName.toLowerCase() === "production" && el.status === 1;
              });
              if (envByParams) {
                _context2.next = 15;
                break;
              }
              return _context2.abrupt("return", result({
                statusCode: statusHttpCode.badRequest400,
                message: "Please check SatuSehat Environment, use PRODUCTION name for send KYC !"
              }));
            case 15:
              execGetEnvAccess = _objectSpread(_objectSpread({}, execGetEnvAccess), {}, {
                variable: envByParams
              });
              return _context2.abrupt("return", result({
                data: execGetEnvAccess
              }));
            case 17:
            case "end":
              return _context2.stop();
          }
        }, _callee2);
      }));
      function getConfig(_x2) {
        return _getConfig.apply(this, arguments);
      }
      return getConfig;
    }()
  }]);
}();
module.exports = {
  ApiGateWay: ApiGateWay
};