"use strict";

require("core-js/modules/es.object.define-property.js");
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _express = _interopRequireDefault(require("express"));
var _controllers = require("./controllers");
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
var routes = (0, _express["default"])();
var source_fasyankes = "/source/fasyankes";
var source_fasyankes_stages = source_fasyankes + "/stage";
routes.route(source_fasyankes_stages).get(_controllers.Controllers_Source.getTahapanFasyankes);
var _default = exports["default"] = routes;