"use strict";

require("core-js/modules/es.object.define-property.js");
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _express = _interopRequireDefault(require("express"));
var _controllers = require("./controllers");
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
// routes/cronRoutes.js

var router = _express["default"].Router();

// Route to manually start a job by its ID
router.post('/start/:jobId', _controllers.startCronJob);

// Route to manually stop a job by its ID
router.post('/stop/:jobId', _controllers.stopCronJob);
var _default = exports["default"] = router;