"use strict";

require("core-js/modules/es.symbol.js");
require("core-js/modules/es.symbol.description.js");
require("core-js/modules/es.symbol.async-iterator.js");
require("core-js/modules/es.symbol.iterator.js");
require("core-js/modules/es.symbol.to-primitive.js");
require("core-js/modules/es.symbol.to-string-tag.js");
require("core-js/modules/es.array.for-each.js");
require("core-js/modules/es.array.from.js");
require("core-js/modules/es.array.is-array.js");
require("core-js/modules/es.array.iterator.js");
require("core-js/modules/es.array.reverse.js");
require("core-js/modules/es.array.slice.js");
require("core-js/modules/es.date.to-primitive.js");
require("core-js/modules/es.date.to-string.js");
require("core-js/modules/es.function.name.js");
require("core-js/modules/es.json.to-string-tag.js");
require("core-js/modules/es.math.to-string-tag.js");
require("core-js/modules/es.number.constructor.js");
require("core-js/modules/es.object.create.js");
require("core-js/modules/es.object.define-property.js");
require("core-js/modules/es.object.get-own-property-descriptor.js");
require("core-js/modules/es.object.get-prototype-of.js");
require("core-js/modules/es.object.set-prototype-of.js");
require("core-js/modules/es.regexp.to-string.js");
require("core-js/modules/es.string.iterator.js");
require("core-js/modules/es.weak-map.js");
require("core-js/modules/web.dom-collections.for-each.js");
require("core-js/modules/web.dom-collections.iterator.js");
function _typeof(o) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) { return typeof o; } : function (o) { return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o; }, _typeof(o); }
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = cronJobRepository;
require("core-js/modules/es.object.to-string.js");
require("core-js/modules/es.parse-int.js");
require("core-js/modules/es.promise.js");
require("core-js/modules/es.regexp.exec.js");
require("core-js/modules/es.string.search.js");
var _index = require("../../models/index.js");
var _responseApi = require("../../libs/utils/responseApi.js");
var _sequelize = _interopRequireWildcard(require("sequelize"));
var _helpers = _interopRequireDefault(require("../../libs/utils/helpers.js"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != _typeof(e) && "function" != typeof e) return { "default": e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && {}.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n["default"] = e, t && t.set(e, n), n; }
function _slicedToArray(r, e) { return _arrayWithHoles(r) || _iterableToArrayLimit(r, e) || _unsupportedIterableToArray(r, e) || _nonIterableRest(); }
function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }
function _unsupportedIterableToArray(r, a) { if (r) { if ("string" == typeof r) return _arrayLikeToArray(r, a); var t = {}.toString.call(r).slice(8, -1); return "Object" === t && r.constructor && (t = r.constructor.name), "Map" === t || "Set" === t ? Array.from(r) : "Arguments" === t || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(t) ? _arrayLikeToArray(r, a) : void 0; } }
function _arrayLikeToArray(r, a) { (null == a || a > r.length) && (a = r.length); for (var e = 0, n = Array(a); e < a; e++) n[e] = r[e]; return n; }
function _iterableToArrayLimit(r, l) { var t = null == r ? null : "undefined" != typeof Symbol && r[Symbol.iterator] || r["@@iterator"]; if (null != t) { var e, n, i, u, a = [], f = !0, o = !1; try { if (i = (t = t.call(r)).next, 0 === l) { if (Object(t) !== t) return; f = !1; } else for (; !(f = (e = i.call(t)).done) && (a.push(e.value), a.length !== l); f = !0); } catch (r) { o = !0, n = r; } finally { try { if (!f && null != t["return"] && (u = t["return"](), Object(u) !== u)) return; } finally { if (o) throw n; } } return a; } }
function _arrayWithHoles(r) { if (Array.isArray(r)) return r; }
function _regeneratorRuntime() { "use strict"; /*! regenerator-runtime -- Copyright (c) 2014-present, Facebook, Inc. -- license (MIT): https://github.com/facebook/regenerator/blob/main/LICENSE */ _regeneratorRuntime = function _regeneratorRuntime() { return e; }; var t, e = {}, r = Object.prototype, n = r.hasOwnProperty, o = Object.defineProperty || function (t, e, r) { t[e] = r.value; }, i = "function" == typeof Symbol ? Symbol : {}, a = i.iterator || "@@iterator", c = i.asyncIterator || "@@asyncIterator", u = i.toStringTag || "@@toStringTag"; function define(t, e, r) { return Object.defineProperty(t, e, { value: r, enumerable: !0, configurable: !0, writable: !0 }), t[e]; } try { define({}, ""); } catch (t) { define = function define(t, e, r) { return t[e] = r; }; } function wrap(t, e, r, n) { var i = e && e.prototype instanceof Generator ? e : Generator, a = Object.create(i.prototype), c = new Context(n || []); return o(a, "_invoke", { value: makeInvokeMethod(t, r, c) }), a; } function tryCatch(t, e, r) { try { return { type: "normal", arg: t.call(e, r) }; } catch (t) { return { type: "throw", arg: t }; } } e.wrap = wrap; var h = "suspendedStart", l = "suspendedYield", f = "executing", s = "completed", y = {}; function Generator() {} function GeneratorFunction() {} function GeneratorFunctionPrototype() {} var p = {}; define(p, a, function () { return this; }); var d = Object.getPrototypeOf, v = d && d(d(values([]))); v && v !== r && n.call(v, a) && (p = v); var g = GeneratorFunctionPrototype.prototype = Generator.prototype = Object.create(p); function defineIteratorMethods(t) { ["next", "throw", "return"].forEach(function (e) { define(t, e, function (t) { return this._invoke(e, t); }); }); } function AsyncIterator(t, e) { function invoke(r, o, i, a) { var c = tryCatch(t[r], t, o); if ("throw" !== c.type) { var u = c.arg, h = u.value; return h && "object" == _typeof(h) && n.call(h, "__await") ? e.resolve(h.__await).then(function (t) { invoke("next", t, i, a); }, function (t) { invoke("throw", t, i, a); }) : e.resolve(h).then(function (t) { u.value = t, i(u); }, function (t) { return invoke("throw", t, i, a); }); } a(c.arg); } var r; o(this, "_invoke", { value: function value(t, n) { function callInvokeWithMethodAndArg() { return new e(function (e, r) { invoke(t, n, e, r); }); } return r = r ? r.then(callInvokeWithMethodAndArg, callInvokeWithMethodAndArg) : callInvokeWithMethodAndArg(); } }); } function makeInvokeMethod(e, r, n) { var o = h; return function (i, a) { if (o === f) throw Error("Generator is already running"); if (o === s) { if ("throw" === i) throw a; return { value: t, done: !0 }; } for (n.method = i, n.arg = a;;) { var c = n.delegate; if (c) { var u = maybeInvokeDelegate(c, n); if (u) { if (u === y) continue; return u; } } if ("next" === n.method) n.sent = n._sent = n.arg;else if ("throw" === n.method) { if (o === h) throw o = s, n.arg; n.dispatchException(n.arg); } else "return" === n.method && n.abrupt("return", n.arg); o = f; var p = tryCatch(e, r, n); if ("normal" === p.type) { if (o = n.done ? s : l, p.arg === y) continue; return { value: p.arg, done: n.done }; } "throw" === p.type && (o = s, n.method = "throw", n.arg = p.arg); } }; } function maybeInvokeDelegate(e, r) { var n = r.method, o = e.iterator[n]; if (o === t) return r.delegate = null, "throw" === n && e.iterator["return"] && (r.method = "return", r.arg = t, maybeInvokeDelegate(e, r), "throw" === r.method) || "return" !== n && (r.method = "throw", r.arg = new TypeError("The iterator does not provide a '" + n + "' method")), y; var i = tryCatch(o, e.iterator, r.arg); if ("throw" === i.type) return r.method = "throw", r.arg = i.arg, r.delegate = null, y; var a = i.arg; return a ? a.done ? (r[e.resultName] = a.value, r.next = e.nextLoc, "return" !== r.method && (r.method = "next", r.arg = t), r.delegate = null, y) : a : (r.method = "throw", r.arg = new TypeError("iterator result is not an object"), r.delegate = null, y); } function pushTryEntry(t) { var e = { tryLoc: t[0] }; 1 in t && (e.catchLoc = t[1]), 2 in t && (e.finallyLoc = t[2], e.afterLoc = t[3]), this.tryEntries.push(e); } function resetTryEntry(t) { var e = t.completion || {}; e.type = "normal", delete e.arg, t.completion = e; } function Context(t) { this.tryEntries = [{ tryLoc: "root" }], t.forEach(pushTryEntry, this), this.reset(!0); } function values(e) { if (e || "" === e) { var r = e[a]; if (r) return r.call(e); if ("function" == typeof e.next) return e; if (!isNaN(e.length)) { var o = -1, i = function next() { for (; ++o < e.length;) if (n.call(e, o)) return next.value = e[o], next.done = !1, next; return next.value = t, next.done = !0, next; }; return i.next = i; } } throw new TypeError(_typeof(e) + " is not iterable"); } return GeneratorFunction.prototype = GeneratorFunctionPrototype, o(g, "constructor", { value: GeneratorFunctionPrototype, configurable: !0 }), o(GeneratorFunctionPrototype, "constructor", { value: GeneratorFunction, configurable: !0 }), GeneratorFunction.displayName = define(GeneratorFunctionPrototype, u, "GeneratorFunction"), e.isGeneratorFunction = function (t) { var e = "function" == typeof t && t.constructor; return !!e && (e === GeneratorFunction || "GeneratorFunction" === (e.displayName || e.name)); }, e.mark = function (t) { return Object.setPrototypeOf ? Object.setPrototypeOf(t, GeneratorFunctionPrototype) : (t.__proto__ = GeneratorFunctionPrototype, define(t, u, "GeneratorFunction")), t.prototype = Object.create(g), t; }, e.awrap = function (t) { return { __await: t }; }, defineIteratorMethods(AsyncIterator.prototype), define(AsyncIterator.prototype, c, function () { return this; }), e.AsyncIterator = AsyncIterator, e.async = function (t, r, n, o, i) { void 0 === i && (i = Promise); var a = new AsyncIterator(wrap(t, r, n, o), i); return e.isGeneratorFunction(r) ? a : a.next().then(function (t) { return t.done ? t.value : a.next(); }); }, defineIteratorMethods(g), define(g, u, "Generator"), define(g, a, function () { return this; }), define(g, "toString", function () { return "[object Generator]"; }), e.keys = function (t) { var e = Object(t), r = []; for (var n in e) r.push(n); return r.reverse(), function next() { for (; r.length;) { var t = r.pop(); if (t in e) return next.value = t, next.done = !1, next; } return next.done = !0, next; }; }, e.values = values, Context.prototype = { constructor: Context, reset: function reset(e) { if (this.prev = 0, this.next = 0, this.sent = this._sent = t, this.done = !1, this.delegate = null, this.method = "next", this.arg = t, this.tryEntries.forEach(resetTryEntry), !e) for (var r in this) "t" === r.charAt(0) && n.call(this, r) && !isNaN(+r.slice(1)) && (this[r] = t); }, stop: function stop() { this.done = !0; var t = this.tryEntries[0].completion; if ("throw" === t.type) throw t.arg; return this.rval; }, dispatchException: function dispatchException(e) { if (this.done) throw e; var r = this; function handle(n, o) { return a.type = "throw", a.arg = e, r.next = n, o && (r.method = "next", r.arg = t), !!o; } for (var o = this.tryEntries.length - 1; o >= 0; --o) { var i = this.tryEntries[o], a = i.completion; if ("root" === i.tryLoc) return handle("end"); if (i.tryLoc <= this.prev) { var c = n.call(i, "catchLoc"), u = n.call(i, "finallyLoc"); if (c && u) { if (this.prev < i.catchLoc) return handle(i.catchLoc, !0); if (this.prev < i.finallyLoc) return handle(i.finallyLoc); } else if (c) { if (this.prev < i.catchLoc) return handle(i.catchLoc, !0); } else { if (!u) throw Error("try statement without catch or finally"); if (this.prev < i.finallyLoc) return handle(i.finallyLoc); } } } }, abrupt: function abrupt(t, e) { for (var r = this.tryEntries.length - 1; r >= 0; --r) { var o = this.tryEntries[r]; if (o.tryLoc <= this.prev && n.call(o, "finallyLoc") && this.prev < o.finallyLoc) { var i = o; break; } } i && ("break" === t || "continue" === t) && i.tryLoc <= e && e <= i.finallyLoc && (i = null); var a = i ? i.completion : {}; return a.type = t, a.arg = e, i ? (this.method = "next", this.next = i.finallyLoc, y) : this.complete(a); }, complete: function complete(t, e) { if ("throw" === t.type) throw t.arg; return "break" === t.type || "continue" === t.type ? this.next = t.arg : "return" === t.type ? (this.rval = this.arg = t.arg, this.method = "return", this.next = "end") : "normal" === t.type && e && (this.next = e), y; }, finish: function finish(t) { for (var e = this.tryEntries.length - 1; e >= 0; --e) { var r = this.tryEntries[e]; if (r.finallyLoc === t) return this.complete(r.completion, r.afterLoc), resetTryEntry(r), y; } }, "catch": function _catch(t) { for (var e = this.tryEntries.length - 1; e >= 0; --e) { var r = this.tryEntries[e]; if (r.tryLoc === t) { var n = r.completion; if ("throw" === n.type) { var o = n.arg; resetTryEntry(r); } return o; } } throw Error("illegal catch attempt"); }, delegateYield: function delegateYield(e, r, n) { return this.delegate = { iterator: values(e), resultName: r, nextLoc: n }, "next" === this.method && (this.arg = t), y; } }, e; }
function _defineProperty(e, r, t) { return (r = _toPropertyKey(r)) in e ? Object.defineProperty(e, r, { value: t, enumerable: !0, configurable: !0, writable: !0 }) : e[r] = t, e; }
function _toPropertyKey(t) { var i = _toPrimitive(t, "string"); return "symbol" == _typeof(i) ? i : i + ""; }
function _toPrimitive(t, r) { if ("object" != _typeof(t) || !t) return t; var e = t[Symbol.toPrimitive]; if (void 0 !== e) { var i = e.call(t, r || "default"); if ("object" != _typeof(i)) return i; throw new TypeError("@@toPrimitive must return a primitive value."); } return ("string" === r ? String : Number)(t); }
function asyncGeneratorStep(n, t, e, r, o, a, c) { try { var i = n[a](c), u = i.value; } catch (n) { return void e(n); } i.done ? t(u) : Promise.resolve(u).then(r, o); }
function _asyncToGenerator(n) { return function () { var t = this, e = arguments; return new Promise(function (r, o) { var a = n.apply(t, e); function _next(n) { asyncGeneratorStep(a, r, o, _next, _throw, "next", n); } function _throw(n) { asyncGeneratorStep(a, r, o, _next, _throw, "throw", n); } _next(void 0); }); }; }
function cronJobRepository() {
  // List all cron jobs
  var getCronJobs = /*#__PURE__*/function () {
    var _ref = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee(req) {
      var _req$query, page, size, search, orderBy, condition, _helpers$getPaginatio, limit, offset, order, data;
      return _regeneratorRuntime().wrap(function _callee$(_context) {
        while (1) switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            _req$query = req.query, page = _req$query.page, size = _req$query.size, search = _req$query.search, orderBy = _req$query.orderBy;
            condition = search ? _defineProperty({}, _sequelize.Op.or, [{
              jobName: _defineProperty({}, _sequelize.Op.like, "%".concat(search, "%"))
            }, {
              daysOfWeek: _defineProperty({}, _sequelize.Op.like, "%".concat(search, "%"))
            }]) : {};
            _helpers$getPaginatio = (0, _helpers["default"])().getPaginationNew(page, size, orderBy), limit = _helpers$getPaginatio.limit, offset = _helpers$getPaginatio.offset, order = _helpers$getPaginatio.order;
            _context.next = 6;
            return _index.cronJob.findAndCountAll({
              where: condition,
              include: [{
                model: _index.users,
                attributes: []
              }],
              attributes: ["id", "idUser", [_sequelize["default"].literal('"user"."username"'), "username"], "jobName", "startTime", "endTime", "daysOfWeek", "isActive"],
              limit: limit,
              offset: offset,
              order: order
            });
          case 6:
            data = _context.sent;
            return _context.abrupt("return", (0, _responseApi.result)({
              data: (0, _helpers["default"])().getPagingData(data, page, limit)
            }));
          case 10:
            _context.prev = 10;
            _context.t0 = _context["catch"](0);
            console.log("error >> ", _context.t0);
            _context.t0.message = "CronJobRepository.showList: " + _context.t0;
            throw _context.t0;
          case 15:
          case "end":
            return _context.stop();
        }
      }, _callee, null, [[0, 10]]);
    }));
    return function getCronJobs(_x) {
      return _ref.apply(this, arguments);
    };
  }();

  // Get cron job detail by ID
  var getCronJobDetail = /*#__PURE__*/function () {
    var _ref3 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee2(req) {
      var id, cronJob;
      return _regeneratorRuntime().wrap(function _callee2$(_context2) {
        while (1) switch (_context2.prev = _context2.next) {
          case 0:
            id = req.params.id;
            _context2.prev = 1;
            _context2.next = 4;
            return _index.cronJob.findByPk(id, {
              include: [{
                model: _index.users,
                attributes: []
              }],
              attributes: ["id", "idUser", [_sequelize["default"].literal('"user"."username"'), "username"], "jobName", "startTime", "endTime", "daysOfWeek", "isActive", "createdBy", "updateBy", "createdAt", "updatedAt", "deletedAt"]
            });
          case 4:
            cronJob = _context2.sent;
            if (cronJob) {
              _context2.next = 7;
              break;
            }
            return _context2.abrupt("return", (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.notFound404,
              message: "CronJob id ".concat(id, " not found")
            }));
          case 7:
            return _context2.abrupt("return", (0, _responseApi.result)({
              data: cronJob
            }));
          case 10:
            _context2.prev = 10;
            _context2.t0 = _context2["catch"](1);
            _context2.t0.message = "CronJobRepository.getCronJobDetail: " + _context2.t0;
            throw _context2.t0;
          case 14:
          case "end":
            return _context2.stop();
        }
      }, _callee2, null, [[1, 10]]);
    }));
    return function getCronJobDetail(_x2) {
      return _ref3.apply(this, arguments);
    };
  }();

  // Update cron job
  var updateCronJob = /*#__PURE__*/function () {
    var _ref4 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee3(req) {
      var id, getUsers, getStatusUser, _yield$cronJobModel$u, _yield$cronJobModel$u2, updated, updatedCronJob;
      return _regeneratorRuntime().wrap(function _callee3$(_context3) {
        while (1) switch (_context3.prev = _context3.next) {
          case 0:
            id = req.params.id;
            _context3.prev = 1;
            _context3.next = 4;
            return _index.users.findByPk(req.body.idUser, {
              attributes: ["id", "status", "fullname", "username"]
            });
          case 4:
            getUsers = _context3.sent;
            if (getUsers) {
              _context3.next = 7;
              break;
            }
            return _context3.abrupt("return", (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.badRequest400,
              message: "Client User not found !",
              data: getUsers
            }));
          case 7:
            getStatusUser = parseInt(getUsers.status) == 1;
            if (getStatusUser) {
              _context3.next = 10;
              break;
            }
            return _context3.abrupt("return", (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.badRequest400,
              message: "Client User status is not active! , please call administrator to active user",
              data: getUsers
            }));
          case 10:
            _context3.next = 12;
            return _index.cronJob.update(req.body, {
              where: {
                id: id
              }
            });
          case 12:
            _yield$cronJobModel$u = _context3.sent;
            _yield$cronJobModel$u2 = _slicedToArray(_yield$cronJobModel$u, 1);
            updated = _yield$cronJobModel$u2[0];
            if (!(updated === 0)) {
              _context3.next = 17;
              break;
            }
            return _context3.abrupt("return", (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.notFound404,
              message: "CronJob id ".concat(id, " not found")
            }));
          case 17:
            _context3.next = 19;
            return _index.cronJob.findByPk(id);
          case 19:
            updatedCronJob = _context3.sent;
            return _context3.abrupt("return", (0, _responseApi.result)({
              data: updatedCronJob
            }));
          case 23:
            _context3.prev = 23;
            _context3.t0 = _context3["catch"](1);
            _context3.t0.message = "CronJobRepository.updateCronJob: " + _context3.t0;
            throw _context3.t0;
          case 27:
          case "end":
            return _context3.stop();
        }
      }, _callee3, null, [[1, 23]]);
    }));
    return function updateCronJob(_x3) {
      return _ref4.apply(this, arguments);
    };
  }();

  // Create a new cron job
  var createCronJob = /*#__PURE__*/function () {
    var _ref5 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee4(req) {
      var getUsers, getStatusUser, newCronJob;
      return _regeneratorRuntime().wrap(function _callee4$(_context4) {
        while (1) switch (_context4.prev = _context4.next) {
          case 0:
            _context4.prev = 0;
            _context4.next = 3;
            return _index.users.findByPk(req.body.idUser, {
              attributes: ["id", "status", "fullname", "username"]
            });
          case 3:
            getUsers = _context4.sent;
            if (getUsers) {
              _context4.next = 6;
              break;
            }
            return _context4.abrupt("return", (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.badRequest400,
              message: "Client User not found !",
              data: getUsers
            }));
          case 6:
            getStatusUser = parseInt(getUsers.status) == 1;
            if (getStatusUser) {
              _context4.next = 9;
              break;
            }
            return _context4.abrupt("return", (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.badRequest400,
              message: "Client User status is not active! , please call administrator to active user",
              data: getUsers
            }));
          case 9:
            _context4.next = 11;
            return _index.cronJob.create(req.body);
          case 11:
            newCronJob = _context4.sent;
            return _context4.abrupt("return", (0, _responseApi.result)({
              data: newCronJob
            }));
          case 15:
            _context4.prev = 15;
            _context4.t0 = _context4["catch"](0);
            _context4.t0.message = "CronJobRepository.createCronJob: " + _context4.t0;
            throw _context4.t0;
          case 19:
          case "end":
            return _context4.stop();
        }
      }, _callee4, null, [[0, 15]]);
    }));
    return function createCronJob(_x4) {
      return _ref5.apply(this, arguments);
    };
  }();

  // Delete a cron job
  var deleteCronJob = /*#__PURE__*/function () {
    var _ref6 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee5(req) {
      var id, deleted;
      return _regeneratorRuntime().wrap(function _callee5$(_context5) {
        while (1) switch (_context5.prev = _context5.next) {
          case 0:
            id = req.params.id;
            _context5.prev = 1;
            _context5.next = 4;
            return _index.cronJob.destroy({
              where: {
                id: id
              }
            });
          case 4:
            deleted = _context5.sent;
            if (!(deleted === 0)) {
              _context5.next = 7;
              break;
            }
            return _context5.abrupt("return", (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.notFound404,
              message: "CronJob id ".concat(id, " not found")
            }));
          case 7:
            return _context5.abrupt("return", (0, _responseApi.result)({
              message: "Deleted CronJob id ".concat(id)
            }));
          case 10:
            _context5.prev = 10;
            _context5.t0 = _context5["catch"](1);
            _context5.t0.message = "CronJobRepository.deleteCronJob: " + _context5.t0;
            throw _context5.t0;
          case 14:
          case "end":
            return _context5.stop();
        }
      }, _callee5, null, [[1, 10]]);
    }));
    return function deleteCronJob(_x5) {
      return _ref6.apply(this, arguments);
    };
  }();
  return {
    getCronJobs: getCronJobs,
    getCronJobDetail: getCronJobDetail,
    updateCronJob: updateCronJob,
    createCronJob: createCronJob,
    deleteCronJob: deleteCronJob
  };
}