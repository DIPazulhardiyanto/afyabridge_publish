"use strict";

// import * as dotenv from "dotenv";
var dotenv = require("dotenv");
dotenv.config();
module.exports = {
  development: {
    service: {
      port: 5001
    },
    db: {
      username: "sa",
      password: "sa123456",
      host: "127.0.0.1",
      dialect: "mssql",
      name: "AfyaBridge",
      schema: "dbo"
    }
  },
  production: {
    service: {
      port: 5001
    },
    db: {
      username: "sa",
      password: "Serverh5n",
      host: "192.168.10.208",
      dialect: "mssql",
      name: "AfyaBridge",
      schema: "dbo"
    }
  },
  dip_extron: {
    service: {
      port: 5001
    },
    db: {
      username: "sa",
      password: "Serverh5n",
      host: "192.168.10.40",
      instanceName: "MSSQLSERVER2022",
      //10.42.111.14\\MSSQLSERVER2022
      dialect: "mssql",
      name: "AfyaBridge",
      schema: "dbo"
    }
  },
  dip_cloud: {
    service: {
      port: 5001
    },
    db: {
      username: "sa",
      password: "Serverh5n",
      host: "147.139.166.26",
      instanceName: null,
      dialect: "mssql",
      name: "AfyaBridge",
      schema: "dbo"
    }
  },
  rs_hermina_ptng_priuk_production: {
    service: {
      port: 5001,
      baseurl: "http://10.39.111.51:7001/api"
    },
    db: {
      username: "sa",
      password: "Serverh5n",
      host: "10.39.111.59",
      instanceName: "MSSQLSERVER2022",
      //10.42.111.14\\MSSQLSERVER2022
      dialect: "mssql",
      name: "AfyaBridge",
      schema: "dbo"
    },
    authApi: {
      username: "hospital",
      password: "serverh5n"
    }
  },
  rs_hermina_mtlnd_cbt_production: {
    service: {
      port: 5001,
      baseurl: "https://10.42.111.12:8068/api"
    },
    db: {
      username: "sa",
      password: "Serverh5n",
      host: "10.42.111.14",
      instanceName: "MSSQLSERVER2022",
      //10.42.111.14\\MSSQLSERVER2022
      dialect: "mssql",
      name: "AfyaBridge",
      schema: "dbo"
    },
    authApi: {
      username: "hospital",
      password: "serverh5n"
    }
  },
  rs_hermina_ikn_production: {
    service: {
      port: 5001,
      baseurl: "https://10.50.111.6:8095/api"
    },
    db: {
      username: "sa",
      password: "Serverh5n",
      host: "10.50.111.11",
      instanceName: "MSSQLSERVERPROD",
      //10.50.111.11\\MSSQLSERVERPROD
      dialect: "mssql",
      name: "AfyaBridge",
      schema: "dbo"
    },
    authApi: {
      username: "hospital",
      password: "serverh5n"
    }
  },
  rs_hermina_pik2_production: {
    service: {
      port: 5001,
      baseurl: "http://10.52.111.12:8071/api"
    },
    db: {
      username: "sa",
      password: "serverh5n",
      host: "VM-HPIK2-DB",
      instanceName: "MSSQLSERVER2022",
      //VM-HPIK2-DB\\MSSQLSERVER2022
      dialect: "mssql",
      name: "AfyaBridge",
      schema: "dbo"
    },
    authApi: {
      username: "hospital",
      password: "serverh5n"
    }
  },
  rshga_production: {
    service: {
      port: 5002,
      baseurl: "https://127.0.0.1:8081/api" //ketika Production (Server RS)
      // baseurl: "https://simrs.rs-hga.co.id:8081/api", // ketika Debugging Local Only
    },
    db: {
      username: "sa",
      password: "p@ssw0rd",
      host: "192.168.1.109",
      dialect: "mssql",
      name: "AfyaBridge",
      schema: "dbo"
    },
    authApi: {
      username: "hospital",
      password: "serverh5n"
    }
  },
  rshga_test: {
    service: {
      port: 5001
    },
    db: {
      username: "sa",
      password: "p@ssw0rd",
      host: "192.168.1.107",
      dialect: "mssql",
      name: "AfyaBridge",
      schema: "dbo"
    }
  },
  rsui_production: {
    service: {
      port: 5002
    },
    db: {
      username: "sa",
      password: "Serverh5n",
      host: "152.118.52.75",
      dialect: "mssql",
      name: "AfyaBridge",
      schema: "dbo"
    }
  },
  rsui_test: {
    service: {
      port: 5001
    },
    db: {
      username: "sa",
      password: "123456@rsu1",
      host: "10.121.35.152",
      dialect: "mssql",
      name: "AfyaBridge",
      schema: "dbo",
      instanceName: "HOM2014"
    }
  },
  rsks_test: {
    service: {
      port: 5001
    },
    db: {
      username: "sa",
      password: "Serverh5ndiprsks",
      host: "192.168.0.3",
      dialect: "mssql",
      name: "AfyaBridge",
      schema: "dbo",
      instanceName: "MSSQLSERVERTEST"
    }
  },
  rsks_production: {
    service: {
      port: 5002,
      base_url: "https://192.168.0.10:8080/api"
    },
    db: {
      username: "sa",
      password: "Serverh5ndiprsks",
      host: "192.168.0.3",
      dialect: "mssql",
      name: "AfyaBridge",
      schema: "dbo",
      instanceName: "MSSQLSERVERPROD"
    },
    authApi: {
      username: "hospital",
      password: "serverh5n"
    }
  }
};