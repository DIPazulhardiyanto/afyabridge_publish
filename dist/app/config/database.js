"use strict";

function _typeof(o) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) { return typeof o; } : function (o) { return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o; }, _typeof(o); }
require("core-js/modules/es.symbol.js");
require("core-js/modules/es.symbol.description.js");
require("core-js/modules/es.symbol.iterator.js");
require("core-js/modules/es.array.iterator.js");
require("core-js/modules/es.object.define-property.js");
require("core-js/modules/es.object.get-own-property-descriptor.js");
require("core-js/modules/es.object.to-string.js");
require("core-js/modules/es.string.iterator.js");
require("core-js/modules/es.weak-map.js");
require("core-js/modules/web.dom-collections.iterator.js");
require("core-js/modules/es.function.name.js");
var dotenv = _interopRequireWildcard(require("dotenv"));
var _setup = _interopRequireDefault(require("./setup"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != _typeof(e) && "function" != typeof e) return { "default": e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && {}.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n["default"] = e, t && t.set(e, n), n; }
dotenv.config();
console.log("Environtment :", process.env.NODE_ENV);
console.log("Database :", _setup["default"][process.env.NODE_ENV].db.dialect);

// module.exports = {
//   secret: "ilovescotchyscotch",
//   username: setup[process.env.NODE_ENV].db.username,
//   password: setup[process.env.NODE_ENV].db.password,
//   database: setup[process.env.NODE_ENV].db.name,
//   host: setup[process.env.NODE_ENV].db.host,
//   dialect: setup[process.env.NODE_ENV].db.dialect,
//   dialectOptions: {
//     ssl: {
//       require: true,
//       rejectUnauthorized: false, // <<<<<<< YOU NEED THIS
//     },
//     requestTimeout: 150000,
//     options: {
//       instanceName: setup[process.env.NODE_ENV].db.instanceName
//         ? setup[process.env.NODE_ENV].db.instanceName
//         : "",
//       encrypt: false,
//     },
//   },
//   timezone: "+07:00",
//   migrationStorageTableSchema: "public",
//   schema: setup[process.env.NODE_ENV].db.schema,
//   logQueryParameters: false,
//   logging: false,
//   pool: {
//     max: 5,
//     min: 0,
//     acquire: 30000,
//     idle: 10000,
//   },
// };

module.exports = {
  secret: "ilovescotchyscotch",
  username: _setup["default"][process.env.NODE_ENV].db.username,
  password: _setup["default"][process.env.NODE_ENV].db.password,
  database: _setup["default"][process.env.NODE_ENV].db.name,
  host: _setup["default"][process.env.NODE_ENV].db.host,
  // host untuk 10.50.111.11
  dialect: _setup["default"][process.env.NODE_ENV].db.dialect,
  dialectOptions: {
    ssl: {
      require: true,
      rejectUnauthorized: false
    },
    requestTimeout: 150000,
    options: {
      //instanceName digunakan untuk  MSSQLSERVERPROD from: 10.50.111.11\\MSSQLSERVERPROD jika tidak ada instance maka tidak perlu dipakai
      instanceName: _setup["default"][process.env.NODE_ENV].db.instanceName ? _setup["default"][process.env.NODE_ENV].db.instanceName : "",
      encrypt: false
    }
  },
  timezone: "+07:00",
  migrationStorageTableSchema: "dbo",
  // Change to dbo if using MSSQL
  schema: _setup["default"][process.env.NODE_ENV].db.schema || "dbo",
  // Default to dbo if not set
  logQueryParameters: false,
  logging: false,
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
};