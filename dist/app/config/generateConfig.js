"use strict";

require("core-js/modules/es.symbol.js");
require("core-js/modules/es.symbol.description.js");
require("core-js/modules/es.symbol.iterator.js");
require("core-js/modules/es.symbol.to-primitive.js");
require("core-js/modules/es.array.filter.js");
require("core-js/modules/es.array.for-each.js");
require("core-js/modules/es.array.iterator.js");
require("core-js/modules/es.date.to-primitive.js");
require("core-js/modules/es.number.constructor.js");
require("core-js/modules/es.object.define-properties.js");
require("core-js/modules/es.object.define-property.js");
require("core-js/modules/es.object.get-own-property-descriptor.js");
require("core-js/modules/es.object.get-own-property-descriptors.js");
require("core-js/modules/es.string.iterator.js");
require("core-js/modules/web.dom-collections.for-each.js");
require("core-js/modules/web.dom-collections.iterator.js");
function _typeof(o) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) { return typeof o; } : function (o) { return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o; }, _typeof(o); }
require("core-js/modules/es.array.join.js");
require("core-js/modules/es.array.reduce.js");
require("core-js/modules/es.date.to-json.js");
require("core-js/modules/es.function.name.js");
require("core-js/modules/es.object.keys.js");
require("core-js/modules/es.object.to-string.js");
function ownKeys(e, r) { var t = Object.keys(e); if (Object.getOwnPropertySymbols) { var o = Object.getOwnPropertySymbols(e); r && (o = o.filter(function (r) { return Object.getOwnPropertyDescriptor(e, r).enumerable; })), t.push.apply(t, o); } return t; }
function _objectSpread(e) { for (var r = 1; r < arguments.length; r++) { var t = null != arguments[r] ? arguments[r] : {}; r % 2 ? ownKeys(Object(t), !0).forEach(function (r) { _defineProperty(e, r, t[r]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys(Object(t)).forEach(function (r) { Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r)); }); } return e; }
function _defineProperty(e, r, t) { return (r = _toPropertyKey(r)) in e ? Object.defineProperty(e, r, { value: t, enumerable: !0, configurable: !0, writable: !0 }) : e[r] = t, e; }
function _toPropertyKey(t) { var i = _toPrimitive(t, "string"); return "symbol" == _typeof(i) ? i : i + ""; }
function _toPrimitive(t, r) { if ("object" != _typeof(t) || !t) return t; var e = t[Symbol.toPrimitive]; if (void 0 !== e) { var i = e.call(t, r || "default"); if ("object" != _typeof(i)) return i; throw new TypeError("@@toPrimitive must return a primitive value."); } return ("string" === r ? String : Number)(t); }
var fs = require("fs");
var path = require("path");

// Import file setup.js
var setupConfig = require("./setup");

// Path ke file config.json
var configPath = path.join(__dirname, "config.json");

// Fungsi untuk membuat atau memperbarui file config.json
function createOrUpdateConfigFile() {
  // Konversi setup.js ke format config.json
  var configJson = Object.keys(setupConfig).reduce(function (acc, env) {
    var _setupConfig$env, _setupConfig$env$db, _setupConfig$env2, _setupConfig$env2$db, _setupConfig$env3, _setupConfig$env3$db, _setupConfig$env4, _setupConfig$env4$db, _setupConfig$env5, _setupConfig$env5$db, _setupConfig$env6, _setupConfig$env6$db, _setupConfig$env7, _setupConfig$env7$db;
    acc[env] = _objectSpread({
      username: (_setupConfig$env = setupConfig[env]) === null || _setupConfig$env === void 0 ? void 0 : (_setupConfig$env$db = _setupConfig$env.db) === null || _setupConfig$env$db === void 0 ? void 0 : _setupConfig$env$db.username,
      password: (_setupConfig$env2 = setupConfig[env]) === null || _setupConfig$env2 === void 0 ? void 0 : (_setupConfig$env2$db = _setupConfig$env2.db) === null || _setupConfig$env2$db === void 0 ? void 0 : _setupConfig$env2$db.password,
      database: (_setupConfig$env3 = setupConfig[env]) === null || _setupConfig$env3 === void 0 ? void 0 : (_setupConfig$env3$db = _setupConfig$env3.db) === null || _setupConfig$env3$db === void 0 ? void 0 : _setupConfig$env3$db.name,
      host: (_setupConfig$env4 = setupConfig[env]) === null || _setupConfig$env4 === void 0 ? void 0 : (_setupConfig$env4$db = _setupConfig$env4.db) === null || _setupConfig$env4$db === void 0 ? void 0 : _setupConfig$env4$db.host,
      dialect: (_setupConfig$env5 = setupConfig[env]) === null || _setupConfig$env5 === void 0 ? void 0 : (_setupConfig$env5$db = _setupConfig$env5.db) === null || _setupConfig$env5$db === void 0 ? void 0 : _setupConfig$env5$db.dialect
    }, ((_setupConfig$env6 = setupConfig[env]) === null || _setupConfig$env6 === void 0 ? void 0 : (_setupConfig$env6$db = _setupConfig$env6.db) === null || _setupConfig$env6$db === void 0 ? void 0 : _setupConfig$env6$db.instanceName) && {
      instanceName: (_setupConfig$env7 = setupConfig[env]) === null || _setupConfig$env7 === void 0 ? void 0 : (_setupConfig$env7$db = _setupConfig$env7.db) === null || _setupConfig$env7$db === void 0 ? void 0 : _setupConfig$env7$db.instanceName
    });
    return acc;
  }, {});
  if (fs.existsSync(configPath)) {
    // Jika file config.json sudah ada, lakukan update
    var existingConfig = JSON.parse(fs.readFileSync(configPath, "utf-8"));

    // Gabungkan konfigurasi baru ke file yang sudah ada
    var updatedConfig = _objectSpread(_objectSpread({}, existingConfig), configJson);

    // Tulis ulang file config.json dengan konfigurasi terbaru
    fs.writeFileSync(configPath, JSON.stringify(updatedConfig, null, 2), "utf-8");
    console.log("File config.json berhasil diperbarui!");
  } else {
    // Jika file config.json belum ada, buat file baru
    fs.writeFileSync(configPath, JSON.stringify(configJson, null, 2), "utf-8");
    console.log("File config.json berhasil dibuat!");
  }
}

// Jalankan fungsi
createOrUpdateConfigFile();