"use strict";

require("core-js/modules/es.symbol.js");
require("core-js/modules/es.symbol.description.js");
require("core-js/modules/es.symbol.async-iterator.js");
require("core-js/modules/es.symbol.iterator.js");
require("core-js/modules/es.symbol.to-primitive.js");
require("core-js/modules/es.symbol.to-string-tag.js");
require("core-js/modules/es.array.filter.js");
require("core-js/modules/es.array.for-each.js");
require("core-js/modules/es.array.from.js");
require("core-js/modules/es.array.reverse.js");
require("core-js/modules/es.array.slice.js");
require("core-js/modules/es.date.to-primitive.js");
require("core-js/modules/es.function.name.js");
require("core-js/modules/es.json.to-string-tag.js");
require("core-js/modules/es.math.to-string-tag.js");
require("core-js/modules/es.number.constructor.js");
require("core-js/modules/es.object.create.js");
require("core-js/modules/es.object.define-properties.js");
require("core-js/modules/es.object.define-property.js");
require("core-js/modules/es.object.get-own-property-descriptor.js");
require("core-js/modules/es.object.get-own-property-descriptors.js");
require("core-js/modules/es.object.get-prototype-of.js");
require("core-js/modules/es.object.set-prototype-of.js");
require("core-js/modules/es.promise.js");
require("core-js/modules/web.dom-collections.for-each.js");
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = helpers;
exports.migrationField = void 0;
require("core-js/modules/es.array.find.js");
require("core-js/modules/es.array.find-index.js");
require("core-js/modules/es.array.includes.js");
require("core-js/modules/es.array.is-array.js");
require("core-js/modules/es.array.iterator.js");
require("core-js/modules/es.array.join.js");
require("core-js/modules/es.array.map.js");
require("core-js/modules/es.array.splice.js");
require("core-js/modules/es.date.now.js");
require("core-js/modules/es.date.to-iso-string.js");
require("core-js/modules/es.date.to-string.js");
require("core-js/modules/es.object.get-own-property-names.js");
require("core-js/modules/es.object.keys.js");
require("core-js/modules/es.object.to-string.js");
require("core-js/modules/es.parse-int.js");
require("core-js/modules/es.regexp.exec.js");
require("core-js/modules/es.regexp.to-string.js");
require("core-js/modules/es.string.ends-with.js");
require("core-js/modules/es.string.includes.js");
require("core-js/modules/es.string.iterator.js");
require("core-js/modules/es.string.pad-start.js");
require("core-js/modules/es.string.replace.js");
require("core-js/modules/es.string.starts-with.js");
require("core-js/modules/web.dom-collections.iterator.js");
var _path = _interopRequireDefault(require("path"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
function ownKeys(e, r) { var t = Object.keys(e); if (Object.getOwnPropertySymbols) { var o = Object.getOwnPropertySymbols(e); r && (o = o.filter(function (r) { return Object.getOwnPropertyDescriptor(e, r).enumerable; })), t.push.apply(t, o); } return t; }
function _objectSpread(e) { for (var r = 1; r < arguments.length; r++) { var t = null != arguments[r] ? arguments[r] : {}; r % 2 ? ownKeys(Object(t), !0).forEach(function (r) { _defineProperty(e, r, t[r]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys(Object(t)).forEach(function (r) { Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r)); }); } return e; }
function _defineProperty(e, r, t) { return (r = _toPropertyKey(r)) in e ? Object.defineProperty(e, r, { value: t, enumerable: !0, configurable: !0, writable: !0 }) : e[r] = t, e; }
function _toPropertyKey(t) { var i = _toPrimitive(t, "string"); return "symbol" == _typeof(i) ? i : i + ""; }
function _toPrimitive(t, r) { if ("object" != _typeof(t) || !t) return t; var e = t[Symbol.toPrimitive]; if (void 0 !== e) { var i = e.call(t, r || "default"); if ("object" != _typeof(i)) return i; throw new TypeError("@@toPrimitive must return a primitive value."); } return ("string" === r ? String : Number)(t); }
function _typeof(o) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) { return typeof o; } : function (o) { return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o; }, _typeof(o); }
function _createForOfIteratorHelper(r, e) { var t = "undefined" != typeof Symbol && r[Symbol.iterator] || r["@@iterator"]; if (!t) { if (Array.isArray(r) || (t = _unsupportedIterableToArray(r)) || e && r && "number" == typeof r.length) { t && (r = t); var _n = 0, F = function F() {}; return { s: F, n: function n() { return _n >= r.length ? { done: !0 } : { done: !1, value: r[_n++] }; }, e: function e(r) { throw r; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var o, a = !0, u = !1; return { s: function s() { t = t.call(r); }, n: function n() { var r = t.next(); return a = r.done, r; }, e: function e(r) { u = !0, o = r; }, f: function f() { try { a || null == t["return"] || t["return"](); } finally { if (u) throw o; } } }; }
function _unsupportedIterableToArray(r, a) { if (r) { if ("string" == typeof r) return _arrayLikeToArray(r, a); var t = {}.toString.call(r).slice(8, -1); return "Object" === t && r.constructor && (t = r.constructor.name), "Map" === t || "Set" === t ? Array.from(r) : "Arguments" === t || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(t) ? _arrayLikeToArray(r, a) : void 0; } }
function _arrayLikeToArray(r, a) { (null == a || a > r.length) && (a = r.length); for (var e = 0, n = Array(a); e < a; e++) n[e] = r[e]; return n; }
function _regeneratorRuntime() { "use strict"; /*! regenerator-runtime -- Copyright (c) 2014-present, Facebook, Inc. -- license (MIT): https://github.com/facebook/regenerator/blob/main/LICENSE */ _regeneratorRuntime = function _regeneratorRuntime() { return e; }; var t, e = {}, r = Object.prototype, n = r.hasOwnProperty, o = Object.defineProperty || function (t, e, r) { t[e] = r.value; }, i = "function" == typeof Symbol ? Symbol : {}, a = i.iterator || "@@iterator", c = i.asyncIterator || "@@asyncIterator", u = i.toStringTag || "@@toStringTag"; function define(t, e, r) { return Object.defineProperty(t, e, { value: r, enumerable: !0, configurable: !0, writable: !0 }), t[e]; } try { define({}, ""); } catch (t) { define = function define(t, e, r) { return t[e] = r; }; } function wrap(t, e, r, n) { var i = e && e.prototype instanceof Generator ? e : Generator, a = Object.create(i.prototype), c = new Context(n || []); return o(a, "_invoke", { value: makeInvokeMethod(t, r, c) }), a; } function tryCatch(t, e, r) { try { return { type: "normal", arg: t.call(e, r) }; } catch (t) { return { type: "throw", arg: t }; } } e.wrap = wrap; var h = "suspendedStart", l = "suspendedYield", f = "executing", s = "completed", y = {}; function Generator() {} function GeneratorFunction() {} function GeneratorFunctionPrototype() {} var p = {}; define(p, a, function () { return this; }); var d = Object.getPrototypeOf, v = d && d(d(values([]))); v && v !== r && n.call(v, a) && (p = v); var g = GeneratorFunctionPrototype.prototype = Generator.prototype = Object.create(p); function defineIteratorMethods(t) { ["next", "throw", "return"].forEach(function (e) { define(t, e, function (t) { return this._invoke(e, t); }); }); } function AsyncIterator(t, e) { function invoke(r, o, i, a) { var c = tryCatch(t[r], t, o); if ("throw" !== c.type) { var u = c.arg, h = u.value; return h && "object" == _typeof(h) && n.call(h, "__await") ? e.resolve(h.__await).then(function (t) { invoke("next", t, i, a); }, function (t) { invoke("throw", t, i, a); }) : e.resolve(h).then(function (t) { u.value = t, i(u); }, function (t) { return invoke("throw", t, i, a); }); } a(c.arg); } var r; o(this, "_invoke", { value: function value(t, n) { function callInvokeWithMethodAndArg() { return new e(function (e, r) { invoke(t, n, e, r); }); } return r = r ? r.then(callInvokeWithMethodAndArg, callInvokeWithMethodAndArg) : callInvokeWithMethodAndArg(); } }); } function makeInvokeMethod(e, r, n) { var o = h; return function (i, a) { if (o === f) throw Error("Generator is already running"); if (o === s) { if ("throw" === i) throw a; return { value: t, done: !0 }; } for (n.method = i, n.arg = a;;) { var c = n.delegate; if (c) { var u = maybeInvokeDelegate(c, n); if (u) { if (u === y) continue; return u; } } if ("next" === n.method) n.sent = n._sent = n.arg;else if ("throw" === n.method) { if (o === h) throw o = s, n.arg; n.dispatchException(n.arg); } else "return" === n.method && n.abrupt("return", n.arg); o = f; var p = tryCatch(e, r, n); if ("normal" === p.type) { if (o = n.done ? s : l, p.arg === y) continue; return { value: p.arg, done: n.done }; } "throw" === p.type && (o = s, n.method = "throw", n.arg = p.arg); } }; } function maybeInvokeDelegate(e, r) { var n = r.method, o = e.iterator[n]; if (o === t) return r.delegate = null, "throw" === n && e.iterator["return"] && (r.method = "return", r.arg = t, maybeInvokeDelegate(e, r), "throw" === r.method) || "return" !== n && (r.method = "throw", r.arg = new TypeError("The iterator does not provide a '" + n + "' method")), y; var i = tryCatch(o, e.iterator, r.arg); if ("throw" === i.type) return r.method = "throw", r.arg = i.arg, r.delegate = null, y; var a = i.arg; return a ? a.done ? (r[e.resultName] = a.value, r.next = e.nextLoc, "return" !== r.method && (r.method = "next", r.arg = t), r.delegate = null, y) : a : (r.method = "throw", r.arg = new TypeError("iterator result is not an object"), r.delegate = null, y); } function pushTryEntry(t) { var e = { tryLoc: t[0] }; 1 in t && (e.catchLoc = t[1]), 2 in t && (e.finallyLoc = t[2], e.afterLoc = t[3]), this.tryEntries.push(e); } function resetTryEntry(t) { var e = t.completion || {}; e.type = "normal", delete e.arg, t.completion = e; } function Context(t) { this.tryEntries = [{ tryLoc: "root" }], t.forEach(pushTryEntry, this), this.reset(!0); } function values(e) { if (e || "" === e) { var r = e[a]; if (r) return r.call(e); if ("function" == typeof e.next) return e; if (!isNaN(e.length)) { var o = -1, i = function next() { for (; ++o < e.length;) if (n.call(e, o)) return next.value = e[o], next.done = !1, next; return next.value = t, next.done = !0, next; }; return i.next = i; } } throw new TypeError(_typeof(e) + " is not iterable"); } return GeneratorFunction.prototype = GeneratorFunctionPrototype, o(g, "constructor", { value: GeneratorFunctionPrototype, configurable: !0 }), o(GeneratorFunctionPrototype, "constructor", { value: GeneratorFunction, configurable: !0 }), GeneratorFunction.displayName = define(GeneratorFunctionPrototype, u, "GeneratorFunction"), e.isGeneratorFunction = function (t) { var e = "function" == typeof t && t.constructor; return !!e && (e === GeneratorFunction || "GeneratorFunction" === (e.displayName || e.name)); }, e.mark = function (t) { return Object.setPrototypeOf ? Object.setPrototypeOf(t, GeneratorFunctionPrototype) : (t.__proto__ = GeneratorFunctionPrototype, define(t, u, "GeneratorFunction")), t.prototype = Object.create(g), t; }, e.awrap = function (t) { return { __await: t }; }, defineIteratorMethods(AsyncIterator.prototype), define(AsyncIterator.prototype, c, function () { return this; }), e.AsyncIterator = AsyncIterator, e.async = function (t, r, n, o, i) { void 0 === i && (i = Promise); var a = new AsyncIterator(wrap(t, r, n, o), i); return e.isGeneratorFunction(r) ? a : a.next().then(function (t) { return t.done ? t.value : a.next(); }); }, defineIteratorMethods(g), define(g, u, "Generator"), define(g, a, function () { return this; }), define(g, "toString", function () { return "[object Generator]"; }), e.keys = function (t) { var e = Object(t), r = []; for (var n in e) r.push(n); return r.reverse(), function next() { for (; r.length;) { var t = r.pop(); if (t in e) return next.value = t, next.done = !1, next; } return next.done = !0, next; }; }, e.values = values, Context.prototype = { constructor: Context, reset: function reset(e) { if (this.prev = 0, this.next = 0, this.sent = this._sent = t, this.done = !1, this.delegate = null, this.method = "next", this.arg = t, this.tryEntries.forEach(resetTryEntry), !e) for (var r in this) "t" === r.charAt(0) && n.call(this, r) && !isNaN(+r.slice(1)) && (this[r] = t); }, stop: function stop() { this.done = !0; var t = this.tryEntries[0].completion; if ("throw" === t.type) throw t.arg; return this.rval; }, dispatchException: function dispatchException(e) { if (this.done) throw e; var r = this; function handle(n, o) { return a.type = "throw", a.arg = e, r.next = n, o && (r.method = "next", r.arg = t), !!o; } for (var o = this.tryEntries.length - 1; o >= 0; --o) { var i = this.tryEntries[o], a = i.completion; if ("root" === i.tryLoc) return handle("end"); if (i.tryLoc <= this.prev) { var c = n.call(i, "catchLoc"), u = n.call(i, "finallyLoc"); if (c && u) { if (this.prev < i.catchLoc) return handle(i.catchLoc, !0); if (this.prev < i.finallyLoc) return handle(i.finallyLoc); } else if (c) { if (this.prev < i.catchLoc) return handle(i.catchLoc, !0); } else { if (!u) throw Error("try statement without catch or finally"); if (this.prev < i.finallyLoc) return handle(i.finallyLoc); } } } }, abrupt: function abrupt(t, e) { for (var r = this.tryEntries.length - 1; r >= 0; --r) { var o = this.tryEntries[r]; if (o.tryLoc <= this.prev && n.call(o, "finallyLoc") && this.prev < o.finallyLoc) { var i = o; break; } } i && ("break" === t || "continue" === t) && i.tryLoc <= e && e <= i.finallyLoc && (i = null); var a = i ? i.completion : {}; return a.type = t, a.arg = e, i ? (this.method = "next", this.next = i.finallyLoc, y) : this.complete(a); }, complete: function complete(t, e) { if ("throw" === t.type) throw t.arg; return "break" === t.type || "continue" === t.type ? this.next = t.arg : "return" === t.type ? (this.rval = this.arg = t.arg, this.method = "return", this.next = "end") : "normal" === t.type && e && (this.next = e), y; }, finish: function finish(t) { for (var e = this.tryEntries.length - 1; e >= 0; --e) { var r = this.tryEntries[e]; if (r.finallyLoc === t) return this.complete(r.completion, r.afterLoc), resetTryEntry(r), y; } }, "catch": function _catch(t) { for (var e = this.tryEntries.length - 1; e >= 0; --e) { var r = this.tryEntries[e]; if (r.tryLoc === t) { var n = r.completion; if ("throw" === n.type) { var o = n.arg; resetTryEntry(r); } return o; } } throw Error("illegal catch attempt"); }, delegateYield: function delegateYield(e, r, n) { return this.delegate = { iterator: values(e), resultName: r, nextLoc: n }, "next" === this.method && (this.arg = t), y; } }, e; }
function asyncGeneratorStep(n, t, e, r, o, a, c) { try { var i = n[a](c), u = i.value; } catch (n) { return void e(n); } i.done ? t(u) : Promise.resolve(u).then(r, o); }
function _asyncToGenerator(n) { return function () { var t = this, e = arguments; return new Promise(function (r, o) { var a = n.apply(t, e); function _next(n) { asyncGeneratorStep(a, r, o, _next, _throw, "next", n); } function _throw(n) { asyncGeneratorStep(a, r, o, _next, _throw, "throw", n); } _next(void 0); }); }; }
function helpers() {
  var result;
  var getPagingData = function getPagingData(data, page, limit) {
    var totalItems = data.count,
      rows = data.rows;
    var currentPage = page ? +page : 0;
    var totalPages = Math.ceil(totalItems / limit);
    return {
      totalItems: totalItems,
      rows: rows,
      totalPages: totalPages,
      currentPage: currentPage
    };
  };
  var getPagination = function getPagination(page, size, descending) {
    if (page > 0) {
      page -= 1;
    }
    var limit = size ? size : 5;
    var offset = page ? limit * page : 0;
    var desc = !descending ? "DESC" : descending.toUpperCase();
    var order = [["createdAt", desc]];
    limit = parseInt(limit);
    return {
      limit: limit,
      offset: offset,
      order: order
    };
  };
  var getPaginationNew = function getPaginationNew(page, size, orderBy) {
    if (page > 0) {
      page -= 1;
    }
    var limit = size ? size : 5;
    var offset = page ? limit * page : 0;
    var order = [[orderBy.length > 0 ? orderBy : 'createdAt', orderBy.length > 0 ? "DESC" : "ASC"]];
    limit = parseInt(limit);
    return {
      limit: limit,
      offset: offset,
      order: order
    };
  };
  var getUniqueRegistrationToken = /*#__PURE__*/function () {
    var _ref = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee(usersRepository) {
      var isUnique, generator, token;
      return _regeneratorRuntime().wrap(function _callee$(_context) {
        while (1) switch (_context.prev = _context.next) {
          case 0:
            isUnique = false;
            generator = require("generate-password");
            token = "";
          case 3:
            if (isUnique) {
              _context.next = 11;
              break;
            }
            _context.next = 6;
            return generator.generate({
              length: 100
            });
          case 6:
            token = _context.sent;
            _context.next = 9;
            return usersRepository.findOne({
              where: {
                token: token
              }
            }).then(function (user) {
              if (user == null) {
                isUnique = true;
              }
            })["catch"](function (error) {
              return console.log(error);
            });
          case 9:
            _context.next = 3;
            break;
          case 11:
            return _context.abrupt("return", token);
          case 12:
          case "end":
            return _context.stop();
        }
      }, _callee);
    }));
    return function getUniqueRegistrationToken(_x) {
      return _ref.apply(this, arguments);
    };
  }();
  var _arrayToObjectAll = /*#__PURE__*/function () {
    var _ref2 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee2(objData) {
      var _iterator, _step, arrayItem, _i, _Object$keys, key, propOwn;
      return _regeneratorRuntime().wrap(function _callee2$(_context2) {
        while (1) switch (_context2.prev = _context2.next) {
          case 0:
            if (!(objData != null)) {
              _context2.next = 32;
              break;
            }
            if (!Array.isArray(objData)) {
              _context2.next = 21;
              break;
            }
            _iterator = _createForOfIteratorHelper(objData);
            _context2.prev = 3;
            _iterator.s();
          case 5:
            if ((_step = _iterator.n()).done) {
              _context2.next = 11;
              break;
            }
            arrayItem = _step.value;
            _context2.next = 9;
            return _arrayToObjectAll(arrayItem);
          case 9:
            _context2.next = 5;
            break;
          case 11:
            _context2.next = 16;
            break;
          case 13:
            _context2.prev = 13;
            _context2.t0 = _context2["catch"](3);
            _iterator.e(_context2.t0);
          case 16:
            _context2.prev = 16;
            _iterator.f();
            return _context2.finish(16);
          case 19:
            _context2.next = 32;
            break;
          case 21:
            if (!(_typeof(objData) == "object")) {
              _context2.next = 32;
              break;
            }
            _i = 0, _Object$keys = Object.keys(objData);
          case 23:
            if (!(_i < _Object$keys.length)) {
              _context2.next = 32;
              break;
            }
            key = _Object$keys[_i];
            propOwn = Object.getOwnPropertyNames(objData);
            result = propOwn.length > 2 ? objData : objData[key];
            _context2.next = 29;
            return _arrayToObjectAll(objData[key]);
          case 29:
            _i++;
            _context2.next = 23;
            break;
          case 32:
            return _context2.abrupt("return", result);
          case 33:
          case "end":
            return _context2.stop();
        }
      }, _callee2, null, [[3, 13, 16, 19]]);
    }));
    return function arrayToObjectAll(_x2) {
      return _ref2.apply(this, arguments);
    };
  }();
  var findAndMoveToLast = /*#__PURE__*/function () {
    var _ref3 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee3(array, condition) {
      var index, objectToMove;
      return _regeneratorRuntime().wrap(function _callee3$(_context3) {
        while (1) switch (_context3.prev = _context3.next) {
          case 0:
            index = array.findIndex(condition);
            if (index !== -1) {
              objectToMove = array.splice(index, 1)[0];
              array.push(objectToMove);
            }
          case 2:
          case "end":
            return _context3.stop();
        }
      }, _callee3);
    }));
    return function findAndMoveToLast(_x3, _x4) {
      return _ref3.apply(this, arguments);
    };
  }();

  /** Khusus untuk mencari error dari http code */
  var plas = {};
  var _findNestedErrorHttp = /*#__PURE__*/function () {
    var _ref4 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee4(objData) {
      var result,
        errCode,
        _iterator2,
        _step2,
        arrayItem,
        _i2,
        _Object$keys2,
        key,
        _args4 = arguments;
      return _regeneratorRuntime().wrap(function _callee4$(_context4) {
        while (1) switch (_context4.prev = _context4.next) {
          case 0:
            result = _args4.length > 1 && _args4[1] !== undefined ? _args4[1] : {};
            plas = {};
            errCode = [400, 401, 404, 500];
            if (!Array.isArray(objData)) {
              _context4.next = 23;
              break;
            }
            _iterator2 = _createForOfIteratorHelper(objData);
            _context4.prev = 5;
            _iterator2.s();
          case 7:
            if ((_step2 = _iterator2.n()).done) {
              _context4.next = 13;
              break;
            }
            arrayItem = _step2.value;
            _context4.next = 11;
            return _findNestedErrorHttp(arrayItem, result);
          case 11:
            _context4.next = 7;
            break;
          case 13:
            _context4.next = 18;
            break;
          case 15:
            _context4.prev = 15;
            _context4.t0 = _context4["catch"](5);
            _iterator2.e(_context4.t0);
          case 18:
            _context4.prev = 18;
            _iterator2.f();
            return _context4.finish(18);
          case 21:
            _context4.next = 44;
            break;
          case 23:
            if (!(objData && _typeof(objData) == "object")) {
              _context4.next = 44;
              break;
            }
            if (!objData.hasOwnProperty("code")) {
              _context4.next = 29;
              break;
            }
            if (errCode.includes(parseInt(objData.code))) {
              plas = {
                code: objData.code,
                message: objData && objData.hasOwnProperty("message") && objData.message ? objData.message : "Client Server Error",
                obj: objData
              };
            }
            return _context4.abrupt("return", plas);
          case 29:
            _i2 = 0, _Object$keys2 = Object.keys(objData);
          case 30:
            if (!(_i2 < _Object$keys2.length)) {
              _context4.next = 44;
              break;
            }
            key = _Object$keys2[_i2];
            if (!(objData[key] != null)) {
              _context4.next = 41;
              break;
            }
            if (!objData[key].hasOwnProperty("code")) {
              _context4.next = 39;
              break;
            }
            if (!errCode.includes(parseInt(objData[key].code))) {
              _context4.next = 37;
              break;
            }
            plas = {
              code: objData[key].code,
              message: objData[key] && objData[key].hasOwnProperty("message") && objData[key].message ? objData[key].message : "Client Server Error",
              obj: objData
            };
            return _context4.abrupt("break", 44);
          case 37:
            _context4.next = 41;
            break;
          case 39:
            _context4.next = 41;
            return _findNestedErrorHttp(objData[key]);
          case 41:
            _i2++;
            _context4.next = 30;
            break;
          case 44:
            return _context4.abrupt("return", plas);
          case 45:
          case "end":
            return _context4.stop();
        }
      }, _callee4, null, [[5, 15, 18, 21]]);
    }));
    return function findNestedErrorHttp(_x5) {
      return _ref4.apply(this, arguments);
    };
  }();
  function replacingValue(srcReplace, toReplace) {
    return srcReplace.replace(/#([^#]+)#/g, function (match, key) {
      return toReplace !== undefined ? toReplace : "";
    });
  }
  function isTokenExpired(token) {
    var payloadBase64 = token.split(".")[1];
    var decodedJson = Buffer.from(payloadBase64, "base64").toString();
    var decoded = JSON.parse(decodedJson);
    var exp = decoded.exp;
    var expired = Date.now() >= exp * 1000;
    var expDt = exp * 1000;
    var d = new Date(expDt);
    return toIsoString(d);
  }
  function toIsoString(date) {
    var tzo = -date.getTimezoneOffset(),
      dif = tzo >= 0 ? "+" : "-",
      pad = function pad(num) {
        return (num < 10 ? "0" : "") + num;
      };
    return date.getFullYear() + "-" + pad(date.getMonth() + 1) + "-" + pad(date.getDate()) + "T" + pad(date.getHours()) + ":" + pad(date.getMinutes()) + ":" + pad(date.getSeconds()) + dif + pad(Math.floor(Math.abs(tzo) / 60)) + ":" + pad(Math.abs(tzo) % 60);
  }

  /**
   * For Check Path Is SubPath
   * */
  function isSubPathOf(subPath, parentPath) {
    parentPath = normalize(parentPath);
    if (subPath.length <= parentPath.length) return false;
    function normalize(p) {
      p = _path["default"].normalize(p);
      if (!p.endsWith(_path["default"].sep)) p += _path["default"].sep;
      return p;
    }
    subPath = normalize(subPath);
    return subPath.startsWith(parentPath);
  }

  /**
   * @desc For Request Body Mapping
   */
  var _deepChangesObjectKey = /*#__PURE__*/function () {
    var _ref5 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee5(targetObj, sourceObj) {
      var matches,
        _iterator3,
        _step3,
        arrayItem,
        _loop,
        _i3,
        _Object$keys3,
        _args6 = arguments;
      return _regeneratorRuntime().wrap(function _callee5$(_context6) {
        while (1) switch (_context6.prev = _context6.next) {
          case 0:
            matches = _args6.length > 2 && _args6[2] !== undefined ? _args6[2] : {};
            if (!(targetObj != null)) {
              _context6.next = 30;
              break;
            }
            if (!Array.isArray(targetObj)) {
              _context6.next = 22;
              break;
            }
            _iterator3 = _createForOfIteratorHelper(targetObj);
            _context6.prev = 4;
            _iterator3.s();
          case 6:
            if ((_step3 = _iterator3.n()).done) {
              _context6.next = 12;
              break;
            }
            arrayItem = _step3.value;
            _context6.next = 10;
            return _deepChangesObjectKey(arrayItem, sourceObj, matches);
          case 10:
            _context6.next = 6;
            break;
          case 12:
            _context6.next = 17;
            break;
          case 14:
            _context6.prev = 14;
            _context6.t0 = _context6["catch"](4);
            _iterator3.e(_context6.t0);
          case 17:
            _context6.prev = 17;
            _iterator3.f();
            return _context6.finish(17);
          case 20:
            _context6.next = 30;
            break;
          case 22:
            if (!(_typeof(targetObj) == "object")) {
              _context6.next = 30;
              break;
            }
            _loop = /*#__PURE__*/_regeneratorRuntime().mark(function _loop() {
              var key, changesVal, getTargetValue;
              return _regeneratorRuntime().wrap(function _loop$(_context5) {
                while (1) switch (_context5.prev = _context5.next) {
                  case 0:
                    key = _Object$keys3[_i3];
                    if (_typeof(targetObj[key]) !== "object" && !Array.isArray(targetObj[key])) {
                      //Find objectKey with the same ObjectKey from ClientResourceExtend to execute changes
                      changesVal = sourceObj.find(function (x) {
                        return x.clientResourceExtend.key === key;
                      });
                      if (changesVal) {
                        if (changesVal.clientResourceExtend.type.toLowerCase() === "changes") {
                          if (Array.isArray(changesVal.clientResourceExtend.value)) {
                            getTargetValue = changesVal.clientResourceExtend.value.find(function (x) {
                              return x.key === targetObj[key];
                            });
                            if (getTargetValue) {
                              targetObj[key] = getTargetValue.value;
                            }
                          } else {
                            targetObj[key] = changesVal.clientResourceExtend.value;
                          }
                        }
                      }
                      matches = targetObj;
                    }
                    _context5.next = 4;
                    return _deepChangesObjectKey(targetObj[key], sourceObj, matches);
                  case 4:
                  case "end":
                    return _context5.stop();
                }
              }, _loop);
            });
            _i3 = 0, _Object$keys3 = Object.keys(targetObj);
          case 25:
            if (!(_i3 < _Object$keys3.length)) {
              _context6.next = 30;
              break;
            }
            return _context6.delegateYield(_loop(), "t1", 27);
          case 27:
            _i3++;
            _context6.next = 25;
            break;
          case 30:
            return _context6.abrupt("return", matches);
          case 31:
          case "end":
            return _context6.stop();
        }
      }, _callee5, null, [[4, 14, 17, 20]]);
    }));
    return function deepChangesObjectKey(_x6, _x7) {
      return _ref5.apply(this, arguments);
    };
  }();
  var getObjectWithChanges = /*#__PURE__*/function () {
    var _ref6 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee6(requestBody, clResource) {
      var newReqBody;
      return _regeneratorRuntime().wrap(function _callee6$(_context7) {
        while (1) switch (_context7.prev = _context7.next) {
          case 0:
            _context7.next = 2;
            return _deepChangesObjectKey(requestBody, clResource);
          case 2:
            newReqBody = _context7.sent;
            /**
             * @desc add new object from extend
             * */
            clResource.map(function (el) {
              if (el.clientResourceExtend.type.toLowerCase() === "new") {
                if (Array.isArray(el.clientResourceExtend.value)) {
                  var resObj;
                  el.clientResourceExtend.value.map(function (elCL) {
                    var objKey = elCL.key;
                    resObj = _objectSpread(_objectSpread({}, resObj), {}, _defineProperty({}, objKey, elCL.value));
                  });
                  el.clientResourceExtend.value = resObj;
                }
                newReqBody[el.clientResourceExtend.key] = el.clientResourceExtend.value;
              }
            });
            return _context7.abrupt("return", newReqBody);
          case 5:
          case "end":
            return _context7.stop();
        }
      }, _callee6);
    }));
    return function getObjectWithChanges(_x8, _x9) {
      return _ref6.apply(this, arguments);
    };
  }();
  /** removeSpecialCase */
  var removeSpecialCase = function removeSpecialCase(value) {
    return value.replace(/[^\w\s]/gi, "");
  };
  function getDateTimeNowFormattedIndonesianDateTime() {
    var now = new Date();

    // Define options for formatting
    var options = {
      year: "numeric",
      month: "2-digit",
      day: "2-digit",
      hour: "2-digit",
      minute: "2-digit",
      second: "2-digit",
      timeZone: "Asia/Jakarta"
    };

    // Format the date and time using the options
    var formattedDateTime = now.toLocaleString("en-ID", options);
    return formattedDateTime;
  }
  var getMonthDateRange = function getMonthDateRange(year, month) {
    // Create the start date at the beginning of the month
    var startDate = new Date(Date.UTC(year, month - 1, 1));
    // Create the end date at the last day of the month
    var endDate = new Date(Date.UTC(year, month, 0));

    // Format the dates to YYYY-MM-DD
    var formatDate = function formatDate(date) {
      return date.toISOString().split("T")[0];
    };
    return {
      startDate: formatDate(startDate),
      endDate: formatDate(endDate)
    };
  };
  var getFormatTime = function getFormatTime(ms) {
    var totalSeconds = Math.floor(ms / 1000);
    var hours = Math.floor(totalSeconds / 3600);
    var minutes = Math.floor(totalSeconds % 3600 / 60);
    var seconds = totalSeconds % 60;
    return [hours.toString().padStart(2, "0"), minutes.toString().padStart(2, "0"), seconds.toString().padStart(2, "0")].join(":");
  };
  var dateTimeFormatUTC = function dateTimeFormatUTC(dateTime) {
    // Calculate the time difference in hours (e.g., for UTC+7)
    var offsetInHours = 7; // Replace with your desired timezone offset

    // Adjust the time by the offset (convert to milliseconds)
    var adjustedTime = new Date(dateTime.getTime() + offsetInHours * 60 * 60 * 1000);

    // Format the adjusted time as ISO string (similar to your format)
    var formattedTime = adjustedTime.toISOString().replace("Z", "") + "Z"; // Keep the 'Z' at the end
    return formattedTime;
  };
  return {
    getPagination: getPagination,
    getPaginationNew: getPaginationNew,
    getPagingData: getPagingData,
    getUniqueRegistrationToken: getUniqueRegistrationToken,
    arrayToObjectAll: _arrayToObjectAll,
    findNestedErrorHttp: _findNestedErrorHttp,
    getObjectWithChanges: getObjectWithChanges,
    replacingValue: replacingValue,
    isTokenExpired: isTokenExpired,
    isSubPathOf: isSubPathOf,
    removeSpecialCase: removeSpecialCase,
    findAndMoveToLast: findAndMoveToLast,
    getDateTimeNowFormattedIndonesianDateTime: getDateTimeNowFormattedIndonesianDateTime,
    getMonthDateRange: getMonthDateRange,
    getFormatTime: getFormatTime,
    dateTimeFormatUTC: dateTimeFormatUTC
  };
}
var migrationField = exports.migrationField = function migrationField(Sequelize) {
  return {
    createdBy: {
      allowNull: true,
      type: Sequelize.STRING
    },
    updateBy: {
      allowNull: true,
      type: Sequelize.STRING
    },
    deletedAt: {
      allowNull: true,
      type: Sequelize.DATE,
      defaultValue: null
    },
    createdAt: {
      allowNull: false,
      type: Sequelize.DATE,
      defaultValue: Sequelize.NOW
    },
    updatedAt: {
      allowNull: true,
      type: Sequelize.DATE,
      defaultValue: null
    }
  };
};