"use strict";

require("core-js/modules/es.object.define-property.js");
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.error = error;
exports.findStatusHttpByCode = void 0;
exports.result = result;
exports.statusHttpCode = void 0;
exports.success = success;
exports.validation = validation;
require("core-js/modules/es.array.find.js");
require("core-js/modules/es.array.map.js");
require("core-js/modules/es.object.keys.js");
require("core-js/modules/es.object.to-string.js");
/**
 * @desc    Send any success response
 *
 * @param   {string} message
 * @param   {object | array} results
 * @param   {number} statusCode
 */
function success(results, statusCode) {
  var message = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : "";
  var isSuccess = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;
  return {
    isSuccess: isSuccess,
    message: message,
    error: false,
    code: statusCode,
    results: results
  };
}

/**
 * @desc    Send any error response
 *
 * @param   {string} message
 * @param   {number} statusCode
 */
function error(message, statusCode, data) {
  var isSuccess = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;
  // List of common HTTP request code
  var codes = [200, 201, 400, 401, 404, 403, 422, 405, 500];

  // Get matched code
  // const findCode = codes.find((code) => code == statusCode);
  //
  // if (!findCode) statusCode = 500;
  // else statusCode = findCode;

  return {
    message: message,
    code: statusCode,
    error: true,
    results: data,
    isSuccess: isSuccess
  };
}

/**
 * @desc    Send any validation response
 *
 * @param   {object | array} errors
 */
function validation(errors) {
  return {
    message: "Validation errors",
    error: true,
    code: 422,
    errors: errors
  };
}
function result(_ref) {
  var _ref$data = _ref.data,
    data = _ref$data === void 0 ? null : _ref$data,
    _ref$message = _ref.message,
    message = _ref$message === void 0 ? null : _ref$message,
    _ref$statusCode = _ref.statusCode,
    statusCode = _ref$statusCode === void 0 ? statusHttpCode.ok200 : _ref$statusCode,
    _ref$isSuccess = _ref.isSuccess,
    isSuccess = _ref$isSuccess === void 0 ? false : _ref$isSuccess;
  var getCode = {};
  Object.keys(statusHttpCode).map(function (el) {
    if (statusHttpCode[el].code === statusCode.code) {
      getCode = statusHttpCode[el];
    }
  });
  var resultData;
  if (getCode.code > 400) {
    resultData = error(message ? message : getCode.message, getCode.code, data, isSuccess);
  } else {
    resultData = success(data, getCode.code, message ? message : getCode.message, isSuccess);
  }
  return resultData;
}

// List of common HTTP request code
var statusHttpCode = exports.statusHttpCode = {
  ok200: {
    code: 200,
    message: "OK"
  },
  created201: {
    code: 201,
    message: "Created"
  },
  accepted202: {
    code: 202,
    message: "Accepted"
  },
  nonAuthoritativeInformation203: {
    code: 203,
    message: "Non-Authoritative Information"
  },
  noContent204: {
    code: 204,
    message: "No Content"
  },
  resetContent205: {
    code: 205,
    message: "Reset Content"
  },
  partialContent206: {
    code: 206,
    message: "Partial Content"
  },
  multiStatus207: {
    code: 207,
    message: "Multi-Status"
  },
  alreadyReported208: {
    code: 208,
    message: "Already Reported"
  },
  imUsed226: {
    code: 226,
    message: "IM Used"
  },
  multipleChoices300: {
    code: 300,
    message: "Multiple Choices"
  },
  movedPermanently301: {
    code: 301,
    message: "Moved Permanently"
  },
  found302: {
    code: 302,
    message: "Found"
  },
  seeOther303: {
    code: 303,
    message: "See Other"
  },
  notModified304: {
    code: 304,
    message: "Not Modified"
  },
  useProxy305: {
    code: 305,
    message: "Use Proxy"
  },
  switchProxy306: {
    code: 306,
    message: "Switch Proxy"
  },
  temporaryRedirect307: {
    code: 307,
    message: "Temporary Redirect"
  },
  permanentRedirect308: {
    code: 308,
    message: "Permanent Redirect"
  },
  badRequest400: {
    code: 400,
    message: "Bad Request"
  },
  unauthorized401: {
    code: 401,
    message: "Unauthorized"
  },
  paymentRequired402: {
    code: 402,
    message: "Payment Required"
  },
  forbidden403: {
    code: 403,
    message: "Forbidden"
  },
  notFound404: {
    code: 404,
    message: "Not Found"
  },
  methodNotAllowed405: {
    code: 405,
    message: "Method Not Allowed"
  },
  notAcceptable406: {
    code: 406,
    message: "Not Acceptable"
  },
  proxyAuthenticationRequired407: {
    code: 407,
    message: "Proxy Authentication Required"
  },
  requestTimeout408: {
    code: 408,
    message: "Request Timeout"
  },
  conflict409: {
    code: 409,
    message: "Conflict"
  },
  gone410: {
    code: 410,
    message: "Gone"
  },
  lengthRequired411: {
    code: 411,
    message: "Length Required"
  },
  preconditionFailed412: {
    code: 412,
    message: "Precondition Failed"
  },
  payloadTooLarge413: {
    code: 413,
    message: "Payload Too Large"
  },
  uriTooLong414: {
    code: 414,
    message: "URI Too Long"
  },
  unsupportedMediaType415: {
    code: 415,
    message: "Unsupported Media Type"
  },
  rangeNotSatisfiable416: {
    code: 416,
    message: "Range Not Satisfiable"
  },
  expectationFailed417: {
    code: 417,
    message: "Expectation Failed"
  },
  imATeapot418: {
    code: 418,
    message: "I'm a teapot"
  },
  misdirectedRequest421: {
    code: 421,
    message: "Misdirected Request"
  },
  unprocessableEntity422: {
    code: 422,
    message: "Unprocessable Entity"
  },
  locked423: {
    code: 423,
    message: "Locked"
  },
  failedDependency424: {
    code: 424,
    message: "Failed Dependency"
  },
  tooEarly425: {
    code: 425,
    message: "Too Early"
  },
  upgradeRequired426: {
    code: 426,
    message: "Upgrade Required"
  },
  preconditionRequired428: {
    code: 428,
    message: "Precondition Required"
  },
  tooManyRequests429: {
    code: 429,
    message: "Too Many Requests"
  },
  requestHeaderFieldsTooLarge431: {
    code: 431,
    message: "Request Header Fields Too Large"
  },
  unavailableForLegalReasons451: {
    code: 451,
    message: "Unavailable For Legal Reasons"
  },
  internalServerError500: {
    code: 500,
    message: "Internal Server Error"
  },
  notImplemented501: {
    code: 501,
    message: "Not Implemented"
  },
  badGateway502: {
    code: 502,
    message: "Bad Gateway"
  },
  serviceUnavailable503: {
    code: 503,
    message: "Service Unavailable"
  },
  gatewayTimeout504: {
    code: 504,
    message: "Gateway Timeout"
  },
  httpVersionNotSupported505: {
    code: 505,
    message: "HTTP Version Not Supported"
  },
  variantAlsoNegotiates506: {
    code: 506,
    message: "Variant Also Negotiates"
  },
  insufficientStorage507: {
    code: 507,
    message: "Insufficient Storage"
  },
  loopDetected508: {
    code: 508,
    message: "Loop Detected"
  },
  notExtended510: {
    code: 510,
    message: "Not Extended"
  },
  networkAuthenticationRequired511: {
    code: 511,
    message: "Network Authentication Required"
  }
};

// export const getStatusHttpByCode = (code) => {
//     for (let httpCode in statusHttpCode) {
//         if (statusHttpCode[httpCode].code === code) {
//             return statusHttpCode[httpCode];
//         }
//     }
// };

var findStatusHttpByCode = exports.findStatusHttpByCode = function findStatusHttpByCode(code) {
  try {
    var key = Object.keys(statusHttpCode).find(function (key) {
      return statusHttpCode[key].code === code;
    });
    var value = statusHttpCode[key];
    return {
      keyStatusHttpCode: key,
      code: value.code,
      message: value.message
    };
  } catch (error) {
    throw new Error(message = "findStatusHttpByCode not found");
  }
};