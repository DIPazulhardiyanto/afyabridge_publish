"use strict";

require("core-js/modules/es.object.define-property.js");
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ApiAddress_MonitSatuSehat = void 0;
var base_url_satusehat_monit = "https://satusehat-dataviz.kemkes.go.id/api/trpc/implementasiSatusehatDashboard";
var ApiAddress_MonitSatuSehat = exports.ApiAddress_MonitSatuSehat = {
  path_province: base_url_satusehat_monit + ".getFilterProvince",
  path_city: base_url_satusehat_monit + ".getFilterCity",
  path_jenis_sarana: base_url_satusehat_monit + ".getFilterFacility",
  path_status: base_url_satusehat_monit + ".getFilterStatus",
  path_kepemilikan: base_url_satusehat_monit + ".getFilterOwnership",
  path_fasyankes: base_url_satusehat_monit + ".getFilterFacilityName",
  path_table_tahapan_fasyankes: base_url_satusehat_monit + ".getTabelTahapanFasyankes"
};