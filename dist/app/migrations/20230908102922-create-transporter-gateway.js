'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('transporterGateway', {
      id: {
        allowNull: false,
        autoIncrement: false,
        primaryKey: true,
        type: Sequelize.STRING,
        unique: true,
      },
      requestId: {
        type: Sequelize.STRING,
        references: {
          model: "transporter",
          key: "id"
        },
        onDelete: 'cascade',
        onUpdate: 'cascade'
      },
      createdBy: {
        type: Sequelize.STRING
      },
      updateBy: {
        type: Sequelize.STRING
      },
      deletedBy: {
        type: Sequelize.STRING
      },
      createdAt: {
        type: Sequelize.DATE
      },
      updatedAt: {
        type: Sequelize.DATE
      },
      deletedAt: {
        type: Sequelize.DATE
      },
      dimensions: {
        type: Sequelize.TEXT
      },
      handleBounds: {
        type: Sequelize.TEXT
      },
      computedPosition: {
        type: Sequelize.TEXT
      },
      selected: {
        type: Sequelize.BOOLEAN
      },
      dragging: {
        type: Sequelize.BOOLEAN
      },
      resizing: {
        type: Sequelize.BOOLEAN
      },
      initialized: {
        type: Sequelize.BOOLEAN
      },
      label: {
        type: Sequelize.STRING
      },
      position: {
        type: Sequelize.TEXT
      },
      status: {
        type: Sequelize.STRING
      },
      sourceHandle: {
        type: Sequelize.STRING
      },
      targetHandle: {
        type: Sequelize.STRING
      },
      type: {
        type: Sequelize.STRING
      },
      source: {
        type: Sequelize.STRING
      },
      target: {
        type: Sequelize.STRING
      },
      updatable: {
        type: Sequelize.BOOLEAN
      },
      data: {
        type: Sequelize.TEXT
      },
      events: {
        type: Sequelize.TEXT
      },
      animated: {
        type: Sequelize.BOOLEAN
      },
      style: {
        type: Sequelize.TEXT
      },
      sourceX: {
        type: Sequelize.FLOAT
      },
      sourceY: {
        type: Sequelize.FLOAT
      },
      targetX: {
        type: Sequelize.FLOAT
      },
      targetY: {
        type: Sequelize.FLOAT
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('transporterGateway');
  }
};
