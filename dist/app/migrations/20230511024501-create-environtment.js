'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
    async up(queryInterface, Sequelize) {
        /**
         * Add altering commands here.
         *
         * Example:
         * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
         */
        await queryInterface.createTable('environtment', {
            id: {
                allowNull: false,
                autoIncrement: false,
                primaryKey: true,
                type: Sequelize.STRING,
                unique: true,
            },
            createdBy: {
                allowNull: false,
                type: Sequelize.STRING,
                defaultValue: "0", //None
            },
            updateBy: {
                allowNull: true,
                type: Sequelize.STRING,
            },
            deletedBy: {
                allowNull: true,
                type: Sequelize.STRING,
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
            updatedAt: {
                allowNull: true,
                type: Sequelize.DATE,
            },
            deletedAt: {
                allowNull: true,
                type: Sequelize.DATE,
            },
            status: {
                allowNull: false,
                type: Sequelize.STRING,
                defaultValue: "0", //None
            },
            envName: {
                allowNull: false,
                type: Sequelize.STRING
            },
            variable: {
                allowNull: true,
                type: Sequelize.TEXT
            },
            header: {
                allowNull: true,
                type: Sequelize.TEXT
            },
            authentication: {
                allowNull: true,
                type: Sequelize.TEXT
            },
            isLogin: {
                allowNull: false,
                type: Sequelize.BOOLEAN,
                defaultValue: false
            },
        });
    },

    async down(queryInterface, Sequelize) {
        /**
         * Add reverting commands here.
         *
         * Example:
         */
        await queryInterface.dropTable('environtment');
    }
};
