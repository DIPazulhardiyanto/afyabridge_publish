'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    await queryInterface.createTable('clientResource', {
      id: {
        allowNull: false,
        autoIncrement: false,
        primaryKey: true,
        type: Sequelize.STRING,
        unique: true,
      },
      idTemplateResource: {
        allowNull: true,
        type: Sequelize.STRING,
        references: {
          model: "templateResource",
          key: "id"
        },
        onDelete: 'cascade',
        onUpdate: 'cascade'
      },
      createdBy: {
        allowNull: false,
        type: Sequelize.STRING,
        defaultValue: "0", //None
      },
      updateBy: {
        allowNull: true,
        type: Sequelize.STRING,
      },
      deletedBy: {
        allowNull: true,
        type: Sequelize.STRING,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: true,
        type: Sequelize.DATE,
      },
      deletedAt: {
        allowNull: true,
        type: Sequelize.DATE,
      },
      status: {
        allowNull: false,
        type: Sequelize.STRING,
        defaultValue: "0", //None
      },
      integration: {
        allowNull: false,
        type: Sequelize.STRING,
        defaultValue: "", //None
      },
      groupName: {
        allowNull: false,
        type: Sequelize.STRING,
        defaultValue: "GroupName Required", //None
      },
      resourceName: {
        allowNull: false,
        type: Sequelize.STRING,
        defaultValue: "ResourceName Required", //None
      },
      version: {
        allowNull: false,
        type: Sequelize.INTEGER,
        defaultValue: 1, //None
      },
      params: {
        allowNull: true,
        type: Sequelize.TEXT,
        defaultValue: "{}", //None
      },
      bodyRequest: {
        allowNull: true,
        type: Sequelize.TEXT,
        defaultValue: "{}", //None
      },
      mappingTemplate: {
        allowNull: true,
        type: Sequelize.TEXT,
        defaultValue: "{}", //None
      },
    });
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('clientResource');
     */
    await queryInterface.dropTable('clientResource');
  }
};
