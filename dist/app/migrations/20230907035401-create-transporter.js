'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('transporter', {
      id: {
        allowNull: false,
        autoIncrement: false,
        primaryKey: true,
        type: Sequelize.STRING,
        unique: true,
      },
      createdBy: {
        allowNull: false,
        type: Sequelize.STRING,
        defaultValue: "0", //None
      },
      updateBy: {
        allowNull: true,
        type: Sequelize.STRING,
      },
      deletedBy: {
        allowNull: true,
        type: Sequelize.STRING,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: true,
        type: Sequelize.DATE,
      },
      deletedAt: {
        allowNull: true,
        type: Sequelize.DATE,
      },
      environment: {
        allowNull: false,
        type: Sequelize.STRING,
        defaultValue: "development", //None
      },
      status: {
        allowNull: false,
        type: Sequelize.STRING,
        defaultValue: "0", //None
      },
      groupName: {
        allowNull: false,
        type: Sequelize.STRING,
        defaultValue: "", //None
      },
      name: {
        allowNull: false,
        type: Sequelize.STRING,
        defaultValue: "", //None
      },
      description: {
        allowNull: false,
        type: Sequelize.TEXT,
        defaultValue: "", //None
      },
      version: {
        allowNull: false,
        type: Sequelize.INTEGER,
        defaultValue: 1, //None
      },
      path: {
        allowNull: false,
        type: Sequelize.STRING,
        defaultValue: "", //None
      },
      method: {
        allowNull: false,
        type: Sequelize.STRING,
        defaultValue: "", //None
      },
      params: {
        allowNull: false,
        type: Sequelize.TEXT,
        defaultValue: "{}", //None
      },
      bodyRequest: {
        allowNull: false,
        type: Sequelize.TEXT,
        defaultValue: "{}", //None
      },
      requestExample: {
        allowNull: false,
        type: Sequelize.TEXT,
        defaultValue: "{}", //None
      },
      responseExample: {
        allowNull: false,
        type: Sequelize.TEXT,
        defaultValue: "{}", //None
      },
      request: {
        allowNull: false,
        type: Sequelize.TEXT,
        defaultValue: "{}", //None
      },
      mappingTemplate: {
        allowNull: false,
        type: Sequelize.TEXT,
        defaultValue: "{}", //None
      },
      responseTrigger: {
        allowNull: false,
        type: Sequelize.TEXT,
        defaultValue: "{}", //None
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('transporters');
  }
};
