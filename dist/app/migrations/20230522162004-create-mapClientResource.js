'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    await queryInterface.createTable('mapClientResource', {
      id: {
        allowNull: false,
        autoIncrement: false,
        primaryKey: true,
        type: Sequelize.STRING,
        unique: true,
      },
      idEndpointIntegration: {
        type: Sequelize.STRING,
        references: {
          model: "endpointResourceIntegration",
          key: "id"
        },
        onDelete: 'cascade',
        onUpdate: 'cascade'
      },
      idClientResource: {
        type: Sequelize.STRING,
        references: {
          model: "clientResource",
          key: "id"
        },
        onDelete: 'cascade',
        onUpdate: 'cascade'
      },
      idClientResourceExtend: {
        type: Sequelize.STRING,
        references: {
          model: "clientResourceExtends",
          key: "id"
        },
        onDelete: 'cascade',
        onUpdate: 'cascade'
      },
      createdBy: {
        allowNull: false,
        type: Sequelize.STRING,
        defaultValue: "0", //None
      },
      updateBy: {
        allowNull: true,
        type: Sequelize.STRING,
      },
      deletedBy: {
        allowNull: true,
        type: Sequelize.STRING,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: true,
        type: Sequelize.DATE,
      },
      deletedAt: {
        allowNull: true,
        type: Sequelize.DATE,
      },
      status: {
        allowNull: false,
        type: Sequelize.STRING,
        defaultValue: "0", //None
      },
      resourceType: {
        allowNull: true,
        type: Sequelize.STRING,
        defaultValue: "request", //None
      }
    });
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    await queryInterface.dropTable('mapClientResource');
  }
};
