'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    await queryInterface.createTable('integrationResource', {
      id: {
        allowNull: false,
        autoIncrement: false,
        primaryKey: true,
        type: Sequelize.STRING,
        unique: true,
      },
      idClientResource: {
        type: Sequelize.STRING,
        references: {
          model: "clientResource",
          key: "id"
        },
        onDelete: 'cascade',
        onUpdate: 'cascade'
      },
      idEnv: {
        type: Sequelize.STRING,
        references: {
          model: "environtment",
          key: "id"
        },
        onDelete: 'cascade',
        onUpdate: 'cascade'
      },
      createdBy: {
        allowNull: false,
        type: Sequelize.STRING,
        defaultValue: "0", //None
      },
      updateBy: {
        allowNull: true,
        type: Sequelize.STRING,
      },
      deletedBy: {
        allowNull: true,
        type: Sequelize.STRING,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: true,
        type: Sequelize.DATE,
      },
      deletedAt: {
        allowNull: true,
        type: Sequelize.DATE,
      },
      status: {
        allowNull: false,
        type: Sequelize.STRING,
        defaultValue: "0", //None
      },
      url: {
        allowNull: false,
        type: Sequelize.STRING,
        defaultValue: "", //None
      },
      groupName: {
        allowNull: false,
        type: Sequelize.STRING,
        defaultValue: "GroupName Required", //None
      },
      resourceName: {
        allowNull: false,
        type: Sequelize.STRING,
        defaultValue: "ResourceName Required", //None
      },
      version: {
        allowNull: false,
        type: Sequelize.INTEGER,
        defaultValue: 1, //None
      },
      description: {
        allowNull: true,
        type: Sequelize.TEXT,
        defaultValue: "{}", //None
      },
      path: {
        allowNull: true,
        type: Sequelize.STRING,
        defaultValue: "", //None
      },
      method: {
        allowNull: true,
        type: Sequelize.STRING,
        defaultValue: "GET", //None
      },
      requestExample: {
        allowNull: true,
        type: Sequelize.TEXT,
        defaultValue: "{}", //None
      },
      responseExample: {
        allowNull: true,
        type: Sequelize.TEXT,
        defaultValue: "{}", //None
      },
    });
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    await queryInterface.dropTable('integrationResource');
  }
};
