"use strict";

module.exports = {
    up: async (queryInterface, Sequelize) => {
        await queryInterface.createTable("logHistory", {
            id: {
                type: Sequelize.INTEGER,
                autoIncrement: true,
                primaryKey: true,
                allowNull: false,
            },
            idUser: Sequelize.STRING,
            createdBy: {
                allowNull: false,
                type: Sequelize.STRING,
                defaultValue: "0", //None
            },
            updateBy: {
                allowNull: true,
                type: Sequelize.STRING,
            },
            deletedBy: {
                allowNull: true,
                type: Sequelize.STRING,
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
            updatedAt: {
                allowNull: true,
                type: Sequelize.DATE,
            },
            deletedAt: {
                allowNull: true,
                type: Sequelize.DATE,
            },
            path: {
                type: Sequelize.STRING(255),
                allowNull: false,
            },
            method: {
                type: Sequelize.STRING(255),
                allowNull: false,
            },
            resourceName: {
                type: Sequelize.STRING(255),
                allowNull: false,
            },
            groupName: {
                type: Sequelize.STRING(255),
                allowNull: false,
            },
            message: {
                type: Sequelize.STRING(255),
                allowNull: false,
            },
            httpCode: {
                type: Sequelize.STRING(255),
                allowNull: false,
            },
            request: {
                type: Sequelize.TEXT,
                allowNull: false,
            },
            response: {
                type: Sequelize.TEXT,
                allowNull: false,
            },
        });
    },

    down: async (queryInterface, Sequelize) => {
        await queryInterface.dropTable("logHistory");
    },
};
