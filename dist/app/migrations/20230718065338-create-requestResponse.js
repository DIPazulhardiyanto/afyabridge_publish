'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
    async up(queryInterface, Sequelize) {
        /**
         * Add altering commands here.
         *
         * Example:
         * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
         */
        await queryInterface.createTable('requestResponse', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER,
                unique: true,
            },
            createdBy: {
                allowNull: false,
                type: Sequelize.STRING,
                defaultValue: "0", //None
            },
            updateBy: {
                allowNull: true,
                type: Sequelize.STRING,
            },
            deletedBy: {
                allowNull: true,
                type: Sequelize.STRING,
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
            updatedAt: {
                allowNull: true,
                type: Sequelize.DATE,
            },
            deletedAt: {
                allowNull: true,
                type: Sequelize.DATE,
            },
            requestId: {
                allowNull: false,
                autoIncrement: false,
                type: Sequelize.STRING,
            },
            path: {
                allowNull: false,
                autoIncrement: false,
                type: Sequelize.STRING,
            },
            method: {
                allowNull: false,
                autoIncrement: false,
                type: Sequelize.STRING,
            },
            resourceName: {
                allowNull: false,
                autoIncrement: false,
                type: Sequelize.STRING,
            },
            groupName: {
                allowNull: false,
                autoIncrement: false,
                type: Sequelize.STRING,
            },
            idEnv: {
                allowNull: false,
                autoIncrement: false,
                type: Sequelize.STRING,
            },
            version: {
                allowNull: false,
                autoIncrement: false,
                type: Sequelize.STRING,
            },
            message: {
                allowNull: false,
                autoIncrement: false,
                type: Sequelize.STRING,
            },
            httpCode: {
                allowNull: false,
                autoIncrement: false,
                type: Sequelize.STRING(4),
            },
            request: {
                allowNull: false,
                autoIncrement: false,
                type: Sequelize.TEXT,
            },
            response: {
                allowNull: false,
                autoIncrement: false,
                type: Sequelize.TEXT,
            },
            overideRequest: {
                type: Sequelize.TEXT,
            },
            overideResponse: {
                type: Sequelize.TEXT,
            },
            variableName: {
                type: Sequelize.STRING(255),
            },
        });

    },

    async down(queryInterface, Sequelize) {
        /**
         * Add reverting commands here.
         *
         * Example:
         * await queryInterface.dropTable('users');
         */
    }
};
