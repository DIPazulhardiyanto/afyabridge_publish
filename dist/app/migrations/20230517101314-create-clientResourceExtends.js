'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    await queryInterface.createTable('clientResourceExtends', {
      id: {
        allowNull: false,
        autoIncrement: false,
        primaryKey: true,
        type: Sequelize.STRING,
        unique: true,
      },
      createdBy: {
        allowNull: true,
        type: Sequelize.STRING,
      },
      updateBy: {
        allowNull: true,
        type: Sequelize.STRING,
      },
      deletedBy: {
        allowNull: true,
        type: Sequelize.STRING,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: true,
        type: Sequelize.DATE,
        defaultValue: null
      },
      deletedAt: {
        allowNull: true,
        type: Sequelize.DATE,
        defaultValue: null
      },
      status: {
        allowNull: false,
        type: Sequelize.STRING,
        defaultValue: "0", //None
      },
      resourceName: {
        allowNull: false,
        type: Sequelize.STRING,
        defaultValue: "0", //None
      },
      type: {
        allowNull: false,
        type: Sequelize.STRING,
        defaultValue: "", //None
      },
      key: {
        allowNull: false,
        type: Sequelize.STRING,
        defaultValue: "0", //None
      },
      value: {
        allowNull: false,
        type: Sequelize.TEXT,
        defaultValue: "{}", //None
      },
      version: {
        allowNull: false,
        type: Sequelize.INTEGER,
        defaultValue: 1, //None
      },
    });
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('clientResourceExtends');
     */
    await queryInterface.dropTable('clientResourceExtends');
  }
};
