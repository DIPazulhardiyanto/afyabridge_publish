"use strict";

const { migrationField } = require("../libs/utils/helpers");

/** @type {import('sequelize-cli').Migration} */
module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.createTable("cronJobs", {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true,
            },
            idUser: {
                allowNull: false,
                autoIncrement: false,
                type: Sequelize.STRING,
            },
            jobName: {
                type: Sequelize.STRING(255),
                allowNull: false,
            },
            startTime: {
                type: Sequelize.STRING(6),
                allowNull: false,
            },
            endTime: {
                type: Sequelize.STRING(6),
                allowNull: true,
            },
            daysOfWeek: {
                type: Sequelize.STRING(100),
                allowNull: false,
            },
            isActive: {
                type: Sequelize.BOOLEAN,
                defaultValue: true,
            },
            ...migrationField(Sequelize),
        });
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.dropTable("cronJobs");
    },
};
