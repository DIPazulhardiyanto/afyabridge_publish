'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.createTable('environmentAccess', {
            id: {
                allowNull: false,
                primaryKey: true,
                unique: true,
                type: Sequelize.STRING
            },
            idEnv: {
                type: Sequelize.STRING,
                references: {
                    model: "environtment",
                    key: "id"
                },
                onDelete: 'cascade',
                onUpdate: 'cascade'
            },
            idUser: {
                type: Sequelize.STRING
            },
            createdBy: {
                type: Sequelize.STRING
            },
            updateBy: {
                type: Sequelize.STRING
            },
            deletedAt: {
                allowNull: true,
                type: Sequelize.DATE
            },
            status: {
                type: Sequelize.BOOLEAN
            },
            variable: {
                type: Sequelize.TEXT
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: true,
                type: Sequelize.DATE
            }
        });
    },
    async down(queryInterface, Sequelize) {
        await queryInterface.dropTable('environmentAccess');
    }
};
