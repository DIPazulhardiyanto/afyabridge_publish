'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    await queryInterface.createTable('endpointResourceIntegration', {
      id: {
        allowNull: false,
        autoIncrement: false,
        primaryKey: true,
        type: Sequelize.STRING,
        unique: true,
      },
      idEnv: {
        type: Sequelize.STRING,
        references: {
          model: "environtment",
          key: "id"
        },
        onDelete: 'cascade',
        onUpdate: 'cascade'
      },
      createdBy: {
        allowNull: false,
        type: Sequelize.STRING,
        defaultValue: "0", //None
      },
      updateBy: {
        allowNull: true,
        type: Sequelize.STRING,
      },
      deletedBy: {
        allowNull: true,
        type: Sequelize.STRING,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: true,
        type: Sequelize.DATE,
      },
      deletedAt: {
        allowNull: true,
        type: Sequelize.DATE,
      },
      status: {
        allowNull: false,
        type: Sequelize.STRING,
        defaultValue: "0", //None
      },
      groupName: {
        allowNull: false,
        type: Sequelize.STRING,
        defaultValue: "", //None
      },
      name: {
        allowNull: false,
        type: Sequelize.STRING,
        defaultValue: "", //None
      },
      description: {
        allowNull: false,
        type: Sequelize.TEXT,
        defaultValue: "", //None
      },
      version: {
        allowNull: false,
        type: Sequelize.INTEGER,
        defaultValue: 1, //None
      },
      path: {
        allowNull: false,
        type: Sequelize.STRING,
        defaultValue: "", //None
      },
      method: {
        allowNull: false,
        type: Sequelize.STRING,
        defaultValue: "", //None
      },
      params: {
        allowNull: false,
        type: Sequelize.TEXT,
        defaultValue: "{}", //None
      },
      bodyRequest: {
        allowNull: false,
        type: Sequelize.TEXT,
        defaultValue: "{}", //None
      },
      requestExample: {
        allowNull: false,
        type: Sequelize.TEXT,
        defaultValue: "{}", //None
      },
      responseExample: {
        allowNull: false,
        type: Sequelize.TEXT,
        defaultValue: "{}", //None
      },
      request: {
        allowNull: false,
        type: Sequelize.TEXT,
        defaultValue: "{}", //None
      },
      mappingTemplate: {
        allowNull: false,
        type: Sequelize.TEXT,
        defaultValue: "{}", //None
      },
      responseTrigger: {
        allowNull: false,
        type: Sequelize.TEXT,
        defaultValue: "{}", //None
      },
    });
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};
