'use strict';

function _typeof(o) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) { return typeof o; } : function (o) { return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o; }, _typeof(o); }
require("core-js/modules/es.symbol.js");
require("core-js/modules/es.symbol.description.js");
require("core-js/modules/es.symbol.iterator.js");
require("core-js/modules/es.symbol.to-primitive.js");
require("core-js/modules/es.array.iterator.js");
require("core-js/modules/es.date.to-primitive.js");
require("core-js/modules/es.function.bind.js");
require("core-js/modules/es.number.constructor.js");
require("core-js/modules/es.object.create.js");
require("core-js/modules/es.object.define-property.js");
require("core-js/modules/es.object.get-prototype-of.js");
require("core-js/modules/es.object.set-prototype-of.js");
require("core-js/modules/es.object.to-string.js");
require("core-js/modules/es.reflect.construct.js");
require("core-js/modules/es.string.iterator.js");
require("core-js/modules/web.dom-collections.iterator.js");
function _classCallCheck(a, n) { if (!(a instanceof n)) throw new TypeError("Cannot call a class as a function"); }
function _defineProperties(e, r) { for (var t = 0; t < r.length; t++) { var o = r[t]; o.enumerable = o.enumerable || !1, o.configurable = !0, "value" in o && (o.writable = !0), Object.defineProperty(e, _toPropertyKey(o.key), o); } }
function _createClass(e, r, t) { return r && _defineProperties(e.prototype, r), t && _defineProperties(e, t), Object.defineProperty(e, "prototype", { writable: !1 }), e; }
function _toPropertyKey(t) { var i = _toPrimitive(t, "string"); return "symbol" == _typeof(i) ? i : i + ""; }
function _toPrimitive(t, r) { if ("object" != _typeof(t) || !t) return t; var e = t[Symbol.toPrimitive]; if (void 0 !== e) { var i = e.call(t, r || "default"); if ("object" != _typeof(i)) return i; throw new TypeError("@@toPrimitive must return a primitive value."); } return ("string" === r ? String : Number)(t); }
function _callSuper(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
function _possibleConstructorReturn(t, e) { if (e && ("object" == _typeof(e) || "function" == typeof e)) return e; if (void 0 !== e) throw new TypeError("Derived constructors may only return object or undefined"); return _assertThisInitialized(t); }
function _assertThisInitialized(e) { if (void 0 === e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); return e; }
function _isNativeReflectConstruct() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct = function _isNativeReflectConstruct() { return !!t; })(); }
function _getPrototypeOf(t) { return _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf.bind() : function (t) { return t.__proto__ || Object.getPrototypeOf(t); }, _getPrototypeOf(t); }
function _inherits(t, e) { if ("function" != typeof e && null !== e) throw new TypeError("Super expression must either be null or a function"); t.prototype = Object.create(e && e.prototype, { constructor: { value: t, writable: !0, configurable: !0 } }), Object.defineProperty(t, "prototype", { writable: !1 }), e && _setPrototypeOf(t, e); }
function _setPrototypeOf(t, e) { return _setPrototypeOf = Object.setPrototypeOf ? Object.setPrototypeOf.bind() : function (t, e) { return t.__proto__ = e, t; }, _setPrototypeOf(t, e); }
var _require = require('sequelize'),
  Model = _require.Model;
module.exports = function (sequelize, DataTypes) {
  var transporterGateway = /*#__PURE__*/function (_Model) {
    function transporterGateway() {
      _classCallCheck(this, transporterGateway);
      return _callSuper(this, transporterGateway, arguments);
    }
    _inherits(transporterGateway, _Model);
    return _createClass(transporterGateway, null, [{
      key: "associate",
      value:
      /**
       * Helper method for defining associations.
       * This method is not a part of Sequelize lifecycle.
       * The `models/index` file will call this method automatically.
       */
      function associate(models) {
        // define association here
        this.belongsTo(models.transporter, {
          foreignKey: "id"
        });
        this.hasOne(models.transporterGateway, {
          foreignKey: "target",
          as: 'gate1'
        });
        this.hasOne(models.transporterGateway, {
          foreignKey: "id",
          as: 'node'
        });
      }
    }]);
  }(Model);
  transporterGateway.init({
    id: {
      allowNull: false,
      autoIncrement: false,
      primaryKey: true,
      type: DataTypes.STRING,
      unique: true
    },
    requestId: {
      type: DataTypes.STRING,
      references: {
        model: "transporter",
        key: "id"
      },
      onDelete: 'cascade',
      onUpdate: 'cascade'
    },
    createdBy: DataTypes.STRING,
    updateBy: DataTypes.STRING,
    deletedBy: DataTypes.STRING,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
    deletedAt: DataTypes.DATE,
    type: DataTypes.STRING,
    dimensions: DataTypes.TEXT,
    handleBounds: DataTypes.TEXT,
    computedPosition: DataTypes.TEXT,
    selected: DataTypes.BOOLEAN,
    dragging: DataTypes.BOOLEAN,
    resizing: DataTypes.BOOLEAN,
    initialized: DataTypes.BOOLEAN,
    label: DataTypes.STRING,
    position: DataTypes.TEXT,
    status: DataTypes.STRING,
    sourceHandle: DataTypes.STRING,
    targetHandle: DataTypes.STRING,
    source: DataTypes.STRING,
    target: {
      type: DataTypes.STRING,
      references: [{
        model: "gate1",
        key: "target"
      }],
      onDelete: 'cascade',
      onUpdate: 'cascade'
    },
    updatable: DataTypes.BOOLEAN,
    data: DataTypes.TEXT,
    events: DataTypes.TEXT,
    animated: DataTypes.BOOLEAN,
    style: DataTypes.TEXT,
    sourceX: DataTypes.FLOAT,
    sourceY: DataTypes.FLOAT,
    targetX: DataTypes.FLOAT,
    targetY: DataTypes.FLOAT
  }, {
    sequelize: sequelize,
    freezeTableName: true,
    //digunakan agar nama table tidak bertambah 's' dibelakang pada database
    paranoid: true,
    // use for soft delete
    modelName: 'transporterGateway'
  });
  return transporterGateway;
};