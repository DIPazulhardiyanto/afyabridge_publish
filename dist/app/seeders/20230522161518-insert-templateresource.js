'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
    async up(queryInterface, Sequelize) {
        /**
         * Add seed commands here.
         *
         * Example:
         * await queryInterface.bulkInsert('People', [{
         *   name: 'John Doe',
         *   isBetaMember: false
         * }], {});
         */
        await queryInterface.bulkInsert('templateResource', [
            {
                "createdAt": "2023-05-17T22:59:20.000Z",
                "updatedAt": "2023-05-17T23:15:22.000Z",
                "deletedAt": null,
                "id": "1684339160276",
                "createdBy": "ADMIN",
                "updateBy": "ADMIN",
                "deletedBy": null,
                "status": "disabled",
                "resourceName": "Organization",
                "description": "Create new organization FHIR Kemkes",
                "version": 1,
                "integration": "KEMENKES",
                "params": "{\"query\":[],\"variable\":[]}",
                "bodyRequest": "{\"SelectType\":\"rawJson\",\"formData\":[],\"formUrlEncoded\":[],\"rawJson\":{\"resourceType\":\"Organization\",\"active\":true,\"identifier\":[{\"use\":\"official\",\"system\":\"http://sys-ids.kemkes.go.id/organization/1000079374\",\"value\":\"Pos Imunisasi LUBUK BATANG\"}],\"type\":[{\"coding\":[{\"system\":\"http://terminology.hl7.org/CodeSystem/organization-type\",\"code\":\"dept\",\"display\":\"Hospital Department\"}]}],\"name\":\"Pos Imunisasi\",\"telecom\":[{\"system\":\"phone\",\"value\":\"+6221-783042654\",\"use\":\"work\"},{\"system\":\"email\",\"value\":\"rs-satusehat@gmail.com\",\"use\":\"work\"},{\"system\":\"url\",\"value\":\"www.rs-satusehat@gmail.com\",\"use\":\"work\"}],\"address\":[{\"use\":\"work\",\"type\":\"both\",\"line\":[\"Jalan Jati Asih\"],\"city\":\"Jakarta\",\"postalCode\":\"55292\",\"country\":\"ID\",\"extension\":[{\"url\":\"https://fhir.kemkes.go.id/r4/StructureDefinition/administrativeCode\",\"extension\":[{\"url\":\"province\",\"valueCode\":\"31\"},{\"url\":\"city\",\"valueCode\":\"3171\"},{\"url\":\"district\",\"valueCode\":\"317101\"},{\"url\":\"village\",\"valueCode\":\"31710101\"}]}]}],\"partOf\":{\"reference\":\"Organization/10000004\"}}}"
            }
        ], {});
    },

    async down(queryInterface, Sequelize) {
        /**
         * Add commands to revert seed here.
         *
         * Example:
         * await queryInterface.bulkDelete('People', null, {});
         */
    }
};
