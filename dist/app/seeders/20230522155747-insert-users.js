'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
    async up(queryInterface, Sequelize) {
        /**
         * Add seed commands here.
         *
         * Example:
         * await queryInterface.bulkInsert('People', [{
         *   name: 'John Doe',
         *   isBetaMember: false
         * }], {});
         */
        await queryInterface.bulkInsert('users', [
            {
                "createdAt": "2023-05-13T09:52:54.000Z",
                "updatedAt": null,
                "deletedAt": null,
                "id": "1683946374351",
                "createdBy": "PUBLIC",
                "updateBy": null,
                "deletedBy": null,
                "status": "0",
                "fullname": "andre stinky",
                "username": "admin1@gmail.com",
                "email": "admin1@gmail.com",
                "password": "$2a$10$0W8TtsEvlYSHzWkcriuE8.zdjAYDOGeh.rXB9ogjiXC7aosVlL6Bm",
                "companyName": "PT Daya Adi",
                "role": 1,
                "token": "pXiFTfkaQkHzCCHZJoWRxnuowjgQhFsESteqoanvkyeFBJGJqmFtaNoCGOMvdFHyoGaqYlPYDsZFlhwuwbLClCJjwgpHWaqPfxCX"
            },
            {
                "createdAt": "2023-05-13T10:00:14.000Z",
                "updatedAt": "2023-05-13T10:40:11.000Z",
                "deletedAt": null,
                "id": "1683946814613",
                "createdBy": "ADMIN",
                "updateBy": "ADMIN",
                "deletedBy": null,
                "status": "0",
                "fullname": "edit fullname",
                "username": "user_E1",
                "email": "user_E1@gmail.com",
                "password": "dev123456",
                "companyName": "companyName",
                "role": 2,
                "token": "cpuEGnlhEsYPNvIAHtGlxNfBlnkMTqagLkZgyySRfHClkpHNuTWDjhWVPOBLvAyHuPqIjIOqXQotCptujYtDqpuSAxdGqLIBDubn"
            }
        ], {});
    },

    async down(queryInterface, Sequelize) {
        /**
         * Add commands to revert seed here.
         *
         * Example:
         * await queryInterface.bulkDelete('People', null, {});
         */
    }
};
