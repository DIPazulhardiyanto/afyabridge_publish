'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
    async up(queryInterface, Sequelize) {
        /**
         * Add seed commands here.
         *
         * Example:
         * await queryInterface.bulkInsert('People', [{
         *   name: 'John Doe',
         *   isBetaMember: false
         * }], {});
         */
        await queryInterface.bulkInsert('environtment', [
            {
                "createdAt": "2023-05-17T11:28:32.000Z",
                "updatedAt": "2023-05-19T23:15:22.000Z",
                "deletedAt": null,
                "id": "1684297712585",
                "createdBy": "ADMIN",
                "updateBy": "ADMIN",
                "deletedBy": null,
                "status": "0",
                "envName": "API_HOM",
                "variable": "[{\"varName\":\"Development\",\"status\":1,\"port\":null,\"code\":[{\"key\":\"auth_url\",\"value\":\"http://192.168.10.208:9093/api\"},{\"key\":\"token\",\"value\":\"\"},{\"key\":\"tokenFromResponseKey\",\"value\":\"tokenKey\"},{\"key\":\"token_type\",\"value\":null}]}]",
                "header": "[{\"key\":\"Authorization\",\"value\":\"token\"}]",
                "authentication": "{\"path\":\"/v1/auth/login\",\"variableCodeKey\":{\"value\":\"auth_url\"},\"method\":\"POST\",\"header\":[{\"key\":\"Content-Type\",\"value\":\"application/json\"}],\"params\":{\"query\":[],\"variable\":[]},\"bodyRequest\":{\"selectType\":\"rawJson\",\"formData\":[],\"formUrlEncoded\":[],\"rawJson\":{\"username\":\"hospital\",\"password\":\"serverh5n\"}}}",
                "isLogin": true
            },
            {
                "createdAt": "2023-05-17T11:24:37.000Z",
                "updatedAt": "2023-05-19T22:20:15.000Z",
                "deletedAt": null,
                "id": "1684297477024",
                "createdBy": "ADMIN",
                "updateBy": "ADMIN",
                "deletedBy": null,
                "status": "0",
                "envName": "SatuSehat",
                "variable": "[{\"varName\":\"Development\",\"status\":1,\"port\":null,\"code\":[{\"key\":\"auth_url\",\"value\":\"https://api-satusehat-dev.dto.kemkes.go.id/oauth2/v1\"},{\"key\":\"base_url\",\"value\":\"https://api-satusehat-dev.dto.kemkes.go.id/fhir-r4/v1\"},{\"key\":\"consent_url\",\"value\":\"https://api-satusehat-dev.dto.kemkes.go.id/consent/v1\"},{\"key\":\"client_id\",\"value\":\"enURVy2FkaAwUAG9xUDrNnDPepfLRYAYGKGW4yz6tLrmXMXb\"},{\"key\":\"client_secret\",\"value\":\"0h7gtFKTkDyGLwsJAGp3wJ8Pez0OdAubBAck6SoSzqAm1WH2hPrOOrpLcGcCrXrL\"},{\"key\":\"token\",\"value\":\"Bearer undefined\"},{\"key\":\"tokenFromResponseKey\",\"value\":\"access_token\"},{\"key\":\"token_type\",\"value\":\"Bearer\"}]}]",
                "header": "[{\"key\":\"Authorization\",\"value\":\"token\"}]",
                "authentication": "{\"path\":\"/accesstoken\",\"variableCodeKey\":{\"value\":\"auth_url\"},\"method\":\"POST\",\"header\":[{\"key\":\"Content-Type\",\"value\":\"application/x-www-form-urlencoded\"}],\"params\":{\"query\":[{\"key\":\"grant_type\",\"value\":\"client_credentials\"}],\"variable\":[]},\"bodyRequest\":{\"selectType\":\"formUrlEncoded\",\"formData\":[],\"formUrlEncoded\":[{\"key\":\"client_id\",\"value\":\"client_id\"},{\"key\":\"client_secret\",\"value\":\"client_secret\"}],\"rawJson\":{}}}",
                "isLogin": true
            }
        ], {});
    },

    async down(queryInterface, Sequelize) {
        /**
         * Add commands to revert seed here.
         *
         * Example:
         * await queryInterface.bulkDelete('People', null, {});
         */
    }
};
