'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
    async up(queryInterface, Sequelize) {
        /**
         * Add seed commands here.
         *
         * Example:
         * await queryInterface.bulkInsert('People', [{
         *   name: 'John Doe',
         *   isBetaMember: false
         * }], {});
         */
        await queryInterface.bulkInsert('clientResource', [
            {
                "createdAt": "2023-05-22T09:34:47.000Z",
                "updatedAt": null,
                "deletedAt": null,
                "id": "1684722887063",
                "idTemplateResource": null,
                "createdBy": "ADMIN",
                "updateBy": null,
                "deletedBy": null,
                "status": "0",
                "integration": "SatuSehat",
                "groupName": "Organization",
                "resourceName": "Organization-By ID",
                "version": 1,
                "params": "{\"query\":[],\"variable\":[{\"key\":\"id\",\"value\":\"\"}]}",
                "bodyRequest": "{\"mode\":\"none\",\"formData\":[],\"formUrlEncoded\":[],\"rawJson\":{}}",
                "mappingTemplate": "{}"
            },
            {
                "createdAt": "2023-05-22T09:33:27.000Z",
                "updatedAt": "2023-05-22T13:40:07.000Z",
                "deletedAt": null,
                "id": "1684722807921",
                "idTemplateResource": "1684339160276",
                "createdBy": "ADMIN",
                "updateBy": "ADMIN",
                "deletedBy": null,
                "status": "0",
                "integration": "SatuSehat",
                "groupName": "Organization",
                "resourceName": "Gender",
                "version": 1,
                "params": "{\"query\":[],\"variable\":[]}",
                "bodyRequest": "{\"mode\":\"raw\",\"formData\":[],\"formUrlEncoded\":[],\"rawJson\":{\"KodeRS\":\"RSUI\",\"NamaRS\":\"AFYA Hospital\",\"NoTelp\":\"(021) 50829292\",\"NoFax\":\"(021) 50829292\",\"Alamat\":\"Kampus UI Depok, Jl. Prof. Dr. Bahder Djohan, Pondok Cina, Beji, Kota Depok, Jawa Barat 16424\",\"EmailAddress\":\"rsui@ui.ac.id\",\"URL\":\"https://rs.ui.ac.id\",\"Yayasan\":\"(021) 50829292\",\"IdKelasRS\":\"Kelas C\",\"IdRegional\":\"Regional 1\",\"IdTipeTarifRS\":\"CS\",\"TipeTarifRS\":\"TARIF RS KELAS C SWASTA\",\"IdKabupaten\":\"160\",\"Kabupaten\":\"Kota Depok \"}}",
                "mappingTemplate": "{\"resourceType\":\"Organization\",\"active\":true,\"identifier\":[{\"information\":{\"use\":\"official\",\"system\":\"http://sys-ids.kemkes.go.id/organization/#KodeRS#\",\"value\":\"#NamaRS#\"}}],\"type\":[{\"coding\":[{\"system\":\"http://terminology.hl7.org/CodeSystem/organization-type\",\"code\":\"dept\",\"display\":\"Hospital Department\"}]}],\"name\":\"#NamaRS#\",\"telecom\":[{\"system\":\"phone\",\"value\":\"#Yayasan#\",\"use\":\"work\"},{\"system\":\"email\",\"value\":\"#EmailAddress#\",\"use\":\"work\"},{\"system\":\"url\",\"value\":\"#URL#\",\"use\":\"work\"}],\"address\":[{\"use\":\"work\",\"type\":\"both\",\"line\":[\"#Alamat#\"],\"city\":\"Jakarta\",\"postalCode\":\"55292\",\"country\":\"ID\",\"extension\":[{\"url\":\"https://fhir.kemkes.go.id/r4/StructureDefinition/administrativeCode\",\"extension\":[{\"url\":\"province\",\"valueCode\":\"31\"},{\"url\":\"city\",\"valueCode\":\"3171\"},{\"url\":\"district\",\"valueCode\":\"317101\"},{\"url\":\"village\",\"valueCode\":\"31710101\"}]}]}],\"partOf\":{\"reference\":\"Organization/10000004\"}}"
            }
        ], {});
    },

    async down(queryInterface, Sequelize) {
        /**
         * Add commands to revert seed here.
         *
         * Example:
         * await queryInterface.bulkDelete('People', null, {});
         */
    }
};
