"use strict";

require("core-js/modules/es.object.define-property.js");
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _express = _interopRequireDefault(require("express"));
var _controllers = _interopRequireDefault(require("./controllers"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
var controller = (0, _controllers["default"])();
var routes = (0, _express["default"])();
var basePath = "/testing";
var url_with_body_urlencoded = basePath + "/body/urlencoded";
var url_with_body_raw = basePath + "/body/raw";
var http_code_testing = basePath + "/httpcode";
routes.route(url_with_body_urlencoded).post(controller.requestBodyWIthUrlEncoded);
routes.route(url_with_body_raw).post(controller.requestBodyWIthRaw);
routes.route(http_code_testing).post(controller.testHttpCode);
var _default = exports["default"] = routes;