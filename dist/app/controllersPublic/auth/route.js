"use strict";

require("core-js/modules/es.object.define-property.js");
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _express = _interopRequireDefault(require("express"));
var _controllers = _interopRequireDefault(require("./controllers"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
var registerPath = "/registeradmin";
var loginPath = "/login";
var refreshToken = "/tokenrefresh";
var getTokenPath = "/gettoken";
var controller = (0, _controllers["default"])();
var routes = (0, _express["default"])();
routes.route(registerPath).post(controller.registerAdmin);
routes.route(loginPath).post(controller.login);
routes.route(refreshToken).post(controller.refreshToken);
routes.route(getTokenPath).post(controller.getToken);
var _default = exports["default"] = routes;