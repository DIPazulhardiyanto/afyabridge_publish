"use strict";

require("core-js/modules/es.object.define-property.js");
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.queryListClientResource = void 0;
require("core-js/modules/es.array.concat.js");
require("core-js/modules/es.array.filter.js");
require("core-js/modules/es.object.to-string.js");
var queryListClientResource = exports.queryListClientResource = function queryListClientResource(_ref) {
  var _ref$desc = _ref.desc,
    desc = _ref$desc === void 0 ? 'DESC' : _ref$desc,
    _ref$filter = _ref.filter,
    filter = _ref$filter === void 0 ? '' : _ref$filter;
  var select = "SELECT [id], [createdBy], [updateBy], [deletedBy], [createdAt], \n    [updatedAt], [deletedAt], [status], [resourceName], [type], [key], [version]";
  var from = "FROM [clientResourceExtends] AS [clientResourceExtends]";
  var where = "WHERE ([clientResourceExtends].[deletedAt] IS NULL)";
  var orderByRowNumber = "[clientResourceExtends].[createdAt] ".concat(desc);

  //todo add where clause
  if (filter.length > 0) {
    where += " AND ( [clientResourceExtends].[resourceName] LIKE '%".concat(filter, "%'\n                    OR [clientResourceExtends].[type] LIKE '%").concat(filter, "%' )");
  }
  return {
    select: select,
    from: from,
    where: where,
    orderByRowNumber: orderByRowNumber
  };
};