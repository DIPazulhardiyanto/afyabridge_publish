"use strict";

require("core-js/modules/es.object.define-property.js");
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.queryListHistory = void 0;
require("core-js/modules/es.array.concat.js");
require("core-js/modules/es.array.filter.js");
require("core-js/modules/es.array.map.js");
require("core-js/modules/es.object.to-string.js");
require("core-js/modules/es.regexp.exec.js");
require("core-js/modules/es.string.replace.js");
var queryListHistory = exports.queryListHistory = function queryListHistory(_ref) {
  var _ref$filter = _ref.filter,
    filter = _ref$filter === void 0 ? '' : _ref$filter,
    _ref$category = _ref.category,
    category = _ref$category === void 0 ? '' : _ref$category,
    _ref$desc = _ref.desc,
    desc = _ref$desc === void 0 ? '' : _ref$desc,
    _ref$method = _ref.method,
    method = _ref$method === void 0 ? '' : _ref$method,
    _ref$createdBy = _ref.createdBy,
    createdBy = _ref$createdBy === void 0 ? '' : _ref$createdBy,
    _ref$startDate = _ref.startDate,
    startDate = _ref$startDate === void 0 ? '' : _ref$startDate,
    _ref$endDate = _ref.endDate,
    endDate = _ref$endDate === void 0 ? '' : _ref$endDate,
    _ref$idEnvAccess = _ref.idEnvAccess,
    idEnvAccess = _ref$idEnvAccess === void 0 ? null : _ref$idEnvAccess;
  var select = "SELECT e.envName, u.username, u.id AS userId, [requestResponse].*";
  var from = "FROM [requestResponse] AS [requestResponse] WITH (NOLOCK)\n                INNER JOIN environmentAccess a ON a.id=[requestResponse].idEnvAccess\n                INNER JOIN environtment e ON e.id=a.idEnv\n                INNER JOIN users u ON u.id=a.idUser";
  var where = "WHERE [requestResponse].[deletedAt] IS NULL";
  var orderByRowNumber = "[requestResponse].[createdAt] ".concat(desc);
  if (idEnvAccess && idEnvAccess.length > 0) {
    where += " AND [requestResponse].[idEnvAccess]='".concat(idEnvAccess, "' ");
  }
  if (filter.length > 0) {
    where += " AND (\n            [requestResponse].[id] LIKE '%".concat(filter, "%'\n            OR\n            [requestResponse].[requestId] LIKE '%").concat(filter, "%'\n            OR\n            [requestResponse].[path] LIKE '%").concat(filter, "%'\n            OR\n            [requestResponse].[resourceName] LIKE '%").concat(filter, "%'\n        )");
  }
  if (category.length > 0) {
    var categoryString = '';
    category.split(',').map(function (el) {
      el = el.replace(/\s/g, '');
      if (categoryString.length === 0) {
        categoryString += "[requestResponse].[groupName] = N'".concat(el, "'");
      } else {
        categoryString += " OR [requestResponse].[groupName] = N'".concat(el, "'");
      }
    });
    where += " AND ( ".concat(categoryString, " )");
  }
  if (method.length > 0) {
    where += " AND [requestResponse].[method] = N'".concat(method, "'");
  }
  if (createdBy.length > 0) {
    where += " AND [requestResponse].[createdBy] = N'".concat(createdBy, "'");
  }
  if (startDate && endDate) {
    where += " AND ([requestResponse].[createdAt] >= N'".concat(startDate, "' AND [requestResponse].[createdAt] <= N'").concat(endDate, "')");
  }
  return {
    select: select,
    from: from,
    where: where,
    orderByRowNumber: orderByRowNumber
  };
};