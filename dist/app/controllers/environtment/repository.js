"use strict";

function _typeof(o) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) { return typeof o; } : function (o) { return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o; }, _typeof(o); }
require("core-js/modules/es.symbol.js");
require("core-js/modules/es.symbol.description.js");
require("core-js/modules/es.symbol.async-iterator.js");
require("core-js/modules/es.symbol.iterator.js");
require("core-js/modules/es.symbol.to-primitive.js");
require("core-js/modules/es.symbol.to-string-tag.js");
require("core-js/modules/es.array.for-each.js");
require("core-js/modules/es.array.iterator.js");
require("core-js/modules/es.array.reverse.js");
require("core-js/modules/es.array.slice.js");
require("core-js/modules/es.date.to-primitive.js");
require("core-js/modules/es.function.name.js");
require("core-js/modules/es.json.to-string-tag.js");
require("core-js/modules/es.math.to-string-tag.js");
require("core-js/modules/es.number.constructor.js");
require("core-js/modules/es.object.create.js");
require("core-js/modules/es.object.define-property.js");
require("core-js/modules/es.object.get-prototype-of.js");
require("core-js/modules/es.object.set-prototype-of.js");
require("core-js/modules/es.string.iterator.js");
require("core-js/modules/web.dom-collections.for-each.js");
require("core-js/modules/web.dom-collections.iterator.js");
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = envRepository;
require("core-js/modules/es.array.filter.js");
require("core-js/modules/es.array.map.js");
require("core-js/modules/es.date.now.js");
require("core-js/modules/es.date.to-json.js");
require("core-js/modules/es.object.keys.js");
require("core-js/modules/es.object.to-string.js");
require("core-js/modules/es.promise.js");
var _models = require("../../models");
var _helpers = _interopRequireDefault(require("../../libs/utils/helpers"));
var _sequelize = require("sequelize");
var _responseApi = require("../../libs/utils/responseApi");
var _query = require("./query");
var _callQuery = require("../../libs/utils/callQuery");
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
function _defineProperty(e, r, t) { return (r = _toPropertyKey(r)) in e ? Object.defineProperty(e, r, { value: t, enumerable: !0, configurable: !0, writable: !0 }) : e[r] = t, e; }
function _toPropertyKey(t) { var i = _toPrimitive(t, "string"); return "symbol" == _typeof(i) ? i : i + ""; }
function _toPrimitive(t, r) { if ("object" != _typeof(t) || !t) return t; var e = t[Symbol.toPrimitive]; if (void 0 !== e) { var i = e.call(t, r || "default"); if ("object" != _typeof(i)) return i; throw new TypeError("@@toPrimitive must return a primitive value."); } return ("string" === r ? String : Number)(t); }
function _regeneratorRuntime() { "use strict"; /*! regenerator-runtime -- Copyright (c) 2014-present, Facebook, Inc. -- license (MIT): https://github.com/facebook/regenerator/blob/main/LICENSE */ _regeneratorRuntime = function _regeneratorRuntime() { return e; }; var t, e = {}, r = Object.prototype, n = r.hasOwnProperty, o = Object.defineProperty || function (t, e, r) { t[e] = r.value; }, i = "function" == typeof Symbol ? Symbol : {}, a = i.iterator || "@@iterator", c = i.asyncIterator || "@@asyncIterator", u = i.toStringTag || "@@toStringTag"; function define(t, e, r) { return Object.defineProperty(t, e, { value: r, enumerable: !0, configurable: !0, writable: !0 }), t[e]; } try { define({}, ""); } catch (t) { define = function define(t, e, r) { return t[e] = r; }; } function wrap(t, e, r, n) { var i = e && e.prototype instanceof Generator ? e : Generator, a = Object.create(i.prototype), c = new Context(n || []); return o(a, "_invoke", { value: makeInvokeMethod(t, r, c) }), a; } function tryCatch(t, e, r) { try { return { type: "normal", arg: t.call(e, r) }; } catch (t) { return { type: "throw", arg: t }; } } e.wrap = wrap; var h = "suspendedStart", l = "suspendedYield", f = "executing", s = "completed", y = {}; function Generator() {} function GeneratorFunction() {} function GeneratorFunctionPrototype() {} var p = {}; define(p, a, function () { return this; }); var d = Object.getPrototypeOf, v = d && d(d(values([]))); v && v !== r && n.call(v, a) && (p = v); var g = GeneratorFunctionPrototype.prototype = Generator.prototype = Object.create(p); function defineIteratorMethods(t) { ["next", "throw", "return"].forEach(function (e) { define(t, e, function (t) { return this._invoke(e, t); }); }); } function AsyncIterator(t, e) { function invoke(r, o, i, a) { var c = tryCatch(t[r], t, o); if ("throw" !== c.type) { var u = c.arg, h = u.value; return h && "object" == _typeof(h) && n.call(h, "__await") ? e.resolve(h.__await).then(function (t) { invoke("next", t, i, a); }, function (t) { invoke("throw", t, i, a); }) : e.resolve(h).then(function (t) { u.value = t, i(u); }, function (t) { return invoke("throw", t, i, a); }); } a(c.arg); } var r; o(this, "_invoke", { value: function value(t, n) { function callInvokeWithMethodAndArg() { return new e(function (e, r) { invoke(t, n, e, r); }); } return r = r ? r.then(callInvokeWithMethodAndArg, callInvokeWithMethodAndArg) : callInvokeWithMethodAndArg(); } }); } function makeInvokeMethod(e, r, n) { var o = h; return function (i, a) { if (o === f) throw Error("Generator is already running"); if (o === s) { if ("throw" === i) throw a; return { value: t, done: !0 }; } for (n.method = i, n.arg = a;;) { var c = n.delegate; if (c) { var u = maybeInvokeDelegate(c, n); if (u) { if (u === y) continue; return u; } } if ("next" === n.method) n.sent = n._sent = n.arg;else if ("throw" === n.method) { if (o === h) throw o = s, n.arg; n.dispatchException(n.arg); } else "return" === n.method && n.abrupt("return", n.arg); o = f; var p = tryCatch(e, r, n); if ("normal" === p.type) { if (o = n.done ? s : l, p.arg === y) continue; return { value: p.arg, done: n.done }; } "throw" === p.type && (o = s, n.method = "throw", n.arg = p.arg); } }; } function maybeInvokeDelegate(e, r) { var n = r.method, o = e.iterator[n]; if (o === t) return r.delegate = null, "throw" === n && e.iterator["return"] && (r.method = "return", r.arg = t, maybeInvokeDelegate(e, r), "throw" === r.method) || "return" !== n && (r.method = "throw", r.arg = new TypeError("The iterator does not provide a '" + n + "' method")), y; var i = tryCatch(o, e.iterator, r.arg); if ("throw" === i.type) return r.method = "throw", r.arg = i.arg, r.delegate = null, y; var a = i.arg; return a ? a.done ? (r[e.resultName] = a.value, r.next = e.nextLoc, "return" !== r.method && (r.method = "next", r.arg = t), r.delegate = null, y) : a : (r.method = "throw", r.arg = new TypeError("iterator result is not an object"), r.delegate = null, y); } function pushTryEntry(t) { var e = { tryLoc: t[0] }; 1 in t && (e.catchLoc = t[1]), 2 in t && (e.finallyLoc = t[2], e.afterLoc = t[3]), this.tryEntries.push(e); } function resetTryEntry(t) { var e = t.completion || {}; e.type = "normal", delete e.arg, t.completion = e; } function Context(t) { this.tryEntries = [{ tryLoc: "root" }], t.forEach(pushTryEntry, this), this.reset(!0); } function values(e) { if (e || "" === e) { var r = e[a]; if (r) return r.call(e); if ("function" == typeof e.next) return e; if (!isNaN(e.length)) { var o = -1, i = function next() { for (; ++o < e.length;) if (n.call(e, o)) return next.value = e[o], next.done = !1, next; return next.value = t, next.done = !0, next; }; return i.next = i; } } throw new TypeError(_typeof(e) + " is not iterable"); } return GeneratorFunction.prototype = GeneratorFunctionPrototype, o(g, "constructor", { value: GeneratorFunctionPrototype, configurable: !0 }), o(GeneratorFunctionPrototype, "constructor", { value: GeneratorFunction, configurable: !0 }), GeneratorFunction.displayName = define(GeneratorFunctionPrototype, u, "GeneratorFunction"), e.isGeneratorFunction = function (t) { var e = "function" == typeof t && t.constructor; return !!e && (e === GeneratorFunction || "GeneratorFunction" === (e.displayName || e.name)); }, e.mark = function (t) { return Object.setPrototypeOf ? Object.setPrototypeOf(t, GeneratorFunctionPrototype) : (t.__proto__ = GeneratorFunctionPrototype, define(t, u, "GeneratorFunction")), t.prototype = Object.create(g), t; }, e.awrap = function (t) { return { __await: t }; }, defineIteratorMethods(AsyncIterator.prototype), define(AsyncIterator.prototype, c, function () { return this; }), e.AsyncIterator = AsyncIterator, e.async = function (t, r, n, o, i) { void 0 === i && (i = Promise); var a = new AsyncIterator(wrap(t, r, n, o), i); return e.isGeneratorFunction(r) ? a : a.next().then(function (t) { return t.done ? t.value : a.next(); }); }, defineIteratorMethods(g), define(g, u, "Generator"), define(g, a, function () { return this; }), define(g, "toString", function () { return "[object Generator]"; }), e.keys = function (t) { var e = Object(t), r = []; for (var n in e) r.push(n); return r.reverse(), function next() { for (; r.length;) { var t = r.pop(); if (t in e) return next.value = t, next.done = !1, next; } return next.done = !0, next; }; }, e.values = values, Context.prototype = { constructor: Context, reset: function reset(e) { if (this.prev = 0, this.next = 0, this.sent = this._sent = t, this.done = !1, this.delegate = null, this.method = "next", this.arg = t, this.tryEntries.forEach(resetTryEntry), !e) for (var r in this) "t" === r.charAt(0) && n.call(this, r) && !isNaN(+r.slice(1)) && (this[r] = t); }, stop: function stop() { this.done = !0; var t = this.tryEntries[0].completion; if ("throw" === t.type) throw t.arg; return this.rval; }, dispatchException: function dispatchException(e) { if (this.done) throw e; var r = this; function handle(n, o) { return a.type = "throw", a.arg = e, r.next = n, o && (r.method = "next", r.arg = t), !!o; } for (var o = this.tryEntries.length - 1; o >= 0; --o) { var i = this.tryEntries[o], a = i.completion; if ("root" === i.tryLoc) return handle("end"); if (i.tryLoc <= this.prev) { var c = n.call(i, "catchLoc"), u = n.call(i, "finallyLoc"); if (c && u) { if (this.prev < i.catchLoc) return handle(i.catchLoc, !0); if (this.prev < i.finallyLoc) return handle(i.finallyLoc); } else if (c) { if (this.prev < i.catchLoc) return handle(i.catchLoc, !0); } else { if (!u) throw Error("try statement without catch or finally"); if (this.prev < i.finallyLoc) return handle(i.finallyLoc); } } } }, abrupt: function abrupt(t, e) { for (var r = this.tryEntries.length - 1; r >= 0; --r) { var o = this.tryEntries[r]; if (o.tryLoc <= this.prev && n.call(o, "finallyLoc") && this.prev < o.finallyLoc) { var i = o; break; } } i && ("break" === t || "continue" === t) && i.tryLoc <= e && e <= i.finallyLoc && (i = null); var a = i ? i.completion : {}; return a.type = t, a.arg = e, i ? (this.method = "next", this.next = i.finallyLoc, y) : this.complete(a); }, complete: function complete(t, e) { if ("throw" === t.type) throw t.arg; return "break" === t.type || "continue" === t.type ? this.next = t.arg : "return" === t.type ? (this.rval = this.arg = t.arg, this.method = "return", this.next = "end") : "normal" === t.type && e && (this.next = e), y; }, finish: function finish(t) { for (var e = this.tryEntries.length - 1; e >= 0; --e) { var r = this.tryEntries[e]; if (r.finallyLoc === t) return this.complete(r.completion, r.afterLoc), resetTryEntry(r), y; } }, "catch": function _catch(t) { for (var e = this.tryEntries.length - 1; e >= 0; --e) { var r = this.tryEntries[e]; if (r.tryLoc === t) { var n = r.completion; if ("throw" === n.type) { var o = n.arg; resetTryEntry(r); } return o; } } throw Error("illegal catch attempt"); }, delegateYield: function delegateYield(e, r, n) { return this.delegate = { iterator: values(e), resultName: r, nextLoc: n }, "next" === this.method && (this.arg = t), y; } }, e; }
function asyncGeneratorStep(n, t, e, r, o, a, c) { try { var i = n[a](c), u = i.value; } catch (n) { return void e(n); } i.done ? t(u) : Promise.resolve(u).then(r, o); }
function _asyncToGenerator(n) { return function () { var t = this, e = arguments; return new Promise(function (r, o) { var a = n.apply(t, e); function _next(n) { asyncGeneratorStep(a, r, o, _next, _throw, "next", n); } function _throw(n) { asyncGeneratorStep(a, r, o, _next, _throw, "throw", n); } _next(void 0); }); }; }
var helpers = (0, _helpers["default"])();
function envRepository() {
  var showList = /*#__PURE__*/function () {
    var _ref = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee(req) {
      var _req$query, page, size, descending, _helpers$getPaginatio, limit, offset, order, model, getResult;
      return _regeneratorRuntime().wrap(function _callee$(_context) {
        while (1) switch (_context.prev = _context.next) {
          case 0:
            _req$query = req.query, page = _req$query.page, size = _req$query.size, descending = _req$query.descending;
            _helpers$getPaginatio = helpers.getPagination(page, size, descending), limit = _helpers$getPaginatio.limit, offset = _helpers$getPaginatio.offset, order = _helpers$getPaginatio.order;
            _context.prev = 2;
            _context.next = 5;
            return _models.environtment.findAndCountAll({
              attributes: {
                exclude: ['variable', 'header', 'authentication']
              },
              order: order,
              limit: limit,
              offset: offset
            });
          case 5:
            model = _context.sent;
            getResult = helpers.getPagingData(model, page, limit);
            return _context.abrupt("return", (0, _responseApi.result)({
              data: getResult
            }));
          case 10:
            _context.prev = 10;
            _context.t0 = _context["catch"](2);
            _context.t0.message = "envRepository.showList: " + _context.t0;
            throw _context.t0;
          case 14:
          case "end":
            return _context.stop();
        }
      }, _callee, null, [[2, 10]]);
    }));
    return function showList(_x) {
      return _ref.apply(this, arguments);
    };
  }();
  var showListWithCount = /*#__PURE__*/function () {
    var _ref2 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee2(req) {
      var _req$query2, filter, page, size, orderByCreateAt, _sql_ShowListAndCount, select, from, where, orderByRowNumber, execQuery;
      return _regeneratorRuntime().wrap(function _callee2$(_context2) {
        while (1) switch (_context2.prev = _context2.next) {
          case 0:
            _req$query2 = req.query, filter = _req$query2.filter, page = _req$query2.page, size = _req$query2.size, orderByCreateAt = _req$query2.orderByCreateAt;
            _context2.prev = 1;
            _sql_ShowListAndCount = (0, _query.sql_ShowListAndCount)({
              orderByCreatedAt: orderByCreateAt,
              filter: filter
            }), select = _sql_ShowListAndCount.select, from = _sql_ShowListAndCount.from, where = _sql_ShowListAndCount.where, orderByRowNumber = _sql_ShowListAndCount.orderByRowNumber;
            _context2.next = 5;
            return (0, _callQuery.queryWithPagination)({
              select: select,
              from: from,
              where: where,
              orderByRowNumber: orderByRowNumber,
              pageNumber: page,
              pageSize: size
            });
          case 5:
            execQuery = _context2.sent;
            return _context2.abrupt("return", (0, _responseApi.result)({
              data: execQuery
            }));
          case 9:
            _context2.prev = 9;
            _context2.t0 = _context2["catch"](1);
            _context2.t0.message = "envRepository.showListWithCount: ".concat(_context2.t0);
            throw _context2.t0;
          case 13:
          case "end":
            return _context2.stop();
        }
      }, _callee2, null, [[1, 9]]);
    }));
    return function showListWithCount(_x2) {
      return _ref2.apply(this, arguments);
    };
  }();
  var showListUserByUseEnvironment = /*#__PURE__*/function () {
    var _ref3 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee3(req) {
      var _req$query3, filter, page, size, orderByCreateAt, idEnv, _sql_ShowListEnvByUse, select, from, where, orderByRowNumber, execQuery;
      return _regeneratorRuntime().wrap(function _callee3$(_context3) {
        while (1) switch (_context3.prev = _context3.next) {
          case 0:
            _req$query3 = req.query, filter = _req$query3.filter, page = _req$query3.page, size = _req$query3.size, orderByCreateAt = _req$query3.orderByCreateAt;
            idEnv = req.params.idEnv;
            if (idEnv) {
              _context3.next = 4;
              break;
            }
            return _context3.abrupt("return", (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.badRequest400,
              message: "Environment ID not found"
            }));
          case 4:
            _context3.prev = 4;
            _sql_ShowListEnvByUse = (0, _query.sql_ShowListEnvByUseEnv)({
              envId: idEnv,
              filter: filter,
              orderByCreatedAt: orderByCreateAt
            }), select = _sql_ShowListEnvByUse.select, from = _sql_ShowListEnvByUse.from, where = _sql_ShowListEnvByUse.where, orderByRowNumber = _sql_ShowListEnvByUse.orderByRowNumber;
            _context3.next = 8;
            return (0, _callQuery.queryWithPagination)({
              select: select,
              from: from,
              where: where,
              orderByRowNumber: orderByRowNumber,
              pageNumber: page,
              pageSize: size
            });
          case 8:
            execQuery = _context3.sent;
            return _context3.abrupt("return", (0, _responseApi.result)({
              data: execQuery
            }));
          case 12:
            _context3.prev = 12;
            _context3.t0 = _context3["catch"](4);
            _context3.t0.message = "envRepository.showListUserByUseEnvironment: ".concat(_context3.t0);
            throw _context3.t0;
          case 16:
          case "end":
            return _context3.stop();
        }
      }, _callee3, null, [[4, 12]]);
    }));
    return function showListUserByUseEnvironment(_x3) {
      return _ref3.apply(this, arguments);
    };
  }();
  var create = /*#__PURE__*/function () {
    var _ref4 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee4(req) {
      var _req$body, envName, status, findIsExist, resCreate, getDetail;
      return _regeneratorRuntime().wrap(function _callee4$(_context4) {
        while (1) switch (_context4.prev = _context4.next) {
          case 0:
            _req$body = req.body, envName = _req$body.envName, status = _req$body.status;
            _context4.prev = 1;
            _context4.next = 4;
            return _models.environtment.findOne({
              where: _defineProperty({}, _sequelize.Op.or, {
                envName: _defineProperty({}, _sequelize.Op.like, "%".concat(envName, "%"))
              })
            });
          case 4:
            findIsExist = _context4.sent;
            if (!findIsExist) {
              _context4.next = 7;
              break;
            }
            return _context4.abrupt("return", (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.badRequest400,
              message: "Environment Name Already exist !"
            }));
          case 7:
            _context4.next = 9;
            return _models.environtment.create({
              id: Date.now(),
              envName: envName,
              status: status
            });
          case 9:
            resCreate = _context4.sent;
            req.params = {
              id: resCreate.id
            };
            _context4.next = 13;
            return envRepository().detail(req);
          case 13:
            getDetail = _context4.sent;
            if (!(getDetail.code === _responseApi.statusHttpCode.badRequest400.code)) {
              _context4.next = 16;
              break;
            }
            return _context4.abrupt("return", getDetail);
          case 16:
            return _context4.abrupt("return", (0, _responseApi.result)({
              data: getDetail.results
            }));
          case 19:
            _context4.prev = 19;
            _context4.t0 = _context4["catch"](1);
            _context4.t0.message = "envRepository.create: " + _context4.t0;
            throw _context4.t0;
          case 23:
          case "end":
            return _context4.stop();
        }
      }, _callee4, null, [[1, 19]]);
    }));
    return function create(_x4) {
      return _ref4.apply(this, arguments);
    };
  }();
  var create_old = /*#__PURE__*/function () {
    var _ref5 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee5(req) {
      var _req$body2, envName, variable, header, authentication, isLogin, resCreate, getDetail;
      return _regeneratorRuntime().wrap(function _callee5$(_context5) {
        while (1) switch (_context5.prev = _context5.next) {
          case 0:
            _req$body2 = req.body, envName = _req$body2.envName, variable = _req$body2.variable, header = _req$body2.header, authentication = _req$body2.authentication, isLogin = _req$body2.isLogin;
            _context5.prev = 1;
            _context5.next = 4;
            return _models.environtment.create({
              id: Date.now(),
              envName: envName,
              variable: variable ? JSON.stringify(variable) : null,
              header: header ? JSON.stringify(header) : null,
              authentication: authentication ? JSON.stringify(authentication) : null,
              isLogin: isLogin
            });
          case 4:
            resCreate = _context5.sent;
            req.params = {
              id: resCreate.id
            };
            _context5.next = 8;
            return envRepository().detail(req);
          case 8:
            getDetail = _context5.sent;
            if (!(getDetail.code === _responseApi.statusHttpCode.badRequest400.code)) {
              _context5.next = 11;
              break;
            }
            return _context5.abrupt("return", getDetail);
          case 11:
            return _context5.abrupt("return", (0, _responseApi.result)({
              data: getDetail.results
            }));
          case 14:
            _context5.prev = 14;
            _context5.t0 = _context5["catch"](1);
            _context5.t0.message = "envRepository.create: " + _context5.t0;
            throw _context5.t0;
          case 18:
          case "end":
            return _context5.stop();
        }
      }, _callee5, null, [[1, 14]]);
    }));
    return function create_old(_x5) {
      return _ref5.apply(this, arguments);
    };
  }();
  var update = /*#__PURE__*/function () {
    var _ref6 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee6(req) {
      var id, _req$body3, envName, status, checkId, findIsExist, resultDt;
      return _regeneratorRuntime().wrap(function _callee6$(_context6) {
        while (1) switch (_context6.prev = _context6.next) {
          case 0:
            id = req.params.id;
            _req$body3 = req.body, envName = _req$body3.envName, status = _req$body3.status;
            _context6.prev = 2;
            _context6.next = 5;
            return envRepository().detail(req);
          case 5:
            checkId = _context6.sent;
            if (!(checkId.code === _responseApi.statusHttpCode.badRequest400.code)) {
              _context6.next = 8;
              break;
            }
            return _context6.abrupt("return", checkId);
          case 8:
            _context6.next = 10;
            return _models.environtment.findOne({
              where: _defineProperty(_defineProperty({}, _sequelize.Op.or, {
                envName: _defineProperty({}, _sequelize.Op.like, "%".concat(envName, "%"))
              }), _sequelize.Op.and, {
                id: _defineProperty({}, _sequelize.Op.ne, id)
              })
            });
          case 10:
            findIsExist = _context6.sent;
            if (!findIsExist) {
              _context6.next = 13;
              break;
            }
            return _context6.abrupt("return", (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.badRequest400,
              message: "Environment Name Already exist !"
            }));
          case 13:
            _context6.next = 15;
            return _models.environtment.update({
              envName: envName,
              status: status
            }, {
              where: {
                id: id
              }
            });
          case 15:
            _context6.next = 17;
            return envRepository().detail(req);
          case 17:
            resultDt = _context6.sent;
            return _context6.abrupt("return", (0, _responseApi.result)({
              data: resultDt.results
            }));
          case 21:
            _context6.prev = 21;
            _context6.t0 = _context6["catch"](2);
            _context6.t0.message = "envRepository.update: " + _context6.t0;
            throw _context6.t0;
          case 25:
          case "end":
            return _context6.stop();
        }
      }, _callee6, null, [[2, 21]]);
    }));
    return function update(_x6) {
      return _ref6.apply(this, arguments);
    };
  }();
  var deleted = /*#__PURE__*/function () {
    var _ref7 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee7(req) {
      var id, checkId, queryGetDetailEnv, execQueryGetEnv, messageName;
      return _regeneratorRuntime().wrap(function _callee7$(_context7) {
        while (1) switch (_context7.prev = _context7.next) {
          case 0:
            id = req.params.id;
            _context7.prev = 1;
            _context7.next = 4;
            return envRepository().detail(req);
          case 4:
            checkId = _context7.sent;
            if (!(checkId.code === _responseApi.statusHttpCode.badRequest400.code)) {
              _context7.next = 7;
              break;
            }
            return _context7.abrupt("return", checkId);
          case 7:
            queryGetDetailEnv = (0, _query.sql_ShowDetailAndCount)({
              idEnv: id
            });
            _context7.next = 10;
            return (0, _callQuery.queryExecute)({
              query: queryGetDetailEnv,
              getOneObject: true
            });
          case 10:
            execQueryGetEnv = _context7.sent;
            if (!(execQueryGetEnv.totalEndpoint > 0 || execQueryGetEnv.totalUser > 0)) {
              _context7.next = 15;
              break;
            }
            if (execQueryGetEnv.totalEndpoint > 0) {
              messageName = 'Total Endpoint';
            }
            if (execQueryGetEnv.totalUser > 0) {
              messageName = 'Total User';
            }
            return _context7.abrupt("return", (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.badRequest400,
              message: "Environment cant delete, because " + messageName + " Is Not Empty"
            }));
          case 15:
            _models.environtment.destroy({
              where: {
                id: id
              }
            });
            return _context7.abrupt("return", (0, _responseApi.result)({
              message: "DELETED EnvID ".concat(id)
            }));
          case 19:
            _context7.prev = 19;
            _context7.t0 = _context7["catch"](1);
            _context7.t0.message = "envRepository.delete: " + _context7.t0;
            throw _context7.t0;
          case 23:
          case "end":
            return _context7.stop();
        }
      }, _callee7, null, [[1, 19]]);
    }));
    return function deleted(_x7) {
      return _ref7.apply(this, arguments);
    };
  }();
  var detail = /*#__PURE__*/function () {
    var _ref8 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee8(req) {
      var id, getDetail, queryGetDetailEnv, execQueryGetEnv;
      return _regeneratorRuntime().wrap(function _callee8$(_context8) {
        while (1) switch (_context8.prev = _context8.next) {
          case 0:
            id = req.params.id;
            _context8.prev = 1;
            _context8.next = 4;
            return _models.environtment.findByPk(id);
          case 4:
            getDetail = _context8.sent;
            if (getDetail) {
              _context8.next = 9;
              break;
            }
            _context8.next = 8;
            return (0, _responseApi.result)({
              message: "Env ID ".concat(id, " Not Found"),
              statusCode: _responseApi.statusHttpCode.badRequest400
            });
          case 8:
            return _context8.abrupt("return", _context8.sent);
          case 9:
            queryGetDetailEnv = (0, _query.sql_ShowDetailAndCount)({
              idEnv: id
            });
            _context8.next = 12;
            return (0, _callQuery.queryExecute)({
              query: queryGetDetailEnv,
              getOneObject: true
            });
          case 12:
            execQueryGetEnv = _context8.sent;
            _context8.next = 15;
            return (0, _responseApi.result)({
              data: execQueryGetEnv
            });
          case 15:
            return _context8.abrupt("return", _context8.sent);
          case 18:
            _context8.prev = 18;
            _context8.t0 = _context8["catch"](1);
            _context8.t0.message = "envRepository.getDetail: " + _context8.t0;
            throw _context8.t0;
          case 22:
          case "end":
            return _context8.stop();
        }
      }, _callee8, null, [[1, 18]]);
    }));
    return function detail(_x8) {
      return _ref8.apply(this, arguments);
    };
  }();
  var getIsLoginEnv = /*#__PURE__*/function () {
    var _ref9 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee9(req) {
      var id, condition, model;
      return _regeneratorRuntime().wrap(function _callee9$(_context9) {
        while (1) switch (_context9.prev = _context9.next) {
          case 0:
            id = req.params.id;
            _context9.prev = 1;
            condition = _defineProperty({}, _sequelize.Op.and, [
            //17/07/2023 IsLogin Tidak digunakan
            // {isLogin: {[Op.eq]: true}},
            {
              id: _defineProperty({}, _sequelize.Op.eq, id)
            }]);
            _context9.next = 5;
            return _models.environtment.findOne({
              where: condition
            });
          case 5:
            model = _context9.sent;
            if (model) {
              _context9.next = 10;
              break;
            }
            _context9.next = 9;
            return (0, _responseApi.result)({
              message: "ENV No Data",
              statusCode: _responseApi.statusHttpCode.badRequest400
            });
          case 9:
            return _context9.abrupt("return", _context9.sent);
          case 10:
            //Achmad 17072023 sudah dipindahkan kedalam variable
            // model.variable = model.variable ? JSON.parse(model.variable) : model.variable;
            // model.header = model.header ? JSON.parse(model.header) : model.header;
            // model.authentication = model.authentication ? JSON.parse(model.authentication) : model.authentication;

            model.variable = model.variable ? JSON.parse(model.variable) : model.variable;
            if (model.variable.length > 0) {
              model.variable.map(function (el) {
                /** Check use login and body request raw */
                if (el.useLogin && el.authentication.bodyRequest.mode === 'raw' && el.authentication.bodyRequest.rawJson.length > 0) {
                  el.authentication.bodyRequest.rawJson = JSON.parse(el.authentication.bodyRequest.rawJson);
                }
              });
            }
            // getDetail.header = getDetail.header ? JSON.parse(getDetail.header) : getDetail.header;
            // getDetail.authentication = getDetail.authentication ? JSON.parse(getDetail.authentication) : getDetail.authentication;
            _context9.next = 14;
            return (0, _responseApi.result)({
              data: model
            });
          case 14:
            return _context9.abrupt("return", _context9.sent);
          case 17:
            _context9.prev = 17;
            _context9.t0 = _context9["catch"](1);
            _context9.t0.message = "envRepository.getIsLoginEnv: " + _context9.t0;
            throw _context9.t0;
          case 21:
          case "end":
            return _context9.stop();
        }
      }, _callee9, null, [[1, 17]]);
    }));
    return function getIsLoginEnv(_x9) {
      return _ref9.apply(this, arguments);
    };
  }();
  var getEnvByNameUrl = /*#__PURE__*/function () {
    var _ref11 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee10(_ref10) {
      var _ref10$envName, envName, getDetail;
      return _regeneratorRuntime().wrap(function _callee10$(_context10) {
        while (1) switch (_context10.prev = _context10.next) {
          case 0:
            _ref10$envName = _ref10.envName, envName = _ref10$envName === void 0 ? null : _ref10$envName;
            _context10.prev = 1;
            _context10.next = 4;
            return _models.environtment.findOne({
              where: {
                envName: envName
              }
            });
          case 4:
            getDetail = _context10.sent;
            if (getDetail) {
              _context10.next = 9;
              break;
            }
            _context10.next = 8;
            return (0, _responseApi.result)({
              message: "Service ".concat(envName, " Not Found"),
              statusCode: _responseApi.statusHttpCode.badRequest400
            });
          case 8:
            return _context10.abrupt("return", _context10.sent);
          case 9:
            getDetail.variable = getDetail.variable ? JSON.parse(getDetail.variable) : getDetail.variable;
            getDetail.header = getDetail.header ? JSON.parse(getDetail.header) : getDetail.header;
            getDetail.authentication = getDetail.authentication ? JSON.parse(getDetail.authentication) : getDetail.authentication;
            _context10.next = 14;
            return (0, _responseApi.result)({
              data: getDetail
            });
          case 14:
            return _context10.abrupt("return", _context10.sent);
          case 17:
            _context10.prev = 17;
            _context10.t0 = _context10["catch"](1);
            _context10.t0.message = "envRepository.getEnvByNameUrl: " + _context10.t0;
            throw _context10.t0;
          case 21:
          case "end":
            return _context10.stop();
        }
      }, _callee10, null, [[1, 17]]);
    }));
    return function getEnvByNameUrl(_x10) {
      return _ref11.apply(this, arguments);
    };
  }();
  var groupByEnvName = /*#__PURE__*/function () {
    var _ref12 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee11(req) {
      var model, newModel, initial;
      return _regeneratorRuntime().wrap(function _callee11$(_context11) {
        while (1) switch (_context11.prev = _context11.next) {
          case 0:
            _context11.prev = 0;
            _context11.next = 3;
            return _models.environtment.findAll({
              attributes: ['id', 'envName']
              // group: ['envName']
            });
          case 3:
            model = _context11.sent;
            newModel = [];
            if (model.length > 0) {
              newModel = model.map(function (el) {
                return {
                  'id': el.id,
                  'label': el.envName,
                  'value': el.envName
                };
              });
              initial = {
                'label': 'All',
                'value': ''
              }; //add fist element
              newModel.unshift(initial);
            }
            return _context11.abrupt("return", (0, _responseApi.result)({
              data: newModel
            }));
          case 9:
            _context11.prev = 9;
            _context11.t0 = _context11["catch"](0);
            _context11.t0.message = "envRepository.groupByEnvName: " + _context11.t0;
            throw _context11.t0;
          case 13:
          case "end":
            return _context11.stop();
        }
      }, _callee11, null, [[0, 9]]);
    }));
    return function groupByEnvName(_x11) {
      return _ref12.apply(this, arguments);
    };
  }();
  var getListEnvironmentUnusedUserAccess = /*#__PURE__*/function () {
    var _ref13 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee12(req) {
      var userId, id, isUserId, queryGetEnv, execQueryGetEnv;
      return _regeneratorRuntime().wrap(function _callee12$(_context12) {
        while (1) switch (_context12.prev = _context12.next) {
          case 0:
            userId = req.query.userId;
            _context12.prev = 1;
            id = req.decoded.id;
            isUserId = id; //todo if isUserId null get from token
            if (userId) {
              isUserId = userId;
            }
            queryGetEnv = (0, _query.sql_getEnvironmentByListIsNotExistsUserId)({
              userId: isUserId
            });
            _context12.next = 8;
            return (0, _callQuery.queryExecute)({
              query: queryGetEnv
            });
          case 8:
            execQueryGetEnv = _context12.sent;
            return _context12.abrupt("return", (0, _responseApi.result)({
              data: execQueryGetEnv
            }));
          case 12:
            _context12.prev = 12;
            _context12.t0 = _context12["catch"](1);
            _context12.t0.message = "envRepository.getListEnvironmentByUserAccess: ".concat(_context12.t0);
            throw _context12.t0;
          case 16:
          case "end":
            return _context12.stop();
        }
      }, _callee12, null, [[1, 12]]);
    }));
    return function getListEnvironmentUnusedUserAccess(_x12) {
      return _ref13.apply(this, arguments);
    };
  }();
  return {
    showList: showList,
    create: create,
    update: update,
    detail: detail,
    deleted: deleted,
    getIsLoginEnv: getIsLoginEnv,
    getEnvByNameUrl: getEnvByNameUrl,
    groupByEnvName: groupByEnvName,
    getListEnvironmentUnusedUserAccess: getListEnvironmentUnusedUserAccess,
    showListWithCount: showListWithCount,
    showListUserByUseEnvironment: showListUserByUseEnvironment
  };
}