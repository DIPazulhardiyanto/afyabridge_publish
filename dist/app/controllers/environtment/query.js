"use strict";

require("core-js/modules/es.object.define-property.js");
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.sql_getEnvironmentByListIsNotExistsUserId = exports.sql_ShowListEnvByUseEnv = exports.sql_ShowListAndCount = exports.sql_ShowDetailAndCount = void 0;
require("core-js/modules/es.array.concat.js");
require("core-js/modules/es.array.filter.js");
require("core-js/modules/es.object.to-string.js");
//todo Hanya Menampilkan Environment yang belum digunakan oleh user
var sql_getEnvironmentByListIsNotExistsUserId = exports.sql_getEnvironmentByListIsNotExistsUserId = function sql_getEnvironmentByListIsNotExistsUserId(_ref) {
  var _ref$userId = _ref.userId,
    userId = _ref$userId === void 0 ? null : _ref$userId;
  return "SELECT env.id, env.envName, env.status\n              FROM [environtment] AS env WITH (NOLOCK)\n              WHERE env.[deletedAt] IS NULL AND\n                env.id NOT IN (\n                  SELECT envAccess.idEnv \n                FROM environmentAccess envAccess WITH (NOLOCK)\n                WHERE \n                envAccess.deletedAt IS NULL AND\n                envAccess.idUser = '".concat(userId, "'\n              )\n              ");
};

//todo show list , count endpoint, and count user
var sql_ShowListAndCount = exports.sql_ShowListAndCount = function sql_ShowListAndCount(_ref2) {
  var _ref2$orderByCreatedA = _ref2.orderByCreatedAt,
    orderByCreatedAt = _ref2$orderByCreatedA === void 0 ? 'DESC' : _ref2$orderByCreatedA,
    _ref2$filter = _ref2.filter,
    filter = _ref2$filter === void 0 ? '' : _ref2$filter;
  var select = "SELECT e.id, e.envName, e.status, e.createdAt, e.updatedAt,\n  (SELECT COUNT(id) FROM endpointResourceIntegration ri WITH (NOLOCK) WHERE ri.deletedAt IS NULL AND ri.idEnv=e.id) AS totalEndpoint,\n  (SELECT COUNT(a.idEnv) FROM environmentAccess a WITH (NOLOCK) WHERE a.deletedAt IS NULL AND a.idEnv=e.id) AS totalUser";
  var from = "FROM environtment e";
  var where = "WHERE e.deletedAt IS NULL\n                 AND (e.id LIKE '%".concat(filter, "%' OR e.envName LIKE '%").concat(filter, "%')");
  var orderByRowNumber = "e.createdAt ".concat(orderByCreatedAt);
  return {
    select: select,
    from: from,
    where: where,
    orderByRowNumber: orderByRowNumber
  };
};

//todo show detail and count environment
var sql_ShowDetailAndCount = exports.sql_ShowDetailAndCount = function sql_ShowDetailAndCount(_ref3) {
  var _ref3$idEnv = _ref3.idEnv,
    idEnv = _ref3$idEnv === void 0 ? null : _ref3$idEnv;
  return "\n       SELECT TOP 1 e.id, e.envName, e.status, e.createdAt, e.updatedAt,\n      (SELECT COUNT(id) FROM endpointResourceIntegration ri WITH (NOLOCK) WHERE ri.deletedAt IS NULL AND ri.idEnv=e.id) AS totalEndpoint,\n      (SELECT COUNT(a.idEnv) FROM environmentAccess a WITH (NOLOCK) WHERE a.deletedAt IS NULL AND a.idEnv=e.id) AS totalUser FROM environtment e WITH (NOLOCK) \n      WHERE e.deletedAt IS NULL AND e.id=".concat(idEnv, "\n    ");
};

// todo show list user by use environment
var sql_ShowListEnvByUseEnv = exports.sql_ShowListEnvByUseEnv = function sql_ShowListEnvByUseEnv(_ref4) {
  var _ref4$envId = _ref4.envId,
    envId = _ref4$envId === void 0 ? null : _ref4$envId,
    _ref4$filter = _ref4.filter,
    filter = _ref4$filter === void 0 ? null : _ref4$filter,
    _ref4$orderByCreatedA = _ref4.orderByCreatedAt,
    orderByCreatedAt = _ref4$orderByCreatedA === void 0 ? 'DESC' : _ref4$orderByCreatedA;
  var select = "SELECT a.id,a.idEnv,a.idUser,u.username, u.companyName, a.status AS envAccessStatus";
  var from = " FROM environmentAccess a \n                 INNER JOIN users u ON u.id=a.idUser";
  var where = "WHERE a.deletedAt IS NULL\n                 AND a.idEnv='".concat(envId, "'\n                 AND (\n                      a.id LIKE '%").concat(filter, "%' \n                      OR a.idEnv LIKE '%").concat(filter, "%' \n                      OR a.idUser LIKE '%").concat(filter, "%' \n                      OR u.username LIKE '%").concat(filter, "%' \n                      OR u.companyName LIKE '%").concat(filter, "%'\n                  )");
  var orderByRowNumber = "a.createdAt ".concat(orderByCreatedAt);
  return {
    select: select,
    from: from,
    where: where,
    orderByRowNumber: orderByRowNumber
  };
};