"use strict";

require("core-js/modules/es.object.define-property.js");
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
require("core-js/modules/es.array.concat.js");
var _express = _interopRequireDefault(require("express"));
var _controllers = _interopRequireDefault(require("./controllers"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
var basePath = "/environtment";
var filterPath = "/environtment/filter/groupenv";
var envByUserUsePath = "".concat(basePath, "/unused/user");
var showListPath = "".concat(basePath, "/showlist/count");
var showListUserPath = "".concat(basePath, "/showlist/user");
var setPath = function setPath(path) {
  return "".concat(basePath, "/").concat(path);
};
var setPathEnvUser = function setPathEnvUser(path) {
  return "".concat(showListUserPath, "/").concat(path);
};
var controller = (0, _controllers["default"])();
var routes = (0, _express["default"])();
routes.route(basePath).post(controller.create).get(controller.showList);
routes.route(setPath(":id")).put(controller.update)["delete"](controller.deleted).get(controller.detail);
routes.route(filterPath).get(controller.groupByEnvName);
routes.route(envByUserUsePath).get(controller.getListEnvironmentUnusedUserAccess);
routes.route(showListPath).get(controller.showListAndCount);
routes.route(setPathEnvUser(":idEnv")).get(controller.showListUserByUsedEnv);
var _default = exports["default"] = routes;