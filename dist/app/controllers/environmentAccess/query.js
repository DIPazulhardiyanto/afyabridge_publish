"use strict";

require("core-js/modules/es.object.define-property.js");
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.sql_getTopOneEnvAccessEnvByUser = exports.sql_getListEnvAccess = void 0;
require("core-js/modules/es.array.concat.js");
require("core-js/modules/es.array.filter.js");
require("core-js/modules/es.object.to-string.js");
var sql_getListEnvAccess = exports.sql_getListEnvAccess = function sql_getListEnvAccess(_ref) {
  var _ref$userId = _ref.userId,
    userId = _ref$userId === void 0 ? null : _ref$userId,
    _ref$orderByCreatedAt = _ref.orderByCreatedAt,
    orderByCreatedAt = _ref$orderByCreatedAt === void 0 ? 'DESC' : _ref$orderByCreatedAt,
    _ref$filter = _ref.filter,
    filter = _ref$filter === void 0 ? null : _ref$filter;
  var select = "SELECT a.id,idEnv,a.status, env.envName, a.idUser";
  var from = "FROM environmentAccess a WITH (NOLOCK)";
  from += "INNER JOIN environtment env WITH (NOLOCK) ON env.id=a.idEnv AND env.deletedAt IS NULL";
  var where = "WHERE a.deletedAt IS NULL";
  var orderByRowNumber = "a.createdAt ".concat(orderByCreatedAt);
  if (userId) {
    where += " AND a.idUser = '".concat(userId, "'");
  }
  if (filter) {
    where += " AND ( \n            a.idUser LIKE '%".concat(filter, "%'\n            OR a.idEnv LIKE '%").concat(filter, "%'\n            OR a.id LIKE '%").concat(filter, "%'\n            OR env.envName LIKE '%").concat(filter, "%'\n        )");
  }
  return {
    select: select,
    from: from,
    where: where,
    orderByRowNumber: orderByRowNumber
  };
};
var sql_getTopOneEnvAccessEnvByUser = exports.sql_getTopOneEnvAccessEnvByUser = function sql_getTopOneEnvAccessEnvByUser(_ref2) {
  var _ref2$userId = _ref2.userId,
    userId = _ref2$userId === void 0 ? null : _ref2$userId,
    _ref2$idEnv = _ref2.idEnv,
    idEnv = _ref2$idEnv === void 0 ? null : _ref2$idEnv;
  var queryEnvAccessEnvByUser = "SELECT TOP 1 envAccess.id, envAccess.idEnv, envAccess.idUser, env.envName\n                  FROM environmentAccess envAccess WITH (NOLOCK)\n                  INNER JOIN environtment env WITH (NOLOCK) ON envAccess.idEnv = env.id\n                  AND env.deletedAt IS NULL\n                  WHERE envAccess.deletedAt IS NULL \n                  AND envAccess.idUser = '".concat(userId, "'\n                  AND envAccess.idEnv = '").concat(idEnv, "'");
  return {
    queryEnvAccessEnvByUser: queryEnvAccessEnvByUser
  };
};