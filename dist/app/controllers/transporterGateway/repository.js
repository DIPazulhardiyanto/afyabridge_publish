"use strict";

function _typeof(o) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) { return typeof o; } : function (o) { return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o; }, _typeof(o); }
require("core-js/modules/es.symbol.js");
require("core-js/modules/es.symbol.description.js");
require("core-js/modules/es.symbol.async-iterator.js");
require("core-js/modules/es.symbol.iterator.js");
require("core-js/modules/es.symbol.to-string-tag.js");
require("core-js/modules/es.array.reverse.js");
require("core-js/modules/es.array.slice.js");
require("core-js/modules/es.function.name.js");
require("core-js/modules/es.json.to-string-tag.js");
require("core-js/modules/es.math.to-string-tag.js");
require("core-js/modules/es.object.create.js");
require("core-js/modules/es.object.define-property.js");
require("core-js/modules/es.object.get-prototype-of.js");
require("core-js/modules/es.object.set-prototype-of.js");
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = transporterGatewayRepository;
require("core-js/modules/es.array.for-each.js");
require("core-js/modules/es.array.includes.js");
require("core-js/modules/es.array.iterator.js");
require("core-js/modules/es.array.map.js");
require("core-js/modules/es.date.to-json.js");
require("core-js/modules/es.object.keys.js");
require("core-js/modules/es.object.to-string.js");
require("core-js/modules/es.promise.js");
require("core-js/modules/es.string.includes.js");
require("core-js/modules/es.string.iterator.js");
require("core-js/modules/web.dom-collections.for-each.js");
require("core-js/modules/web.dom-collections.iterator.js");
var _models = require("../../models");
var _responseApi = require("../../libs/utils/responseApi");
var _helpers = _interopRequireDefault(require("../../libs/utils/helpers"));
var _repository = _interopRequireDefault(require("../transporter/repository"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
function _regeneratorRuntime() { "use strict"; /*! regenerator-runtime -- Copyright (c) 2014-present, Facebook, Inc. -- license (MIT): https://github.com/facebook/regenerator/blob/main/LICENSE */ _regeneratorRuntime = function _regeneratorRuntime() { return e; }; var t, e = {}, r = Object.prototype, n = r.hasOwnProperty, o = Object.defineProperty || function (t, e, r) { t[e] = r.value; }, i = "function" == typeof Symbol ? Symbol : {}, a = i.iterator || "@@iterator", c = i.asyncIterator || "@@asyncIterator", u = i.toStringTag || "@@toStringTag"; function define(t, e, r) { return Object.defineProperty(t, e, { value: r, enumerable: !0, configurable: !0, writable: !0 }), t[e]; } try { define({}, ""); } catch (t) { define = function define(t, e, r) { return t[e] = r; }; } function wrap(t, e, r, n) { var i = e && e.prototype instanceof Generator ? e : Generator, a = Object.create(i.prototype), c = new Context(n || []); return o(a, "_invoke", { value: makeInvokeMethod(t, r, c) }), a; } function tryCatch(t, e, r) { try { return { type: "normal", arg: t.call(e, r) }; } catch (t) { return { type: "throw", arg: t }; } } e.wrap = wrap; var h = "suspendedStart", l = "suspendedYield", f = "executing", s = "completed", y = {}; function Generator() {} function GeneratorFunction() {} function GeneratorFunctionPrototype() {} var p = {}; define(p, a, function () { return this; }); var d = Object.getPrototypeOf, v = d && d(d(values([]))); v && v !== r && n.call(v, a) && (p = v); var g = GeneratorFunctionPrototype.prototype = Generator.prototype = Object.create(p); function defineIteratorMethods(t) { ["next", "throw", "return"].forEach(function (e) { define(t, e, function (t) { return this._invoke(e, t); }); }); } function AsyncIterator(t, e) { function invoke(r, o, i, a) { var c = tryCatch(t[r], t, o); if ("throw" !== c.type) { var u = c.arg, h = u.value; return h && "object" == _typeof(h) && n.call(h, "__await") ? e.resolve(h.__await).then(function (t) { invoke("next", t, i, a); }, function (t) { invoke("throw", t, i, a); }) : e.resolve(h).then(function (t) { u.value = t, i(u); }, function (t) { return invoke("throw", t, i, a); }); } a(c.arg); } var r; o(this, "_invoke", { value: function value(t, n) { function callInvokeWithMethodAndArg() { return new e(function (e, r) { invoke(t, n, e, r); }); } return r = r ? r.then(callInvokeWithMethodAndArg, callInvokeWithMethodAndArg) : callInvokeWithMethodAndArg(); } }); } function makeInvokeMethod(e, r, n) { var o = h; return function (i, a) { if (o === f) throw Error("Generator is already running"); if (o === s) { if ("throw" === i) throw a; return { value: t, done: !0 }; } for (n.method = i, n.arg = a;;) { var c = n.delegate; if (c) { var u = maybeInvokeDelegate(c, n); if (u) { if (u === y) continue; return u; } } if ("next" === n.method) n.sent = n._sent = n.arg;else if ("throw" === n.method) { if (o === h) throw o = s, n.arg; n.dispatchException(n.arg); } else "return" === n.method && n.abrupt("return", n.arg); o = f; var p = tryCatch(e, r, n); if ("normal" === p.type) { if (o = n.done ? s : l, p.arg === y) continue; return { value: p.arg, done: n.done }; } "throw" === p.type && (o = s, n.method = "throw", n.arg = p.arg); } }; } function maybeInvokeDelegate(e, r) { var n = r.method, o = e.iterator[n]; if (o === t) return r.delegate = null, "throw" === n && e.iterator["return"] && (r.method = "return", r.arg = t, maybeInvokeDelegate(e, r), "throw" === r.method) || "return" !== n && (r.method = "throw", r.arg = new TypeError("The iterator does not provide a '" + n + "' method")), y; var i = tryCatch(o, e.iterator, r.arg); if ("throw" === i.type) return r.method = "throw", r.arg = i.arg, r.delegate = null, y; var a = i.arg; return a ? a.done ? (r[e.resultName] = a.value, r.next = e.nextLoc, "return" !== r.method && (r.method = "next", r.arg = t), r.delegate = null, y) : a : (r.method = "throw", r.arg = new TypeError("iterator result is not an object"), r.delegate = null, y); } function pushTryEntry(t) { var e = { tryLoc: t[0] }; 1 in t && (e.catchLoc = t[1]), 2 in t && (e.finallyLoc = t[2], e.afterLoc = t[3]), this.tryEntries.push(e); } function resetTryEntry(t) { var e = t.completion || {}; e.type = "normal", delete e.arg, t.completion = e; } function Context(t) { this.tryEntries = [{ tryLoc: "root" }], t.forEach(pushTryEntry, this), this.reset(!0); } function values(e) { if (e || "" === e) { var r = e[a]; if (r) return r.call(e); if ("function" == typeof e.next) return e; if (!isNaN(e.length)) { var o = -1, i = function next() { for (; ++o < e.length;) if (n.call(e, o)) return next.value = e[o], next.done = !1, next; return next.value = t, next.done = !0, next; }; return i.next = i; } } throw new TypeError(_typeof(e) + " is not iterable"); } return GeneratorFunction.prototype = GeneratorFunctionPrototype, o(g, "constructor", { value: GeneratorFunctionPrototype, configurable: !0 }), o(GeneratorFunctionPrototype, "constructor", { value: GeneratorFunction, configurable: !0 }), GeneratorFunction.displayName = define(GeneratorFunctionPrototype, u, "GeneratorFunction"), e.isGeneratorFunction = function (t) { var e = "function" == typeof t && t.constructor; return !!e && (e === GeneratorFunction || "GeneratorFunction" === (e.displayName || e.name)); }, e.mark = function (t) { return Object.setPrototypeOf ? Object.setPrototypeOf(t, GeneratorFunctionPrototype) : (t.__proto__ = GeneratorFunctionPrototype, define(t, u, "GeneratorFunction")), t.prototype = Object.create(g), t; }, e.awrap = function (t) { return { __await: t }; }, defineIteratorMethods(AsyncIterator.prototype), define(AsyncIterator.prototype, c, function () { return this; }), e.AsyncIterator = AsyncIterator, e.async = function (t, r, n, o, i) { void 0 === i && (i = Promise); var a = new AsyncIterator(wrap(t, r, n, o), i); return e.isGeneratorFunction(r) ? a : a.next().then(function (t) { return t.done ? t.value : a.next(); }); }, defineIteratorMethods(g), define(g, u, "Generator"), define(g, a, function () { return this; }), define(g, "toString", function () { return "[object Generator]"; }), e.keys = function (t) { var e = Object(t), r = []; for (var n in e) r.push(n); return r.reverse(), function next() { for (; r.length;) { var t = r.pop(); if (t in e) return next.value = t, next.done = !1, next; } return next.done = !0, next; }; }, e.values = values, Context.prototype = { constructor: Context, reset: function reset(e) { if (this.prev = 0, this.next = 0, this.sent = this._sent = t, this.done = !1, this.delegate = null, this.method = "next", this.arg = t, this.tryEntries.forEach(resetTryEntry), !e) for (var r in this) "t" === r.charAt(0) && n.call(this, r) && !isNaN(+r.slice(1)) && (this[r] = t); }, stop: function stop() { this.done = !0; var t = this.tryEntries[0].completion; if ("throw" === t.type) throw t.arg; return this.rval; }, dispatchException: function dispatchException(e) { if (this.done) throw e; var r = this; function handle(n, o) { return a.type = "throw", a.arg = e, r.next = n, o && (r.method = "next", r.arg = t), !!o; } for (var o = this.tryEntries.length - 1; o >= 0; --o) { var i = this.tryEntries[o], a = i.completion; if ("root" === i.tryLoc) return handle("end"); if (i.tryLoc <= this.prev) { var c = n.call(i, "catchLoc"), u = n.call(i, "finallyLoc"); if (c && u) { if (this.prev < i.catchLoc) return handle(i.catchLoc, !0); if (this.prev < i.finallyLoc) return handle(i.finallyLoc); } else if (c) { if (this.prev < i.catchLoc) return handle(i.catchLoc, !0); } else { if (!u) throw Error("try statement without catch or finally"); if (this.prev < i.finallyLoc) return handle(i.finallyLoc); } } } }, abrupt: function abrupt(t, e) { for (var r = this.tryEntries.length - 1; r >= 0; --r) { var o = this.tryEntries[r]; if (o.tryLoc <= this.prev && n.call(o, "finallyLoc") && this.prev < o.finallyLoc) { var i = o; break; } } i && ("break" === t || "continue" === t) && i.tryLoc <= e && e <= i.finallyLoc && (i = null); var a = i ? i.completion : {}; return a.type = t, a.arg = e, i ? (this.method = "next", this.next = i.finallyLoc, y) : this.complete(a); }, complete: function complete(t, e) { if ("throw" === t.type) throw t.arg; return "break" === t.type || "continue" === t.type ? this.next = t.arg : "return" === t.type ? (this.rval = this.arg = t.arg, this.method = "return", this.next = "end") : "normal" === t.type && e && (this.next = e), y; }, finish: function finish(t) { for (var e = this.tryEntries.length - 1; e >= 0; --e) { var r = this.tryEntries[e]; if (r.finallyLoc === t) return this.complete(r.completion, r.afterLoc), resetTryEntry(r), y; } }, "catch": function _catch(t) { for (var e = this.tryEntries.length - 1; e >= 0; --e) { var r = this.tryEntries[e]; if (r.tryLoc === t) { var n = r.completion; if ("throw" === n.type) { var o = n.arg; resetTryEntry(r); } return o; } } throw Error("illegal catch attempt"); }, delegateYield: function delegateYield(e, r, n) { return this.delegate = { iterator: values(e), resultName: r, nextLoc: n }, "next" === this.method && (this.arg = t), y; } }, e; }
function asyncGeneratorStep(n, t, e, r, o, a, c) { try { var i = n[a](c), u = i.value; } catch (n) { return void e(n); } i.done ? t(u) : Promise.resolve(u).then(r, o); }
function _asyncToGenerator(n) { return function () { var t = this, e = arguments; return new Promise(function (r, o) { var a = n.apply(t, e); function _next(n) { asyncGeneratorStep(a, r, o, _next, _throw, "next", n); } function _throw(n) { asyncGeneratorStep(a, r, o, _next, _throw, "throw", n); } _next(void 0); }); }; }
var helpers = (0, _helpers["default"])();
function transporterGatewayRepository() {
  var create = /*#__PURE__*/function () {
    var _ref = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee(req) {
      var createData, getDetail;
      return _regeneratorRuntime().wrap(function _callee$(_context) {
        while (1) switch (_context.prev = _context.next) {
          case 0:
            req.body.type = req.body.type ? JSON.stringify(req.body.type) : req.body.type;
            req.body.dimensions = req.body.dimensions ? JSON.stringify(req.body.dimensions) : req.body.dimensions;
            req.body.handleBounds = req.body.handleBounds ? JSON.stringify(req.body.handleBounds) : req.body.handleBounds;
            req.body.computedPosition = req.body.computedPosition ? JSON.stringify(req.body.computedPosition) : req.body.computedPosition;
            req.body.position = req.body.position ? JSON.stringify(req.body.position) : req.body.position;
            req.body.data = req.body.data ? JSON.stringify(req.body.data) : req.body.data;
            req.body.events = req.body.events ? JSON.stringify(req.body.events) : req.body.events;
            req.body.style = req.body.style ? JSON.stringify(req.body.style) : req.body.style;
            _context.prev = 8;
            _context.next = 11;
            return _models.transporterGateway.create(req.body);
          case 11:
            createData = _context.sent;
            req.params = {
              id: createData.id
            };
            _context.next = 15;
            return transporterGatewayRepository().detail(req);
          case 15:
            getDetail = _context.sent;
            if (!(getDetail.code === _responseApi.statusHttpCode.badRequest400.code)) {
              _context.next = 18;
              break;
            }
            return _context.abrupt("return", getDetail);
          case 18:
            return _context.abrupt("return", (0, _responseApi.result)({
              data: getDetail.results
            }));
          case 21:
            _context.prev = 21;
            _context.t0 = _context["catch"](8);
            _context.t0.message = "transporterGateway.create: " + _context.t0;
            throw _context.t0;
          case 25:
          case "end":
            return _context.stop();
        }
      }, _callee, null, [[8, 21]]);
    }));
    return function create(_x) {
      return _ref.apply(this, arguments);
    };
  }();
  var update = /*#__PURE__*/function () {
    var _ref2 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee2(req) {
      var id, getDetail;
      return _regeneratorRuntime().wrap(function _callee2$(_context2) {
        while (1) switch (_context2.prev = _context2.next) {
          case 0:
            id = req.params.id;
            req.body.type = req.body.type ? JSON.stringify(req.body.type) : req.body.type;
            req.body.dimensions = req.body.dimensions ? JSON.stringify(req.body.dimensions) : req.body.dimensions;
            req.body.handleBounds = req.body.handleBounds ? JSON.stringify(req.body.handleBounds) : req.body.handleBounds;
            req.body.computedPosition = req.body.computedPosition ? JSON.stringify(req.body.computedPosition) : req.body.computedPosition;
            req.body.position = req.body.position ? JSON.stringify(req.body.position) : req.body.position;
            req.body.data = req.body.data ? JSON.stringify(req.body.data) : req.body.data;
            req.body.events = req.body.events ? JSON.stringify(req.body.events) : req.body.events;
            req.body.style = req.body.style ? JSON.stringify(req.body.style) : req.body.style;
            _context2.prev = 9;
            _context2.next = 12;
            return transporterGatewayRepository().detail(req);
          case 12:
            getDetail = _context2.sent;
            if (!(getDetail.code === _responseApi.statusHttpCode.badRequest400.code)) {
              _context2.next = 15;
              break;
            }
            return _context2.abrupt("return", getDetail);
          case 15:
            _context2.next = 17;
            return _models.transporterGateway.update(req.body, {
              where: {
                id: id
              }
            });
          case 17:
            _context2.next = 19;
            return transporterGatewayRepository().detail(req);
          case 19:
            getDetail = _context2.sent;
            if (!(getDetail.code === _responseApi.statusHttpCode.badRequest400.code)) {
              _context2.next = 22;
              break;
            }
            return _context2.abrupt("return", getDetail);
          case 22:
            return _context2.abrupt("return", (0, _responseApi.result)({
              data: getDetail.results
            }));
          case 25:
            _context2.prev = 25;
            _context2.t0 = _context2["catch"](9);
            _context2.t0.message = "transporterGateway.update: " + _context2.t0;
            throw _context2.t0;
          case 29:
          case "end":
            return _context2.stop();
        }
      }, _callee2, null, [[9, 25]]);
    }));
    return function update(_x2) {
      return _ref2.apply(this, arguments);
    };
  }();
  var detail = /*#__PURE__*/function () {
    var _ref3 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee3(req) {
      var id, getDetail;
      return _regeneratorRuntime().wrap(function _callee3$(_context3) {
        while (1) switch (_context3.prev = _context3.next) {
          case 0:
            id = req.params.id;
            _context3.prev = 1;
            _context3.next = 4;
            return _models.transporterGateway.findOne({
              where: {
                id: id
              }
            });
          case 4:
            getDetail = _context3.sent;
            if (getDetail) {
              _context3.next = 9;
              break;
            }
            _context3.next = 8;
            return (0, _responseApi.result)({
              message: "ID ".concat(id, " Not Found"),
              statusCode: _responseApi.statusHttpCode.badRequest400
            });
          case 8:
            return _context3.abrupt("return", _context3.sent);
          case 9:
            getDetail.type = getDetail.type ? JSON.parse(getDetail.type) : {};
            getDetail.dimensions = getDetail.dimensions ? JSON.parse(getDetail.dimensions) : {};
            getDetail.handleBounds = getDetail.handleBounds ? JSON.parse(getDetail.handleBounds) : {};
            getDetail.computedPosition = getDetail.computedPosition ? JSON.parse(getDetail.computedPosition) : {};
            getDetail.position = getDetail.position ? JSON.parse(getDetail.position) : {};
            getDetail.data = getDetail.data ? JSON.parse(getDetail.data) : {};
            getDetail.events = getDetail.events ? JSON.parse(getDetail.events) : {};
            getDetail.style = getDetail.style ? JSON.parse(getDetail.style) : {};
            _context3.next = 19;
            return (0, _responseApi.result)({
              data: getDetail
            });
          case 19:
            return _context3.abrupt("return", _context3.sent);
          case 22:
            _context3.prev = 22;
            _context3.t0 = _context3["catch"](1);
            _context3.t0.message = "transporterGateway.detail: " + _context3.t0;
            throw _context3.t0;
          case 26:
          case "end":
            return _context3.stop();
        }
      }, _callee3, null, [[1, 22]]);
    }));
    return function detail(_x3) {
      return _ref3.apply(this, arguments);
    };
  }();
  var showList = /*#__PURE__*/function () {
    var _ref4 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee4(req) {
      var _req$query, page, size, _helpers$getPaginatio, limit, offset, order, model, getResult;
      return _regeneratorRuntime().wrap(function _callee4$(_context4) {
        while (1) switch (_context4.prev = _context4.next) {
          case 0:
            _req$query = req.query, page = _req$query.page, size = _req$query.size;
            _helpers$getPaginatio = helpers.getPagination(page, size), limit = _helpers$getPaginatio.limit, offset = _helpers$getPaginatio.offset, order = _helpers$getPaginatio.order;
            _context4.prev = 2;
            _context4.next = 5;
            return _models.transporterGateway.findAndCountAll({
              order: order,
              limit: limit,
              offset: offset
            });
          case 5:
            model = _context4.sent;
            getResult = helpers.getPagingData(model, page, limit);
            return _context4.abrupt("return", (0, _responseApi.result)({
              data: getResult
            }));
          case 10:
            _context4.prev = 10;
            _context4.t0 = _context4["catch"](2);
            _context4.t0.message = "transporterGateway.showList: " + _context4.t0;
            throw _context4.t0;
          case 14:
          case "end":
            return _context4.stop();
        }
      }, _callee4, null, [[2, 10]]);
    }));
    return function showList(_x4) {
      return _ref4.apply(this, arguments);
    };
  }();
  var deleted = /*#__PURE__*/function () {
    var _ref5 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee5(req) {
      var id, checkId;
      return _regeneratorRuntime().wrap(function _callee5$(_context5) {
        while (1) switch (_context5.prev = _context5.next) {
          case 0:
            id = req.params.id;
            _context5.prev = 1;
            _context5.next = 4;
            return transporterGatewayRepository().detail(req);
          case 4:
            checkId = _context5.sent;
            if (!(checkId.code === _responseApi.statusHttpCode.badRequest400.code)) {
              _context5.next = 7;
              break;
            }
            return _context5.abrupt("return", checkId);
          case 7:
            _models.transporterGateway.destroy({
              where: {
                id: id
              }
            });
            return _context5.abrupt("return", (0, _responseApi.result)({
              message: "DELETED ID " + id
            }));
          case 11:
            _context5.prev = 11;
            _context5.t0 = _context5["catch"](1);
            _context5.t0.message = "transporterGateway.deleted: " + _context5.t0;
            throw _context5.t0;
          case 15:
          case "end":
            return _context5.stop();
        }
      }, _callee5, null, [[1, 11]]);
    }));
    return function deleted(_x5) {
      return _ref5.apply(this, arguments);
    };
  }();
  var multipleInsert = /*#__PURE__*/function () {
    var _ref6 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee6(req) {
      return _regeneratorRuntime().wrap(function _callee6$(_context6) {
        while (1) switch (_context6.prev = _context6.next) {
          case 0:
            if (!(req.body.length > 0)) {
              _context6.next = 4;
              break;
            }
            req.body.map(function (el) {
              el.type = el.type ? JSON.stringify(el.type) : el.type;
              el.dimensions = el.dimensions ? JSON.stringify(el.dimensions) : el.dimensions;
              el.handleBounds = el.handleBounds ? JSON.stringify(el.handleBounds) : el.handleBounds;
              el.computedPosition = el.computedPosition ? JSON.stringify(el.computedPosition) : el.computedPosition;
              el.position = el.position ? JSON.stringify(el.position) : el.position;
              el.data = el.data ? JSON.stringify(el.data) : el.data;
              el.events = el.events ? JSON.stringify(el.events) : el.events;
              el.style = el.style ? JSON.stringify(el.style) : el.style;
            });
            _context6.next = 5;
            break;
          case 4:
            return _context6.abrupt("return", (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.badRequest400,
              message: "Please insert body"
            }));
          case 5:
            _context6.prev = 5;
            _context6.next = 8;
            return _models.transporterGateway.bulkCreate(req.body).then(function (el) {
              return (0, _responseApi.result)({
                message: "Data has been save"
              });
            })["catch"](function (err) {
              return (0, _responseApi.result)({
                statusCode: _responseApi.statusHttpCode.badRequest400,
                message: "Insert failed: " + err.message
              });
            });
          case 8:
            return _context6.abrupt("return", _context6.sent);
          case 11:
            _context6.prev = 11;
            _context6.t0 = _context6["catch"](5);
            _context6.t0.message = "transporterGateway.create: " + _context6.t0;
            throw _context6.t0;
          case 15:
          case "end":
            return _context6.stop();
        }
      }, _callee6, null, [[5, 11]]);
    }));
    return function multipleInsert(_x6) {
      return _ref6.apply(this, arguments);
    };
  }();
  var getAllByTransporterId = /*#__PURE__*/function () {
    var _ref7 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee7(req) {
      var id, listNode, listEdge, getDetail;
      return _regeneratorRuntime().wrap(function _callee7$(_context7) {
        while (1) switch (_context7.prev = _context7.next) {
          case 0:
            id = req.params.id;
            _context7.prev = 1;
            listNode = [];
            listEdge = [];
            _context7.next = 6;
            return _models.transporterGateway.findAll({
              where: {
                requestId: id
              }
            });
          case 6:
            getDetail = _context7.sent;
            if (!(getDetail.length === 0)) {
              _context7.next = 11;
              break;
            }
            _context7.next = 10;
            return (0, _responseApi.result)({
              message: "ID ".concat(id, " Not Found"),
              data: {
                listNode: listNode,
                listEdge: listEdge
              }
            });
          case 10:
            return _context7.abrupt("return", _context7.sent);
          case 11:
            getDetail.forEach(function (el, idx) {
              el.type = el.type ? JSON.parse(el.type) : {};
              el.dimensions = el.dimensions ? JSON.parse(el.dimensions) : {};
              el.handleBounds = el.handleBounds ? JSON.parse(el.handleBounds) : {};
              el.computedPosition = el.computedPosition ? JSON.parse(el.computedPosition) : {};
              el.position = el.position ? JSON.parse(el.position) : {};
              el.data = el.data ? JSON.parse(el.data) : {};
              el.events = el.events ? JSON.parse(el.events) : {};
              el.style = el.style ? JSON.parse(el.style) : {};
              if (el.label && el.label.includes("NODE_ID") || el.id && el.dataValues.id.includes('CONDITION') || el.id && el.dataValues.id.includes('OUTPUT') || el.id && el.dataValues.id.includes('INPUT')) {
                //NODES
                delete el.dataValues.sourceHandle;
                delete el.dataValues.targetHandle;
                delete el.dataValues.source;
                delete el.dataValues.target;
                delete el.dataValues.updatable;
                listNode.push(el);
              } else {
                //EDGES
                delete el.dataValues.dimensions;
                delete el.dataValues.handleBounds;
                delete el.dataValues.computedPosition;
                delete el.dataValues.selected;
                delete el.dataValues.dragging;
                delete el.dataValues.resizing;
                delete el.dataValues.initialized;
                delete el.dataValues.position;
                listEdge.push(el);
              }
            });
            _context7.next = 14;
            return (0, _responseApi.result)({
              data: {
                listNode: listNode,
                listEdge: listEdge
              }
            });
          case 14:
            return _context7.abrupt("return", _context7.sent);
          case 17:
            _context7.prev = 17;
            _context7.t0 = _context7["catch"](1);
            _context7.t0.message = "transporterGateway.getAllByTransporterId: " + _context7.t0;
            throw _context7.t0;
          case 21:
          case "end":
            return _context7.stop();
        }
      }, _callee7, null, [[1, 17]]);
    }));
    return function getAllByTransporterId(_x7) {
      return _ref7.apply(this, arguments);
    };
  }();
  var multipleUpdate = /*#__PURE__*/function () {
    var _ref8 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee9(req) {
      var transporterId, checkTransporter, listResult, id, resultDt;
      return _regeneratorRuntime().wrap(function _callee9$(_context9) {
        while (1) switch (_context9.prev = _context9.next) {
          case 0:
            transporterId = req.params.transporterId;
            req.params.id = transporterId;
            _context9.next = 4;
            return (0, _repository["default"])().detail(req);
          case 4:
            checkTransporter = _context9.sent;
            if (!(checkTransporter.code === 400)) {
              _context9.next = 7;
              break;
            }
            return _context9.abrupt("return", (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.badRequest400,
              message: "Transporter ID ".concat(transporterId, " not found")
            }));
          case 7:
            if (!(req.body.length > 0)) {
              _context9.next = 11;
              break;
            }
            req.body.map(function (el) {
              el.type = el.type ? JSON.stringify(el.type) : el.type;
              el.dimensions = el.dimensions ? JSON.stringify(el.dimensions) : el.dimensions;
              el.handleBounds = el.handleBounds ? JSON.stringify(el.handleBounds) : el.handleBounds;
              el.computedPosition = el.computedPosition ? JSON.stringify(el.computedPosition) : el.computedPosition;
              el.position = el.position ? JSON.stringify(el.position) : el.position;
              el.data = el.data ? JSON.stringify(el.data) : el.data;
              el.events = el.events ? JSON.stringify(el.events) : el.events;
              el.style = el.style ? JSON.stringify(el.style) : el.style;
            });
            _context9.next = 12;
            break;
          case 11:
            return _context9.abrupt("return", (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.badRequest400,
              message: "Please insert body"
            }));
          case 12:
            _context9.prev = 12;
            listResult = [];
            id = "";
            req.body.map(/*#__PURE__*/function () {
              var _ref9 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee8(el) {
                return _regeneratorRuntime().wrap(function _callee8$(_context8) {
                  while (1) switch (_context8.prev = _context8.next) {
                    case 0:
                      id = el.id;
                      delete el.id;
                      _context8.next = 4;
                      return _models.transporterGateway.update(el, {
                        where: {
                          id: id
                        }
                      });
                    case 4:
                      listResult.push({
                        id: id,
                        data: el
                      });
                    case 5:
                    case "end":
                      return _context8.stop();
                  }
                }, _callee8);
              }));
              return function (_x9) {
                return _ref9.apply(this, arguments);
              };
            }());
            _context9.next = 18;
            return Promise.all(listResult);
          case 18:
            resultDt = _context9.sent;
            return _context9.abrupt("return", (0, _responseApi.result)({
              data: resultDt
            }));
          case 22:
            _context9.prev = 22;
            _context9.t0 = _context9["catch"](12);
            _context9.t0.message = "transporterGateway.multipleUpdate: " + _context9.t0;
            throw _context9.t0;
          case 26:
          case "end":
            return _context9.stop();
        }
      }, _callee9, null, [[12, 22]]);
    }));
    return function multipleUpdate(_x8) {
      return _ref8.apply(this, arguments);
    };
  }();
  return {
    detail: detail,
    create: create,
    update: update,
    showList: showList,
    deleted: deleted,
    multipleInsert: multipleInsert,
    getAllByTransporterId: getAllByTransporterId,
    multipleUpdate: multipleUpdate
  };
}