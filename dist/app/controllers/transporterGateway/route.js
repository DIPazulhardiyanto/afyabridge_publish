"use strict";

require("core-js/modules/es.object.define-property.js");
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
require("core-js/modules/es.array.concat.js");
var _express = _interopRequireDefault(require("express"));
var _controllers = _interopRequireDefault(require("./controllers"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
var basePath = "/transporter/gateway";
var insertMultiplePath = basePath + "/" + "multipleinsert";
var updateMultiplePath = basePath + "/" + "multipleupdate";
var getAllByTransporterPath = basePath + "/" + "getallbytransporter";
var setPath = function setPath(path) {
  return "".concat(basePath, "/").concat(path);
};
var setGetAllByTransporterId = function setGetAllByTransporterId(path) {
  return "".concat(getAllByTransporterPath, "/").concat(path);
};
var setMultipleUpdate = function setMultipleUpdate(path) {
  return "".concat(updateMultiplePath, "/").concat(path);
};
var controller = (0, _controllers["default"])();
var routes = (0, _express["default"])();
routes.route(basePath).post(controller.create).get(controller.showAll);
routes.route(setPath(':id')).get(controller.detail)["delete"](controller.deleted).put(controller.update);
routes.route(insertMultiplePath).post(controller.multipleInsert);
routes.route(setGetAllByTransporterId(':id')).get(controller.getAllByTransporterId);
routes.route(setMultipleUpdate(':transporterId')).put(controller.multipleUpdate);
var _default = exports["default"] = routes;