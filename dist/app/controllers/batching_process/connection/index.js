"use strict";

function _typeof(o) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) { return typeof o; } : function (o) { return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o; }, _typeof(o); }
require("core-js/modules/es.symbol.js");
require("core-js/modules/es.symbol.description.js");
require("core-js/modules/es.symbol.async-iterator.js");
require("core-js/modules/es.symbol.iterator.js");
require("core-js/modules/es.symbol.to-string-tag.js");
require("core-js/modules/es.array.for-each.js");
require("core-js/modules/es.array.reverse.js");
require("core-js/modules/es.array.slice.js");
require("core-js/modules/es.function.name.js");
require("core-js/modules/es.json.to-string-tag.js");
require("core-js/modules/es.math.to-string-tag.js");
require("core-js/modules/es.object.create.js");
require("core-js/modules/es.object.define-property.js");
require("core-js/modules/es.object.get-own-property-descriptor.js");
require("core-js/modules/es.object.get-prototype-of.js");
require("core-js/modules/es.object.set-prototype-of.js");
require("core-js/modules/es.weak-map.js");
require("core-js/modules/web.dom-collections.for-each.js");
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.apiGateway = void 0;
require("core-js/modules/es.array.iterator.js");
require("core-js/modules/es.date.to-json.js");
require("core-js/modules/es.date.to-string.js");
require("core-js/modules/es.object.to-string.js");
require("core-js/modules/es.promise.js");
require("core-js/modules/es.regexp.to-string.js");
require("core-js/modules/es.string.iterator.js");
require("core-js/modules/web.dom-collections.iterator.js");
require("core-js/modules/web.timers.js");
require("core-js/modules/web.url-search-params.js");
var _setup = _interopRequireDefault(require("../../../config/setup.js"));
var _setup_parameter = require("./setup_parameter.js");
var _nodeFetch = _interopRequireDefault(require("node-fetch"));
var https = _interopRequireWildcard(require("https"));
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != _typeof(e) && "function" != typeof e) return { "default": e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && {}.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n["default"] = e, t && t.set(e, n), n; }
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
function _regeneratorRuntime() { "use strict"; /*! regenerator-runtime -- Copyright (c) 2014-present, Facebook, Inc. -- license (MIT): https://github.com/facebook/regenerator/blob/main/LICENSE */ _regeneratorRuntime = function _regeneratorRuntime() { return e; }; var t, e = {}, r = Object.prototype, n = r.hasOwnProperty, o = Object.defineProperty || function (t, e, r) { t[e] = r.value; }, i = "function" == typeof Symbol ? Symbol : {}, a = i.iterator || "@@iterator", c = i.asyncIterator || "@@asyncIterator", u = i.toStringTag || "@@toStringTag"; function define(t, e, r) { return Object.defineProperty(t, e, { value: r, enumerable: !0, configurable: !0, writable: !0 }), t[e]; } try { define({}, ""); } catch (t) { define = function define(t, e, r) { return t[e] = r; }; } function wrap(t, e, r, n) { var i = e && e.prototype instanceof Generator ? e : Generator, a = Object.create(i.prototype), c = new Context(n || []); return o(a, "_invoke", { value: makeInvokeMethod(t, r, c) }), a; } function tryCatch(t, e, r) { try { return { type: "normal", arg: t.call(e, r) }; } catch (t) { return { type: "throw", arg: t }; } } e.wrap = wrap; var h = "suspendedStart", l = "suspendedYield", f = "executing", s = "completed", y = {}; function Generator() {} function GeneratorFunction() {} function GeneratorFunctionPrototype() {} var p = {}; define(p, a, function () { return this; }); var d = Object.getPrototypeOf, v = d && d(d(values([]))); v && v !== r && n.call(v, a) && (p = v); var g = GeneratorFunctionPrototype.prototype = Generator.prototype = Object.create(p); function defineIteratorMethods(t) { ["next", "throw", "return"].forEach(function (e) { define(t, e, function (t) { return this._invoke(e, t); }); }); } function AsyncIterator(t, e) { function invoke(r, o, i, a) { var c = tryCatch(t[r], t, o); if ("throw" !== c.type) { var u = c.arg, h = u.value; return h && "object" == _typeof(h) && n.call(h, "__await") ? e.resolve(h.__await).then(function (t) { invoke("next", t, i, a); }, function (t) { invoke("throw", t, i, a); }) : e.resolve(h).then(function (t) { u.value = t, i(u); }, function (t) { return invoke("throw", t, i, a); }); } a(c.arg); } var r; o(this, "_invoke", { value: function value(t, n) { function callInvokeWithMethodAndArg() { return new e(function (e, r) { invoke(t, n, e, r); }); } return r = r ? r.then(callInvokeWithMethodAndArg, callInvokeWithMethodAndArg) : callInvokeWithMethodAndArg(); } }); } function makeInvokeMethod(e, r, n) { var o = h; return function (i, a) { if (o === f) throw Error("Generator is already running"); if (o === s) { if ("throw" === i) throw a; return { value: t, done: !0 }; } for (n.method = i, n.arg = a;;) { var c = n.delegate; if (c) { var u = maybeInvokeDelegate(c, n); if (u) { if (u === y) continue; return u; } } if ("next" === n.method) n.sent = n._sent = n.arg;else if ("throw" === n.method) { if (o === h) throw o = s, n.arg; n.dispatchException(n.arg); } else "return" === n.method && n.abrupt("return", n.arg); o = f; var p = tryCatch(e, r, n); if ("normal" === p.type) { if (o = n.done ? s : l, p.arg === y) continue; return { value: p.arg, done: n.done }; } "throw" === p.type && (o = s, n.method = "throw", n.arg = p.arg); } }; } function maybeInvokeDelegate(e, r) { var n = r.method, o = e.iterator[n]; if (o === t) return r.delegate = null, "throw" === n && e.iterator["return"] && (r.method = "return", r.arg = t, maybeInvokeDelegate(e, r), "throw" === r.method) || "return" !== n && (r.method = "throw", r.arg = new TypeError("The iterator does not provide a '" + n + "' method")), y; var i = tryCatch(o, e.iterator, r.arg); if ("throw" === i.type) return r.method = "throw", r.arg = i.arg, r.delegate = null, y; var a = i.arg; return a ? a.done ? (r[e.resultName] = a.value, r.next = e.nextLoc, "return" !== r.method && (r.method = "next", r.arg = t), r.delegate = null, y) : a : (r.method = "throw", r.arg = new TypeError("iterator result is not an object"), r.delegate = null, y); } function pushTryEntry(t) { var e = { tryLoc: t[0] }; 1 in t && (e.catchLoc = t[1]), 2 in t && (e.finallyLoc = t[2], e.afterLoc = t[3]), this.tryEntries.push(e); } function resetTryEntry(t) { var e = t.completion || {}; e.type = "normal", delete e.arg, t.completion = e; } function Context(t) { this.tryEntries = [{ tryLoc: "root" }], t.forEach(pushTryEntry, this), this.reset(!0); } function values(e) { if (e || "" === e) { var r = e[a]; if (r) return r.call(e); if ("function" == typeof e.next) return e; if (!isNaN(e.length)) { var o = -1, i = function next() { for (; ++o < e.length;) if (n.call(e, o)) return next.value = e[o], next.done = !1, next; return next.value = t, next.done = !0, next; }; return i.next = i; } } throw new TypeError(_typeof(e) + " is not iterable"); } return GeneratorFunction.prototype = GeneratorFunctionPrototype, o(g, "constructor", { value: GeneratorFunctionPrototype, configurable: !0 }), o(GeneratorFunctionPrototype, "constructor", { value: GeneratorFunction, configurable: !0 }), GeneratorFunction.displayName = define(GeneratorFunctionPrototype, u, "GeneratorFunction"), e.isGeneratorFunction = function (t) { var e = "function" == typeof t && t.constructor; return !!e && (e === GeneratorFunction || "GeneratorFunction" === (e.displayName || e.name)); }, e.mark = function (t) { return Object.setPrototypeOf ? Object.setPrototypeOf(t, GeneratorFunctionPrototype) : (t.__proto__ = GeneratorFunctionPrototype, define(t, u, "GeneratorFunction")), t.prototype = Object.create(g), t; }, e.awrap = function (t) { return { __await: t }; }, defineIteratorMethods(AsyncIterator.prototype), define(AsyncIterator.prototype, c, function () { return this; }), e.AsyncIterator = AsyncIterator, e.async = function (t, r, n, o, i) { void 0 === i && (i = Promise); var a = new AsyncIterator(wrap(t, r, n, o), i); return e.isGeneratorFunction(r) ? a : a.next().then(function (t) { return t.done ? t.value : a.next(); }); }, defineIteratorMethods(g), define(g, u, "Generator"), define(g, a, function () { return this; }), define(g, "toString", function () { return "[object Generator]"; }), e.keys = function (t) { var e = Object(t), r = []; for (var n in e) r.push(n); return r.reverse(), function next() { for (; r.length;) { var t = r.pop(); if (t in e) return next.value = t, next.done = !1, next; } return next.done = !0, next; }; }, e.values = values, Context.prototype = { constructor: Context, reset: function reset(e) { if (this.prev = 0, this.next = 0, this.sent = this._sent = t, this.done = !1, this.delegate = null, this.method = "next", this.arg = t, this.tryEntries.forEach(resetTryEntry), !e) for (var r in this) "t" === r.charAt(0) && n.call(this, r) && !isNaN(+r.slice(1)) && (this[r] = t); }, stop: function stop() { this.done = !0; var t = this.tryEntries[0].completion; if ("throw" === t.type) throw t.arg; return this.rval; }, dispatchException: function dispatchException(e) { if (this.done) throw e; var r = this; function handle(n, o) { return a.type = "throw", a.arg = e, r.next = n, o && (r.method = "next", r.arg = t), !!o; } for (var o = this.tryEntries.length - 1; o >= 0; --o) { var i = this.tryEntries[o], a = i.completion; if ("root" === i.tryLoc) return handle("end"); if (i.tryLoc <= this.prev) { var c = n.call(i, "catchLoc"), u = n.call(i, "finallyLoc"); if (c && u) { if (this.prev < i.catchLoc) return handle(i.catchLoc, !0); if (this.prev < i.finallyLoc) return handle(i.finallyLoc); } else if (c) { if (this.prev < i.catchLoc) return handle(i.catchLoc, !0); } else { if (!u) throw Error("try statement without catch or finally"); if (this.prev < i.finallyLoc) return handle(i.finallyLoc); } } } }, abrupt: function abrupt(t, e) { for (var r = this.tryEntries.length - 1; r >= 0; --r) { var o = this.tryEntries[r]; if (o.tryLoc <= this.prev && n.call(o, "finallyLoc") && this.prev < o.finallyLoc) { var i = o; break; } } i && ("break" === t || "continue" === t) && i.tryLoc <= e && e <= i.finallyLoc && (i = null); var a = i ? i.completion : {}; return a.type = t, a.arg = e, i ? (this.method = "next", this.next = i.finallyLoc, y) : this.complete(a); }, complete: function complete(t, e) { if ("throw" === t.type) throw t.arg; return "break" === t.type || "continue" === t.type ? this.next = t.arg : "return" === t.type ? (this.rval = this.arg = t.arg, this.method = "return", this.next = "end") : "normal" === t.type && e && (this.next = e), y; }, finish: function finish(t) { for (var e = this.tryEntries.length - 1; e >= 0; --e) { var r = this.tryEntries[e]; if (r.finallyLoc === t) return this.complete(r.completion, r.afterLoc), resetTryEntry(r), y; } }, "catch": function _catch(t) { for (var e = this.tryEntries.length - 1; e >= 0; --e) { var r = this.tryEntries[e]; if (r.tryLoc === t) { var n = r.completion; if ("throw" === n.type) { var o = n.arg; resetTryEntry(r); } return o; } } throw Error("illegal catch attempt"); }, delegateYield: function delegateYield(e, r, n) { return this.delegate = { iterator: values(e), resultName: r, nextLoc: n }, "next" === this.method && (this.arg = t), y; } }, e; }
function asyncGeneratorStep(n, t, e, r, o, a, c) { try { var i = n[a](c), u = i.value; } catch (n) { return void e(n); } i.done ? t(u) : Promise.resolve(u).then(r, o); }
function _asyncToGenerator(n) { return function () { var t = this, e = arguments; return new Promise(function (r, o) { var a = n.apply(t, e); function _next(n) { asyncGeneratorStep(a, r, o, _next, _throw, "next", n); } function _throw(n) { asyncGeneratorStep(a, r, o, _next, _throw, "throw", n); } _next(void 0); }); }; }
var urlAuth = _setup_parameter.setupParameter.urlAuth;
// Set environment variable to ignore SSL certificate errors
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
var maxRetry = 3;
var httpsAgent = new https.Agent({
  rejectUnauthorized: false // Ignore self-signed certificates
});
function authenticate() {
  return _authenticate.apply(this, arguments);
}
function _authenticate() {
  _authenticate = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee2() {
    var baseUrl, authUrl, authBody, response, data;
    return _regeneratorRuntime().wrap(function _callee2$(_context2) {
      while (1) switch (_context2.prev = _context2.next) {
        case 0:
          _context2.prev = 0;
          baseUrl = _setup["default"][process.env.NODE_ENV].service.baseurl;
          authUrl = baseUrl + urlAuth;
          authBody = _setup["default"][process.env.NODE_ENV].authApi;
          _context2.next = 6;
          return (0, _nodeFetch["default"])(authUrl, {
            method: "POST",
            headers: {
              "Content-Type": "application/json"
            },
            agent: httpsAgent,
            body: JSON.stringify(authBody)
          });
        case 6:
          response = _context2.sent;
          _context2.next = 9;
          return response.json();
        case 9:
          data = _context2.sent;
          if (!(data.metadata.code >= 400)) {
            _context2.next = 21;
            break;
          }
          if (!(maxRetry > 0)) {
            _context2.next = 18;
            break;
          }
          _setup_parameter.setupParameter.maxRetry = maxRetry - 1;
          _context2.next = 15;
          return authenticate();
        case 15:
          return _context2.abrupt("return", _context2.sent);
        case 18:
          return _context2.abrupt("return", data);
        case 19:
          _context2.next = 24;
          break;
        case 21:
          _setup_parameter.setupParameter.headerToken = data.results[0].tokenKey;
          _setup_parameter.setupParameter.maxRetry = 3; // Reset retries to 3 upon successful authentication
          return _context2.abrupt("return", data);
        case 24:
          _context2.next = 35;
          break;
        case 26:
          _context2.prev = 26;
          _context2.t0 = _context2["catch"](0);
          if (!(maxRetry > 0)) {
            _context2.next = 33;
            break;
          }
          maxRetry = maxRetry - 1;
          _context2.next = 32;
          return authenticate();
        case 32:
          return _context2.abrupt("return", _context2.sent);
        case 33:
          console.log("ERROR Authentication: ", _context2.t0);
          throw _context2.t0;
        case 35:
        case "end":
          return _context2.stop();
      }
    }, _callee2, null, [[0, 26]]);
  }));
  return _authenticate.apply(this, arguments);
}
function sendRequest(_x) {
  return _sendRequest.apply(this, arguments);
}
function _sendRequest() {
  _sendRequest = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee3(_ref) {
    var _ref$urlParams, urlParams, _ref$requestBody, requestBody, _ref$requestParams, requestParams, _ref$method, method, baseUrl, url, params, response, data, metadata, results, getResponseAuth, code;
    return _regeneratorRuntime().wrap(function _callee3$(_context3) {
      while (1) switch (_context3.prev = _context3.next) {
        case 0:
          _ref$urlParams = _ref.urlParams, urlParams = _ref$urlParams === void 0 ? null : _ref$urlParams, _ref$requestBody = _ref.requestBody, requestBody = _ref$requestBody === void 0 ? null : _ref$requestBody, _ref$requestParams = _ref.requestParams, requestParams = _ref$requestParams === void 0 ? null : _ref$requestParams, _ref$method = _ref.method, method = _ref$method === void 0 ? null : _ref$method;
          _context3.prev = 1;
          baseUrl = _setup["default"][process.env.NODE_ENV].service.baseurl;
          url = baseUrl + urlParams; // If requestBody is provided, add it as query parameters to the URL
          if (requestParams) {
            params = new URLSearchParams(requestParams).toString();
            url += "?".concat(params);
          }
          _context3.next = 7;
          return (0, _nodeFetch["default"])(url, {
            method: method,
            headers: {
              "Content-Type": "application/json",
              token: "".concat(_setup_parameter.setupParameter.headerToken)
            },
            agent: httpsAgent,
            body: JSON.stringify(requestBody)
          });
        case 7:
          response = _context3.sent;
          _context3.next = 10;
          return response.json();
        case 10:
          data = _context3.sent;
          metadata = data.metadata, results = data.results;
          if (!(metadata.code === 401)) {
            _context3.next = 26;
            break;
          }
          _context3.next = 15;
          return authenticate();
        case 15:
          getResponseAuth = _context3.sent;
          if (!getResponseAuth.metadata) {
            _context3.next = 25;
            break;
          }
          code = getResponseAuth.metadata.code;
          if (!(code >= 400)) {
            _context3.next = 20;
            break;
          }
          return _context3.abrupt("return", getResponseAuth);
        case 20:
          _context3.next = 22;
          return sendRequest({
            urlParams: urlParams,
            requestBody: requestBody,
            requestParams: requestParams,
            method: method
          });
        case 22:
          return _context3.abrupt("return", _context3.sent);
        case 25:
          return _context3.abrupt("return", getResponseAuth);
        case 26:
          return _context3.abrupt("return", data);
        case 29:
          _context3.prev = 29;
          _context3.t0 = _context3["catch"](1);
          throw _context3.t0;
        case 32:
        case "end":
          return _context3.stop();
      }
    }, _callee3, null, [[1, 29]]);
  }));
  return _sendRequest.apply(this, arguments);
}
var apiGatewayRetry = 3;
function gatewayRunning(_x2) {
  return _gatewayRunning.apply(this, arguments);
}
function _gatewayRunning() {
  _gatewayRunning = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee4(_ref2) {
    var _ref2$urlParams, urlParams, _ref2$requestBody, requestBody, _ref2$requestParams, requestParams, _ref2$method, method, result, code;
    return _regeneratorRuntime().wrap(function _callee4$(_context4) {
      while (1) switch (_context4.prev = _context4.next) {
        case 0:
          _ref2$urlParams = _ref2.urlParams, urlParams = _ref2$urlParams === void 0 ? null : _ref2$urlParams, _ref2$requestBody = _ref2.requestBody, requestBody = _ref2$requestBody === void 0 ? null : _ref2$requestBody, _ref2$requestParams = _ref2.requestParams, requestParams = _ref2$requestParams === void 0 ? null : _ref2$requestParams, _ref2$method = _ref2.method, method = _ref2$method === void 0 ? null : _ref2$method;
          _context4.prev = 1;
          _context4.next = 4;
          return sendRequest({
            requestParams: requestParams,
            requestBody: requestBody,
            urlParams: urlParams,
            method: method
          });
        case 4:
          result = _context4.sent;
          if (!result.metadata) {
            _context4.next = 18;
            break;
          }
          code = result.metadata.code;
          if (!(code >= 400 && apiGatewayRetry > 0)) {
            _context4.next = 16;
            break;
          }
          apiGatewayRetry = apiGatewayRetry - 1;
          if (!(apiGatewayRetry == 1)) {
            _context4.next = 12;
            break;
          }
          _context4.next = 12;
          return delay(3000);
        case 12:
          _context4.next = 14;
          return gatewayRunning({
            urlParams: urlParams,
            requestBody: requestBody,
            requestParams: requestParams,
            method: method
          });
        case 14:
          _context4.next = 18;
          break;
        case 16:
          apiGatewayRetry = 3;
          return _context4.abrupt("return", result);
        case 18:
          return _context4.abrupt("return", result);
        case 21:
          _context4.prev = 21;
          _context4.t0 = _context4["catch"](1);
          throw _context4.t0;
        case 24:
        case "end":
          return _context4.stop();
      }
    }, _callee4, null, [[1, 21]]);
  }));
  return _gatewayRunning.apply(this, arguments);
}
function delay(ms) {
  return new Promise(function (resolve) {
    return setTimeout(resolve, ms);
  });
}
var apiGateway = exports.apiGateway = /*#__PURE__*/function () {
  var _ref4 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee(_ref3) {
    var _ref3$urlParams, urlParams, _ref3$requestBody, requestBody, _ref3$requestParams, requestParams, _ref3$method, method, result;
    return _regeneratorRuntime().wrap(function _callee$(_context) {
      while (1) switch (_context.prev = _context.next) {
        case 0:
          _ref3$urlParams = _ref3.urlParams, urlParams = _ref3$urlParams === void 0 ? null : _ref3$urlParams, _ref3$requestBody = _ref3.requestBody, requestBody = _ref3$requestBody === void 0 ? null : _ref3$requestBody, _ref3$requestParams = _ref3.requestParams, requestParams = _ref3$requestParams === void 0 ? null : _ref3$requestParams, _ref3$method = _ref3.method, method = _ref3$method === void 0 ? null : _ref3$method;
          _context.prev = 1;
          _context.next = 4;
          return gatewayRunning({
            urlParams: urlParams,
            requestBody: requestBody,
            requestParams: requestParams,
            method: method
          });
        case 4:
          result = _context.sent;
          return _context.abrupt("return", result);
        case 8:
          _context.prev = 8;
          _context.t0 = _context["catch"](1);
          throw _context.t0;
        case 11:
        case "end":
          return _context.stop();
      }
    }, _callee, null, [[1, 8]]);
  }));
  return function apiGateway(_x3) {
    return _ref4.apply(this, arguments);
  };
}();