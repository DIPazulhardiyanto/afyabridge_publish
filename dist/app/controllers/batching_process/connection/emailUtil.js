"use strict";

require("core-js/modules/es.object.define-property.js");
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.generateReportHtml = generateReportHtml;
exports.sendEmail = void 0;
require("core-js/modules/es.array.concat.js");
require("core-js/modules/es.array.join.js");
require("core-js/modules/es.array.map.js");
require("core-js/modules/es.array.reduce.js");
require("core-js/modules/es.object.to-string.js");
var _nodemailer = _interopRequireDefault(require("nodemailer"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
// const nodemailer = require('nodemailer');

// Configure nodemailer transport

var email_main = {
  host: "live.smtp.mailtrap.io",
  port: 587,
  auth: {
    user: "api",
    pass: "79901228ee63370d4e907aaf26914e3a"
  }
};
var email_main_dip = {
  host: "smtp.gmail.com",
  port: 465,
  auth: {
    user: "dipstaging@gmail.com",
    pass: "qyzcozbagkvmwjxt"
  }
};
var transporter = _nodemailer["default"].createTransport(email_main_dip);

// Function to send an email
var sendEmail = exports.sendEmail = function sendEmail(subject, html) {
  var mailOptions = {
    from: "".concat(process.env.NODE_ENV, "@demomailtrap.com"),
    to: "zwebcredential@gmail.com",
    subject: subject,
    html: html
  };
  transporter.sendMail(mailOptions, function (error, info) {
    if (error) {
      console.error("Error sending email:", error);
    }
  });
};

// Function to generate HTML report
function generateReportHtml(_ref) {
  var resultSend = _ref.resultSend,
    pageData = _ref.pageData,
    startTime = _ref.startTime,
    endTime = _ref.endTime,
    totalTime = _ref.totalTime,
    batchProcess = _ref.batchProcess,
    _ref$status = _ref.status,
    status = _ref$status === void 0 ? "FINISH" : _ref$status;
  var pageDataRows = "";
  var aggregatedStartDate = "";
  var aggregatedEndDate = "";
  if (pageData.length > 0) {
    // Extract the minimum startDate and maximum endDate
    aggregatedStartDate = pageData.reduce(function (min, p) {
      return p.startDate < min ? p.startDate : min;
    }, pageData[0].startDate);
    aggregatedEndDate = pageData.reduce(function (max, p) {
      return p.endDate > max ? p.endDate : max;
    }, pageData[0].endDate);
    pageDataRows = pageData.map(function (data) {
      return "\n                    <tr>\n                        <td>".concat(data.startDate, "</td>\n                        <td>").concat(data.endDate, "</td>\n                        <td>").concat(data.currentPage, "</td>\n                        <td>").concat(data.pageSize, "</td>\n                        <td>").concat(data.totalRemining, "</td>\n                        <td>").concat(data.totalPages, "</td>\n                    </tr>\n                ");
    }).join("");
  }
  return "\n        <h1>Batch Delivery Report</h1>\n        <table border=\"1\" cellspacing=\"0\" cellpadding=\"5\">\n            <tr>\n                <th>Server</th>\n                <td>".concat(process.env.NODE_ENV, "</td>\n            </tr>\n            <tr>\n                <th>Batching</th>\n                <td>").concat(batchProcess, "</td>\n            </tr>\n            <tr>\n                <th>Start Time</th>\n                <td>").concat(startTime, "</td>\n            </tr>\n            <tr>\n                <th>End Time</th>\n                <td>").concat(endTime, "</td>\n            </tr>\n            <tr>\n                <th>Total Time</th>\n                <td>").concat(totalTime, "</td>\n            </tr>\n            <tr>\n                <th>Execute Start</th>\n                <td>").concat(aggregatedStartDate, "</td>\n            </tr>\n            <tr>\n                <th>Execute End</th>\n                <td>").concat(aggregatedEndDate, "</td>\n            </tr>\n            <tr>\n                <th>Batch Status</th>\n                <td>").concat(status, "</td>\n            </tr>\n        </table>\n\n        <h2>Results</h2>\n        <table border=\"1\" cellspacing=\"0\" cellpadding=\"5\">\n            <tr>\n                <th>Success Encounters</th>\n                <td>").concat(resultSend.countSuccessEncounter, "</td>\n            </tr>\n            <tr>\n                <th>Failed Encounters</th>\n                <td>").concat(resultSend.countFailedEncounter, "</td>\n            </tr>\n            <tr>\n                <th>Success Conditions</th>\n                <td>").concat(resultSend.countSuccessCondition, "</td>\n            </tr>\n            <tr>\n                <th>Failed Conditions</th>\n                <td>").concat(resultSend.countFailedCondition, "</td>\n            </tr>\n            <tr>\n                <th>Total Data</th>\n                <td>").concat(resultSend.totalData, "</td>\n            </tr>\n        </table>\n        <h2>Page Data</h2>\n        <table border=\"1\">\n            <tr>\n                <th>Start Date</th>\n                <th>End Date</th>\n                <th>Current Page</th>\n                <th>Page Size</th>\n                <th>Total Remaining</th>\n                <th>Total Pages</th>\n            </tr>\n            ").concat(pageDataRows, "\n        </table>\n    ");
}