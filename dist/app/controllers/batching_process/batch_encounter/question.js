"use strict";

require("core-js/modules/es.object.define-property.js");
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.handleAnswer = exports.encounterQuestion = void 0;
require("core-js/modules/es.parse-int.js");
var _readline = _interopRequireDefault(require("readline"));
var _setup_parameter = require("./connection/setup_parameter.mjs");
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
var urlEncounter = _setup_parameter.setupParameter.urlEncounter;
var questions = ['Enter page number: ', 'Enter page size: ', 'Enter stopping page: ', 'Enter year: ', 'Enter month: '];
var answers = [];
var index = 0;
var clientSocket = null;
var encounterQuestion = exports.encounterQuestion = function encounterQuestion(socket, callback) {
  clientSocket = socket;
  askQuestion();
};
var askQuestion = function askQuestion() {
  if (index < questions.length) {
    clientSocket.write(questions[index] + "\n");
  } else {
    var result = {
      pageNumber: parseInt(answers[0]),
      pageSize: parseInt(answers[1]),
      stoppingPage: parseInt(answers[2]),
      years: parseInt(answers[3]),
      month: parseInt(answers[4]),
      method: "POST",
      urlParams: urlEncounter
    };
    callback(result);
    reset();
  }
};
var handleAnswer = exports.handleAnswer = function handleAnswer(answer, socket, callback) {
  answers.push(answer);
  index++;
  askQuestion();
};
var reset = function reset() {
  answers = [];
  index = 0;
  clientSocket = null;
};