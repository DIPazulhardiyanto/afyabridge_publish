"use strict";

function _typeof(o) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) { return typeof o; } : function (o) { return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o; }, _typeof(o); }
require("core-js/modules/es.symbol.js");
require("core-js/modules/es.symbol.description.js");
require("core-js/modules/es.symbol.async-iterator.js");
require("core-js/modules/es.symbol.iterator.js");
require("core-js/modules/es.symbol.to-string-tag.js");
require("core-js/modules/es.array.for-each.js");
require("core-js/modules/es.array.from.js");
require("core-js/modules/es.array.is-array.js");
require("core-js/modules/es.array.iterator.js");
require("core-js/modules/es.array.reverse.js");
require("core-js/modules/es.array.slice.js");
require("core-js/modules/es.function.name.js");
require("core-js/modules/es.json.to-string-tag.js");
require("core-js/modules/es.math.to-string-tag.js");
require("core-js/modules/es.object.create.js");
require("core-js/modules/es.object.define-property.js");
require("core-js/modules/es.object.get-prototype-of.js");
require("core-js/modules/es.object.set-prototype-of.js");
require("core-js/modules/es.regexp.exec.js");
require("core-js/modules/es.regexp.to-string.js");
require("core-js/modules/es.string.iterator.js");
require("core-js/modules/web.dom-collections.for-each.js");
require("core-js/modules/web.dom-collections.iterator.js");
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.runEncounterBatch = runEncounterBatch;
exports.runEncounterBatchByDate = runEncounterBatchByDate;
require("core-js/modules/es.array.concat.js");
require("core-js/modules/es.date.to-string.js");
require("core-js/modules/es.object.to-string.js");
require("core-js/modules/es.promise.js");
require("core-js/modules/es.string.starts-with.js");
var _callQuery = require("../../../libs/utils/callQuery.js");
var _emailUtil = require("../connection/emailUtil.js");
var _index = require("../connection/index.js");
var _helpers = _interopRequireDefault(require("../../../libs/utils/helpers.js"));
var _query = require("./query.js");
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
function _regeneratorRuntime() { "use strict"; /*! regenerator-runtime -- Copyright (c) 2014-present, Facebook, Inc. -- license (MIT): https://github.com/facebook/regenerator/blob/main/LICENSE */ _regeneratorRuntime = function _regeneratorRuntime() { return e; }; var t, e = {}, r = Object.prototype, n = r.hasOwnProperty, o = Object.defineProperty || function (t, e, r) { t[e] = r.value; }, i = "function" == typeof Symbol ? Symbol : {}, a = i.iterator || "@@iterator", c = i.asyncIterator || "@@asyncIterator", u = i.toStringTag || "@@toStringTag"; function define(t, e, r) { return Object.defineProperty(t, e, { value: r, enumerable: !0, configurable: !0, writable: !0 }), t[e]; } try { define({}, ""); } catch (t) { define = function define(t, e, r) { return t[e] = r; }; } function wrap(t, e, r, n) { var i = e && e.prototype instanceof Generator ? e : Generator, a = Object.create(i.prototype), c = new Context(n || []); return o(a, "_invoke", { value: makeInvokeMethod(t, r, c) }), a; } function tryCatch(t, e, r) { try { return { type: "normal", arg: t.call(e, r) }; } catch (t) { return { type: "throw", arg: t }; } } e.wrap = wrap; var h = "suspendedStart", l = "suspendedYield", f = "executing", s = "completed", y = {}; function Generator() {} function GeneratorFunction() {} function GeneratorFunctionPrototype() {} var p = {}; define(p, a, function () { return this; }); var d = Object.getPrototypeOf, v = d && d(d(values([]))); v && v !== r && n.call(v, a) && (p = v); var g = GeneratorFunctionPrototype.prototype = Generator.prototype = Object.create(p); function defineIteratorMethods(t) { ["next", "throw", "return"].forEach(function (e) { define(t, e, function (t) { return this._invoke(e, t); }); }); } function AsyncIterator(t, e) { function invoke(r, o, i, a) { var c = tryCatch(t[r], t, o); if ("throw" !== c.type) { var u = c.arg, h = u.value; return h && "object" == _typeof(h) && n.call(h, "__await") ? e.resolve(h.__await).then(function (t) { invoke("next", t, i, a); }, function (t) { invoke("throw", t, i, a); }) : e.resolve(h).then(function (t) { u.value = t, i(u); }, function (t) { return invoke("throw", t, i, a); }); } a(c.arg); } var r; o(this, "_invoke", { value: function value(t, n) { function callInvokeWithMethodAndArg() { return new e(function (e, r) { invoke(t, n, e, r); }); } return r = r ? r.then(callInvokeWithMethodAndArg, callInvokeWithMethodAndArg) : callInvokeWithMethodAndArg(); } }); } function makeInvokeMethod(e, r, n) { var o = h; return function (i, a) { if (o === f) throw Error("Generator is already running"); if (o === s) { if ("throw" === i) throw a; return { value: t, done: !0 }; } for (n.method = i, n.arg = a;;) { var c = n.delegate; if (c) { var u = maybeInvokeDelegate(c, n); if (u) { if (u === y) continue; return u; } } if ("next" === n.method) n.sent = n._sent = n.arg;else if ("throw" === n.method) { if (o === h) throw o = s, n.arg; n.dispatchException(n.arg); } else "return" === n.method && n.abrupt("return", n.arg); o = f; var p = tryCatch(e, r, n); if ("normal" === p.type) { if (o = n.done ? s : l, p.arg === y) continue; return { value: p.arg, done: n.done }; } "throw" === p.type && (o = s, n.method = "throw", n.arg = p.arg); } }; } function maybeInvokeDelegate(e, r) { var n = r.method, o = e.iterator[n]; if (o === t) return r.delegate = null, "throw" === n && e.iterator["return"] && (r.method = "return", r.arg = t, maybeInvokeDelegate(e, r), "throw" === r.method) || "return" !== n && (r.method = "throw", r.arg = new TypeError("The iterator does not provide a '" + n + "' method")), y; var i = tryCatch(o, e.iterator, r.arg); if ("throw" === i.type) return r.method = "throw", r.arg = i.arg, r.delegate = null, y; var a = i.arg; return a ? a.done ? (r[e.resultName] = a.value, r.next = e.nextLoc, "return" !== r.method && (r.method = "next", r.arg = t), r.delegate = null, y) : a : (r.method = "throw", r.arg = new TypeError("iterator result is not an object"), r.delegate = null, y); } function pushTryEntry(t) { var e = { tryLoc: t[0] }; 1 in t && (e.catchLoc = t[1]), 2 in t && (e.finallyLoc = t[2], e.afterLoc = t[3]), this.tryEntries.push(e); } function resetTryEntry(t) { var e = t.completion || {}; e.type = "normal", delete e.arg, t.completion = e; } function Context(t) { this.tryEntries = [{ tryLoc: "root" }], t.forEach(pushTryEntry, this), this.reset(!0); } function values(e) { if (e || "" === e) { var r = e[a]; if (r) return r.call(e); if ("function" == typeof e.next) return e; if (!isNaN(e.length)) { var o = -1, i = function next() { for (; ++o < e.length;) if (n.call(e, o)) return next.value = e[o], next.done = !1, next; return next.value = t, next.done = !0, next; }; return i.next = i; } } throw new TypeError(_typeof(e) + " is not iterable"); } return GeneratorFunction.prototype = GeneratorFunctionPrototype, o(g, "constructor", { value: GeneratorFunctionPrototype, configurable: !0 }), o(GeneratorFunctionPrototype, "constructor", { value: GeneratorFunction, configurable: !0 }), GeneratorFunction.displayName = define(GeneratorFunctionPrototype, u, "GeneratorFunction"), e.isGeneratorFunction = function (t) { var e = "function" == typeof t && t.constructor; return !!e && (e === GeneratorFunction || "GeneratorFunction" === (e.displayName || e.name)); }, e.mark = function (t) { return Object.setPrototypeOf ? Object.setPrototypeOf(t, GeneratorFunctionPrototype) : (t.__proto__ = GeneratorFunctionPrototype, define(t, u, "GeneratorFunction")), t.prototype = Object.create(g), t; }, e.awrap = function (t) { return { __await: t }; }, defineIteratorMethods(AsyncIterator.prototype), define(AsyncIterator.prototype, c, function () { return this; }), e.AsyncIterator = AsyncIterator, e.async = function (t, r, n, o, i) { void 0 === i && (i = Promise); var a = new AsyncIterator(wrap(t, r, n, o), i); return e.isGeneratorFunction(r) ? a : a.next().then(function (t) { return t.done ? t.value : a.next(); }); }, defineIteratorMethods(g), define(g, u, "Generator"), define(g, a, function () { return this; }), define(g, "toString", function () { return "[object Generator]"; }), e.keys = function (t) { var e = Object(t), r = []; for (var n in e) r.push(n); return r.reverse(), function next() { for (; r.length;) { var t = r.pop(); if (t in e) return next.value = t, next.done = !1, next; } return next.done = !0, next; }; }, e.values = values, Context.prototype = { constructor: Context, reset: function reset(e) { if (this.prev = 0, this.next = 0, this.sent = this._sent = t, this.done = !1, this.delegate = null, this.method = "next", this.arg = t, this.tryEntries.forEach(resetTryEntry), !e) for (var r in this) "t" === r.charAt(0) && n.call(this, r) && !isNaN(+r.slice(1)) && (this[r] = t); }, stop: function stop() { this.done = !0; var t = this.tryEntries[0].completion; if ("throw" === t.type) throw t.arg; return this.rval; }, dispatchException: function dispatchException(e) { if (this.done) throw e; var r = this; function handle(n, o) { return a.type = "throw", a.arg = e, r.next = n, o && (r.method = "next", r.arg = t), !!o; } for (var o = this.tryEntries.length - 1; o >= 0; --o) { var i = this.tryEntries[o], a = i.completion; if ("root" === i.tryLoc) return handle("end"); if (i.tryLoc <= this.prev) { var c = n.call(i, "catchLoc"), u = n.call(i, "finallyLoc"); if (c && u) { if (this.prev < i.catchLoc) return handle(i.catchLoc, !0); if (this.prev < i.finallyLoc) return handle(i.finallyLoc); } else if (c) { if (this.prev < i.catchLoc) return handle(i.catchLoc, !0); } else { if (!u) throw Error("try statement without catch or finally"); if (this.prev < i.finallyLoc) return handle(i.finallyLoc); } } } }, abrupt: function abrupt(t, e) { for (var r = this.tryEntries.length - 1; r >= 0; --r) { var o = this.tryEntries[r]; if (o.tryLoc <= this.prev && n.call(o, "finallyLoc") && this.prev < o.finallyLoc) { var i = o; break; } } i && ("break" === t || "continue" === t) && i.tryLoc <= e && e <= i.finallyLoc && (i = null); var a = i ? i.completion : {}; return a.type = t, a.arg = e, i ? (this.method = "next", this.next = i.finallyLoc, y) : this.complete(a); }, complete: function complete(t, e) { if ("throw" === t.type) throw t.arg; return "break" === t.type || "continue" === t.type ? this.next = t.arg : "return" === t.type ? (this.rval = this.arg = t.arg, this.method = "return", this.next = "end") : "normal" === t.type && e && (this.next = e), y; }, finish: function finish(t) { for (var e = this.tryEntries.length - 1; e >= 0; --e) { var r = this.tryEntries[e]; if (r.finallyLoc === t) return this.complete(r.completion, r.afterLoc), resetTryEntry(r), y; } }, "catch": function _catch(t) { for (var e = this.tryEntries.length - 1; e >= 0; --e) { var r = this.tryEntries[e]; if (r.tryLoc === t) { var n = r.completion; if ("throw" === n.type) { var o = n.arg; resetTryEntry(r); } return o; } } throw Error("illegal catch attempt"); }, delegateYield: function delegateYield(e, r, n) { return this.delegate = { iterator: values(e), resultName: r, nextLoc: n }, "next" === this.method && (this.arg = t), y; } }, e; }
function _createForOfIteratorHelper(r, e) { var t = "undefined" != typeof Symbol && r[Symbol.iterator] || r["@@iterator"]; if (!t) { if (Array.isArray(r) || (t = _unsupportedIterableToArray(r)) || e && r && "number" == typeof r.length) { t && (r = t); var _n = 0, F = function F() {}; return { s: F, n: function n() { return _n >= r.length ? { done: !0 } : { done: !1, value: r[_n++] }; }, e: function e(r) { throw r; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var o, a = !0, u = !1; return { s: function s() { t = t.call(r); }, n: function n() { var r = t.next(); return a = r.done, r; }, e: function e(r) { u = !0, o = r; }, f: function f() { try { a || null == t["return"] || t["return"](); } finally { if (u) throw o; } } }; }
function _unsupportedIterableToArray(r, a) { if (r) { if ("string" == typeof r) return _arrayLikeToArray(r, a); var t = {}.toString.call(r).slice(8, -1); return "Object" === t && r.constructor && (t = r.constructor.name), "Map" === t || "Set" === t ? Array.from(r) : "Arguments" === t || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(t) ? _arrayLikeToArray(r, a) : void 0; } }
function _arrayLikeToArray(r, a) { (null == a || a > r.length) && (a = r.length); for (var e = 0, n = Array(a); e < a; e++) n[e] = r[e]; return n; }
function asyncGeneratorStep(n, t, e, r, o, a, c) { try { var i = n[a](c), u = i.value; } catch (n) { return void e(n); } i.done ? t(u) : Promise.resolve(u).then(r, o); }
function _asyncToGenerator(n) { return function () { var t = this, e = arguments; return new Promise(function (r, o) { var a = n.apply(t, e); function _next(n) { asyncGeneratorStep(a, r, o, _next, _throw, "next", n); } function _throw(n) { asyncGeneratorStep(a, r, o, _next, _throw, "throw", n); } _next(void 0); }); }; }
function runEncounterBatch(_x) {
  return _runEncounterBatch.apply(this, arguments);
}
function _runEncounterBatch() {
  _runEncounterBatch = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee(_ref) {
    var _ref$pageNumber, pageNumber, _ref$pageSize, pageSize, _ref$stoppingPage, stoppingPage, _ref$stoppingMonth, stoppingMonth, _ref$years, years, _ref$month, month, _ref$method, method, _ref$urlParams, urlParams, _ref$runningPageNumbe, runningPageNumber, batchProcess, startTime, _helpers$getMonthDate, startDate, endDate, _encounterQuery$getEn, select, from, where, resultSend, pageData, resultDb, _helpers$getMonthDate2, _helpers$getMonthDate3, _iterator, _step, el, response, _response$metadata, code, message, _response$results, Encounter, Condition, endTimeError, totalTimeError, _reportHtml, _helpers$getMonthDate4, endTime, totalTime, reportHtml;
    return _regeneratorRuntime().wrap(function _callee$(_context) {
      while (1) switch (_context.prev = _context.next) {
        case 0:
          _ref$pageNumber = _ref.pageNumber, pageNumber = _ref$pageNumber === void 0 ? 1 : _ref$pageNumber, _ref$pageSize = _ref.pageSize, pageSize = _ref$pageSize === void 0 ? 10 : _ref$pageSize, _ref$stoppingPage = _ref.stoppingPage, stoppingPage = _ref$stoppingPage === void 0 ? null : _ref$stoppingPage, _ref$stoppingMonth = _ref.stoppingMonth, stoppingMonth = _ref$stoppingMonth === void 0 ? null : _ref$stoppingMonth, _ref$years = _ref.years, years = _ref$years === void 0 ? null : _ref$years, _ref$month = _ref.month, month = _ref$month === void 0 ? null : _ref$month, _ref$method = _ref.method, method = _ref$method === void 0 ? null : _ref$method, _ref$urlParams = _ref.urlParams, urlParams = _ref$urlParams === void 0 ? null : _ref$urlParams, _ref$runningPageNumbe = _ref.runningPageNumber, runningPageNumber = _ref$runningPageNumbe === void 0 ? 0 : _ref$runningPageNumbe;
          batchProcess = "Encounter & Condition to SatuSehat Production";
          startTime = new Date(); // Record start time
          _helpers$getMonthDate = (0, _helpers["default"])().getMonthDateRange(years, month), startDate = _helpers$getMonthDate.startDate, endDate = _helpers$getMonthDate.endDate;
          _encounterQuery$getEn = _query.encounterQuery.getEncounter_withPagination, select = _encounterQuery$getEn.select, from = _encounterQuery$getEn.from, where = _encounterQuery$getEn.where; //for testing loop
          // const { select, from, where } =
          //     encounterQuery.getEncounterForTemp_withPagination;
          resultSend = {
            countSuccessEncounter: 0,
            countFailedEncounter: 0,
            countSuccessCondition: 0,
            countFailedCondition: 0,
            totalData: 0
          };
          pageData = []; // Array to accumulate page data
        case 7:
          if (!true) {
            _context.next = 82;
            break;
          }
          runningPageNumber += 1;

          //enable after test
          _context.next = 11;
          return (0, _callQuery.queryWithPagination)({
            select: select,
            from: from,
            where: where({
              startDate: startDate,
              endDate: endDate
            }),
            orderByRowNumber: "bph.EnteredDate DESC",
            pageNumber: pageNumber,
            pageSize: pageSize
          });
        case 11:
          resultDb = _context.sent;
          if (!(resultDb.rows.length === 0)) {
            _context.next = 34;
            break;
          }
          if (!(stoppingMonth == null && month < 12 || month < stoppingMonth)) {
            _context.next = 23;
            break;
          }
          // Increment month if stoppingMonth is null or not yet reached
          runningPageNumber = 0;
          month += 1;
          pageNumber = 1; // Reset page number for the new month
          _helpers$getMonthDate2 = (0, _helpers["default"])().getMonthDateRange(years, month);
          startDate = _helpers$getMonthDate2.startDate;
          endDate = _helpers$getMonthDate2.endDate;
          return _context.abrupt("continue", 7);
        case 23:
          if (!(month < stoppingMonth)) {
            _context.next = 33;
            break;
          }
          // Increment month if stoppingMonth is null or not yet reached
          runningPageNumber = 0;
          month += 1;
          pageNumber = 1; // Reset page number for the new month
          _helpers$getMonthDate3 = (0, _helpers["default"])().getMonthDateRange(years, month);
          startDate = _helpers$getMonthDate3.startDate;
          endDate = _helpers$getMonthDate3.endDate;
          return _context.abrupt("continue", 7);
        case 33:
          return _context.abrupt("break", 82);
        case 34:
          _iterator = _createForOfIteratorHelper(resultDb.rows);
          _context.prev = 35;
          _iterator.s();
        case 37:
          if ((_step = _iterator.n()).done) {
            _context.next = 57;
            break;
          }
          el = _step.value;
          _context.prev = 39;
          _context.next = 42;
          return (0, _index.apiGateway)({
            method: method,
            urlParams: urlParams,
            requestParams: {
              episodeNumber: el.EpisodeNo
            }
          });
        case 42:
          response = _context.sent;
          if (response) {
            _response$metadata = response.metadata, code = _response$metadata.code, message = _response$metadata.message;
            if (code < 400) {
              _response$results = response.results, Encounter = _response$results.Encounter, Condition = _response$results.Condition;
              if (Encounter) {
                resultSend.countSuccessEncounter += Encounter.code < 400 ? 1 : 0;
                resultSend.countFailedEncounter += Encounter.code >= 400 ? 1 : 0;
              }
              if (Condition) {
                resultSend.countSuccessCondition += Condition.code < 400 ? 1 : 0;
                resultSend.countFailedCondition += Condition.code >= 400 ? 1 : 0;
              }
            } else {
              resultSend.countFailedEncounter += 1;
            }
          } else {
            resultSend.countFailedEncounter += 1;
          }
          _context.next = 55;
          break;
        case 46:
          _context.prev = 46;
          _context.t0 = _context["catch"](39);
          endTimeError = new Date(); // Record end time
          totalTimeError = endTimeError - startTime; // Calculate total time in milliseconds
          // Generate and send the final report email
          _reportHtml = (0, _emailUtil.generateReportHtml)({
            resultSend: resultSend,
            pageData: pageData,
            startTime: startTime,
            endTime: endTimeError,
            totalTime: (0, _helpers["default"])().getFormatTime(totalTimeError),
            batchProcess: batchProcess,
            status: "Not Finish !, " + "Error processing ".concat(el.EpisodeNo, ": ").concat(_context.t0.message)
          });
          resultSend.countFailedEncounter += 1;
          if (process.env.npm_lifecycle_event !== "dev") {
            (0, _emailUtil.sendEmail)("Encounter Batch Delivery Report", _reportHtml);
          }
          if (_context.t0 instanceof TypeError) {
            console.error("Network error or invalid URL:", _context.t0);
          } else if (_context.t0.message.startsWith("HTTP error")) {
            console.error("HTTP error occurred:", _context.t0.message);
          } else {
            console.error("Unexpected error:", _context.t0);
          }
          throw _context.t0;
        case 55:
          _context.next = 37;
          break;
        case 57:
          _context.next = 62;
          break;
        case 59:
          _context.prev = 59;
          _context.t1 = _context["catch"](35);
          _iterator.e(_context.t1);
        case 62:
          _context.prev = 62;
          _iterator.f();
          return _context.finish(62);
        case 65:
          pageData.push({
            startDate: startDate,
            endDate: endDate,
            currentPage: runningPageNumber,
            pageSize: resultDb.pageSize,
            totalRemining: resultDb.totalItems,
            totalPages: resultDb.totalPages
          });
          resultSend.totalData += resultDb.pageSize;
          if (!(resultDb.totalPages > 0)) {
            _context.next = 80;
            break;
          }
          if (!(runningPageNumber === stoppingPage)) {
            _context.next = 80;
            break;
          }
          if (!(stoppingMonth == null && month <= 12 || month < stoppingMonth)) {
            _context.next = 79;
            break;
          }
          // Increment month if stoppingMonth is null or not yet reached
          runningPageNumber = 0;
          month += 1;
          pageNumber = 1; // Reset page number for the new month
          _helpers$getMonthDate4 = (0, _helpers["default"])().getMonthDateRange(years, month);
          startDate = _helpers$getMonthDate4.startDate;
          endDate = _helpers$getMonthDate4.endDate;
          return _context.abrupt("continue", 7);
        case 79:
          return _context.abrupt("break", 82);
        case 80:
          _context.next = 7;
          break;
        case 82:
          endTime = new Date(); // Record end time
          totalTime = endTime - startTime; // Calculate total time in milliseconds
          // Generate and send the final report email
          reportHtml = (0, _emailUtil.generateReportHtml)({
            resultSend: resultSend,
            pageData: pageData,
            startTime: startTime,
            endTime: endTime,
            totalTime: (0, _helpers["default"])().getFormatTime(totalTime),
            batchProcess: batchProcess
          });
          if (process.env.npm_lifecycle_event !== "dev") {
            (0, _emailUtil.sendEmail)("Encounter Batch Delivery Report", reportHtml);
          }
          return _context.abrupt("return", {
            resultSend: resultSend,
            pageData: pageData,
            totalPageSize: pageData.length * pageSize
          });
        case 87:
        case "end":
          return _context.stop();
      }
    }, _callee, null, [[35, 59, 62, 65], [39, 46]]);
  }));
  return _runEncounterBatch.apply(this, arguments);
}
function runEncounterBatchByDate(_x2) {
  return _runEncounterBatchByDate.apply(this, arguments);
}
function _runEncounterBatchByDate() {
  _runEncounterBatchByDate = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee2(_ref2) {
    var _ref2$pageSize, pageSize, _ref2$stoppingPage, stoppingPage, _ref2$selectDate, selectDate, _ref2$method, method, _ref2$urlParams, urlParams, _ref2$runningPageNumb, runningPageNumber, batchProcess, startTime, _encounterQuery$getEn2, select, from, where, resultSend, pageData, resultDb, _iterator2, _step2, el, response, _response$metadata2, code, message, _response$results2, Encounter, Condition, endTimeError, totalTimeError, _reportHtml2, endTime, totalTime, reportHtml;
    return _regeneratorRuntime().wrap(function _callee2$(_context2) {
      while (1) switch (_context2.prev = _context2.next) {
        case 0:
          _ref2$pageSize = _ref2.pageSize, pageSize = _ref2$pageSize === void 0 ? 10 : _ref2$pageSize, _ref2$stoppingPage = _ref2.stoppingPage, stoppingPage = _ref2$stoppingPage === void 0 ? null : _ref2$stoppingPage, _ref2$selectDate = _ref2.selectDate, selectDate = _ref2$selectDate === void 0 ? null : _ref2$selectDate, _ref2$method = _ref2.method, method = _ref2$method === void 0 ? null : _ref2$method, _ref2$urlParams = _ref2.urlParams, urlParams = _ref2$urlParams === void 0 ? null : _ref2$urlParams, _ref2$runningPageNumb = _ref2.runningPageNumber, runningPageNumber = _ref2$runningPageNumb === void 0 ? 0 : _ref2$runningPageNumb;
          _context2.prev = 1;
          batchProcess = "Encounter & Condition to SatuSehat Production";
          startTime = new Date(); // Record start time
          _encounterQuery$getEn2 = _query.encounterQuery.getEncounter_withPagination_byDate, select = _encounterQuery$getEn2.select, from = _encounterQuery$getEn2.from, where = _encounterQuery$getEn2.where;
          resultSend = {
            countSuccessEncounter: 0,
            countFailedEncounter: 0,
            countSuccessCondition: 0,
            countFailedCondition: 0,
            totalData: 0
          };
          pageData = []; // Array to accumulate page data
        case 7:
          if (!true) {
            _context2.next = 55;
            break;
          }
          runningPageNumber += 1;

          //enable after test
          _context2.next = 11;
          return (0, _callQuery.queryWithPagination)({
            select: select,
            from: from,
            where: where({
              todayDate: selectDate
            }),
            orderByRowNumber: "bph.EnteredDate ASC",
            pageNumber: runningPageNumber,
            //auto pageNumber
            pageSize: pageSize
          });
        case 11:
          resultDb = _context2.sent;
          if (!(resultDb.rows.length === 0)) {
            _context2.next = 14;
            break;
          }
          return _context2.abrupt("break", 55);
        case 14:
          _iterator2 = _createForOfIteratorHelper(resultDb.rows);
          _context2.prev = 15;
          _iterator2.s();
        case 17:
          if ((_step2 = _iterator2.n()).done) {
            _context2.next = 37;
            break;
          }
          el = _step2.value;
          _context2.prev = 19;
          _context2.next = 22;
          return (0, _index.apiGateway)({
            method: method,
            urlParams: urlParams,
            requestParams: {
              episodeNumber: el.EpisodeNo
            }
          });
        case 22:
          response = _context2.sent;
          if (response) {
            _response$metadata2 = response.metadata, code = _response$metadata2.code, message = _response$metadata2.message;
            if (code < 400) {
              _response$results2 = response.results, Encounter = _response$results2.Encounter, Condition = _response$results2.Condition;
              if (Encounter) {
                resultSend.countSuccessEncounter += Encounter.code < 400 ? 1 : 0;
                resultSend.countFailedEncounter += Encounter.code >= 400 ? 1 : 0;
              }
              if (Condition) {
                resultSend.countSuccessCondition += Condition.code < 400 ? 1 : 0;
                resultSend.countFailedCondition += Condition.code >= 400 ? 1 : 0;
              }
            } else {
              resultSend.countFailedEncounter += 1;
            }
          } else {
            resultSend.countFailedEncounter += 1;
          }
          _context2.next = 35;
          break;
        case 26:
          _context2.prev = 26;
          _context2.t0 = _context2["catch"](19);
          endTimeError = new Date(); // Record end time
          totalTimeError = endTimeError - startTime; // Calculate total time in milliseconds
          // Generate and send the final report email
          _reportHtml2 = (0, _emailUtil.generateReportHtml)({
            resultSend: resultSend,
            pageData: pageData,
            startTime: (0, _helpers["default"])().dateTimeFormatUTC(startTime),
            endTime: (0, _helpers["default"])().dateTimeFormatUTC(endTimeError),
            totalTime: (0, _helpers["default"])().getFormatTime(totalTimeError),
            batchProcess: batchProcess,
            status: "Not Finish !, " + "Error processing ".concat(el.EpisodeNo, ": ").concat(_context2.t0.message)
          });
          resultSend.countFailedEncounter += 1;

          //Generate and send the final report email
          if (process.env.npm_lifecycle_event !== "dev") {
            (0, _emailUtil.sendEmail)("Encounter Batch Delivery Report", _reportHtml2);
          }
          if (_context2.t0 instanceof TypeError) {
            console.error("Network error or invalid URL:", _context2.t0);
          } else if (_context2.t0.message.startsWith("HTTP error")) {
            console.error("HTTP error occurred:", _context2.t0.message);
          } else {
            console.error("Unexpected error:", _context2.t0);
          }
          throw _context2.t0;
        case 35:
          _context2.next = 17;
          break;
        case 37:
          _context2.next = 42;
          break;
        case 39:
          _context2.prev = 39;
          _context2.t1 = _context2["catch"](15);
          _iterator2.e(_context2.t1);
        case 42:
          _context2.prev = 42;
          _iterator2.f();
          return _context2.finish(42);
        case 45:
          pageData.push({
            startDate: selectDate,
            endDate: selectDate,
            currentPage: runningPageNumber,
            pageSize: resultDb.pageSize,
            totalRemining: resultDb.totalItems,
            totalPages: resultDb.totalPages
          });
          resultSend.totalData += resultDb.pageSize;
          if (!(resultDb.totalPages > 0)) {
            _context2.next = 53;
            break;
          }
          if (!(runningPageNumber === stoppingPage)) {
            _context2.next = 52;
            break;
          }
          return _context2.abrupt("break", 55);
        case 52:
          return _context2.abrupt("continue", 7);
        case 53:
          _context2.next = 7;
          break;
        case 55:
          endTime = new Date(); // Record end time
          totalTime = endTime - startTime; // Calculate total time in milliseconds
          // Generate and send the final report email
          reportHtml = (0, _emailUtil.generateReportHtml)({
            resultSend: resultSend,
            pageData: pageData,
            startTime: (0, _helpers["default"])().dateTimeFormatUTC(startTime),
            endTime: (0, _helpers["default"])().dateTimeFormatUTC(endTime),
            totalTime: (0, _helpers["default"])().getFormatTime(totalTime),
            batchProcess: batchProcess
          });
          if (process.env.npm_lifecycle_event !== "dev") {
            (0, _emailUtil.sendEmail)("Encounter Batch Delivery Report", reportHtml);
          }
          return _context2.abrupt("return", {
            resultSend: resultSend,
            pageData: pageData,
            totalPageSize: pageData.length * pageSize,
            startTime: (0, _helpers["default"])().dateTimeFormatUTC(startTime),
            endTime: (0, _helpers["default"])().dateTimeFormatUTC(endTime),
            totalTime: (0, _helpers["default"])().getFormatTime(totalTime),
            batchProcess: batchProcess
          });
        case 62:
          _context2.prev = 62;
          _context2.t2 = _context2["catch"](1);
          throw new Error(_context2.t2);
        case 65:
        case "end":
          return _context2.stop();
      }
    }, _callee2, null, [[1, 62], [15, 39, 42, 45], [19, 26]]);
  }));
  return _runEncounterBatchByDate.apply(this, arguments);
}