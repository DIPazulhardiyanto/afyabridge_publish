"use strict";

require("core-js/modules/es.object.define-property.js");
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.encounterQuery = void 0;
require("core-js/modules/es.array.concat.js");
var encounterQuery = exports.encounterQuery = {
  getEncounter_withPagination: {
    select: "\n            SELECT\n            bph.NoBilling AS 'EpisodeNo'\n            ,HSN.dbo.FGetPerson_NoMR(bph.PartnerKeyHospital, bph.PersonKeyPatient) AS 'MedicalRecordNo'\n            ,HSN.dbo.FGetPerson_NoID(bph.PersonKeyPatient) AS 'PatientNik'\n            ,bph.NamaPasien\n            ,NULL AS 'PatientIdIhs'\n            ,HSN.dbo.FGetPerson_NoID(bph.DokterKey) AS 'PractitionerNik'\n            ,HSN.dbo.FGetPerson_Nama(bph.DokterKey) AS 'NamaDokter'\n            ,COALESCE(bph.PoliKey, bph.IdLokasi) AS 'LocationId'\n            ,'arrived' AS 'Status'\n            ,bph.EnteredDate\n            ,bph.PersonKeyPatient\n            ,bph.DokterKey AS personKeyDoctor\n        ",
    from: "\n             FROM HSN.dbo.BillingPenjaminHeader bph WITH (NOLOCK)\n        ",
    where: function where(_ref) {
      var startDate = _ref.startDate,
        endDate = _ref.endDate;
      return "\n            WHERE bph.PartnerKeyHospital = 1\n                AND NOT EXISTS (SELECT\n                    1\n                FROM SatuSehat.dbo.MasterEncounter me WITH (NOLOCK)\n                WHERE bph.NoBilling = me.EpisodeNumberHsn)\n                AND NOT EXISTS (SELECT\n                    1\n                FROM SatuSehat.dbo.SendFailedEncounter sfe WITH (NOLOCK)\n                WHERE bph.NoBilling = sfe.NoBilling)\n                AND NOT EXISTS (SELECT\n                1\n                FROM SatuSehat.dbo.MasterCondition mc WITH (NOLOCK)\n                WHERE bph.NoBilling = mc.EpisodeNoHsn)\n                AND NOT EXISTS (SELECT\n                1\n                FROM SatuSehat.dbo.SendFailedCondition sfc WITH (NOLOCK)\n                WHERE bph.NoBilling = sfc.EpisodeNoHsn)\n                AND (bph.ICD1Key IS NOT NULL OR bph.ICD3Key IS NOT NULL)\n                AND bph.EnteredDate BETWEEN '".concat(startDate, "' AND '").concat(endDate, "'\n                AND HSN.dbo.FGetPerson_NoID(bph.PersonKeyPatient) IS NOT NULL\n                AND HSN.dbo.FGetPerson_NoID(bph.PersonKeyPatient) <> ''\n                AND HSN.dbo.FGetPerson_NoID(bph.DokterKey) IS NOT NULL\n                AND HSN.dbo.FGetPerson_NoID(bph.DokterKey) <> ''\n        ");
    }
  },
  getEncounter_withPagination_byDate: {
    select: "\n            SELECT\n            bph.NoBilling AS 'EpisodeNo'\n            ,HSN.dbo.FGetPerson_NoMR(bph.PartnerKeyHospital, bph.PersonKeyPatient) AS 'MedicalRecordNo'\n            ,HSN.dbo.FGetPerson_NoID(bph.PersonKeyPatient) AS 'PatientNik'\n            ,bph.NamaPasien\n            ,NULL AS 'PatientIdIhs'\n            ,HSN.dbo.FGetPerson_NoID(bph.DokterKey) AS 'PractitionerNik'\n            ,HSN.dbo.FGetPerson_Nama(bph.DokterKey) AS 'NamaDokter'\n            ,COALESCE(bph.PoliKey, bph.IdLokasi) AS 'LocationId'\n            ,'arrived' AS 'Status'\n            ,bph.EnteredDate\n            ,bph.PersonKeyPatient\n            ,bph.DokterKey AS personKeyDoctor\n        ",
    from: "\n             FROM HSN.dbo.BillingPenjaminHeader bph WITH (NOLOCK)\n        ",
    where: function where(_ref2) {
      var todayDate = _ref2.todayDate;
      return "\n            WHERE bph.PartnerKeyHospital = 1\n                AND NOT EXISTS (SELECT\n                    1\n                FROM SatuSehat.dbo.MasterEncounter me WITH (NOLOCK)\n                WHERE bph.NoBilling = me.EpisodeNumberHsn)\n                AND NOT EXISTS (SELECT\n                    1\n                FROM SatuSehat.dbo.SendFailedEncounter sfe WITH (NOLOCK)\n                WHERE bph.NoBilling = sfe.NoBilling)\n                AND NOT EXISTS (SELECT\n                1\n                FROM SatuSehat.dbo.MasterCondition mc WITH (NOLOCK)\n                WHERE bph.NoBilling = mc.EpisodeNoHsn)\n                AND NOT EXISTS (SELECT\n                1\n                FROM SatuSehat.dbo.SendFailedCondition sfc WITH (NOLOCK)\n                WHERE bph.NoBilling = sfc.EpisodeNoHsn)\n                AND (bph.ICD1Key IS NOT NULL OR bph.ICD3Key IS NOT NULL)\n                AND CONVERT(DATE, bph.EnteredDate) = '".concat(todayDate, "'\n                AND HSN.dbo.FGetPerson_NoID(bph.PersonKeyPatient) IS NOT NULL\n                AND HSN.dbo.FGetPerson_NoID(bph.PersonKeyPatient) <> ''\n                AND HSN.dbo.FGetPerson_NoID(bph.DokterKey) IS NOT NULL\n                AND HSN.dbo.FGetPerson_NoID(bph.DokterKey) <> ''\n        ");
    }
  },
  insertEncounterTemp: function insertEncounterTemp(_ref3) {
    var _ref3$noBilling = _ref3.noBilling,
      noBilling = _ref3$noBilling === void 0 ? null : _ref3$noBilling;
    return "\n        INSERT INTO SatuSehat.dbo.Temp_Encounter\n            (\n            NoBilling\n            )\n            VALUES\n            (\n                '".concat(noBilling, "' -- NoBilling - varchar(50)\n            );\n        \n        ");
  },
  getEncounterForTemp_withPagination: {
    select: "SELECT\n                bph.NoBilling AS 'EpisodeNo',\n                bph.ICD1Key,\n                bph.ICD2Key,\n                bph.ICD3Key\n                ,HSN.dbo.FGetPerson_NoMR(bph.PartnerKeyHospital, bph.PersonKeyPatient) AS 'MedicalRecordNo'\n                ,HSN.dbo.FGetPerson_NoID(bph.PersonKeyPatient) AS 'PatientNik'\n                ,bph.NamaPasien\n                ,NULL AS 'PatientIdIhs'\n                ,HSN.dbo.FGetPerson_NoID(bph.DokterKey) AS 'PractitionerNik'\n                ,HSN.dbo.FGetPerson_Nama(bph.DokterKey) AS 'NamaDokter'\n                ,COALESCE(bph.PoliKey, bph.IdLokasi) AS 'LocationId'\n                ,'arrived' AS 'Status'\n                ,bph.EnteredDate\n                ,bph.PersonKeyPatient\n                ,bph.DokterKey AS personKeyDoctor",
    from: "FROM HSN.dbo.BillingPenjaminHeader bph WITH (NOLOCK)",
    where: function where(_ref4) {
      var startDate = _ref4.startDate,
        endDate = _ref4.endDate;
      return "\n             WHERE bph.PartnerKeyHospital = 1\n                AND NOT EXISTS (SELECT\n                    1\n                FROM SatuSehat.dbo.Temp_Encounter me WITH (NOLOCK)\n                WHERE me.NoBilling = bph.NoBilling)\n                AND bph.EnteredDate BETWEEN '".concat(startDate, "' AND '").concat(endDate, "'\n            ");
    }
  }
};