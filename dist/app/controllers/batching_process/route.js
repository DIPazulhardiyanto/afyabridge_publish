"use strict";

require("core-js/modules/es.object.define-property.js");
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _express = require("express");
var _controllers = _interopRequireDefault(require("./controllers"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
var baseCronPath = "/batch/cron";
var runEncounterCron = baseCronPath + '/encounter';
var controller = (0, _controllers["default"])();
var routes = (0, _express.Router)();
routes.route(runEncounterCron).post(controller.runEncounter);
var _default = exports["default"] = routes;