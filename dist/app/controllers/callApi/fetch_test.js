"use strict";

require("core-js/modules/es.array.concat.js");
require("core-js/modules/es.array.iterator.js");
require("core-js/modules/es.date.to-string.js");
require("core-js/modules/es.object.to-string.js");
require("core-js/modules/es.regexp.to-string.js");
require("core-js/modules/es.string.iterator.js");
require("core-js/modules/web.dom-collections.iterator.js");
require("core-js/modules/web.url-search-params.js");
var fetch = require('node-fetch'); // Not needed in Node.js v18+

var url = 'https://api-satusehat-stg.dto.kemkes.go.id/oauth2/v1/accesstoken';
var params = new URLSearchParams({
  grant_type: 'client_credentials'
});
var data = {
  client_id: 'enURVy2FkaAwUAG9xUDrNnDPepfLRYAYGKGW4yz6tLrmXMXb',
  client_secret: '0h7gtFKTkDyGLwsJAGp3wJ8Pez0OdAubBAck6SoSzqAm1WH2hPrOOrpLcGcCrXrL'
};
fetch("".concat(url, "?").concat(params.toString()), {
  method: 'POST',
  headers: {
    'Content-Type': 'application/x-www-form-urlencoded'
  },
  body: new URLSearchParams(data),
  timeout: 15000 // Timeout value (in milliseconds)
}).then(function (response) {
  return response.json();
}).then(function (result) {
  console.log('Success:', result);
})["catch"](function (error) {
  console.error('Error:', error);
});