"use strict";

require("core-js/modules/es.symbol.js");
require("core-js/modules/es.symbol.description.js");
require("core-js/modules/es.symbol.async-iterator.js");
require("core-js/modules/es.symbol.iterator.js");
require("core-js/modules/es.symbol.to-primitive.js");
require("core-js/modules/es.symbol.to-string-tag.js");
require("core-js/modules/es.array.filter.js");
require("core-js/modules/es.array.from.js");
require("core-js/modules/es.array.reverse.js");
require("core-js/modules/es.array.slice.js");
require("core-js/modules/es.date.to-primitive.js");
require("core-js/modules/es.date.to-string.js");
require("core-js/modules/es.json.to-string-tag.js");
require("core-js/modules/es.math.to-string-tag.js");
require("core-js/modules/es.number.constructor.js");
require("core-js/modules/es.object.create.js");
require("core-js/modules/es.object.define-properties.js");
require("core-js/modules/es.object.define-property.js");
require("core-js/modules/es.object.get-own-property-descriptor.js");
require("core-js/modules/es.object.get-own-property-descriptors.js");
require("core-js/modules/es.object.get-prototype-of.js");
require("core-js/modules/es.object.set-prototype-of.js");
require("core-js/modules/es.regexp.to-string.js");
require("core-js/modules/es.weak-map.js");
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = callApiRepository;
require("core-js/modules/es.array.find.js");
require("core-js/modules/es.array.find-index.js");
require("core-js/modules/es.array.for-each.js");
require("core-js/modules/es.array.includes.js");
require("core-js/modules/es.array.is-array.js");
require("core-js/modules/es.array.iterator.js");
require("core-js/modules/es.array.map.js");
require("core-js/modules/es.date.to-json.js");
require("core-js/modules/es.function.name.js");
require("core-js/modules/es.object.keys.js");
require("core-js/modules/es.object.to-string.js");
require("core-js/modules/es.promise.js");
require("core-js/modules/es.regexp.exec.js");
require("core-js/modules/es.string.includes.js");
require("core-js/modules/es.string.iterator.js");
require("core-js/modules/es.string.replace.js");
require("core-js/modules/web.dom-collections.for-each.js");
require("core-js/modules/web.dom-collections.iterator.js");
require("core-js/modules/web.timers.js");
require("core-js/modules/web.url.js");
require("core-js/modules/web.url.to-json.js");
require("core-js/modules/web.url-search-params.js");
var _repository = _interopRequireDefault(require("../environtment/repository"));
var _responseApi = require("../../libs/utils/responseApi");
var _axios = _interopRequireDefault(require("axios"));
var _helpers = _interopRequireDefault(require("../../libs/utils/helpers"));
var _repository2 = _interopRequireDefault(require("../integrationResource/repository"));
var _nodeUrl = _interopRequireDefault(require("node:url"));
var _helpers_maprequest_body = require("../../libs/utils/helpers_maprequest_body");
var _repository3 = _interopRequireDefault(require("../environmentAccess/repository"));
var _nodeFetch = _interopRequireDefault(require("node-fetch"));
var https = _interopRequireWildcard(require("https"));
var http = _interopRequireWildcard(require("http"));
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != _typeof(e) && "function" != typeof e) return { "default": e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && {}.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n["default"] = e, t && t.set(e, n), n; }
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
function ownKeys(e, r) { var t = Object.keys(e); if (Object.getOwnPropertySymbols) { var o = Object.getOwnPropertySymbols(e); r && (o = o.filter(function (r) { return Object.getOwnPropertyDescriptor(e, r).enumerable; })), t.push.apply(t, o); } return t; }
function _objectSpread(e) { for (var r = 1; r < arguments.length; r++) { var t = null != arguments[r] ? arguments[r] : {}; r % 2 ? ownKeys(Object(t), !0).forEach(function (r) { _defineProperty(e, r, t[r]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys(Object(t)).forEach(function (r) { Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r)); }); } return e; }
function _defineProperty(e, r, t) { return (r = _toPropertyKey(r)) in e ? Object.defineProperty(e, r, { value: t, enumerable: !0, configurable: !0, writable: !0 }) : e[r] = t, e; }
function _toPropertyKey(t) { var i = _toPrimitive(t, "string"); return "symbol" == _typeof(i) ? i : i + ""; }
function _toPrimitive(t, r) { if ("object" != _typeof(t) || !t) return t; var e = t[Symbol.toPrimitive]; if (void 0 !== e) { var i = e.call(t, r || "default"); if ("object" != _typeof(i)) return i; throw new TypeError("@@toPrimitive must return a primitive value."); } return ("string" === r ? String : Number)(t); }
function _regeneratorRuntime() { "use strict"; /*! regenerator-runtime -- Copyright (c) 2014-present, Facebook, Inc. -- license (MIT): https://github.com/facebook/regenerator/blob/main/LICENSE */ _regeneratorRuntime = function _regeneratorRuntime() { return e; }; var t, e = {}, r = Object.prototype, n = r.hasOwnProperty, o = Object.defineProperty || function (t, e, r) { t[e] = r.value; }, i = "function" == typeof Symbol ? Symbol : {}, a = i.iterator || "@@iterator", c = i.asyncIterator || "@@asyncIterator", u = i.toStringTag || "@@toStringTag"; function define(t, e, r) { return Object.defineProperty(t, e, { value: r, enumerable: !0, configurable: !0, writable: !0 }), t[e]; } try { define({}, ""); } catch (t) { define = function define(t, e, r) { return t[e] = r; }; } function wrap(t, e, r, n) { var i = e && e.prototype instanceof Generator ? e : Generator, a = Object.create(i.prototype), c = new Context(n || []); return o(a, "_invoke", { value: makeInvokeMethod(t, r, c) }), a; } function tryCatch(t, e, r) { try { return { type: "normal", arg: t.call(e, r) }; } catch (t) { return { type: "throw", arg: t }; } } e.wrap = wrap; var h = "suspendedStart", l = "suspendedYield", f = "executing", s = "completed", y = {}; function Generator() {} function GeneratorFunction() {} function GeneratorFunctionPrototype() {} var p = {}; define(p, a, function () { return this; }); var d = Object.getPrototypeOf, v = d && d(d(values([]))); v && v !== r && n.call(v, a) && (p = v); var g = GeneratorFunctionPrototype.prototype = Generator.prototype = Object.create(p); function defineIteratorMethods(t) { ["next", "throw", "return"].forEach(function (e) { define(t, e, function (t) { return this._invoke(e, t); }); }); } function AsyncIterator(t, e) { function invoke(r, o, i, a) { var c = tryCatch(t[r], t, o); if ("throw" !== c.type) { var u = c.arg, h = u.value; return h && "object" == _typeof(h) && n.call(h, "__await") ? e.resolve(h.__await).then(function (t) { invoke("next", t, i, a); }, function (t) { invoke("throw", t, i, a); }) : e.resolve(h).then(function (t) { u.value = t, i(u); }, function (t) { return invoke("throw", t, i, a); }); } a(c.arg); } var r; o(this, "_invoke", { value: function value(t, n) { function callInvokeWithMethodAndArg() { return new e(function (e, r) { invoke(t, n, e, r); }); } return r = r ? r.then(callInvokeWithMethodAndArg, callInvokeWithMethodAndArg) : callInvokeWithMethodAndArg(); } }); } function makeInvokeMethod(e, r, n) { var o = h; return function (i, a) { if (o === f) throw Error("Generator is already running"); if (o === s) { if ("throw" === i) throw a; return { value: t, done: !0 }; } for (n.method = i, n.arg = a;;) { var c = n.delegate; if (c) { var u = maybeInvokeDelegate(c, n); if (u) { if (u === y) continue; return u; } } if ("next" === n.method) n.sent = n._sent = n.arg;else if ("throw" === n.method) { if (o === h) throw o = s, n.arg; n.dispatchException(n.arg); } else "return" === n.method && n.abrupt("return", n.arg); o = f; var p = tryCatch(e, r, n); if ("normal" === p.type) { if (o = n.done ? s : l, p.arg === y) continue; return { value: p.arg, done: n.done }; } "throw" === p.type && (o = s, n.method = "throw", n.arg = p.arg); } }; } function maybeInvokeDelegate(e, r) { var n = r.method, o = e.iterator[n]; if (o === t) return r.delegate = null, "throw" === n && e.iterator["return"] && (r.method = "return", r.arg = t, maybeInvokeDelegate(e, r), "throw" === r.method) || "return" !== n && (r.method = "throw", r.arg = new TypeError("The iterator does not provide a '" + n + "' method")), y; var i = tryCatch(o, e.iterator, r.arg); if ("throw" === i.type) return r.method = "throw", r.arg = i.arg, r.delegate = null, y; var a = i.arg; return a ? a.done ? (r[e.resultName] = a.value, r.next = e.nextLoc, "return" !== r.method && (r.method = "next", r.arg = t), r.delegate = null, y) : a : (r.method = "throw", r.arg = new TypeError("iterator result is not an object"), r.delegate = null, y); } function pushTryEntry(t) { var e = { tryLoc: t[0] }; 1 in t && (e.catchLoc = t[1]), 2 in t && (e.finallyLoc = t[2], e.afterLoc = t[3]), this.tryEntries.push(e); } function resetTryEntry(t) { var e = t.completion || {}; e.type = "normal", delete e.arg, t.completion = e; } function Context(t) { this.tryEntries = [{ tryLoc: "root" }], t.forEach(pushTryEntry, this), this.reset(!0); } function values(e) { if (e || "" === e) { var r = e[a]; if (r) return r.call(e); if ("function" == typeof e.next) return e; if (!isNaN(e.length)) { var o = -1, i = function next() { for (; ++o < e.length;) if (n.call(e, o)) return next.value = e[o], next.done = !1, next; return next.value = t, next.done = !0, next; }; return i.next = i; } } throw new TypeError(_typeof(e) + " is not iterable"); } return GeneratorFunction.prototype = GeneratorFunctionPrototype, o(g, "constructor", { value: GeneratorFunctionPrototype, configurable: !0 }), o(GeneratorFunctionPrototype, "constructor", { value: GeneratorFunction, configurable: !0 }), GeneratorFunction.displayName = define(GeneratorFunctionPrototype, u, "GeneratorFunction"), e.isGeneratorFunction = function (t) { var e = "function" == typeof t && t.constructor; return !!e && (e === GeneratorFunction || "GeneratorFunction" === (e.displayName || e.name)); }, e.mark = function (t) { return Object.setPrototypeOf ? Object.setPrototypeOf(t, GeneratorFunctionPrototype) : (t.__proto__ = GeneratorFunctionPrototype, define(t, u, "GeneratorFunction")), t.prototype = Object.create(g), t; }, e.awrap = function (t) { return { __await: t }; }, defineIteratorMethods(AsyncIterator.prototype), define(AsyncIterator.prototype, c, function () { return this; }), e.AsyncIterator = AsyncIterator, e.async = function (t, r, n, o, i) { void 0 === i && (i = Promise); var a = new AsyncIterator(wrap(t, r, n, o), i); return e.isGeneratorFunction(r) ? a : a.next().then(function (t) { return t.done ? t.value : a.next(); }); }, defineIteratorMethods(g), define(g, u, "Generator"), define(g, a, function () { return this; }), define(g, "toString", function () { return "[object Generator]"; }), e.keys = function (t) { var e = Object(t), r = []; for (var n in e) r.push(n); return r.reverse(), function next() { for (; r.length;) { var t = r.pop(); if (t in e) return next.value = t, next.done = !1, next; } return next.done = !0, next; }; }, e.values = values, Context.prototype = { constructor: Context, reset: function reset(e) { if (this.prev = 0, this.next = 0, this.sent = this._sent = t, this.done = !1, this.delegate = null, this.method = "next", this.arg = t, this.tryEntries.forEach(resetTryEntry), !e) for (var r in this) "t" === r.charAt(0) && n.call(this, r) && !isNaN(+r.slice(1)) && (this[r] = t); }, stop: function stop() { this.done = !0; var t = this.tryEntries[0].completion; if ("throw" === t.type) throw t.arg; return this.rval; }, dispatchException: function dispatchException(e) { if (this.done) throw e; var r = this; function handle(n, o) { return a.type = "throw", a.arg = e, r.next = n, o && (r.method = "next", r.arg = t), !!o; } for (var o = this.tryEntries.length - 1; o >= 0; --o) { var i = this.tryEntries[o], a = i.completion; if ("root" === i.tryLoc) return handle("end"); if (i.tryLoc <= this.prev) { var c = n.call(i, "catchLoc"), u = n.call(i, "finallyLoc"); if (c && u) { if (this.prev < i.catchLoc) return handle(i.catchLoc, !0); if (this.prev < i.finallyLoc) return handle(i.finallyLoc); } else if (c) { if (this.prev < i.catchLoc) return handle(i.catchLoc, !0); } else { if (!u) throw Error("try statement without catch or finally"); if (this.prev < i.finallyLoc) return handle(i.finallyLoc); } } } }, abrupt: function abrupt(t, e) { for (var r = this.tryEntries.length - 1; r >= 0; --r) { var o = this.tryEntries[r]; if (o.tryLoc <= this.prev && n.call(o, "finallyLoc") && this.prev < o.finallyLoc) { var i = o; break; } } i && ("break" === t || "continue" === t) && i.tryLoc <= e && e <= i.finallyLoc && (i = null); var a = i ? i.completion : {}; return a.type = t, a.arg = e, i ? (this.method = "next", this.next = i.finallyLoc, y) : this.complete(a); }, complete: function complete(t, e) { if ("throw" === t.type) throw t.arg; return "break" === t.type || "continue" === t.type ? this.next = t.arg : "return" === t.type ? (this.rval = this.arg = t.arg, this.method = "return", this.next = "end") : "normal" === t.type && e && (this.next = e), y; }, finish: function finish(t) { for (var e = this.tryEntries.length - 1; e >= 0; --e) { var r = this.tryEntries[e]; if (r.finallyLoc === t) return this.complete(r.completion, r.afterLoc), resetTryEntry(r), y; } }, "catch": function _catch(t) { for (var e = this.tryEntries.length - 1; e >= 0; --e) { var r = this.tryEntries[e]; if (r.tryLoc === t) { var n = r.completion; if ("throw" === n.type) { var o = n.arg; resetTryEntry(r); } return o; } } throw Error("illegal catch attempt"); }, delegateYield: function delegateYield(e, r, n) { return this.delegate = { iterator: values(e), resultName: r, nextLoc: n }, "next" === this.method && (this.arg = t), y; } }, e; }
function _typeof(o) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) { return typeof o; } : function (o) { return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o; }, _typeof(o); }
function _createForOfIteratorHelper(r, e) { var t = "undefined" != typeof Symbol && r[Symbol.iterator] || r["@@iterator"]; if (!t) { if (Array.isArray(r) || (t = _unsupportedIterableToArray(r)) || e && r && "number" == typeof r.length) { t && (r = t); var _n = 0, F = function F() {}; return { s: F, n: function n() { return _n >= r.length ? { done: !0 } : { done: !1, value: r[_n++] }; }, e: function e(r) { throw r; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var o, a = !0, u = !1; return { s: function s() { t = t.call(r); }, n: function n() { var r = t.next(); return a = r.done, r; }, e: function e(r) { u = !0, o = r; }, f: function f() { try { a || null == t["return"] || t["return"](); } finally { if (u) throw o; } } }; }
function _unsupportedIterableToArray(r, a) { if (r) { if ("string" == typeof r) return _arrayLikeToArray(r, a); var t = {}.toString.call(r).slice(8, -1); return "Object" === t && r.constructor && (t = r.constructor.name), "Map" === t || "Set" === t ? Array.from(r) : "Arguments" === t || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(t) ? _arrayLikeToArray(r, a) : void 0; } }
function _arrayLikeToArray(r, a) { (null == a || a > r.length) && (a = r.length); for (var e = 0, n = Array(a); e < a; e++) n[e] = r[e]; return n; }
function asyncGeneratorStep(n, t, e, r, o, a, c) { try { var i = n[a](c), u = i.value; } catch (n) { return void e(n); } i.done ? t(u) : Promise.resolve(u).then(r, o); }
function _asyncToGenerator(n) { return function () { var t = this, e = arguments; return new Promise(function (r, o) { var a = n.apply(t, e); function _next(n) { asyncGeneratorStep(a, r, o, _next, _throw, "next", n); } function _throw(n) { asyncGeneratorStep(a, r, o, _next, _throw, "throw", n); } _next(void 0); }); }; }
var envRepository = (0, _repository["default"])();
var environmentAccessRepository = (0, _repository3["default"])();
var helpers = (0, _helpers["default"])();
function callApiRepository() {
  //todo callAuth akan deprecated (Tidak Digunakan Lagi) saat call auth access selesai development
  var callAuth = /*#__PURE__*/function () {
    var _ref = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee(req) {
      var _searchValueByKey, envByParams, _message, getEnvLogin, _getEnvLogin$results, isLogin, authentication, variable, getVariable, indexVariable, objSearch, hostUrl, setPath, header, selectTypeBodyRequest, getObject, objResult, params, options, requestMode, callAxios, getErrorFromResponseSuccess, getObjectAll, getFindToken, tokenResponse, token_type;
      return _regeneratorRuntime().wrap(function _callee$(_context) {
        while (1) switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            //SetAutoDetectValueAuthentication
            _searchValueByKey = function searchValueByKey(targetObj) {
              var matches = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
              if (targetObj != null) {
                if (Array.isArray(targetObj)) {
                  var _iterator = _createForOfIteratorHelper(targetObj),
                    _step;
                  try {
                    for (_iterator.s(); !(_step = _iterator.n()).done;) {
                      var arrayItem = _step.value;
                      _searchValueByKey(arrayItem, targetObj, matches);
                    }
                  } catch (err) {
                    _iterator.e(err);
                  } finally {
                    _iterator.f();
                  }
                } else if (_typeof(targetObj) == "object") {
                  for (var _i = 0, _Object$keys = Object.keys(targetObj); _i < _Object$keys.length; _i++) {
                    var key = _Object$keys[_i];
                    if (targetObj.value) {
                      var dataChanges = objSearch(targetObj.value);
                      if (dataChanges) {
                        targetObj.value = dataChanges;
                      }
                    } else {
                      _searchValueByKey(targetObj[key], matches);
                    }
                  }
                  matches = targetObj;
                }
              }
              return matches;
            };
            envByParams = null;
            if (req.hasOwnProperty("query") && Object.keys(req.query).length > 0) {
              Object.keys(req.query).map(function (el) {
                if (el === "env") {
                  envByParams = req.query["".concat(el)];
                }
              });
            }
            _context.next = 6;
            return envRepository.getIsLoginEnv(req);
          case 6:
            getEnvLogin = _context.sent;
            if (!(getEnvLogin.code === _responseApi.statusHttpCode.badRequest400.code)) {
              _context.next = 9;
              break;
            }
            return _context.abrupt("return", (0, _responseApi.result)({
              message: getEnvLogin.message,
              statusCode: _responseApi.statusHttpCode.badRequest400
            }));
          case 9:
            if (getEnvLogin.results.variable) {
              _context.next = 11;
              break;
            }
            return _context.abrupt("return", (0, _responseApi.result)({
              message: "Variable is empty",
              statusCode: _responseApi.statusHttpCode.badRequest400
            }));
          case 11:
            _getEnvLogin$results = getEnvLogin.results, isLogin = _getEnvLogin$results.isLogin, authentication = _getEnvLogin$results.authentication, variable = _getEnvLogin$results.variable;
            getVariable = {};
            if (envByParams) {
              getVariable = variable.findIndex(function (x) {
                return x.varName.toLowerCase() === envByParams.toLowerCase();
              });
            } else {
              getVariable = variable.findIndex(function (x) {
                return x.status === 1;
              });
            }
            if (!(getVariable < 0)) {
              _context.next = 16;
              break;
            }
            return _context.abrupt("return", (0, _responseApi.result)({
              message: "Variable is not active",
              statusCode: _responseApi.statusHttpCode.badRequest400
            }));
          case 16:
            indexVariable = getVariable;
            getVariable = variable[indexVariable];
            if (!(getVariable.useLogin && !getVariable.authentication)) {
              _context.next = 20;
              break;
            }
            return _context.abrupt("return", (0, _responseApi.result)({
              message: "Authentication is can't null",
              statusCode: _responseApi.statusHttpCode.badRequest400
            }));
          case 20:
            //Set Authentication From Variable Submit
            authentication = getVariable.authentication;
            objSearch = function objSearch(targetObj) {
              var getFind = getVariable.code.find(function (x) {
                return x.key === targetObj;
              });
              if (getFind) {
                return getFind.value;
              }
            };
            _context.next = 24;
            return _searchValueByKey(authentication);
          case 24:
            authentication = _context.sent;
            hostUrl = getVariable.code.find(function (el) {
              return el.key === authentication.variableCodeKey.value.replace(/#/g, "");
            }); //DI cek dulu jika authentication.variableCodeKey tidak di trigger variable #base_url# replace akan undefined
            hostUrl = !hostUrl ? authentication.variableCodeKey.value.length > 0 && authentication.variableCodeKey.value ? {
              value: authentication.variableCodeKey.value
            } : null : hostUrl;
            if (hostUrl) {
              _context.next = 29;
              break;
            }
            return _context.abrupt("return", (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.notFound404,
              message: "Host url not found please check your env host"
            }));
          case 29:
            setPath = hostUrl.value + authentication.path;
            header = {}; //Get Header
            if (authentication.header.length > 0) {
              authentication.header.map(function (el) {
                header[el.key] = el.value;
              });
            }
            selectTypeBodyRequest = authentication.bodyRequest.mode;
            getObject = [];
            _context.t0 = selectTypeBodyRequest;
            _context.next = _context.t0 === "raw" ? 37 : _context.t0 === "urlencoded" ? 39 : _context.t0 === "formdata" ? 41 : 43;
            break;
          case 37:
            getObject = authentication.bodyRequest.rawJson;
            return _context.abrupt("break", 43);
          case 39:
            getObject = authentication.bodyRequest.formUrlEncoded;
            return _context.abrupt("break", 43);
          case 41:
            getObject = authentication.bodyRequest.formData;
            return _context.abrupt("break", 43);
          case 43:
            objResult = {};
            if (getObject.length > 0 && selectTypeBodyRequest !== "raw") {
              getObject.map(function (el) {
                objResult[el.key] = el.value;
              });
            } else {
              objResult = getObject;
            }
            params = {};
            if (authentication.params && authentication.params.query.length > 0) {
              authentication.params.query.map(function (el) {
                params[el.key] = el.value;
              });
            }
            options = {
              timeout: 5000,
              method: authentication.method,
              url: setPath,
              headers: header,
              params: params,
              data: objResult
            };
            /** use for mode test */
            if (!(req.hasOwnProperty("query") && Object.keys(req.query).length > 0)) {
              _context.next = 52;
              break;
            }
            requestMode = Object.keys(req.query).map(function (el) {
              return el === "test_request" && req.query["".concat(el)] === "true";
            });
            if (!requestMode[0]) {
              _context.next = 52;
              break;
            }
            return _context.abrupt("return", (0, _responseApi.result)({
              data: options
            }));
          case 52:
            //SetVariable to Options
            options.variable = getVariable.varName;
            _context.next = 55;
            return (0, _axios["default"])(options).then(function (response) {
              return (0, _responseApi.result)({
                data: response.data
              });
            })["catch"](function (error) {
              if (error.response) {
                // The request was made and the server responded with a status code
                // that falls out of the range of 2xx

                return (0, _responseApi.result)({
                  isSuccess: true,
                  data: options,
                  statusCode: _responseApi.statusHttpCode.unauthorized401
                });
              } else if (error.request) {
                // The request was made but no response was received
                // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                // http.ClientRequest in node.js
                throw new Error("cant connect client server");
              } else {
                // Something happened in setting up the request that triggered an Error
                console.log("Error", error.message);
              }
              console.log("error.config >>> ", error.config);
            });
          case 55:
            callAxios = _context.sent;
            if (!(callAxios.code !== _responseApi.statusHttpCode.ok200.code)) {
              _context.next = 58;
              break;
            }
            return _context.abrupt("return", callAxios);
          case 58:
            _context.next = 60;
            return helpers.findNestedErrorHttp(callAxios);
          case 60:
            getErrorFromResponseSuccess = _context.sent;
            if (!(Object.keys(getErrorFromResponseSuccess).length > 0)) {
              _context.next = 63;
              break;
            }
            return _context.abrupt("return", (0, _responseApi.result)({
              isSuccess: true,
              message: getErrorFromResponseSuccess.message,
              statusCode: (0, _responseApi.getStatusHttpByCode)(getErrorFromResponseSuccess.code),
              data: options
            }));
          case 63:
            _context.next = 65;
            return helpers.arrayToObjectAll(callAxios.results);
          case 65:
            getObjectAll = _context.sent;
            callAxios.results = Array.isArray(getObjectAll) ? getObjectAll[0] : getObjectAll;
            getFindToken = getVariable.code.find(function (x) {
              return x.key === "token";
            });
            tokenResponse = getVariable.code.find(function (x) {
              return x.key === "tokenFromResponseKey";
            });
            token_type = getVariable.code.find(function (x) {
              return x.key === "token_type";
            });
            if (!getFindToken) {
              _message = "token cant set into variable";
            } else {
              getFindToken.value = callAxios.results[tokenResponse.value];
              if (token_type && token_type.value) {
                getFindToken.value = token_type.value + " " + getFindToken.value;
              }
            }

            //update to env
            variable[indexVariable] = getVariable;
            req.params = {
              id: getEnvLogin.results.id
            };
            req.body = getEnvLogin.results;
            //Digunakan untuk update env token variable saat berhasil login
            _context.next = 76;
            return envRepository.update(req);
          case 76:
            return _context.abrupt("return", (0, _responseApi.result)({
              message: _message,
              data: callAxios.results
            }));
          case 79:
            _context.prev = 79;
            _context.t1 = _context["catch"](0);
            _context.t1.message = "callApiRepository.callAuth: " + _context.t1;
            throw _context.t1;
          case 83:
          case "end":
            return _context.stop();
        }
      }, _callee, null, [[0, 79]]);
    }));
    return function callAuth(_x) {
      return _ref.apply(this, arguments);
    };
  }();

  //todo callAuthAccess
  var callAuthAccess = /*#__PURE__*/function () {
    var _ref2 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee3(req, fromCall) {
      var _searchValueByKey2, id, idUser, envByParams, isUserId, getEnvAccessLogin, _getEnvAccessLogin$re, authentication, variable, _getVariable, indexVariable, _objSearch, hostUrl, setPath, header, selectTypeBodyRequest, getObject, objResult, params, options, requestMode, urlWithParams, isHttps, agent, method, _options$body, body, fetchOptions, resultSend, getErrorFromResponseSuccess, getObjectAll, getFindToken, tokenResponse, token_type;
      return _regeneratorRuntime().wrap(function _callee3$(_context3) {
        while (1) switch (_context3.prev = _context3.next) {
          case 0:
            _context3.prev = 0;
            //SetAutoDetectValueAuthentication
            _searchValueByKey2 = function searchValueByKey(targetObj) {
              var matches = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
              if (targetObj != null) {
                if (Array.isArray(targetObj)) {
                  var _iterator2 = _createForOfIteratorHelper(targetObj),
                    _step2;
                  try {
                    for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
                      var arrayItem = _step2.value;
                      _searchValueByKey2(arrayItem, targetObj, matches);
                    }
                  } catch (err) {
                    _iterator2.e(err);
                  } finally {
                    _iterator2.f();
                  }
                } else if (_typeof(targetObj) == "object") {
                  for (var _i2 = 0, _Object$keys2 = Object.keys(targetObj); _i2 < _Object$keys2.length; _i2++) {
                    var key = _Object$keys2[_i2];
                    if (targetObj.value) {
                      var dataChanges = _objSearch(targetObj.value);
                      if (dataChanges) {
                        targetObj.value = dataChanges;
                      }
                    } else {
                      _searchValueByKey2(targetObj[key], matches);
                    }
                  }
                  matches = targetObj;
                }
              }
              return matches;
            };
            id = req.params.id;
            idUser = req.query.idUser;
            envByParams = null;
            if (req.hasOwnProperty("query") && Object.keys(req.query).length > 0) {
              Object.keys(req.query).map(function (el) {
                if (el === "env") {
                  envByParams = req.query["".concat(el)];
                }
              });
            }
            if (idUser) {
              isUserId = idUser;
            }

            // todo environment access show all requirement access
            _context3.next = 9;
            return environmentAccessRepository.getIsLoginEnvAccess({
              idEnv: id,
              userId: isUserId
            });
          case 9:
            getEnvAccessLogin = _context3.sent;
            if (!(getEnvAccessLogin.code > 400)) {
              _context3.next = 12;
              break;
            }
            return _context3.abrupt("return", getEnvAccessLogin);
          case 12:
            if (getEnvAccessLogin.results.variable) {
              _context3.next = 14;
              break;
            }
            return _context3.abrupt("return", (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.unauthorized401,
              message: "Variable is empty"
            }));
          case 14:
            _getEnvAccessLogin$re = getEnvAccessLogin.results, authentication = _getEnvAccessLogin$re.authentication, variable = _getEnvAccessLogin$re.variable; // todo get variable for result variable environment access
            if (envByParams) {
              _getVariable = variable.findIndex(function (x) {
                return x.varName.toLowerCase() === envByParams.toLowerCase();
              });
            } else {
              _getVariable = variable.findIndex(function (x) {
                return x.status === 1;
              });
            }
            if (!(_getVariable < 0)) {
              _context3.next = 18;
              break;
            }
            return _context3.abrupt("return", (0, _responseApi.result)({
              message: "Variable is not active",
              statusCode: _responseApi.statusHttpCode.badRequest400
            }));
          case 18:
            indexVariable = _getVariable;
            _getVariable = variable[indexVariable];
            if (!(_getVariable.useLogin && !_getVariable.authentication)) {
              _context3.next = 22;
              break;
            }
            return _context3.abrupt("return", (0, _responseApi.result)({
              message: "Authentication is can't null",
              statusCode: _responseApi.statusHttpCode.badRequest400
            }));
          case 22:
            //Set Authentication From Variable Submit
            authentication = _getVariable.authentication;
            _objSearch = function _objSearch(targetObj) {
              var getFind = _getVariable.code.find(function (x) {
                return x.key === targetObj;
              });
              if (getFind) {
                return getFind.value;
              }
            };
            _context3.next = 26;
            return _searchValueByKey2(authentication);
          case 26:
            authentication = _context3.sent;
            hostUrl = _getVariable.code.find(function (el) {
              return el.key === authentication.variableCodeKey.value.replace(/#/g, "");
            }); //DI cek dulu jika authentication.variableCodeKey tidak di trigger variable #base_url# replace akan undefined
            hostUrl = !hostUrl ? authentication.variableCodeKey.value.length > 0 && authentication.variableCodeKey.value ? {
              value: authentication.variableCodeKey.value
            } : null : hostUrl;
            if (hostUrl) {
              _context3.next = 31;
              break;
            }
            return _context3.abrupt("return", (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.notFound404,
              message: "Host url not found please check your env host"
            }));
          case 31:
            setPath = hostUrl.value + authentication.path;
            header = {}; //Get Header
            if (authentication.header.length > 0) {
              authentication.header.map(function (el) {
                header[el.key] = el.value;
              });
            }
            selectTypeBodyRequest = authentication.bodyRequest.mode;
            getObject = [];
            _context3.t0 = selectTypeBodyRequest;
            _context3.next = _context3.t0 === "raw" ? 39 : _context3.t0 === "urlencoded" ? 41 : _context3.t0 === "formdata" ? 43 : 45;
            break;
          case 39:
            getObject = authentication.bodyRequest.rawJson;
            return _context3.abrupt("break", 45);
          case 41:
            getObject = authentication.bodyRequest.formUrlEncoded;
            return _context3.abrupt("break", 45);
          case 43:
            getObject = authentication.bodyRequest.formData;
            return _context3.abrupt("break", 45);
          case 45:
            objResult = {};
            if (getObject.length > 0 && selectTypeBodyRequest !== "raw") {
              getObject.map(function (el) {
                objResult[el.key] = el.value;
              });
            } else {
              objResult = getObject;
            }
            params = {};
            if (authentication.params && authentication.params.query.length > 0) {
              authentication.params.query.map(function (el) {
                params[el.key] = el.value;
              });
            }
            options = {
              timeout: 15000,
              method: authentication.method,
              url: setPath,
              headers: header,
              params: params,
              data: objResult
            };
            /** use for mode test */
            if (!(req.hasOwnProperty("query") && Object.keys(req.query).length > 0)) {
              _context3.next = 54;
              break;
            }
            requestMode = Object.keys(req.query).map(function (el) {
              return el === "test_request" && req.query["".concat(el)] === "true";
            });
            if (!requestMode[0]) {
              _context3.next = 54;
              break;
            }
            return _context3.abrupt("return", (0, _responseApi.result)({
              data: options
            }));
          case 54:
            //SetVariable to Options
            options.variable = _getVariable.varName;

            /** USE FOR FETCH ONLY */
            urlWithParams = new URL(options.url);
            if (params && _typeof(params) === "object") {
              Object.keys(params).forEach(function (key) {
                return urlWithParams.searchParams.append(key, params[key]);
              });
            }
            // Determine the protocol and set the appropriate agent
            isHttps = urlWithParams.protocol === "https:";
            agent = isHttps ? new https.Agent({
              rejectUnauthorized: false
            }) : new http.Agent();
            method = options.method, _options$body = options.body, body = _options$body === void 0 ? options.data : _options$body; // Set up the fetch options
            fetchOptions = _objectSpread({
              method: method,
              headers: header,
              agent: agent,
              timeout: options.timeout
            }, body && method !== "GET" && {
              body: selectTypeBodyRequest == "raw" ? JSON.stringify(body) : new URLSearchParams(body)
            });
            /** END */
            // let resultSend_old = await fetchWithTimeout(
            //     urlWithParams,
            //     fetchOptions,
            //     fetchOptions.timeout
            // )
            //     .then(async (response) => {
            //         let resultData = {};
            //         // Iterate over keys in statusHttpCode object
            //         let statusCode = null;
            //         for (let key in statusHttpCode) {
            //             if (statusHttpCode.hasOwnProperty(key)) {
            //                 const status = statusHttpCode[key];
            //                 if (response.status === status.code) {
            //                     statusCode = status;
            //                     break; // Exit loop once status is found
            //                 }
            //             }
            //         }
            //         // Now statusCode holds the matched status object
            //         if (statusCode.code > 400) {
            //             return result({
            //                 message:
            //                     "Authentication Failed! check your environment.",
            //                 data: await response.json(),
            //                 statusCode: statusHttpCode.unauthorized401,
            //             });
            //         }
            //         return result({
            //             message: response.statusText,
            //             data: await response.json(),
            //             statusCode: statusCode,
            //         });
            //     })
            //     .then(async (data) => {
            //         return data;
            //     })
            //     .catch((error) => {
            //         if (error.response) {
            //             return result({
            //                 isSuccess: true,
            //                 data: error.response,
            //                 statusCode: statusHttpCode.unauthorized401,
            //                 message:
            //                     "Authentication Failed! check your environment.",
            //             });
            //         } else if (error.request) {
            //             return result({
            //                 isSuccess: true,
            //                 data: error.request,
            //                 statusCode: statusHttpCode.unauthorized401,
            //                 message:
            //                     "Authentication Failed! check your environment.",
            //             });
            //         } else {
            //             return result({
            //                 message:
            //                     "Authentication Failed! check your environment.",
            //                 data: error.response,
            //                 statusCode: statusHttpCode.unauthorized401,
            //             });
            //         }
            //     });
            _context3.next = 63;
            return fetchWithTimeout(urlWithParams, fetchOptions, fetchOptions.timeout).then(/*#__PURE__*/function () {
              var _ref3 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee2(response) {
                var contentType, statusCode, findStatusCode, responseData, resData;
                return _regeneratorRuntime().wrap(function _callee2$(_context2) {
                  while (1) switch (_context2.prev = _context2.next) {
                    case 0:
                      contentType = response.headers.get("content-type");
                      statusCode = response.status;
                      findStatusCode = (0, _responseApi.findStatusHttpByCode)(statusCode);
                      if (!(contentType && contentType.includes("application/json"))) {
                        _context2.next = 9;
                        break;
                      }
                      _context2.next = 6;
                      return response.json();
                    case 6:
                      responseData = _context2.sent;
                      _context2.next = 12;
                      break;
                    case 9:
                      _context2.next = 11;
                      return response.text();
                    case 11:
                      responseData = _context2.sent;
                    case 12:
                      // Handle response data based on different formats
                      resData = {};
                      if (responseData.metadata) {
                        findStatusCode = (0, _responseApi.findStatusHttpByCode)(responseData.metadata.code);
                        resData = {
                          statusCode: statusCode >= 400 ? _responseApi.statusHttpCode.unauthorized401 : _responseApi.statusHttpCode.ok200,
                          isSuccess: false,
                          message: responseData.metadata.message || response.statusText,
                          data: responseData.results || responseData
                        };
                      } else {
                        findStatusCode = (0, _responseApi.findStatusHttpByCode)(statusCode);
                        resData = {
                          statusCode: statusCode >= 400 ? _responseApi.statusHttpCode.unauthorized401 : _responseApi.statusHttpCode.ok200,
                          isSuccess: findStatusCode.keyStatusHttpCode >= 200 && findStatusCode.keyStatusHttpCode < 300,
                          message: response.statusText,
                          data: responseData
                        };
                      }
                      return _context2.abrupt("return", (0, _responseApi.result)(resData));
                    case 15:
                    case "end":
                      return _context2.stop();
                  }
                }, _callee2);
              }));
              return function (_x4) {
                return _ref3.apply(this, arguments);
              };
            }())["catch"](function (error) {
              var errorMessage = "Request failed!";
              var statusCode = _responseApi.statusHttpCode.badRequest400;

              // Customize error handling based on error type
              if (error.message === "Timeout") {
                errorMessage = "Request timed out!";
                statusCode = _responseApi.statusHttpCode.gatewayTimeout504;
              } else if (error.response) {
                errorMessage = "Authentication Failed! Check your environment.";
                statusCode = _responseApi.statusHttpCode.unauthorized401;
              }
              return (0, _responseApi.result)({
                message: errorMessage,
                data: options,
                statusCode: statusCode,
                isSuccess: false
              });
            });
          case 63:
            resultSend = _context3.sent;
            if (!(resultSend.code !== _responseApi.statusHttpCode.ok200.code)) {
              _context3.next = 66;
              break;
            }
            return _context3.abrupt("return", resultSend);
          case 66:
            _context3.next = 68;
            return helpers.findNestedErrorHttp(resultSend);
          case 68:
            getErrorFromResponseSuccess = _context3.sent;
            if (!(Object.keys(getErrorFromResponseSuccess).length > 0)) {
              _context3.next = 71;
              break;
            }
            return _context3.abrupt("return", (0, _responseApi.result)({
              isSuccess: true,
              message: getErrorFromResponseSuccess.message,
              statusCode: (0, _responseApi.getStatusHttpByCode)(getErrorFromResponseSuccess.code),
              data: options
            }));
          case 71:
            _context3.next = 73;
            return helpers.arrayToObjectAll(resultSend.results);
          case 73:
            getObjectAll = _context3.sent;
            resultSend.results = Array.isArray(getObjectAll) ? getObjectAll[0] : getObjectAll;

            // // todo find token by static key, static key merupakan field object yang sudah ditetapkan untuk mendapatkan token
            getFindToken = _getVariable.code.find(function (x) {
              return x.key === "token";
            });
            tokenResponse = _getVariable.code.find(function (x) {
              return x.key === "tokenFromResponseKey";
            });
            token_type = _getVariable.code.find(function (x) {
              return x.key === "token_type";
            }); // let message;
            if (getFindToken) {
              _context3.next = 82;
              break;
            }
            message = "token cant set into variable";
            _context3.next = 87;
            break;
          case 82:
            if (!tokenResponse) {
              _context3.next = 87;
              break;
            }
            getFindToken.value = resultSend.results[tokenResponse.value];
            if (getFindToken.value) {
              _context3.next = 86;
              break;
            }
            return _context3.abrupt("return", (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.badRequest400,
              message: "access/auth: Cant get tokenFromResponseKey !"
            }));
          case 86:
            if (token_type && token_type.value) {
              getFindToken.value = token_type.value + " " + getFindToken.value;
            }
          case 87:
            // todo requupdate to env
            variable[indexVariable] = _getVariable;
            req.params = {
              id: getEnvAccessLogin.results.id
            };
            req.body = getEnvAccessLogin.results;

            // //todo update environmentAccess
            _context3.next = 92;
            return environmentAccessRepository.update(req);
          case 92:
            return _context3.abrupt("return", (0, _responseApi.result)({
              data: resultSend.results
            }));
          case 95:
            _context3.prev = 95;
            _context3.t1 = _context3["catch"](0);
            _context3.t1.message = "callApiRepository.callAuthAccess:  " + _context3.t1;
            throw _context3.t1;
          case 99:
          case "end":
            return _context3.stop();
        }
      }, _callee3, null, [[0, 95]]);
    }));
    return function callAuthAccess(_x2, _x3) {
      return _ref2.apply(this, arguments);
    };
  }();
  var fetchWithTimeout_old = /*#__PURE__*/function () {
    var _ref4 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee4(url, options) {
      var timeout,
        _args4 = arguments;
      return _regeneratorRuntime().wrap(function _callee4$(_context4) {
        while (1) switch (_context4.prev = _context4.next) {
          case 0:
            timeout = _args4.length > 2 && _args4[2] !== undefined ? _args4[2] : 1000;
            return _context4.abrupt("return", Promise.race([(0, _nodeFetch["default"])(url, options), new Promise(function (_, reject) {
              return setTimeout(function () {
                return reject(new Error("Timeout"));
              }, timeout);
            })]));
          case 2:
          case "end":
            return _context4.stop();
        }
      }, _callee4);
    }));
    return function fetchWithTimeout_old(_x5, _x6) {
      return _ref4.apply(this, arguments);
    };
  }();
  var fetchWithTimeout = /*#__PURE__*/function () {
    var _ref5 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee5(url, options) {
      var timeout,
        controller,
        timeoutId,
        response,
        _args5 = arguments;
      return _regeneratorRuntime().wrap(function _callee5$(_context5) {
        while (1) switch (_context5.prev = _context5.next) {
          case 0:
            timeout = _args5.length > 2 && _args5[2] !== undefined ? _args5[2] : 1000;
            controller = new AbortController();
            timeoutId = setTimeout(function () {
              return controller.abort();
            }, timeout);
            _context5.prev = 3;
            _context5.next = 6;
            return (0, _nodeFetch["default"])(url, _objectSpread(_objectSpread({}, options), {}, {
              signal: controller.signal
            }));
          case 6:
            response = _context5.sent;
            clearTimeout(timeoutId);
            return _context5.abrupt("return", response);
          case 11:
            _context5.prev = 11;
            _context5.t0 = _context5["catch"](3);
            clearTimeout(timeoutId);
            if (!(_context5.t0.name === "AbortError")) {
              _context5.next = 18;
              break;
            }
            throw new Error("Timeout");
          case 18:
            throw _context5.t0;
          case 19:
          case "end":
            return _context5.stop();
        }
      }, _callee5, null, [[3, 11]]);
    }));
    return function fetchWithTimeout(_x7, _x8) {
      return _ref5.apply(this, arguments);
    };
  }();
  return {
    callAuth: callAuth,
    callAuthAccess: callAuthAccess,
    fetchWithTimeout: fetchWithTimeout
  };
}