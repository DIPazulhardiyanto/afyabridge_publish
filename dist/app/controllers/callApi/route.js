"use strict";

require("core-js/modules/es.object.define-property.js");
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
require("core-js/modules/es.array.concat.js");
var _express = _interopRequireDefault(require("express"));
var _controllers = _interopRequireDefault(require("./controllers"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
var basePath = "/call";
var accessPath = basePath + "/access";
var setAuthPath = function setAuthPath(path) {
  return "".concat(basePath, "/auth/").concat(path);
};
var setAccessAuthPath = function setAccessAuthPath(path) {
  return "".concat(accessPath, "/auth/").concat(path);
};
var setPath = function setPath(path) {
  return "".concat(basePath, "/").concat(path);
};
var controller = (0, _controllers["default"])();
var routes = (0, _express["default"])();
routes.route(setAuthPath(":id")).post(controller.callAuth);
routes.route(setAccessAuthPath(":id")).post(controller.callAuthAccess);
routes.route(basePath).post(controller.callIntegration);
var _default = exports["default"] = routes;