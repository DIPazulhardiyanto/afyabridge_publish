"use strict";

require("core-js/modules/es.symbol.js");
require("core-js/modules/es.symbol.description.js");
require("core-js/modules/es.symbol.async-iterator.js");
require("core-js/modules/es.symbol.iterator.js");
require("core-js/modules/es.symbol.to-primitive.js");
require("core-js/modules/es.symbol.to-string-tag.js");
require("core-js/modules/es.array.for-each.js");
require("core-js/modules/es.array.iterator.js");
require("core-js/modules/es.array.reverse.js");
require("core-js/modules/es.array.slice.js");
require("core-js/modules/es.date.to-primitive.js");
require("core-js/modules/es.function.name.js");
require("core-js/modules/es.json.to-string-tag.js");
require("core-js/modules/es.math.to-string-tag.js");
require("core-js/modules/es.number.constructor.js");
require("core-js/modules/es.object.create.js");
require("core-js/modules/es.object.define-properties.js");
require("core-js/modules/es.object.define-property.js");
require("core-js/modules/es.object.get-own-property-descriptor.js");
require("core-js/modules/es.object.get-own-property-descriptors.js");
require("core-js/modules/es.object.get-prototype-of.js");
require("core-js/modules/es.object.set-prototype-of.js");
require("core-js/modules/es.string.iterator.js");
require("core-js/modules/es.weak-map.js");
require("core-js/modules/web.dom-collections.for-each.js");
require("core-js/modules/web.dom-collections.iterator.js");
function _typeof(o) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) { return typeof o; } : function (o) { return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o; }, _typeof(o); }
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.mappingParameterSendAccess = void 0;
require("core-js/modules/es.array.filter.js");
require("core-js/modules/es.array.find.js");
require("core-js/modules/es.array.find-index.js");
require("core-js/modules/es.array.map.js");
require("core-js/modules/es.object.keys.js");
require("core-js/modules/es.object.to-string.js");
require("core-js/modules/es.promise.js");
require("core-js/modules/es.regexp.exec.js");
require("core-js/modules/es.string.replace.js");
var _responseApi = require("../../libs/utils/responseApi");
var _repository = _interopRequireDefault(require("../environmentAccess/repository"));
var _A_call_auth_access = require("./A_call_auth_access");
var https = _interopRequireWildcard(require("https"));
var _A_call_axios_integration = require("./A_call_axios_integration");
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != _typeof(e) && "function" != typeof e) return { "default": e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && {}.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n["default"] = e, t && t.set(e, n), n; }
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
function _regeneratorRuntime() { "use strict"; /*! regenerator-runtime -- Copyright (c) 2014-present, Facebook, Inc. -- license (MIT): https://github.com/facebook/regenerator/blob/main/LICENSE */ _regeneratorRuntime = function _regeneratorRuntime() { return e; }; var t, e = {}, r = Object.prototype, n = r.hasOwnProperty, o = Object.defineProperty || function (t, e, r) { t[e] = r.value; }, i = "function" == typeof Symbol ? Symbol : {}, a = i.iterator || "@@iterator", c = i.asyncIterator || "@@asyncIterator", u = i.toStringTag || "@@toStringTag"; function define(t, e, r) { return Object.defineProperty(t, e, { value: r, enumerable: !0, configurable: !0, writable: !0 }), t[e]; } try { define({}, ""); } catch (t) { define = function define(t, e, r) { return t[e] = r; }; } function wrap(t, e, r, n) { var i = e && e.prototype instanceof Generator ? e : Generator, a = Object.create(i.prototype), c = new Context(n || []); return o(a, "_invoke", { value: makeInvokeMethod(t, r, c) }), a; } function tryCatch(t, e, r) { try { return { type: "normal", arg: t.call(e, r) }; } catch (t) { return { type: "throw", arg: t }; } } e.wrap = wrap; var h = "suspendedStart", l = "suspendedYield", f = "executing", s = "completed", y = {}; function Generator() {} function GeneratorFunction() {} function GeneratorFunctionPrototype() {} var p = {}; define(p, a, function () { return this; }); var d = Object.getPrototypeOf, v = d && d(d(values([]))); v && v !== r && n.call(v, a) && (p = v); var g = GeneratorFunctionPrototype.prototype = Generator.prototype = Object.create(p); function defineIteratorMethods(t) { ["next", "throw", "return"].forEach(function (e) { define(t, e, function (t) { return this._invoke(e, t); }); }); } function AsyncIterator(t, e) { function invoke(r, o, i, a) { var c = tryCatch(t[r], t, o); if ("throw" !== c.type) { var u = c.arg, h = u.value; return h && "object" == _typeof(h) && n.call(h, "__await") ? e.resolve(h.__await).then(function (t) { invoke("next", t, i, a); }, function (t) { invoke("throw", t, i, a); }) : e.resolve(h).then(function (t) { u.value = t, i(u); }, function (t) { return invoke("throw", t, i, a); }); } a(c.arg); } var r; o(this, "_invoke", { value: function value(t, n) { function callInvokeWithMethodAndArg() { return new e(function (e, r) { invoke(t, n, e, r); }); } return r = r ? r.then(callInvokeWithMethodAndArg, callInvokeWithMethodAndArg) : callInvokeWithMethodAndArg(); } }); } function makeInvokeMethod(e, r, n) { var o = h; return function (i, a) { if (o === f) throw Error("Generator is already running"); if (o === s) { if ("throw" === i) throw a; return { value: t, done: !0 }; } for (n.method = i, n.arg = a;;) { var c = n.delegate; if (c) { var u = maybeInvokeDelegate(c, n); if (u) { if (u === y) continue; return u; } } if ("next" === n.method) n.sent = n._sent = n.arg;else if ("throw" === n.method) { if (o === h) throw o = s, n.arg; n.dispatchException(n.arg); } else "return" === n.method && n.abrupt("return", n.arg); o = f; var p = tryCatch(e, r, n); if ("normal" === p.type) { if (o = n.done ? s : l, p.arg === y) continue; return { value: p.arg, done: n.done }; } "throw" === p.type && (o = s, n.method = "throw", n.arg = p.arg); } }; } function maybeInvokeDelegate(e, r) { var n = r.method, o = e.iterator[n]; if (o === t) return r.delegate = null, "throw" === n && e.iterator["return"] && (r.method = "return", r.arg = t, maybeInvokeDelegate(e, r), "throw" === r.method) || "return" !== n && (r.method = "throw", r.arg = new TypeError("The iterator does not provide a '" + n + "' method")), y; var i = tryCatch(o, e.iterator, r.arg); if ("throw" === i.type) return r.method = "throw", r.arg = i.arg, r.delegate = null, y; var a = i.arg; return a ? a.done ? (r[e.resultName] = a.value, r.next = e.nextLoc, "return" !== r.method && (r.method = "next", r.arg = t), r.delegate = null, y) : a : (r.method = "throw", r.arg = new TypeError("iterator result is not an object"), r.delegate = null, y); } function pushTryEntry(t) { var e = { tryLoc: t[0] }; 1 in t && (e.catchLoc = t[1]), 2 in t && (e.finallyLoc = t[2], e.afterLoc = t[3]), this.tryEntries.push(e); } function resetTryEntry(t) { var e = t.completion || {}; e.type = "normal", delete e.arg, t.completion = e; } function Context(t) { this.tryEntries = [{ tryLoc: "root" }], t.forEach(pushTryEntry, this), this.reset(!0); } function values(e) { if (e || "" === e) { var r = e[a]; if (r) return r.call(e); if ("function" == typeof e.next) return e; if (!isNaN(e.length)) { var o = -1, i = function next() { for (; ++o < e.length;) if (n.call(e, o)) return next.value = e[o], next.done = !1, next; return next.value = t, next.done = !0, next; }; return i.next = i; } } throw new TypeError(_typeof(e) + " is not iterable"); } return GeneratorFunction.prototype = GeneratorFunctionPrototype, o(g, "constructor", { value: GeneratorFunctionPrototype, configurable: !0 }), o(GeneratorFunctionPrototype, "constructor", { value: GeneratorFunction, configurable: !0 }), GeneratorFunction.displayName = define(GeneratorFunctionPrototype, u, "GeneratorFunction"), e.isGeneratorFunction = function (t) { var e = "function" == typeof t && t.constructor; return !!e && (e === GeneratorFunction || "GeneratorFunction" === (e.displayName || e.name)); }, e.mark = function (t) { return Object.setPrototypeOf ? Object.setPrototypeOf(t, GeneratorFunctionPrototype) : (t.__proto__ = GeneratorFunctionPrototype, define(t, u, "GeneratorFunction")), t.prototype = Object.create(g), t; }, e.awrap = function (t) { return { __await: t }; }, defineIteratorMethods(AsyncIterator.prototype), define(AsyncIterator.prototype, c, function () { return this; }), e.AsyncIterator = AsyncIterator, e.async = function (t, r, n, o, i) { void 0 === i && (i = Promise); var a = new AsyncIterator(wrap(t, r, n, o), i); return e.isGeneratorFunction(r) ? a : a.next().then(function (t) { return t.done ? t.value : a.next(); }); }, defineIteratorMethods(g), define(g, u, "Generator"), define(g, a, function () { return this; }), define(g, "toString", function () { return "[object Generator]"; }), e.keys = function (t) { var e = Object(t), r = []; for (var n in e) r.push(n); return r.reverse(), function next() { for (; r.length;) { var t = r.pop(); if (t in e) return next.value = t, next.done = !1, next; } return next.done = !0, next; }; }, e.values = values, Context.prototype = { constructor: Context, reset: function reset(e) { if (this.prev = 0, this.next = 0, this.sent = this._sent = t, this.done = !1, this.delegate = null, this.method = "next", this.arg = t, this.tryEntries.forEach(resetTryEntry), !e) for (var r in this) "t" === r.charAt(0) && n.call(this, r) && !isNaN(+r.slice(1)) && (this[r] = t); }, stop: function stop() { this.done = !0; var t = this.tryEntries[0].completion; if ("throw" === t.type) throw t.arg; return this.rval; }, dispatchException: function dispatchException(e) { if (this.done) throw e; var r = this; function handle(n, o) { return a.type = "throw", a.arg = e, r.next = n, o && (r.method = "next", r.arg = t), !!o; } for (var o = this.tryEntries.length - 1; o >= 0; --o) { var i = this.tryEntries[o], a = i.completion; if ("root" === i.tryLoc) return handle("end"); if (i.tryLoc <= this.prev) { var c = n.call(i, "catchLoc"), u = n.call(i, "finallyLoc"); if (c && u) { if (this.prev < i.catchLoc) return handle(i.catchLoc, !0); if (this.prev < i.finallyLoc) return handle(i.finallyLoc); } else if (c) { if (this.prev < i.catchLoc) return handle(i.catchLoc, !0); } else { if (!u) throw Error("try statement without catch or finally"); if (this.prev < i.finallyLoc) return handle(i.finallyLoc); } } } }, abrupt: function abrupt(t, e) { for (var r = this.tryEntries.length - 1; r >= 0; --r) { var o = this.tryEntries[r]; if (o.tryLoc <= this.prev && n.call(o, "finallyLoc") && this.prev < o.finallyLoc) { var i = o; break; } } i && ("break" === t || "continue" === t) && i.tryLoc <= e && e <= i.finallyLoc && (i = null); var a = i ? i.completion : {}; return a.type = t, a.arg = e, i ? (this.method = "next", this.next = i.finallyLoc, y) : this.complete(a); }, complete: function complete(t, e) { if ("throw" === t.type) throw t.arg; return "break" === t.type || "continue" === t.type ? this.next = t.arg : "return" === t.type ? (this.rval = this.arg = t.arg, this.method = "return", this.next = "end") : "normal" === t.type && e && (this.next = e), y; }, finish: function finish(t) { for (var e = this.tryEntries.length - 1; e >= 0; --e) { var r = this.tryEntries[e]; if (r.finallyLoc === t) return this.complete(r.completion, r.afterLoc), resetTryEntry(r), y; } }, "catch": function _catch(t) { for (var e = this.tryEntries.length - 1; e >= 0; --e) { var r = this.tryEntries[e]; if (r.tryLoc === t) { var n = r.completion; if ("throw" === n.type) { var o = n.arg; resetTryEntry(r); } return o; } } throw Error("illegal catch attempt"); }, delegateYield: function delegateYield(e, r, n) { return this.delegate = { iterator: values(e), resultName: r, nextLoc: n }, "next" === this.method && (this.arg = t), y; } }, e; }
function ownKeys(e, r) { var t = Object.keys(e); if (Object.getOwnPropertySymbols) { var o = Object.getOwnPropertySymbols(e); r && (o = o.filter(function (r) { return Object.getOwnPropertyDescriptor(e, r).enumerable; })), t.push.apply(t, o); } return t; }
function _objectSpread(e) { for (var r = 1; r < arguments.length; r++) { var t = null != arguments[r] ? arguments[r] : {}; r % 2 ? ownKeys(Object(t), !0).forEach(function (r) { _defineProperty(e, r, t[r]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys(Object(t)).forEach(function (r) { Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r)); }); } return e; }
function _defineProperty(e, r, t) { return (r = _toPropertyKey(r)) in e ? Object.defineProperty(e, r, { value: t, enumerable: !0, configurable: !0, writable: !0 }) : e[r] = t, e; }
function _toPropertyKey(t) { var i = _toPrimitive(t, "string"); return "symbol" == _typeof(i) ? i : i + ""; }
function _toPrimitive(t, r) { if ("object" != _typeof(t) || !t) return t; var e = t[Symbol.toPrimitive]; if (void 0 !== e) { var i = e.call(t, r || "default"); if ("object" != _typeof(i)) return i; throw new TypeError("@@toPrimitive must return a primitive value."); } return ("string" === r ? String : Number)(t); }
function asyncGeneratorStep(n, t, e, r, o, a, c) { try { var i = n[a](c), u = i.value; } catch (n) { return void e(n); } i.done ? t(u) : Promise.resolve(u).then(r, o); }
function _asyncToGenerator(n) { return function () { var t = this, e = arguments; return new Promise(function (r, o) { var a = n.apply(t, e); function _next(n) { asyncGeneratorStep(a, r, o, _next, _throw, "next", n); } function _throw(n) { asyncGeneratorStep(a, r, o, _next, _throw, "throw", n); } _next(void 0); }); }; }
var countMappingParameterSendAccess = 0;
var _mappingParameterSendAccess = exports.mappingParameterSendAccess = /*#__PURE__*/function () {
  var _ref = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee(getPathIntegration, getFromReq, getListMappingClientRsc) {
    var resultsIntegration, paramsForAuth, getEnvLogin, envByParams, variable, header, getVariable, headers, option, urlApi, paramsQuery, params, bodyRequest, mappingTemplate, request, method, data, getTokenVariable, hostUrl, newOption, checkTestRequest, mode, getCallAuth, getEnvDetailError, _resultCall, resultCall, codeResult, _getCallAuth, _getEnvDetailError, checkValueMetaData, key;
    return _regeneratorRuntime().wrap(function _callee$(_context) {
      while (1) switch (_context.prev = _context.next) {
        case 0:
          _context.prev = 0;
          /**
           * Get Headers ContentType From List send integration API
           * */
          resultsIntegration = getPathIntegration;
          paramsForAuth = {
            params: {
              id: resultsIntegration.idEnv
            }
          };
          delete resultsIntegration.originalBodyRequest.resChanges;

          //todo Environment Access
          _context.next = 6;
          return (0, _repository["default"])().getIsLoginEnvAccess({
            userId: getFromReq.environmentAccess.idUser,
            idEnv: resultsIntegration.idEnv
          });
        case 6:
          getEnvLogin = _context.sent.results;
          if (!(getEnvLogin.variable.length === 0)) {
            _context.next = 9;
            break;
          }
          return _context.abrupt("return", {
            resultTest: (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.notFound404,
              message: "SEND_API: Variable not set"
            }),
            resultCall: (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.notFound404,
              message: "SEND_API: Variable not set"
            })
          });
        case 9:
          /** SET UP ENV BY QUERY */
          // let envByParams = null
          envByParams = getEnvLogin.variable.findIndex(function (el) {
            return el.varName.toLowerCase() === getFromReq.env && el.status === 1;
          });
          if (!(envByParams < 0)) {
            _context.next = 12;
            break;
          }
          return _context.abrupt("return", {
            resultTest: (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.badRequest400,
              message: "Variable " + "'".concat(getFromReq.env.toUpperCase(), "'") + " is not found / IsNotActive"
            }),
            resultCall: (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.badRequest400,
              message: "Variable " + "'".concat(getFromReq.env.toUpperCase(), "'") + " is not found / IsNotActive"
            })
          });
        case 12:
          paramsForAuth = _objectSpread(_objectSpread({}, paramsForAuth), {}, _defineProperty({}, "query", {
            env: getEnvLogin.variable[envByParams].varName,
            idUser: getFromReq.environmentAccess.idUser
          }));
          variable = getEnvLogin.variable, header = getEnvLogin.header;
          getVariable = variable[envByParams];
          /**
           * GET Api Resource SEND
           * */
          headers = {};
          option = {};
          urlApi = "";
          paramsQuery = null;
          params = resultsIntegration.params, bodyRequest = resultsIntegration.bodyRequest, mappingTemplate = resultsIntegration.mappingTemplate, request = resultsIntegration.request, method = resultsIntegration.method;
          data = null;
          /**
           * Get Header Form ENV
           * */
          getTokenVariable = {};
          if (getVariable.header.length > 0) {
            getVariable.header.map(function (headersEnv) {
              getTokenVariable = getVariable.code.find(function (el) {
                return el.key === headersEnv.value;
              });
              if (getTokenVariable) {
                headers["".concat(headersEnv.key)] = getTokenVariable.value;
              }
            });
          }

          /**
           * Get Header Form Resource SendAPI
           * */
          request.header.map(function (elHeader) {
            headers["".concat(elHeader.key)] = elHeader.value;
          });
          hostUrl = getVariable.code.find(function (el) {
            return el.key === request.url.host.replace(/#/g, "");
          }); // is host url not found please check your env host
          if (hostUrl) {
            _context.next = 27;
            break;
          }
          return _context.abrupt("return", {
            resultTest: (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.notFound404,
              message: "Host url not found please check your env host",
              data: request.url
            }),
            resultCall: (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.notFound404,
              message: "Host url not found please check your env host",
              data: request.url
            })
          });
        case 27:
          /**
           * SET URL API
           * */
          urlApi = hostUrl.value + getFromReq.path;
          /**
           * SET Params Query
           * */
          if (params.query.length > 0) {
            paramsQuery = {};
            params.query.map(function (elParamsQuery) {
              paramsQuery["".concat(elParamsQuery.key)] = elParamsQuery.value;
            });
          }

          /**
           * SET BODY PARAMETER
           * */
          //di comment dulu
          if (Object.keys(mappingTemplate).length !== 0) {
            data = mappingTemplate;
          } else if (bodyRequest.mode === "raw" && Object.keys(mappingTemplate).length === 0 && Object.keys(bodyRequest.rawJson).length !== 0) {
            data = bodyRequest.rawJson;
          } else if (bodyRequest.mode === "urlencoded" && Object.keys(mappingTemplate).length === 0 && Object.keys(bodyRequest.formUrlEncoded).length !== 0) {
            data = bodyRequest.formUrlEncoded;
          }

          /**
           * SET OPTION AXIOS
           * */
          option["method"] = method;
          option["url"] = urlApi;
          option["headers"] = headers;
          option["params"] = paramsQuery;
          option["timeout"] = 15000; //ms
          option["variable"] = getVariable.varName;
          option["httpsAgent"] = new https.Agent({
            rejectUnauthorized: false
          });
          if (data) {
            option["data"] = data;
          }
          newOption = {};
          /** Show Header BY ENV PROCESS NODE_ENV : development */
          checkTestRequest = Object.keys(getFromReq.query).length > 0 ? Object.keys(getFromReq.query).filter(function (el) {
            return el === "test_request";
          }) : [];
          /** Test Request Return */
          if (!(checkTestRequest.length > 0 && getFromReq.query[checkTestRequest] === "true")) {
            _context.next = 44;
            break;
          }
          if (process.env.npm_lifecycle_event != "dev") {
            newOption = _objectSpread(_objectSpread({}, newOption), {}, {
              url: getFromReq.fullUrl,
              params: option.params,
              method: option.method,
              variable: option.variable,
              data: option.data,
              originalRequest: {}
            });
          } else {
            newOption = _objectSpread(_objectSpread({}, option), {}, {
              originalRequest: {}
            });
          }
          if (bodyRequest.mode.toLowerCase() !== "none" && getListMappingClientRsc.length > 0 || Object.keys(mappingTemplate).length !== 0) {
            mode = resultsIntegration.bodyRequest.mode == 'raw' ? 'rawJson' : resultsIntegration.bodyRequest.mode;
            newOption["originalRequest"] = resultsIntegration.bodyRequest[mode];
          }
          return _context.abrupt("return", {
            resultTest: newOption,
            resultCall: (0, _responseApi.result)({
              isSuccess: false,
              message: "Test Request Success",
              data: newOption
            })
          });
        case 44:
          if (!(getTokenVariable && getVariable.hasOwnProperty("useLogin") && getVariable.useLogin && getTokenVariable.value === "null")) {
            _context.next = 59;
            break;
          }
          _context.next = 47;
          return (0, _A_call_auth_access.callTheAuthAccess)(paramsForAuth);
        case 47:
          getCallAuth = _context.sent;
          if (!(getCallAuth.resultCall.code >= 400)) {
            _context.next = 55;
            break;
          }
          getEnvDetailError = {
            server: getFromReq.url,
            varName: getEnvLogin.variable[envByParams].varName,
            message: getCallAuth.resultCall.message,
            data: getCallAuth.resultCall.results
          };
          getCallAuth.resultCall.message = "Authentication Failed! Check your environment.";
          getCallAuth.resultCall.results = getEnvDetailError;
          return _context.abrupt("return", {
            resultTest: getCallAuth.resultCall,
            resultCall: getCallAuth.resultCall
          });
        case 55:
          _context.next = 57;
          return _mappingParameterSendAccess(resultsIntegration, getFromReq, getListMappingClientRsc);
        case 57:
          _resultCall = _context.sent;
          return _context.abrupt("return", {
            resultTest: _resultCall.resultTest,
            resultCall: _resultCall.resultCall
          });
        case 59:
          _context.next = 61;
          return (0, _A_call_axios_integration.callAxiosIntegration)(option, getFromReq);
        case 61:
          resultCall = _context.sent;
          //todo path is login run this `DON'T Recursively Authentication`
          codeResult = null;
          if (!(Object.keys(getVariable.authentication).length > 0)) {
            _context.next = 79;
            break;
          }
          if (!(getPathIntegration.path === getVariable.authentication.path)) {
            _context.next = 79;
            break;
          }
          _context.t0 = resultCall.code;
          _context.next = _context.t0 === _responseApi.statusHttpCode.badRequest400.code ? 68 : _context.t0 === _responseApi.statusHttpCode.unauthorized401.code ? 70 : _context.t0 === _responseApi.statusHttpCode.notFound404.code ? 72 : _context.t0 === _responseApi.statusHttpCode.methodNotAllowed405.code ? 74 : 76;
          break;
        case 68:
          codeResult = _responseApi.statusHttpCode.badRequest400;
          return _context.abrupt("break", 78);
        case 70:
          codeResult = _responseApi.statusHttpCode.unauthorized401;
          return _context.abrupt("break", 78);
        case 72:
          codeResult = _responseApi.statusHttpCode.notFound404;
          return _context.abrupt("break", 78);
        case 74:
          codeResult = _responseApi.statusHttpCode.methodNotAllowed405;
          return _context.abrupt("break", 78);
        case 76:
          codeResult = _responseApi.statusHttpCode.ok200;
          return _context.abrupt("break", 78);
        case 78:
          return _context.abrupt("return", {
            resultTest: newOption,
            resultCall: (0, _responseApi.result)({
              message: resultCall.message,
              statusCode: codeResult,
              data: resultCall.results
            })
          });
        case 79:
          _context.t1 = resultCall.code;
          _context.next = _context.t1 === _responseApi.statusHttpCode.badRequest400.code ? 82 : _context.t1 === _responseApi.statusHttpCode.unauthorized401.code ? 84 : _context.t1 === _responseApi.statusHttpCode.notFound404.code ? 86 : _context.t1 === _responseApi.statusHttpCode.methodNotAllowed405.code ? 88 : 90;
          break;
        case 82:
          codeResult = _responseApi.statusHttpCode.badRequest400;
          return _context.abrupt("break", 92);
        case 84:
          codeResult = _responseApi.statusHttpCode.unauthorized401;
          return _context.abrupt("break", 92);
        case 86:
          codeResult = _responseApi.statusHttpCode.notFound404;
          return _context.abrupt("break", 92);
        case 88:
          codeResult = _responseApi.statusHttpCode.methodNotAllowed405;
          return _context.abrupt("break", 92);
        case 90:
          codeResult = _responseApi.statusHttpCode.ok200;
          return _context.abrupt("break", 92);
        case 92:
          if (!(resultCall.code === _responseApi.statusHttpCode.unauthorized401.code && getVariable.hasOwnProperty("useLogin") && getVariable.useLogin)) {
            _context.next = 119;
            break;
          }
          _context.prev = 93;
          _context.next = 96;
          return (0, _A_call_auth_access.callTheAuthAccess)(paramsForAuth);
        case 96:
          _getCallAuth = _context.sent;
          if (!(_getCallAuth.resultCall.code >= 400)) {
            _context.next = 104;
            break;
          }
          _getEnvDetailError = {
            server: getFromReq.url,
            varName: getEnvLogin.variable[envByParams].varName,
            message: _getCallAuth.resultCall.message,
            data: _getCallAuth.resultCall.results
          };
          _getCallAuth.resultCall.message = "Authentication Failed! Check your environment.";
          _getCallAuth.resultCall.results = _getEnvDetailError;
          return _context.abrupt("return", _getCallAuth);
        case 104:
          // to stop recursive if error
          countMappingParameterSendAccess = countMappingParameterSendAccess + 1;
          if (!(countMappingParameterSendAccess == 3)) {
            _context.next = 108;
            break;
          }
          countMappingParameterSendAccess = 0;
          return _context.abrupt("return", {
            resultTest: newOption,
            resultCall: (0, _responseApi.result)({
              isSuccess: true,
              message: resultCall.message,
              statusCode: codeResult,
              data: resultCall.results
            })
          });
        case 108:
          _context.next = 110;
          return _mappingParameterSendAccess(getPathIntegration, getFromReq, getListMappingClientRsc);
        case 110:
          resultCall = _context.sent;
          return _context.abrupt("return", resultCall);
        case 112:
          _context.next = 117;
          break;
        case 114:
          _context.prev = 114;
          _context.t2 = _context["catch"](93);
          throw _context.t2;
        case 117:
          _context.next = 122;
          break;
        case 119:
          //Mendapatkan result data dari metadata jika ada parameter getValueFromMetadata
          checkValueMetaData = getEnvLogin.variable[envByParams].code.find(function (el) {
            return el.key == "getValueFromMetadata";
          });
          if (checkValueMetaData && checkValueMetaData.value != "null") {
            for (key in resultCall.results) {
              if (checkValueMetaData.value == key) {
                resultCall.results = resultCall.results[key];
              }
            }
          }
          return _context.abrupt("return", {
            resultTest: newOption,
            resultCall: resultCall
          });
        case 122:
          _context.next = 127;
          break;
        case 124:
          _context.prev = 124;
          _context.t3 = _context["catch"](0);
          throw _context.t3;
        case 127:
        case "end":
          return _context.stop();
      }
    }, _callee, null, [[0, 124], [93, 114]]);
  }));
  return function mappingParameterSendAccess(_x, _x2, _x3) {
    return _ref.apply(this, arguments);
  };
}();