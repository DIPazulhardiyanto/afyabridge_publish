"use strict";

require("core-js/modules/es.symbol.js");
require("core-js/modules/es.symbol.description.js");
require("core-js/modules/es.symbol.async-iterator.js");
require("core-js/modules/es.symbol.iterator.js");
require("core-js/modules/es.symbol.to-primitive.js");
require("core-js/modules/es.symbol.to-string-tag.js");
require("core-js/modules/es.array.for-each.js");
require("core-js/modules/es.array.from.js");
require("core-js/modules/es.array.reverse.js");
require("core-js/modules/es.date.to-primitive.js");
require("core-js/modules/es.date.to-string.js");
require("core-js/modules/es.function.name.js");
require("core-js/modules/es.json.to-string-tag.js");
require("core-js/modules/es.math.to-string-tag.js");
require("core-js/modules/es.number.constructor.js");
require("core-js/modules/es.object.create.js");
require("core-js/modules/es.object.define-properties.js");
require("core-js/modules/es.object.define-property.js");
require("core-js/modules/es.object.get-own-property-descriptor.js");
require("core-js/modules/es.object.get-own-property-descriptors.js");
require("core-js/modules/es.object.get-prototype-of.js");
require("core-js/modules/es.object.set-prototype-of.js");
require("core-js/modules/web.dom-collections.for-each.js");
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = callApiTransporterRepository;
require("core-js/modules/es.array.concat.js");
require("core-js/modules/es.array.filter.js");
require("core-js/modules/es.array.find.js");
require("core-js/modules/es.array.find-index.js");
require("core-js/modules/es.array.includes.js");
require("core-js/modules/es.array.is-array.js");
require("core-js/modules/es.array.iterator.js");
require("core-js/modules/es.array.join.js");
require("core-js/modules/es.array.map.js");
require("core-js/modules/es.array.slice.js");
require("core-js/modules/es.object.keys.js");
require("core-js/modules/es.object.to-string.js");
require("core-js/modules/es.promise.js");
require("core-js/modules/es.regexp.constructor.js");
require("core-js/modules/es.regexp.exec.js");
require("core-js/modules/es.regexp.to-string.js");
require("core-js/modules/es.string.includes.js");
require("core-js/modules/es.string.iterator.js");
require("core-js/modules/es.string.match.js");
require("core-js/modules/es.string.replace.js");
require("core-js/modules/web.dom-collections.iterator.js");
var _responseApi = require("../../libs/utils/responseApi");
var _models = require("../../models");
var _sequelize = require("sequelize");
var _repository = _interopRequireDefault(require("../environtment/repository"));
var _repository_sendapi = _interopRequireDefault(require("./repository_sendapi"));
var _repository2 = _interopRequireDefault(require("../mapClientResource/repository"));
var _repository3 = _interopRequireDefault(require("../requestResponse/repository"));
var _helpers = _interopRequireDefault(require("../../libs/utils/helpers"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
function _typeof(o) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) { return typeof o; } : function (o) { return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o; }, _typeof(o); }
function _createForOfIteratorHelper(r, e) { var t = "undefined" != typeof Symbol && r[Symbol.iterator] || r["@@iterator"]; if (!t) { if (Array.isArray(r) || (t = _unsupportedIterableToArray(r)) || e && r && "number" == typeof r.length) { t && (r = t); var _n = 0, F = function F() {}; return { s: F, n: function n() { return _n >= r.length ? { done: !0 } : { done: !1, value: r[_n++] }; }, e: function e(r) { throw r; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var o, a = !0, u = !1; return { s: function s() { t = t.call(r); }, n: function n() { var r = t.next(); return a = r.done, r; }, e: function e(r) { u = !0, o = r; }, f: function f() { try { a || null == t["return"] || t["return"](); } finally { if (u) throw o; } } }; }
function _unsupportedIterableToArray(r, a) { if (r) { if ("string" == typeof r) return _arrayLikeToArray(r, a); var t = {}.toString.call(r).slice(8, -1); return "Object" === t && r.constructor && (t = r.constructor.name), "Map" === t || "Set" === t ? Array.from(r) : "Arguments" === t || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(t) ? _arrayLikeToArray(r, a) : void 0; } }
function _arrayLikeToArray(r, a) { (null == a || a > r.length) && (a = r.length); for (var e = 0, n = Array(a); e < a; e++) n[e] = r[e]; return n; }
function _regeneratorRuntime() { "use strict"; /*! regenerator-runtime -- Copyright (c) 2014-present, Facebook, Inc. -- license (MIT): https://github.com/facebook/regenerator/blob/main/LICENSE */ _regeneratorRuntime = function _regeneratorRuntime() { return e; }; var t, e = {}, r = Object.prototype, n = r.hasOwnProperty, o = Object.defineProperty || function (t, e, r) { t[e] = r.value; }, i = "function" == typeof Symbol ? Symbol : {}, a = i.iterator || "@@iterator", c = i.asyncIterator || "@@asyncIterator", u = i.toStringTag || "@@toStringTag"; function define(t, e, r) { return Object.defineProperty(t, e, { value: r, enumerable: !0, configurable: !0, writable: !0 }), t[e]; } try { define({}, ""); } catch (t) { define = function define(t, e, r) { return t[e] = r; }; } function wrap(t, e, r, n) { var i = e && e.prototype instanceof Generator ? e : Generator, a = Object.create(i.prototype), c = new Context(n || []); return o(a, "_invoke", { value: makeInvokeMethod(t, r, c) }), a; } function tryCatch(t, e, r) { try { return { type: "normal", arg: t.call(e, r) }; } catch (t) { return { type: "throw", arg: t }; } } e.wrap = wrap; var h = "suspendedStart", l = "suspendedYield", f = "executing", s = "completed", y = {}; function Generator() {} function GeneratorFunction() {} function GeneratorFunctionPrototype() {} var p = {}; define(p, a, function () { return this; }); var d = Object.getPrototypeOf, v = d && d(d(values([]))); v && v !== r && n.call(v, a) && (p = v); var g = GeneratorFunctionPrototype.prototype = Generator.prototype = Object.create(p); function defineIteratorMethods(t) { ["next", "throw", "return"].forEach(function (e) { define(t, e, function (t) { return this._invoke(e, t); }); }); } function AsyncIterator(t, e) { function invoke(r, o, i, a) { var c = tryCatch(t[r], t, o); if ("throw" !== c.type) { var u = c.arg, h = u.value; return h && "object" == _typeof(h) && n.call(h, "__await") ? e.resolve(h.__await).then(function (t) { invoke("next", t, i, a); }, function (t) { invoke("throw", t, i, a); }) : e.resolve(h).then(function (t) { u.value = t, i(u); }, function (t) { return invoke("throw", t, i, a); }); } a(c.arg); } var r; o(this, "_invoke", { value: function value(t, n) { function callInvokeWithMethodAndArg() { return new e(function (e, r) { invoke(t, n, e, r); }); } return r = r ? r.then(callInvokeWithMethodAndArg, callInvokeWithMethodAndArg) : callInvokeWithMethodAndArg(); } }); } function makeInvokeMethod(e, r, n) { var o = h; return function (i, a) { if (o === f) throw Error("Generator is already running"); if (o === s) { if ("throw" === i) throw a; return { value: t, done: !0 }; } for (n.method = i, n.arg = a;;) { var c = n.delegate; if (c) { var u = maybeInvokeDelegate(c, n); if (u) { if (u === y) continue; return u; } } if ("next" === n.method) n.sent = n._sent = n.arg;else if ("throw" === n.method) { if (o === h) throw o = s, n.arg; n.dispatchException(n.arg); } else "return" === n.method && n.abrupt("return", n.arg); o = f; var p = tryCatch(e, r, n); if ("normal" === p.type) { if (o = n.done ? s : l, p.arg === y) continue; return { value: p.arg, done: n.done }; } "throw" === p.type && (o = s, n.method = "throw", n.arg = p.arg); } }; } function maybeInvokeDelegate(e, r) { var n = r.method, o = e.iterator[n]; if (o === t) return r.delegate = null, "throw" === n && e.iterator["return"] && (r.method = "return", r.arg = t, maybeInvokeDelegate(e, r), "throw" === r.method) || "return" !== n && (r.method = "throw", r.arg = new TypeError("The iterator does not provide a '" + n + "' method")), y; var i = tryCatch(o, e.iterator, r.arg); if ("throw" === i.type) return r.method = "throw", r.arg = i.arg, r.delegate = null, y; var a = i.arg; return a ? a.done ? (r[e.resultName] = a.value, r.next = e.nextLoc, "return" !== r.method && (r.method = "next", r.arg = t), r.delegate = null, y) : a : (r.method = "throw", r.arg = new TypeError("iterator result is not an object"), r.delegate = null, y); } function pushTryEntry(t) { var e = { tryLoc: t[0] }; 1 in t && (e.catchLoc = t[1]), 2 in t && (e.finallyLoc = t[2], e.afterLoc = t[3]), this.tryEntries.push(e); } function resetTryEntry(t) { var e = t.completion || {}; e.type = "normal", delete e.arg, t.completion = e; } function Context(t) { this.tryEntries = [{ tryLoc: "root" }], t.forEach(pushTryEntry, this), this.reset(!0); } function values(e) { if (e || "" === e) { var r = e[a]; if (r) return r.call(e); if ("function" == typeof e.next) return e; if (!isNaN(e.length)) { var o = -1, i = function next() { for (; ++o < e.length;) if (n.call(e, o)) return next.value = e[o], next.done = !1, next; return next.value = t, next.done = !0, next; }; return i.next = i; } } throw new TypeError(_typeof(e) + " is not iterable"); } return GeneratorFunction.prototype = GeneratorFunctionPrototype, o(g, "constructor", { value: GeneratorFunctionPrototype, configurable: !0 }), o(GeneratorFunctionPrototype, "constructor", { value: GeneratorFunction, configurable: !0 }), GeneratorFunction.displayName = define(GeneratorFunctionPrototype, u, "GeneratorFunction"), e.isGeneratorFunction = function (t) { var e = "function" == typeof t && t.constructor; return !!e && (e === GeneratorFunction || "GeneratorFunction" === (e.displayName || e.name)); }, e.mark = function (t) { return Object.setPrototypeOf ? Object.setPrototypeOf(t, GeneratorFunctionPrototype) : (t.__proto__ = GeneratorFunctionPrototype, define(t, u, "GeneratorFunction")), t.prototype = Object.create(g), t; }, e.awrap = function (t) { return { __await: t }; }, defineIteratorMethods(AsyncIterator.prototype), define(AsyncIterator.prototype, c, function () { return this; }), e.AsyncIterator = AsyncIterator, e.async = function (t, r, n, o, i) { void 0 === i && (i = Promise); var a = new AsyncIterator(wrap(t, r, n, o), i); return e.isGeneratorFunction(r) ? a : a.next().then(function (t) { return t.done ? t.value : a.next(); }); }, defineIteratorMethods(g), define(g, u, "Generator"), define(g, a, function () { return this; }), define(g, "toString", function () { return "[object Generator]"; }), e.keys = function (t) { var e = Object(t), r = []; for (var n in e) r.push(n); return r.reverse(), function next() { for (; r.length;) { var t = r.pop(); if (t in e) return next.value = t, next.done = !1, next; } return next.done = !0, next; }; }, e.values = values, Context.prototype = { constructor: Context, reset: function reset(e) { if (this.prev = 0, this.next = 0, this.sent = this._sent = t, this.done = !1, this.delegate = null, this.method = "next", this.arg = t, this.tryEntries.forEach(resetTryEntry), !e) for (var r in this) "t" === r.charAt(0) && n.call(this, r) && !isNaN(+r.slice(1)) && (this[r] = t); }, stop: function stop() { this.done = !0; var t = this.tryEntries[0].completion; if ("throw" === t.type) throw t.arg; return this.rval; }, dispatchException: function dispatchException(e) { if (this.done) throw e; var r = this; function handle(n, o) { return a.type = "throw", a.arg = e, r.next = n, o && (r.method = "next", r.arg = t), !!o; } for (var o = this.tryEntries.length - 1; o >= 0; --o) { var i = this.tryEntries[o], a = i.completion; if ("root" === i.tryLoc) return handle("end"); if (i.tryLoc <= this.prev) { var c = n.call(i, "catchLoc"), u = n.call(i, "finallyLoc"); if (c && u) { if (this.prev < i.catchLoc) return handle(i.catchLoc, !0); if (this.prev < i.finallyLoc) return handle(i.finallyLoc); } else if (c) { if (this.prev < i.catchLoc) return handle(i.catchLoc, !0); } else { if (!u) throw Error("try statement without catch or finally"); if (this.prev < i.finallyLoc) return handle(i.finallyLoc); } } } }, abrupt: function abrupt(t, e) { for (var r = this.tryEntries.length - 1; r >= 0; --r) { var o = this.tryEntries[r]; if (o.tryLoc <= this.prev && n.call(o, "finallyLoc") && this.prev < o.finallyLoc) { var i = o; break; } } i && ("break" === t || "continue" === t) && i.tryLoc <= e && e <= i.finallyLoc && (i = null); var a = i ? i.completion : {}; return a.type = t, a.arg = e, i ? (this.method = "next", this.next = i.finallyLoc, y) : this.complete(a); }, complete: function complete(t, e) { if ("throw" === t.type) throw t.arg; return "break" === t.type || "continue" === t.type ? this.next = t.arg : "return" === t.type ? (this.rval = this.arg = t.arg, this.method = "return", this.next = "end") : "normal" === t.type && e && (this.next = e), y; }, finish: function finish(t) { for (var e = this.tryEntries.length - 1; e >= 0; --e) { var r = this.tryEntries[e]; if (r.finallyLoc === t) return this.complete(r.completion, r.afterLoc), resetTryEntry(r), y; } }, "catch": function _catch(t) { for (var e = this.tryEntries.length - 1; e >= 0; --e) { var r = this.tryEntries[e]; if (r.tryLoc === t) { var n = r.completion; if ("throw" === n.type) { var o = n.arg; resetTryEntry(r); } return o; } } throw Error("illegal catch attempt"); }, delegateYield: function delegateYield(e, r, n) { return this.delegate = { iterator: values(e), resultName: r, nextLoc: n }, "next" === this.method && (this.arg = t), y; } }, e; }
function ownKeys(e, r) { var t = Object.keys(e); if (Object.getOwnPropertySymbols) { var o = Object.getOwnPropertySymbols(e); r && (o = o.filter(function (r) { return Object.getOwnPropertyDescriptor(e, r).enumerable; })), t.push.apply(t, o); } return t; }
function _objectSpread(e) { for (var r = 1; r < arguments.length; r++) { var t = null != arguments[r] ? arguments[r] : {}; r % 2 ? ownKeys(Object(t), !0).forEach(function (r) { _defineProperty(e, r, t[r]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys(Object(t)).forEach(function (r) { Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r)); }); } return e; }
function _defineProperty(e, r, t) { return (r = _toPropertyKey(r)) in e ? Object.defineProperty(e, r, { value: t, enumerable: !0, configurable: !0, writable: !0 }) : e[r] = t, e; }
function _toPropertyKey(t) { var i = _toPrimitive(t, "string"); return "symbol" == _typeof(i) ? i : i + ""; }
function _toPrimitive(t, r) { if ("object" != _typeof(t) || !t) return t; var e = t[Symbol.toPrimitive]; if (void 0 !== e) { var i = e.call(t, r || "default"); if ("object" != _typeof(i)) return i; throw new TypeError("@@toPrimitive must return a primitive value."); } return ("string" === r ? String : Number)(t); }
function asyncGeneratorStep(n, t, e, r, o, a, c) { try { var i = n[a](c), u = i.value; } catch (n) { return void e(n); } i.done ? t(u) : Promise.resolve(u).then(r, o); }
function _asyncToGenerator(n) { return function () { var t = this, e = arguments; return new Promise(function (r, o) { var a = n.apply(t, e); function _next(n) { asyncGeneratorStep(a, r, o, _next, _throw, "next", n); } function _throw(n) { asyncGeneratorStep(a, r, o, _next, _throw, "throw", n); } _next(void 0); }); }; }
var envRepository = (0, _repository["default"])();
var sendApiRepository = (0, _repository_sendapi["default"])();
var mapClientResourceRepository = (0, _repository2["default"])();
var requestResponseRepo = (0, _repository3["default"])();
var helpers = (0, _helpers["default"])();
function callApiTransporterRepository() {
  var dataRes = [];
  var idx = 0;
  var apiResult = [];
  var matchData = [];
  var checkSourceEdge = [];
  var collectingRequestApi = /*#__PURE__*/function () {
    var _ref = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee(req) {
      var getFromReq, listResponse, getDetail, resultsGetDetail, getDataFlow, startPoint, flowData, sendApiQueue, resultSend, itemIdx, data, apiData, requirementSend, resultSendApi, transporterResult;
      return _regeneratorRuntime().wrap(function _callee$(_context) {
        while (1) switch (_context.prev = _context.next) {
          case 0:
            getFromReq = {};
            dataRes = [];
            listResponse = [];
            idx = 0;
            apiResult = [];
            matchData = [];
            checkSourceEdge = [];
            getFromReq['path'] = "/" + req.path.split('/').slice(1).join('/');
            getFromReq['method'] = req.method;
            getFromReq['url'] = req.originalUrl.split('/')[3];
            getFromReq['query'] = Object.keys(req.query).length > 0 ? req.query : null;
            getFromReq['fullUrl'] = req.protocol + '://' + req.get('host') + req.originalUrl;
            if (!(!getFromReq.query || !getFromReq.query.hasOwnProperty('request_id'))) {
              _context.next = 14;
              break;
            }
            return _context.abrupt("return", (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.methodNotAllowed405,
              message: "Request not allowed: parameter request_id not found"
            }));
          case 14:
            _context.next = 16;
            return detailTransporter(getFromReq.query.request_id);
          case 16:
            getDetail = _context.sent;
            if (!(getDetail.code !== 200)) {
              _context.next = 19;
              break;
            }
            return _context.abrupt("return", getDetail);
          case 19:
            resultsGetDetail = getDetail.results; // Menyamakan path dengan data dari db
            if (!(resultsGetDetail.path !== getFromReq.path)) {
              _context.next = 22;
              break;
            }
            return _context.abrupt("return", (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.badRequest400,
              message: "Path not match for request_id: " + getFromReq.query.request_id
            }));
          case 22:
            _context.next = 24;
            return getSourceFlow(getFromReq.query.request_id);
          case 24:
            getDataFlow = _context.sent;
            if (getDataFlow) {
              _context.next = 27;
              break;
            }
            return _context.abrupt("return", (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.badRequest400,
              message: "Node no connected !"
            }));
          case 27:
            _context.next = 29;
            return getStartPoint(getFromReq.query.request_id, getDataFlow);
          case 29:
            startPoint = _context.sent;
            if (startPoint) {
              _context.next = 32;
              break;
            }
            return _context.abrupt("return", (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.badRequest400,
              message: "Cant find starting point !"
            }));
          case 32:
            //Push starting point
            dataRes.push(startPoint);

            // Get flow all
            _context.next = 35;
            return _getSourceEdgeRecurse(startPoint, getDataFlow);
          case 35:
            flowData = _context.sent;
            if (flowData) {
              _context.next = 38;
              break;
            }
            return _context.abrupt("return", (0, _responseApi.result)({
              message: "Flow data cant found",
              data: dataRes
            }));
          case 38:
            _context.next = 40;
            return _queueSendAPI({
              dataRes: dataRes,
              objData: startPoint
            }, req);
          case 40:
            sendApiQueue = _context.sent;
            if (!sendApiQueue) {
              _context.next = 43;
              break;
            }
            return _context.abrupt("return", (0, _responseApi.result)({
              data: {
                dataRes: {
                  length: dataRes.length,
                  data: dataRes
                },
                checkSourceEdge: {
                  length: checkSourceEdge.length,
                  data: checkSourceEdge
                }
              }
            }));
          case 43:
            itemIdx = 0;
          case 44:
            if (!(itemIdx < dataRes.length)) {
              _context.next = 76;
              break;
            }
            if (!(dataRes[itemIdx].node && dataRes[itemIdx].node.data)) {
              _context.next = 73;
              break;
            }
            data = dataRes[itemIdx].node.data; // Jika endpoint berasal dari MasterAPI <Trigger Parameter di Exclude dulu fokus send to API>
            if (!(data.hasOwnProperty('environtment') && data.hasOwnProperty('masterApi'))) {
              _context.next = 73;
              break;
            }
            // tambahan query untuk testing
            data.nodeParameter.params.query.push({
              key: {
                keyValue: "test_request",
                readOnly: true
              },
              value: {
                keyValue: "false",
                readOnly: true
              },
              disabledDelete: true
            });
            _context.next = 51;
            return masterApiMapping(data, dataRes[itemIdx], req);
          case 51:
            apiData = _context.sent;
            if (!(apiData.code && apiData.code !== 200)) {
              _context.next = 54;
              break;
            }
            return _context.abrupt("return", apiData);
          case 54:
            requirementSend = {
              path: "/" + apiData.path.split('/').slice(7).join('/'),
              method: apiData.method,
              url: apiData.environtment.envName,
              query: apiData.nodeParameter.params.query,
              fullUrl: apiData.fullPath,
              body: apiData.nodeParameter.request,
              env: apiData.envSelected.varName.toLowerCase()
            }; // return await sendApiRepository.mappingRequestApiFromTransporter(requirementSend);
            _context.next = 57;
            return sendApiRepository.mappingRequestApiFromTransporter(requirementSend);
          case 57:
            resultSendApi = _context.sent;
            transporterResult = {
              flowId: dataRes[itemIdx].id,
              target: dataRes[itemIdx].target,
              source: dataRes[itemIdx].source,
              fullPath: apiData.fullPath,
              requestId: requirementSend.query.request_id,
              path: requirementSend.path,
              service: requirementSend.url,
              params: apiData.nodeParameter.params,
              environment: requirementSend.env,
              body: requirementSend.body,
              response: resultSendApi,
              responseData: resultSendApi.results
            };
            if (!(resultSendApi.code !== 200)) {
              _context.next = 61;
              break;
            }
            return _context.abrupt("return", _objectSpread(_objectSpread({}, resultSendApi), {}, _defineProperty({}, 'results', transporterResult)));
          case 61:
            if (!(apiData.nodeParameter.responsePropertyKey && apiData.nodeParameter.responsePropertyKey.length > 0)) {
              _context.next = 67;
              break;
            }
            if (resultSendApi.results.hasOwnProperty("".concat(apiData.nodeParameter.responsePropertyKey))) {
              _context.next = 64;
              break;
            }
            return _context.abrupt("return", (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.badRequest400,
              message: "Response property key not found",
              data: transporterResult
            }));
          case 64:
            if (!Array.isArray(resultSendApi.results["".concat(apiData.nodeParameter.responsePropertyKey)])) {
              _context.next = 67;
              break;
            }
            if (!(resultSendApi.results["".concat(apiData.nodeParameter.responsePropertyKey)].length > 1)) {
              _context.next = 67;
              break;
            }
            return _context.abrupt("return", (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.badRequest400,
              message: "Results exceed more than 1 data, please get one data with a more specific search !",
              data: transporterResult
            }));
          case 67:
            if (!(!apiData.nodeParameter.responsePropertyKey && Array.isArray(resultSendApi.results))) {
              _context.next = 72;
              break;
            }
            if (!(resultSendApi.results.length > 1 && itemIdx !== dataRes.length - 1)) {
              _context.next = 70;
              break;
            }
            return _context.abrupt("return", (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.badRequest400,
              message: "Results exceed more than 1 data, please get one data with a more specific search !",
              data: transporterResult
            }));
          case 70:
            // Jika response list index dalam posisi terakhir maka response dengan hasil list tetap ditambahkan ke dalam response
            if (itemIdx === dataRes.length - 1) {
              transporterResult.responseData = resultSendApi.results;
            }
            // Jika response list index tidak dalam posisi terakhir maka list tersebut akan di rubah ke object
            if (itemIdx !== dataRes.length - 1) {
              transporterResult.responseData = resultSendApi.results[0];
            }
          case 72:
            //Push result data
            listResponse.push(transporterResult);

            // return result({
            //     data: transporterResult
            // })
          case 73:
            itemIdx++;
            _context.next = 44;
            break;
          case 76:
            return _context.abrupt("return", (0, _responseApi.result)({
              data: listResponse
            }));
          case 77:
          case "end":
            return _context.stop();
        }
      }, _callee);
    }));
    return function collectingRequestApi(_x) {
      return _ref.apply(this, arguments);
    };
  }();

  // todo loop send API
  var _queueSendAPI = /*#__PURE__*/function () {
    var _ref3 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee2(_ref2, req) {
      var _ref2$dataRes, dataRes, _ref2$objData, objData, dataFlow, apiData, requirementSend, resultSendApi, checkNextData, checkCondition;
      return _regeneratorRuntime().wrap(function _callee2$(_context2) {
        while (1) switch (_context2.prev = _context2.next) {
          case 0:
            _ref2$dataRes = _ref2.dataRes, dataRes = _ref2$dataRes === void 0 ? [] : _ref2$dataRes, _ref2$objData = _ref2.objData, objData = _ref2$objData === void 0 ? {} : _ref2$objData;
            // idx += 1
            dataFlow = {
              id: objData.id,
              source: objData.source,
              target: objData.target
            }; // console.log('dataFlow', dataFlow)
            if (!(Object.keys(objData).length > 0)) {
              _context2.next = 41;
              break;
            }
            if (typeof objData.node.data === "string" && objData.node.data.length > 0) {
              objData.node.data = JSON.parse(objData.node.data);
            }
            if (!(!objData.target.includes('OUTPUT') && !objData.target.includes('CONDITION'))) {
              _context2.next = 36;
              break;
            }
            _context2.next = 7;
            return masterApiMapping(objData.node.data, objData, req);
          case 7:
            apiData = _context2.sent;
            if (!(apiData.code && apiData.code !== 200)) {
              _context2.next = 10;
              break;
            }
            return _context2.abrupt("return", apiData);
          case 10:
            requirementSend = {
              path: "/" + apiData.path.split('/').slice(7).join('/'),
              method: apiData.method,
              url: apiData.environtment.envName,
              query: apiData.nodeParameter.params.query,
              fullUrl: apiData.fullPath,
              body: apiData.nodeParameter.request,
              env: apiData.envSelected.varName.toLowerCase()
            };
            _context2.next = 13;
            return sendApiRepository.mappingRequestApiFromTransporter(requirementSend);
          case 13:
            resultSendApi = _context2.sent;
            apiResult.push(objData);
            if (!(idx < dataRes.length)) {
              _context2.next = 36;
              break;
            }
            idx += 1;
            checkNextData = dataRes[idx];
            console.log('checkNextData', {
              dataSource: dataFlow,
              id: checkNextData.id,
              source: checkNextData.source,
              target: checkNextData.target,
              label: checkNextData.label
            });
            if (!(checkNextData.source === objData.source)) {
              _context2.next = 24;
              break;
            }
            _context2.next = 22;
            return _getSourceEdgeRecurse_returned(checkNextData, dataRes, {
              match: []
            });
          case 22:
            checkSourceEdge = _context2.sent;
            // dataRes = checkSourceEdge // di comment dulu untuk periksa getSourceEdgeRecurse_returned
            if (checkSourceEdge.length > 0) {
              console.log('checkSourceEdge >> ', checkSourceEdge.length);
            }
          case 24:
            if (!(resultSendApi.code <= 400)) {
              _context2.next = 36;
              break;
            }
            if (!checkNextData.target.includes('OUTPUT')) {
              _context2.next = 28;
              break;
            }
            _context2.next = 28;
            return _queueSendAPI({
              dataRes: dataRes,
              objData: dataRes[idx]
            }, req);
          case 28:
            if (!checkNextData.target.includes('CONDITION')) {
              _context2.next = 35;
              break;
            }
            checkCondition = dataRes.findIndex(function (el) {
              return el.source === checkNextData.target && el.label === 'yes';
            }); // Jika condition tidak ada target maka dia error
            if (!(checkCondition === -1)) {
              _context2.next = 32;
              break;
            }
            return _context2.abrupt("return", (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.badRequest400,
              message: "There is no target YES after condition !",
              data: {
                id: objData.id,
                source: objData.source,
                target: objData.target
              }
            }));
          case 32:
            idx = checkCondition;
            _context2.next = 35;
            return _queueSendAPI({
              dataRes: dataRes,
              objData: dataRes[idx]
            }, req);
          case 35:
            if (!checkNextData.target.includes('OUTPUT') && !checkNextData.target.includes('CONDITION')) {
              console.log('checkNextData_TRUE', {
                dataSource: dataFlow,
                dataRes: {
                  id: dataRes[idx].id,
                  source: dataRes[idx].source,
                  target: dataRes[idx].target,
                  label: dataRes[idx].label
                },
                id: checkNextData.id,
                source: checkNextData.source,
                target: checkNextData.target,
                label: checkNextData.label
              });
              // await queueSendAPI({dataRes: dataRes, objData: dataRes[idx]}, req);
            }
          case 36:
            if (!objData.target.includes('OUTPUT')) {
              _context2.next = 41;
              break;
            }
            if (!(idx < dataRes.length)) {
              _context2.next = 41;
              break;
            }
            idx += 1;
            _context2.next = 41;
            return _queueSendAPI({
              dataRes: dataRes,
              objData: dataRes[idx]
            }, req);
          case 41:
            return _context2.abrupt("return", true);
          case 42:
          case "end":
            return _context2.stop();
        }
      }, _callee2);
    }));
    return function queueSendAPI(_x2, _x3) {
      return _ref3.apply(this, arguments);
    };
  }();
  var queueSendAPI_old = /*#__PURE__*/function () {
    var _ref5 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee3(_ref4, req) {
      var _ref4$dataRes, dataRes, _ref4$objData, objData, dataFlow, apiData, requirementSend, resultSendApi, findNextProcess;
      return _regeneratorRuntime().wrap(function _callee3$(_context3) {
        while (1) switch (_context3.prev = _context3.next) {
          case 0:
            _ref4$dataRes = _ref4.dataRes, dataRes = _ref4$dataRes === void 0 ? [] : _ref4$dataRes, _ref4$objData = _ref4.objData, objData = _ref4$objData === void 0 ? {} : _ref4$objData;
            if (!(dataRes.length === 0)) {
              _context3.next = 3;
              break;
            }
            return _context3.abrupt("return", (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.badRequest400,
              message: "No data to send API"
            }));
          case 3:
            dataFlow = {
              id: objData.id,
              source: objData.source,
              target: objData.target
            };
            if (!(idx === dataRes.length - 1)) {
              _context3.next = 6;
              break;
            }
            return _context3.abrupt("return", (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.ok200,
              message: "No data to send API"
            }));
          case 6:
            console.log('LOSS', dataFlow);
            if (!(Object.keys(objData).length > 0)) {
              _context3.next = 45;
              break;
            }
            if (!(!objData.target.includes('OUTPUT') && !objData.target.includes('CONDITION'))) {
              _context3.next = 39;
              break;
            }
            // Parse data jika tersedia dan bersifat string
            if (typeof objData.node.data === "string" && objData.node.data.length > 0) {
              objData.node.data = JSON.parse(objData.node.data);
            }
            // console.log(objData.node.data)

            //Send API
            _context3.next = 12;
            return masterApiMapping(objData.node.data, objData, req);
          case 12:
            apiData = _context3.sent;
            if (!(apiData.code && apiData.code !== 200)) {
              _context3.next = 15;
              break;
            }
            return _context3.abrupt("return", apiData);
          case 15:
            requirementSend = {
              path: "/" + apiData.path.split('/').slice(7).join('/'),
              method: apiData.method,
              url: apiData.environtment.envName,
              query: apiData.nodeParameter.params.query,
              fullUrl: apiData.fullPath,
              body: apiData.nodeParameter.request,
              env: apiData.envSelected.varName.toLowerCase()
            };
            _context3.next = 18;
            return sendApiRepository.mappingRequestApiFromTransporter(requirementSend);
          case 18:
            resultSendApi = _context3.sent;
            apiResult.push(objData);
            findNextProcess = dataRes.find(function (elNextProcess) {
              return elNextProcess.source === objData.target;
            }); // console.log("findNextProcess > ", !findNextProcess)
            if (findNextProcess) {
              _context3.next = 26;
              break;
            }
            if (!(idx <= dataRes.length - 1)) {
              _context3.next = 26;
              break;
            }
            idx += 1;
            _context3.next = 26;
            return _queueSendAPI({
              dataRes: dataRes,
              objData: dataRes[idx]
            }, req);
          case 26:
            if (!(findNextProcess && findNextProcess.target.includes('OUTPUT') || findNextProcess.target.includes('CONDITION'))) {
              _context3.next = 31;
              break;
            }
            if (!(idx <= dataRes.length - 1)) {
              _context3.next = 31;
              break;
            }
            idx += 1;
            _context3.next = 31;
            return _queueSendAPI({
              dataRes: dataRes,
              objData: dataRes[idx]
            }, req);
          case 31:
            if (!(resultSendApi.code >= 400 && !findNextProcess)) {
              _context3.next = 33;
              break;
            }
            return _context3.abrupt("return", (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.badRequest400,
              message: "Request failed",
              data: requirementSend
            }));
          case 33:
            console.log('find >> ', idx, ' length > ', dataRes.length);
            if (!(idx <= dataRes.length - 1)) {
              _context3.next = 39;
              break;
            }
            idx += 1;
            console.log();
            _context3.next = 39;
            return _queueSendAPI({
              dataRes: dataRes,
              objData: dataRes[idx]
            }, req);
          case 39:
            if (!objData.target.includes('OUTPUT')) {
              _context3.next = 45;
              break;
            }
            if (!(idx <= dataRes.length - 1)) {
              _context3.next = 45;
              break;
            }
            apiResult.push(dataRes[idx]);
            idx += 1;
            _context3.next = 45;
            return _queueSendAPI({
              dataRes: dataRes,
              objData: dataRes[idx]
            }, req);
          case 45:
          case "end":
            return _context3.stop();
        }
      }, _callee3);
    }));
    return function queueSendAPI_old(_x4, _x5) {
      return _ref5.apply(this, arguments);
    };
  }();

  // Berasal Dari Master API Mapping
  var masterApiMapping = /*#__PURE__*/function () {
    var _ref6 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee4(dataObj, dataFlow, requestFromStart) {
      var getFromReq, objQueryRequest, objParamsRequest, objUrlEncodedRequest, resultMapping, fullPath, path, queryAvailable, variableAvailable, rawJson, resultCheck;
      return _regeneratorRuntime().wrap(function _callee4$(_context4) {
        while (1) switch (_context4.prev = _context4.next) {
          case 0:
            getFromReq = {};
            objQueryRequest = {};
            objParamsRequest = {};
            objUrlEncodedRequest = {};
            resultMapping = {};
            if (!(dataObj.nodeParameter.params.query.length === 0)) {
              _context4.next = 7;
              break;
            }
            return _context4.abrupt("return", (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.badRequest400,
              message: "Invalid request, query not found !",
              data: dataObj.nodeParameter.params.query
            }));
          case 7:
            if (!(!dataObj.envSelected || Object.keys(dataObj.envSelected).length === 0)) {
              _context4.next = 9;
              break;
            }
            return _context4.abrupt("return", (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.badRequest400,
              message: "Invalid request, env not selected !",
              data: dataObj
            }));
          case 9:
            fullPath = dataObj.masterApi.serverAddress + dataObj.masterApi.baseUrl + '/' + dataObj.environtment.envName + '/' + dataObj.envSelected.varName.toLowerCase() + dataObj.masterApi.path;
            path = fullPath; // Todo query mapping ke URL
            queryAvailable = "";
            dataObj.nodeParameter.params.query.map(function (el) {
              var valueDt = el.value.keyValue;

              // todo check value jika terdapat tag maka data tersebut perlu di rubah
              if (/#([^#]+)#/g.test(valueDt)) {
                if (valueDt.includes('INPUT') && valueDt.includes('Request')) {
                  valueDt = valueDt.replace(/#/g, '');
                  var fieldFromRequest = valueDt.split('_').slice(3).join('');
                  var fieldFromRequestBody = Object.keys(requestFromStart.body).find(function (el) {
                    return el === fieldFromRequest;
                  });
                  if (!fieldFromRequestBody) {
                    resultMapping = {
                      statusCode: _responseApi.statusHttpCode.badRequest400,
                      message: "API ".concat(dataFlow.target, " Params query from field : ").concat(fieldFromRequest, " not found by request")
                    };
                  }
                  valueDt = requestFromStart.body[fieldFromRequestBody];
                }
              }
              objQueryRequest = _objectSpread(_objectSpread({}, objQueryRequest), {}, _defineProperty({}, el.key.keyValue, valueDt));
              if (queryAvailable.length === 0) {
                queryAvailable = queryAvailable + '?' + "".concat(el.key.keyValue, "=").concat(valueDt);
              } else {
                queryAvailable = queryAvailable + '&' + "".concat(el.key.keyValue, "=").concat(valueDt);
              }
              dataObj.nodeParameter.params.query = objQueryRequest;
            });

            // Todo params variable mapping to url
            variableAvailable = "";
            if (dataObj.nodeParameter.params.variable.length > 0) {
              dataObj.nodeParameter.params.variable.map(function (el) {
                var valueDt = el.value.keyValue;

                // todo check value jika terdapat tag maka data tersebut perlu di rubah
                if (/#([^#]+)#/g.test(valueDt)) {
                  if (valueDt.includes('INPUT') && valueDt.includes('Request')) {
                    valueDt = valueDt.replace(/#/g, '');
                    var fieldFromRequest = valueDt.split('_').slice(3).join('');
                    var fieldFromRequestBody = Object.keys(requestFromStart.body).find(function (el) {
                      return el === fieldFromRequest;
                    });
                    if (!fieldFromRequestBody) {
                      resultMapping = {
                        statusCode: _responseApi.statusHttpCode.badRequest400,
                        message: "API ".concat(dataFlow.target, " Params query from field : ").concat(fieldFromRequest, " not found by request")
                      };
                    }
                    valueDt = requestFromStart.body[fieldFromRequestBody];
                  }
                }
                objParamsRequest = _objectSpread(_objectSpread({}, objParamsRequest), {}, _defineProperty({}, el.key.keyValue, valueDt));
                var pattern = new RegExp(":".concat(el.key.keyValue));
                if (variableAvailable.length === 0) {
                  variableAvailable = fullPath.replace(pattern, valueDt);
                } else {
                  variableAvailable = variableAvailable.replace(pattern, valueDt);
                }
                dataObj.nodeParameter.params.variable = objParamsRequest;
              });
              fullPath = variableAvailable;
              path = variableAvailable;
            }

            // todo setup new full path after trigger
            dataObj.fullPath = fullPath + queryAvailable;
            dataObj.path = path;

            // todo Replacing request value from mode raw
            if (!(dataObj.nodeParameter && dataObj.nodeParameter.request && dataObj.nodeParameter.request.mode === 'raw')) {
              _context4.next = 26;
              break;
            }
            if (typeof dataObj.nodeParameter.request.rawJson === 'string' && dataObj.nodeParameter.request.rawJson.length > 0) {
              rawJson = JSON.parse(dataObj.nodeParameter.request.rawJson);
            } else {
              rawJson = dataObj.nodeParameter.request.rawJson;
            }
            _context4.next = 21;
            return _checkObjectParameter(rawJson, requestFromStart);
          case 21:
            resultCheck = _context4.sent;
            if (resultCheck) {
              _context4.next = 25;
              break;
            }
            dataObj.nodeParameter.request.rawJson = rawJson;
            return _context4.abrupt("return", (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.badRequest400,
              message: "Key property not valid for this object !",
              request: {
                flowId: dataFlow.id,
                source: dataFlow.source,
                target: dataFlow.target,
                data: dataObj.nodeParameter
              }
            }));
          case 25:
            dataObj.nodeParameter.request.rawJson = rawJson;
          case 26:
            // todo Replacing request value from mode urlencoded
            if (dataObj.nodeParameter && dataObj.nodeParameter.request && dataObj.nodeParameter.request.mode === 'urlencoded') {
              if (dataObj.nodeParameter.request.formUrlEncoded.length > 0) {
                dataObj.nodeParameter.request.formUrlEncoded.map(function (el) {
                  var valueDt = el.value.keyValue;
                  if (/#([^#]+)#/g.test(valueDt)) {
                    // todo get resource from request
                    if (valueDt.includes('INPUT') && valueDt.includes('Request')) {
                      valueDt = valueDt.replace(/#/g, '');
                      var fieldFromRequest = valueDt.split('_').slice(3).join('');
                      var fieldFromRequestBody = Object.keys(requestFromStart.body).find(function (el) {
                        return el === fieldFromRequest;
                      });
                      if (!fieldFromRequestBody) {
                        resultMapping = {
                          statusCode: _responseApi.statusHttpCode.badRequest400,
                          message: "API ".concat(dataFlow.target, " Request UrlEncoded from field : ").concat(fieldFromRequest, " not found by request")
                        };
                      }
                      el.value.keyValue = requestFromStart.body[fieldFromRequestBody];
                    }
                  }
                  objUrlEncodedRequest = _objectSpread(_objectSpread({}, objUrlEncodedRequest), {}, _defineProperty({}, el.key.keyValue, valueDt));
                });
                dataObj.nodeParameter.request.formUrlEncoded = objUrlEncodedRequest;
              }
            }

            // todo finally point
            if (!(Object.keys(resultMapping).length > 0 && resultMapping.statusCode.code === 400)) {
              _context4.next = 29;
              break;
            }
            return _context4.abrupt("return", (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.badRequest400,
              message: resultMapping.message
            }));
          case 29:
            return _context4.abrupt("return", dataObj);
          case 30:
          case "end":
            return _context4.stop();
        }
      }, _callee4);
    }));
    return function masterApiMapping(_x6, _x7, _x8) {
      return _ref6.apply(this, arguments);
    };
  }();

  /** todo helpers */
  var _checkObjectParameter = /*#__PURE__*/function () {
    var _ref7 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee5(objRes, req) {
      var matches,
        _iterator,
        _step,
        arrayItem,
        _loop,
        _ret,
        _i,
        _Object$keys,
        _args6 = arguments;
      return _regeneratorRuntime().wrap(function _callee5$(_context6) {
        while (1) switch (_context6.prev = _context6.next) {
          case 0:
            matches = _args6.length > 2 && _args6[2] !== undefined ? _args6[2] : {};
            if (!objRes) {
              _context6.next = 33;
              break;
            }
            if (!Array.isArray(objRes)) {
              _context6.next = 22;
              break;
            }
            _iterator = _createForOfIteratorHelper(objRes);
            _context6.prev = 4;
            _iterator.s();
          case 6:
            if ((_step = _iterator.n()).done) {
              _context6.next = 12;
              break;
            }
            arrayItem = _step.value;
            _context6.next = 10;
            return _checkObjectParameter(arrayItem, req, matches);
          case 10:
            _context6.next = 6;
            break;
          case 12:
            _context6.next = 17;
            break;
          case 14:
            _context6.prev = 14;
            _context6.t0 = _context6["catch"](4);
            _iterator.e(_context6.t0);
          case 17:
            _context6.prev = 17;
            _iterator.f();
            return _context6.finish(17);
          case 20:
            _context6.next = 33;
            break;
          case 22:
            if (!(_typeof(objRes) == 'object')) {
              _context6.next = 33;
              break;
            }
            _loop = /*#__PURE__*/_regeneratorRuntime().mark(function _loop() {
              var key, removeTag, checkNodeDataFor, matchingKey;
              return _regeneratorRuntime().wrap(function _loop$(_context5) {
                while (1) switch (_context5.prev = _context5.next) {
                  case 0:
                    key = _Object$keys[_i];
                    if (!(typeof objRes[key] === 'string')) {
                      _context5.next = 19;
                      break;
                    }
                    if (!/#([^#]+)#/g.test(objRes[key])) {
                      _context5.next = 17;
                      break;
                    }
                    // console.log(objRes)
                    removeTag = objRes[key].replace(/#/g, '');
                    checkNodeDataFor = removeTag.split('_'); // Jika 4 maka field berdasarkan dari INPUT
                    if (!(checkNodeDataFor.length === 4 && removeTag.includes('INPUT'))) {
                      _context5.next = 16;
                      break;
                    }
                    if (!(checkNodeDataFor[2].toLowerCase() === 'request')) {
                      _context5.next = 16;
                      break;
                    }
                    _context5.next = 9;
                    return Object.keys(req.body).find(function (key) {
                      return key.toLowerCase() === checkNodeDataFor[3].toLowerCase();
                    });
                  case 9:
                    matchingKey = _context5.sent;
                    if (!matchingKey) {
                      _context5.next = 15;
                      break;
                    }
                    objRes[key] = req.body[matchingKey];
                    matches = objRes;
                    _context5.next = 16;
                    break;
                  case 15:
                    return _context5.abrupt("return", {
                      v: null
                    });
                  case 16:
                    // Jika 3 maka field berdasarkan dari RESPONSE
                    if (checkNodeDataFor.length === 3) {}
                  case 17:
                    _context5.next = 21;
                    break;
                  case 19:
                    _context5.next = 21;
                    return _checkObjectParameter(objRes[key], req, matches);
                  case 21:
                  case "end":
                    return _context5.stop();
                }
              }, _loop);
            });
            _i = 0, _Object$keys = Object.keys(objRes);
          case 25:
            if (!(_i < _Object$keys.length)) {
              _context6.next = 33;
              break;
            }
            return _context6.delegateYield(_loop(), "t1", 27);
          case 27:
            _ret = _context6.t1;
            if (!_ret) {
              _context6.next = 30;
              break;
            }
            return _context6.abrupt("return", _ret.v);
          case 30:
            _i++;
            _context6.next = 25;
            break;
          case 33:
            return _context6.abrupt("return", matches);
          case 34:
          case "end":
            return _context6.stop();
        }
      }, _callee5, null, [[4, 14, 17, 20]]);
    }));
    return function checkObjectParameter(_x9, _x10) {
      return _ref7.apply(this, arguments);
    };
  }();
  var detailTransporter = /*#__PURE__*/function () {
    var _ref8 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee6(id) {
      var getDetail;
      return _regeneratorRuntime().wrap(function _callee6$(_context7) {
        while (1) switch (_context7.prev = _context7.next) {
          case 0:
            _context7.prev = 0;
            _context7.next = 3;
            return _models.transporter.findOne({
              where: {
                id: id
              }
            });
          case 3:
            getDetail = _context7.sent;
            if (getDetail) {
              _context7.next = 8;
              break;
            }
            _context7.next = 7;
            return (0, _responseApi.result)({
              message: "ID ".concat(id, " Not Found"),
              statusCode: _responseApi.statusHttpCode.badRequest400
            });
          case 7:
            return _context7.abrupt("return", _context7.sent);
          case 8:
            getDetail.responseTrigger = getDetail.responseTrigger ? JSON.parse(getDetail.responseTrigger) : getDetail.responseTrigger;
            getDetail.params = getDetail.params ? JSON.parse(getDetail.params) : getDetail.params;
            getDetail.bodyRequest = getDetail.bodyRequest ? JSON.parse(getDetail.bodyRequest) : getDetail.bodyRequest;
            getDetail.mappingTemplate = getDetail.mappingTemplate ? JSON.parse(getDetail.mappingTemplate) : getDetail.mappingTemplate;
            getDetail.request = getDetail.request ? JSON.parse(getDetail.request) : getDetail.request;
            getDetail.requestExample = getDetail.requestExample ? JSON.parse(getDetail.requestExample) : getDetail.requestExample;
            getDetail.responseExample = getDetail.responseExample ? JSON.parse(getDetail.responseExample) : getDetail.responseExample;
            _context7.next = 17;
            return (0, _responseApi.result)({
              data: getDetail
            });
          case 17:
            return _context7.abrupt("return", _context7.sent);
          case 20:
            _context7.prev = 20;
            _context7.t0 = _context7["catch"](0);
            _context7.t0.message = "transporter.detail: " + _context7.t0;
            throw _context7.t0;
          case 24:
          case "end":
            return _context7.stop();
        }
      }, _callee6, null, [[0, 20]]);
    }));
    return function detailTransporter(_x11) {
      return _ref8.apply(this, arguments);
    };
  }();
  var getSourceFlow = /*#__PURE__*/function () {
    var _ref9 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee7(requestId) {
      return _regeneratorRuntime().wrap(function _callee7$(_context8) {
        while (1) switch (_context8.prev = _context8.next) {
          case 0:
            _context8.prev = 0;
            _context8.next = 3;
            return _models.transporterGateway.findAll({
              attributes: [[_sequelize.Sequelize.col('gate1.id'), "id"], [_sequelize.Sequelize.col('gate1.source'), "source"], [_sequelize.Sequelize.col('gate1.target'), "target"], [_sequelize.Sequelize.col('gate1.label'), "label"]],
              include: [{
                model: _models.transporterGateway,
                as: "gate1",
                required: true,
                attributes: []
              }, {
                model: _models.transporterGateway,
                as: "node",
                required: true,
                attributes: ['id', 'label', 'data']
              }],
              where: {
                requestId: requestId
              }
            });
          case 3:
            return _context8.abrupt("return", _context8.sent);
          case 6:
            _context8.prev = 6;
            _context8.t0 = _context8["catch"](0);
            _context8.t0.message = "transporterGateway.getSourceFlow: " + _context8.t0;
            throw _context8.t0;
          case 10:
          case "end":
            return _context8.stop();
        }
      }, _callee7, null, [[0, 6]]);
    }));
    return function getSourceFlow(_x12) {
      return _ref9.apply(this, arguments);
    };
  }();
  var getStartPoint = /*#__PURE__*/function () {
    var _ref10 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee8(requestId, flow) {
      var conditionGate, startPoint;
      return _regeneratorRuntime().wrap(function _callee8$(_context9) {
        while (1) switch (_context9.prev = _context9.next) {
          case 0:
            _context9.prev = 0;
            conditionGate = _defineProperty({}, _sequelize.Op.and, {
              source: _defineProperty({}, _sequelize.Op.like, "%INPUT%")
            });
            _context9.next = 4;
            return _models.transporterGateway.findOne({
              attributes: [[_sequelize.Sequelize.col('gate1.id'), "id"], [_sequelize.Sequelize.col('gate1.source'), "source"], [_sequelize.Sequelize.col('gate1.target'), "target"], [_sequelize.Sequelize.col('gate1.label'), "label"]],
              include: [{
                model: _models.transporterGateway,
                as: "gate1",
                required: true,
                attributes: [],
                where: conditionGate
              }],
              where: {
                requestId: requestId
              }
            });
          case 4:
            startPoint = _context9.sent;
            if (startPoint) {
              _context9.next = 7;
              break;
            }
            return _context9.abrupt("return", startPoint);
          case 7:
            return _context9.abrupt("return", flow.find(function (el) {
              if (el.source === startPoint.source) {
                if (el.node.data && Object.keys(el.node.data).length > 0) {
                  el.node.data = JSON.parse(el.node.data);
                }
                return el;
              }
            }));
          case 10:
            _context9.prev = 10;
            _context9.t0 = _context9["catch"](0);
            _context9.t0.message = "transporterGateway.getStartPoint: " + _context9.t0;
            throw _context9.t0;
          case 14:
          case "end":
            return _context9.stop();
        }
      }, _callee8, null, [[0, 10]]);
    }));
    return function getStartPoint(_x13, _x14) {
      return _ref10.apply(this, arguments);
    };
  }();
  // todo get flow recursively
  var _getSourceEdgeRecurse = /*#__PURE__*/function () {
    var _ref11 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee10(source, flow) {
      var dataFlow, checkMultiLine, dataToPush, resEdge, _resEdge, findSourceInFlow, findSourceInDataRes;
      return _regeneratorRuntime().wrap(function _callee10$(_context11) {
        while (1) switch (_context11.prev = _context11.next) {
          case 0:
            dataFlow = {
              id: source.id,
              source: source.source,
              target: source.target
            };
            checkMultiLine = flow.filter(function (el) {
              return el.source === source.source;
            });
            if (!(checkMultiLine.length > 1)) {
              _context11.next = 14;
              break;
            }
            dataToPush = null;
            checkMultiLine.map(/*#__PURE__*/function () {
              var _ref12 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee9(elMultiLine) {
                var checkDataRes, resEdge;
                return _regeneratorRuntime().wrap(function _callee9$(_context10) {
                  while (1) switch (_context10.prev = _context10.next) {
                    case 0:
                      if (!(dataRes.length > 0)) {
                        _context10.next = 8;
                        break;
                      }
                      checkDataRes = dataRes.find(function (elDataRes) {
                        return elDataRes.id === elMultiLine.id;
                      }); // Jika tidak ada
                      if (!checkDataRes) {
                        dataRes.push(elMultiLine);
                      }
                      resEdge = flow.find(function (el) {
                        if (el.source === elMultiLine.target) {
                          return el;
                        }
                      });
                      if (!resEdge) {
                        _context10.next = 8;
                        break;
                      }
                      dataRes.push(resEdge);
                      _context10.next = 8;
                      return _getSourceEdgeRecurse(resEdge, flow);
                    case 8:
                    case "end":
                      return _context10.stop();
                  }
                }, _callee9);
              }));
              return function (_x17) {
                return _ref12.apply(this, arguments);
              };
            }());
            if (!dataToPush) {
              _context11.next = 12;
              break;
            }
            _context11.next = 8;
            return helpers.findAndMoveToLast(dataRes, function (el) {
              return el.id === dataToPush.id;
            });
          case 8:
            resEdge = flow.find(function (el) {
              if (el.source === dataToPush.target) {
                return el;
              }
            });
            dataRes.push(resEdge);
            _context11.next = 12;
            return _getSourceEdgeRecurse(resEdge, flow);
          case 12:
            _context11.next = 31;
            break;
          case 14:
            _resEdge = flow.find(function (el) {
              if (el.source === source.target) {
                return el;
              }
            });
            if (!_resEdge) {
              _context11.next = 22;
              break;
            }
            if (_resEdge.dataValues.node.data && Object.keys(_resEdge.dataValues.node.data).length > 0) {
              _resEdge.dataValues.node.data = JSON.parse(_resEdge.dataValues.node.data);
            }
            dataRes.push(_resEdge);
            _context11.next = 20;
            return _getSourceEdgeRecurse(_resEdge, flow);
          case 20:
            _context11.next = 31;
            break;
          case 22:
            findSourceInFlow = flow.find(function (el) {
              return el.id !== source.id && el.source === source.source;
            });
            if (!findSourceInFlow) {
              _context11.next = 31;
              break;
            }
            if (!(dataRes.length > 0)) {
              _context11.next = 31;
              break;
            }
            findSourceInDataRes = dataRes.find(function (el) {
              return el.id === findSourceInFlow.id;
            });
            if (findSourceInDataRes) {
              _context11.next = 31;
              break;
            }
            if (findSourceInFlow.dataValues.node.data && Object.keys(findSourceInFlow.dataValues.node.data).length > 0) {
              findSourceInFlow.dataValues.node.data = JSON.parse(findSourceInFlow.dataValues.node.data);
            }
            dataRes.push(findSourceInFlow);
            _context11.next = 31;
            return _getSourceEdgeRecurse(findSourceInFlow, flow);
          case 31:
            return _context11.abrupt("return", true);
          case 32:
          case "end":
            return _context11.stop();
        }
      }, _callee10);
    }));
    return function getSourceEdgeRecurse(_x15, _x16) {
      return _ref11.apply(this, arguments);
    };
  }();
  var _getSourceEdgeRecurse_returned = /*#__PURE__*/function () {
    var _ref14 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee12(source, flow, _ref13) {
      var _ref13$match, match, dataFlow, resEdge, checkMultiLine;
      return _regeneratorRuntime().wrap(function _callee12$(_context13) {
        while (1) switch (_context13.prev = _context13.next) {
          case 0:
            _ref13$match = _ref13.match, match = _ref13$match === void 0 ? [] : _ref13$match;
            dataFlow = {
              id: source.id,
              source: source.source,
              target: source.target
            };
            resEdge = flow.find(function (el) {
              if (el.source === source.target) {
                return el;
              }
            });
            checkMultiLine = flow.filter(function (el) {
              return el.source === source.source;
            }); // let checkMultiLine = [{
            //     "id": "FLOW_1695488355766",
            //     "source": "INPUT_1695305079238",
            //     "target": "1695305089621",
            //     "label": null,
            //     "node": {
            //         "id": "1695305089621",
            //         "label": "NODE_ID 1695305089621",
            //         "data": {
            //             "createdAt": "2023-09-21T21:03:12.000Z",
            //             "updatedAt": null,
            //             "deletedAt": null,
            //             "id": "1695304992421",
            //             "idEnv": "1695304947621",
            //             "createdBy": "admin1",
            //             "updateBy": null,
            //             "deletedBy": null,
            //             "status": "1",
            //             "groupName": "patient",
            //             "name": "Get Patient List",
            //             "description": "null",
            //             "version": 1,
            //             "path": "http://localhost:5001/api/call/MockAPI_DIP/development/homapi/patient/detail/list",
            //             "method": "GET",
            //             "responseTrigger": "{}",
            //             "environtment": {
            //                 "createdAt": "2023-09-21T21:02:27.000Z",
            //                 "updatedAt": null,
            //                 "deletedAt": null,
            //                 "id": "1695304947621",
            //                 "createdBy": "admin1",
            //                 "updateBy": null,
            //                 "deletedBy": null,
            //                 "status": "0",
            //                 "envName": "MockAPI_DIP",
            //                 "variable": [
            //                     {
            //                         "varName": "DEVELOPMENT",
            //                         "status": 1,
            //                         "port": null,
            //                         "code": [
            //                             {
            //                                 "key": "base_url",
            //                                 "value": " https://c8a54ff5-ad0e-40f5-9403-1814108baeb8.mock.pstmn.io"
            //                             }
            //                         ],
            //                         "useLogin": false,
            //                         "authentication": {
            //                             "path": "",
            //                             "variableCodeKey": {
            //                                 "value": ""
            //                             },
            //                             "method": "POST",
            //                             "header": [],
            //                             "params": {
            //                                 "query": [],
            //                                 "variable": []
            //                             },
            //                             "bodyRequest": {
            //                                 "mode": "none",
            //                                 "formData": [],
            //                                 "formUrlEncoded": [],
            //                                 "rawJson": {}
            //                             }
            //                         },
            //                         "header": []
            //                     }
            //                 ],
            //                 "header": null,
            //                 "authentication": null,
            //                 "isLogin": false
            //             },
            //             "pathName": "/homapi/patient/detail/list",
            //             "isLoading": false,
            //             "isReloadPage": false,
            //             "masterApi": {
            //                 "createdAt": "2023-09-21T21:03:12.000Z",
            //                 "updatedAt": null,
            //                 "deletedAt": null,
            //                 "id": "1695304992421",
            //                 "idEnv": "1695304947621",
            //                 "createdBy": "admin1",
            //                 "updateBy": null,
            //                 "deletedBy": null,
            //                 "status": "1",
            //                 "groupName": "patient",
            //                 "name": "Get Patient List",
            //                 "description": "null",
            //                 "version": 1,
            //                 "path": "/homapi/patient/detail/list",
            //                 "method": "GET",
            //                 "params": {
            //                     "variable": [],
            //                     "query": [
            //                         {
            //                             "key": {
            //                                 "keyValue": "request_id",
            //                                 "readOnly": true
            //                             },
            //                             "value": {
            //                                 "keyValue": "1695304992421",
            //                                 "readOnly": true
            //                             },
            //                             "disabledDelete": true
            //                         },
            //                         {
            //                             "key": {
            //                                 "keyValue": "overrideResponse",
            //                                 "readOnly": true
            //                             },
            //                             "value": {
            //                                 "keyValue": false,
            //                                 "readOnly": true
            //                             },
            //                             "disabledDelete": true
            //                         }
            //                     ],
            //                     "triggerParamsQuery": []
            //                 },
            //                 "bodyRequest": {
            //                     "mode": "none",
            //                     "formData": [],
            //                     "formUrlEncoded": [],
            //                     "rawJson": {}
            //                 },
            //                 "requestExample": {},
            //                 "responseExample": {},
            //                 "request": {
            //                     "method": "GET",
            //                     "url": {
            //                         "raw": "/homapi/patient/detail/list",
            //                         "host": "#base_url#"
            //                     }
            //                 },
            //                 "mappingTemplate": {},
            //                 "responseTrigger": {},
            //                 "environtment": {
            //                     "id": "1695304947621",
            //                     "envName": "MockAPI_DIP"
            //                 },
            //                 "pathName": "MockAPI_DIP/homapi/patient/detail/list",
            //                 "serverAddress": "http://localhost:5001",
            //                 "baseUrl": "/api/call",
            //                 "fullPath": "http://localhost:5001/api/call/MockAPI_DIP/homapi/patient/detail/list"
            //             },
            //             "nodeParameter": {
            //                 "request": {
            //                     "mode": "none",
            //                     "formData": [],
            //                     "formUrlEncoded": [],
            //                     "rawJson": {}
            //                 },
            //                 "params": {
            //                     "variable": [],
            //                     "query": {
            //                         "request_id": "1695304992421",
            //                         "overrideResponse": false
            //                     },
            //                     "triggerParamsQuery": []
            //                 },
            //                 "responsePropertyKey": ""
            //             },
            //             "envSelected": {
            //                 "varName": "DEVELOPMENT",
            //                 "status": 1,
            //                 "port": null,
            //                 "code": [
            //                     {
            //                         "key": "base_url",
            //                         "value": " https://c8a54ff5-ad0e-40f5-9403-1814108baeb8.mock.pstmn.io"
            //                     }
            //                 ],
            //                 "useLogin": false,
            //                 "authentication": {
            //                     "path": "",
            //                     "variableCodeKey": {
            //                         "value": ""
            //                     },
            //                     "method": "POST",
            //                     "header": [],
            //                     "params": {
            //                         "query": [],
            //                         "variable": []
            //                     },
            //                     "bodyRequest": {
            //                         "mode": "none",
            //                         "formData": [],
            //                         "formUrlEncoded": [],
            //                         "rawJson": {}
            //                     }
            //                 },
            //                 "header": []
            //             },
            //             "fullPath": "http://localhost:5001/api/call/MockAPI_DIP/development/homapi/patient/detail/list?request_id=1695304992421&overrideResponse=false"
            //         }
            //     }
            // },
            //     {
            //         "id": "FLOW_1695483056733",
            //         "source": "1695305089621",
            //         "target": "OUTPUT_1695397238290",
            //         "label": null,
            //         "node": {
            //             "id": "OUTPUT_1695397238290",
            //             "label": "End Of Process",
            //             "data": {}
            //         }
            //     }]
            if (!resEdge) {
              _context13.next = 12;
              break;
            }
            //Filter Turunannya
            flow = flow.filter(function (el) {
              return el.id !== resEdge.id;
            });
            // Filter Parentnya
            flow = flow.filter(function (el) {
              return el.id !== source.id;
            });
            matchData.push(flow);
            _context13.next = 10;
            return _getSourceEdgeRecurse_returned(resEdge, flow, {
              match: flow
            });
          case 10:
            _context13.next = 13;
            break;
          case 12:
            if (checkMultiLine.length > 0) {
              checkMultiLine.map(/*#__PURE__*/function () {
                var _ref15 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee11(s) {
                  var resEdgeMultiline;
                  return _regeneratorRuntime().wrap(function _callee11$(_context12) {
                    while (1) switch (_context12.prev = _context12.next) {
                      case 0:
                        resEdgeMultiline = flow.find(function (el) {
                          if (el.source === s.target) {
                            return el;
                          }
                        });
                        if (!resEdgeMultiline) {
                          _context12.next = 6;
                          break;
                        }
                        _context12.next = 4;
                        return _getSourceEdgeRecurse_returned(resEdgeMultiline, flow, {
                          match: flow
                        });
                      case 4:
                        _context12.next = 8;
                        break;
                      case 6:
                        flow = flow.filter(function (el) {
                          return el.id !== s.id;
                        });
                        matchData = flow;
                      case 8:
                      case "end":
                        return _context12.stop();
                    }
                  }, _callee11);
                }));
                return function (_x21) {
                  return _ref15.apply(this, arguments);
                };
              }());
            } else {
              //Filter Turunannya
              flow = flow.filter(function (el) {
                return el.id !== s.id;
              });
              matchData = flow;
            }
          case 13:
            return _context13.abrupt("return", matchData);
          case 14:
          case "end":
            return _context13.stop();
        }
      }, _callee12);
    }));
    return function getSourceEdgeRecurse_returned(_x18, _x19, _x20) {
      return _ref14.apply(this, arguments);
    };
  }();
  var getSourceEdgeRecurse_old_1 = /*#__PURE__*/function () {
    var _ref16 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee14(source, flow) {
      var dataFlow, checkMultiLine, dataToPush, resEdge, _resEdge2, findSourceInFlow, findSourceInDataRes;
      return _regeneratorRuntime().wrap(function _callee14$(_context15) {
        while (1) switch (_context15.prev = _context15.next) {
          case 0:
            dataFlow = {
              id: source.id,
              source: source.source,
              target: source.target
            }; // console.log('start >> ', dataFlow)
            checkMultiLine = flow.filter(function (el) {
              return el.source === source.source;
            });
            if (!(checkMultiLine.length > 1)) {
              _context15.next = 14;
              break;
            }
            // console.log('dataFlow >> ', checkMultiLine)
            dataToPush = null;
            checkMultiLine.map(/*#__PURE__*/function () {
              var _ref17 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee13(elMultiLine) {
                var checkDataRes, resEdge, check;
                return _regeneratorRuntime().wrap(function _callee13$(_context14) {
                  while (1) switch (_context14.prev = _context14.next) {
                    case 0:
                      if (dataRes.length > 0) {
                        checkDataRes = dataRes.find(function (elDataRes) {
                          return elDataRes.id === elMultiLine.id;
                        }); // Jika tidak ada
                        if (!checkDataRes) {
                          dataRes.push(elMultiLine);
                        }
                        resEdge = flow.find(function (el) {
                          if (el.source === elMultiLine.target) {
                            return el;
                          }
                        });
                        if (resEdge) {
                          check = dataRes.find(function (elDataRes) {
                            return elDataRes.target === resEdge.source;
                          });
                          console.log({
                            id: resEdge.id,
                            source: resEdge.source,
                            target: resEdge.target
                          });
                          // dataToPush = resEdge
                          dataToPush = dataRes.find(function (elDataRes) {
                            return elDataRes.target === resEdge.source;
                          });
                        }
                      }
                    case 1:
                    case "end":
                      return _context14.stop();
                  }
                }, _callee13);
              }));
              return function (_x24) {
                return _ref17.apply(this, arguments);
              };
            }());
            // console.log(dataToPush)
            if (!dataToPush) {
              _context15.next = 12;
              break;
            }
            _context15.next = 8;
            return helpers.findAndMoveToLast(dataRes, function (el) {
              return el.id === dataToPush.id;
            });
          case 8:
            resEdge = flow.find(function (el) {
              if (el.source === dataToPush.target) {
                return el;
              }
            });
            dataRes.push(resEdge);
            _context15.next = 12;
            return _getSourceEdgeRecurse(resEdge, flow);
          case 12:
            _context15.next = 31;
            break;
          case 14:
            // console.log('checkMultiLine False >> ', {
            //     id: source.id,
            //     source: source.source,
            //     target: source.target
            // })
            _resEdge2 = flow.find(function (el) {
              if (el.source === source.target) {
                return el;
              }
            });
            if (!_resEdge2) {
              _context15.next = 22;
              break;
            }
            if (_resEdge2.dataValues.node.data && Object.keys(_resEdge2.dataValues.node.data).length > 0) {
              _resEdge2.dataValues.node.data = JSON.parse(_resEdge2.dataValues.node.data);
            }
            dataRes.push(_resEdge2);
            _context15.next = 20;
            return _getSourceEdgeRecurse(_resEdge2, flow);
          case 20:
            _context15.next = 31;
            break;
          case 22:
            findSourceInFlow = flow.find(function (el) {
              return el.id !== source.id && el.source === source.source;
            });
            if (!findSourceInFlow) {
              _context15.next = 31;
              break;
            }
            if (!(dataRes.length > 0)) {
              _context15.next = 31;
              break;
            }
            findSourceInDataRes = dataRes.find(function (el) {
              return el.id === findSourceInFlow.id;
            });
            if (findSourceInDataRes) {
              _context15.next = 31;
              break;
            }
            if (findSourceInFlow.dataValues.node.data && Object.keys(findSourceInFlow.dataValues.node.data).length > 0) {
              findSourceInFlow.dataValues.node.data = JSON.parse(findSourceInFlow.dataValues.node.data);
            }
            dataRes.push(findSourceInFlow);
            _context15.next = 31;
            return _getSourceEdgeRecurse(findSourceInFlow, flow);
          case 31:
            return _context15.abrupt("return", true);
          case 32:
          case "end":
            return _context15.stop();
        }
      }, _callee14);
    }));
    return function getSourceEdgeRecurse_old_1(_x22, _x23) {
      return _ref16.apply(this, arguments);
    };
  }();
  var getSourceEdgeRecurse_old = /*#__PURE__*/function () {
    var _ref18 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee15(source, flow) {
      var dataFlow, resEdge, findSourceInFlow, findSourceInDataRes;
      return _regeneratorRuntime().wrap(function _callee15$(_context16) {
        while (1) switch (_context16.prev = _context16.next) {
          case 0:
            dataFlow = {
              id: source.id,
              source: source.source,
              target: source.target
            };
            console.log('dataFlow >> ', dataFlow);
            resEdge = flow.find(function (el) {
              if (el.source === source.target) {
                return el;
              }
            });
            if (!resEdge) {
              _context16.next = 10;
              break;
            }
            if (resEdge.dataValues.node.data && Object.keys(resEdge.dataValues.node.data).length > 0) {
              resEdge.dataValues.node.data = JSON.parse(resEdge.dataValues.node.data);
            }
            dataRes.push(resEdge);
            _context16.next = 8;
            return _getSourceEdgeRecurse(resEdge, flow);
          case 8:
            _context16.next = 19;
            break;
          case 10:
            findSourceInFlow = flow.find(function (el) {
              return el.id !== source.id && el.source === source.source;
            });
            if (!findSourceInFlow) {
              _context16.next = 19;
              break;
            }
            if (!(dataRes.length > 0)) {
              _context16.next = 19;
              break;
            }
            findSourceInDataRes = dataRes.find(function (el) {
              return el.id === findSourceInFlow.id;
            });
            if (findSourceInDataRes) {
              _context16.next = 19;
              break;
            }
            if (findSourceInFlow.dataValues.node.data && Object.keys(findSourceInFlow.dataValues.node.data).length > 0) {
              findSourceInFlow.dataValues.node.data = JSON.parse(findSourceInFlow.dataValues.node.data);
            }
            dataRes.push(findSourceInFlow);
            _context16.next = 19;
            return _getSourceEdgeRecurse(findSourceInFlow, flow);
          case 19:
            return _context16.abrupt("return", true);
          case 20:
          case "end":
            return _context16.stop();
        }
      }, _callee15);
    }));
    return function getSourceEdgeRecurse_old(_x25, _x26) {
      return _ref18.apply(this, arguments);
    };
  }();
  return {
    collectingRequestApi: collectingRequestApi
  };
}