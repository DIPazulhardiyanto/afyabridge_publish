"use strict";

require("core-js/modules/es.symbol.js");
require("core-js/modules/es.symbol.description.js");
require("core-js/modules/es.symbol.async-iterator.js");
require("core-js/modules/es.symbol.iterator.js");
require("core-js/modules/es.symbol.to-primitive.js");
require("core-js/modules/es.symbol.to-string-tag.js");
require("core-js/modules/es.array.for-each.js");
require("core-js/modules/es.array.reverse.js");
require("core-js/modules/es.date.to-primitive.js");
require("core-js/modules/es.json.to-string-tag.js");
require("core-js/modules/es.math.to-string-tag.js");
require("core-js/modules/es.number.constructor.js");
require("core-js/modules/es.object.create.js");
require("core-js/modules/es.object.define-properties.js");
require("core-js/modules/es.object.define-property.js");
require("core-js/modules/es.object.get-own-property-descriptor.js");
require("core-js/modules/es.object.get-own-property-descriptors.js");
require("core-js/modules/es.object.get-prototype-of.js");
require("core-js/modules/es.object.set-prototype-of.js");
require("core-js/modules/web.dom-collections.for-each.js");
function _typeof(o) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) { return typeof o; } : function (o) { return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o; }, _typeof(o); }
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.RequestAccessAPI = void 0;
require("core-js/modules/es.array.filter.js");
require("core-js/modules/es.array.find.js");
require("core-js/modules/es.array.find-index.js");
require("core-js/modules/es.array.is-array.js");
require("core-js/modules/es.array.iterator.js");
require("core-js/modules/es.array.join.js");
require("core-js/modules/es.array.map.js");
require("core-js/modules/es.array.slice.js");
require("core-js/modules/es.function.name.js");
require("core-js/modules/es.object.keys.js");
require("core-js/modules/es.object.to-string.js");
require("core-js/modules/es.promise.js");
require("core-js/modules/es.regexp.constructor.js");
require("core-js/modules/es.regexp.exec.js");
require("core-js/modules/es.regexp.to-string.js");
require("core-js/modules/es.string.iterator.js");
require("core-js/modules/web.dom-collections.iterator.js");
var _callQuery = require("../../libs/utils/callQuery");
var _helpers = _interopRequireDefault(require("../../libs/utils/helpers"));
var _helpers_maprequest_body = require("../../libs/utils/helpers_maprequest_body");
var _responseApi = require("../../libs/utils/responseApi");
var _repository = _interopRequireDefault(require("../environmentAccess/repository"));
var _repository2 = _interopRequireDefault(require("../environtment/repository"));
var _repository3 = _interopRequireDefault(require("../mapClientResource/repository"));
var _repository4 = _interopRequireDefault(require("../requestResponse/repository"));
var _A_get_integration_bypath = require("./A_get_integration_bypath");
var _A_mapping_params_access = require("./A_mapping_params_access");
var _A_trigger = require("./A_trigger");
var _query = require("./query");
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
function _regeneratorRuntime() { "use strict"; /*! regenerator-runtime -- Copyright (c) 2014-present, Facebook, Inc. -- license (MIT): https://github.com/facebook/regenerator/blob/main/LICENSE */ _regeneratorRuntime = function _regeneratorRuntime() { return e; }; var t, e = {}, r = Object.prototype, n = r.hasOwnProperty, o = Object.defineProperty || function (t, e, r) { t[e] = r.value; }, i = "function" == typeof Symbol ? Symbol : {}, a = i.iterator || "@@iterator", c = i.asyncIterator || "@@asyncIterator", u = i.toStringTag || "@@toStringTag"; function define(t, e, r) { return Object.defineProperty(t, e, { value: r, enumerable: !0, configurable: !0, writable: !0 }), t[e]; } try { define({}, ""); } catch (t) { define = function define(t, e, r) { return t[e] = r; }; } function wrap(t, e, r, n) { var i = e && e.prototype instanceof Generator ? e : Generator, a = Object.create(i.prototype), c = new Context(n || []); return o(a, "_invoke", { value: makeInvokeMethod(t, r, c) }), a; } function tryCatch(t, e, r) { try { return { type: "normal", arg: t.call(e, r) }; } catch (t) { return { type: "throw", arg: t }; } } e.wrap = wrap; var h = "suspendedStart", l = "suspendedYield", f = "executing", s = "completed", y = {}; function Generator() {} function GeneratorFunction() {} function GeneratorFunctionPrototype() {} var p = {}; define(p, a, function () { return this; }); var d = Object.getPrototypeOf, v = d && d(d(values([]))); v && v !== r && n.call(v, a) && (p = v); var g = GeneratorFunctionPrototype.prototype = Generator.prototype = Object.create(p); function defineIteratorMethods(t) { ["next", "throw", "return"].forEach(function (e) { define(t, e, function (t) { return this._invoke(e, t); }); }); } function AsyncIterator(t, e) { function invoke(r, o, i, a) { var c = tryCatch(t[r], t, o); if ("throw" !== c.type) { var u = c.arg, h = u.value; return h && "object" == _typeof(h) && n.call(h, "__await") ? e.resolve(h.__await).then(function (t) { invoke("next", t, i, a); }, function (t) { invoke("throw", t, i, a); }) : e.resolve(h).then(function (t) { u.value = t, i(u); }, function (t) { return invoke("throw", t, i, a); }); } a(c.arg); } var r; o(this, "_invoke", { value: function value(t, n) { function callInvokeWithMethodAndArg() { return new e(function (e, r) { invoke(t, n, e, r); }); } return r = r ? r.then(callInvokeWithMethodAndArg, callInvokeWithMethodAndArg) : callInvokeWithMethodAndArg(); } }); } function makeInvokeMethod(e, r, n) { var o = h; return function (i, a) { if (o === f) throw Error("Generator is already running"); if (o === s) { if ("throw" === i) throw a; return { value: t, done: !0 }; } for (n.method = i, n.arg = a;;) { var c = n.delegate; if (c) { var u = maybeInvokeDelegate(c, n); if (u) { if (u === y) continue; return u; } } if ("next" === n.method) n.sent = n._sent = n.arg;else if ("throw" === n.method) { if (o === h) throw o = s, n.arg; n.dispatchException(n.arg); } else "return" === n.method && n.abrupt("return", n.arg); o = f; var p = tryCatch(e, r, n); if ("normal" === p.type) { if (o = n.done ? s : l, p.arg === y) continue; return { value: p.arg, done: n.done }; } "throw" === p.type && (o = s, n.method = "throw", n.arg = p.arg); } }; } function maybeInvokeDelegate(e, r) { var n = r.method, o = e.iterator[n]; if (o === t) return r.delegate = null, "throw" === n && e.iterator["return"] && (r.method = "return", r.arg = t, maybeInvokeDelegate(e, r), "throw" === r.method) || "return" !== n && (r.method = "throw", r.arg = new TypeError("The iterator does not provide a '" + n + "' method")), y; var i = tryCatch(o, e.iterator, r.arg); if ("throw" === i.type) return r.method = "throw", r.arg = i.arg, r.delegate = null, y; var a = i.arg; return a ? a.done ? (r[e.resultName] = a.value, r.next = e.nextLoc, "return" !== r.method && (r.method = "next", r.arg = t), r.delegate = null, y) : a : (r.method = "throw", r.arg = new TypeError("iterator result is not an object"), r.delegate = null, y); } function pushTryEntry(t) { var e = { tryLoc: t[0] }; 1 in t && (e.catchLoc = t[1]), 2 in t && (e.finallyLoc = t[2], e.afterLoc = t[3]), this.tryEntries.push(e); } function resetTryEntry(t) { var e = t.completion || {}; e.type = "normal", delete e.arg, t.completion = e; } function Context(t) { this.tryEntries = [{ tryLoc: "root" }], t.forEach(pushTryEntry, this), this.reset(!0); } function values(e) { if (e || "" === e) { var r = e[a]; if (r) return r.call(e); if ("function" == typeof e.next) return e; if (!isNaN(e.length)) { var o = -1, i = function next() { for (; ++o < e.length;) if (n.call(e, o)) return next.value = e[o], next.done = !1, next; return next.value = t, next.done = !0, next; }; return i.next = i; } } throw new TypeError(_typeof(e) + " is not iterable"); } return GeneratorFunction.prototype = GeneratorFunctionPrototype, o(g, "constructor", { value: GeneratorFunctionPrototype, configurable: !0 }), o(GeneratorFunctionPrototype, "constructor", { value: GeneratorFunction, configurable: !0 }), GeneratorFunction.displayName = define(GeneratorFunctionPrototype, u, "GeneratorFunction"), e.isGeneratorFunction = function (t) { var e = "function" == typeof t && t.constructor; return !!e && (e === GeneratorFunction || "GeneratorFunction" === (e.displayName || e.name)); }, e.mark = function (t) { return Object.setPrototypeOf ? Object.setPrototypeOf(t, GeneratorFunctionPrototype) : (t.__proto__ = GeneratorFunctionPrototype, define(t, u, "GeneratorFunction")), t.prototype = Object.create(g), t; }, e.awrap = function (t) { return { __await: t }; }, defineIteratorMethods(AsyncIterator.prototype), define(AsyncIterator.prototype, c, function () { return this; }), e.AsyncIterator = AsyncIterator, e.async = function (t, r, n, o, i) { void 0 === i && (i = Promise); var a = new AsyncIterator(wrap(t, r, n, o), i); return e.isGeneratorFunction(r) ? a : a.next().then(function (t) { return t.done ? t.value : a.next(); }); }, defineIteratorMethods(g), define(g, u, "Generator"), define(g, a, function () { return this; }), define(g, "toString", function () { return "[object Generator]"; }), e.keys = function (t) { var e = Object(t), r = []; for (var n in e) r.push(n); return r.reverse(), function next() { for (; r.length;) { var t = r.pop(); if (t in e) return next.value = t, next.done = !1, next; } return next.done = !0, next; }; }, e.values = values, Context.prototype = { constructor: Context, reset: function reset(e) { if (this.prev = 0, this.next = 0, this.sent = this._sent = t, this.done = !1, this.delegate = null, this.method = "next", this.arg = t, this.tryEntries.forEach(resetTryEntry), !e) for (var r in this) "t" === r.charAt(0) && n.call(this, r) && !isNaN(+r.slice(1)) && (this[r] = t); }, stop: function stop() { this.done = !0; var t = this.tryEntries[0].completion; if ("throw" === t.type) throw t.arg; return this.rval; }, dispatchException: function dispatchException(e) { if (this.done) throw e; var r = this; function handle(n, o) { return a.type = "throw", a.arg = e, r.next = n, o && (r.method = "next", r.arg = t), !!o; } for (var o = this.tryEntries.length - 1; o >= 0; --o) { var i = this.tryEntries[o], a = i.completion; if ("root" === i.tryLoc) return handle("end"); if (i.tryLoc <= this.prev) { var c = n.call(i, "catchLoc"), u = n.call(i, "finallyLoc"); if (c && u) { if (this.prev < i.catchLoc) return handle(i.catchLoc, !0); if (this.prev < i.finallyLoc) return handle(i.finallyLoc); } else if (c) { if (this.prev < i.catchLoc) return handle(i.catchLoc, !0); } else { if (!u) throw Error("try statement without catch or finally"); if (this.prev < i.finallyLoc) return handle(i.finallyLoc); } } } }, abrupt: function abrupt(t, e) { for (var r = this.tryEntries.length - 1; r >= 0; --r) { var o = this.tryEntries[r]; if (o.tryLoc <= this.prev && n.call(o, "finallyLoc") && this.prev < o.finallyLoc) { var i = o; break; } } i && ("break" === t || "continue" === t) && i.tryLoc <= e && e <= i.finallyLoc && (i = null); var a = i ? i.completion : {}; return a.type = t, a.arg = e, i ? (this.method = "next", this.next = i.finallyLoc, y) : this.complete(a); }, complete: function complete(t, e) { if ("throw" === t.type) throw t.arg; return "break" === t.type || "continue" === t.type ? this.next = t.arg : "return" === t.type ? (this.rval = this.arg = t.arg, this.method = "return", this.next = "end") : "normal" === t.type && e && (this.next = e), y; }, finish: function finish(t) { for (var e = this.tryEntries.length - 1; e >= 0; --e) { var r = this.tryEntries[e]; if (r.finallyLoc === t) return this.complete(r.completion, r.afterLoc), resetTryEntry(r), y; } }, "catch": function _catch(t) { for (var e = this.tryEntries.length - 1; e >= 0; --e) { var r = this.tryEntries[e]; if (r.tryLoc === t) { var n = r.completion; if ("throw" === n.type) { var o = n.arg; resetTryEntry(r); } return o; } } throw Error("illegal catch attempt"); }, delegateYield: function delegateYield(e, r, n) { return this.delegate = { iterator: values(e), resultName: r, nextLoc: n }, "next" === this.method && (this.arg = t), y; } }, e; }
function ownKeys(e, r) { var t = Object.keys(e); if (Object.getOwnPropertySymbols) { var o = Object.getOwnPropertySymbols(e); r && (o = o.filter(function (r) { return Object.getOwnPropertyDescriptor(e, r).enumerable; })), t.push.apply(t, o); } return t; }
function _objectSpread(e) { for (var r = 1; r < arguments.length; r++) { var t = null != arguments[r] ? arguments[r] : {}; r % 2 ? ownKeys(Object(t), !0).forEach(function (r) { _defineProperty(e, r, t[r]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys(Object(t)).forEach(function (r) { Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r)); }); } return e; }
function _defineProperty(e, r, t) { return (r = _toPropertyKey(r)) in e ? Object.defineProperty(e, r, { value: t, enumerable: !0, configurable: !0, writable: !0 }) : e[r] = t, e; }
function _toPropertyKey(t) { var i = _toPrimitive(t, "string"); return "symbol" == _typeof(i) ? i : i + ""; }
function _toPrimitive(t, r) { if ("object" != _typeof(t) || !t) return t; var e = t[Symbol.toPrimitive]; if (void 0 !== e) { var i = e.call(t, r || "default"); if ("object" != _typeof(i)) return i; throw new TypeError("@@toPrimitive must return a primitive value."); } return ("string" === r ? String : Number)(t); }
function asyncGeneratorStep(n, t, e, r, o, a, c) { try { var i = n[a](c), u = i.value; } catch (n) { return void e(n); } i.done ? t(u) : Promise.resolve(u).then(r, o); }
function _asyncToGenerator(n) { return function () { var t = this, e = arguments; return new Promise(function (r, o) { var a = n.apply(t, e); function _next(n) { asyncGeneratorStep(a, r, o, _next, _throw, "next", n); } function _throw(n) { asyncGeneratorStep(a, r, o, _next, _throw, "throw", n); } _next(void 0); }); }; }
var RequestAccessAPI = exports.RequestAccessAPI = /*#__PURE__*/function () {
  var _ref = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee(req) {
    var getFromReq, idUser, isEnabledAccess, _req$decoded, id, role, _req$query, accessUserId, enabledAccess, getEnvironmentAccess, execGetEnvAccess, getEnv, getEndpointByAccess, execEndpointByAccess, getPathIntegration, _getPathIntegration, bodyRequest, mapClientResources, mappingTemplate, request, varQuery, _getPathIntegration2, params, fromParams, _loop, _ret, key, queryParams, _loop2, _ret2, queryKey, getListMappingClientRsc, getListMappingClientRscResponse, mode, originBodyRequest, originalBody, resultParamSend, originalResponse, requestHistory, _getPathIntegration3, name, groupName, version, idEnv, _id;
    return _regeneratorRuntime().wrap(function _callee$(_context3) {
      while (1) switch (_context3.prev = _context3.next) {
        case 0:
          getFromReq = {};
          idUser = "";
          isEnabledAccess = false; // todo get user login
          _req$decoded = req.decoded, id = _req$decoded.id, role = _req$decoded.role;
          getFromReq["path"] = "/" + req.path.split("/").slice(4).join("/");
          getFromReq["method"] = req.method;
          getFromReq["url"] = req.originalUrl.split("/")[4];
          getFromReq["query"] = Object.keys(req.query).length > 0 ? req.query : null;
          getFromReq["fullUrl"] = req.protocol + "://" + req.get("host") + req.originalUrl;
          getFromReq["env"] = req.originalUrl.split("/")[5];

          // todo untuk admin dapat memilih menggunakan user lain untuk execute endpoint
          _req$query = req.query, accessUserId = _req$query.accessUserId, enabledAccess = _req$query.enabledAccess; //todo khusus untuk ADMIN
          if (role === 1) {
            idUser = accessUserId ? accessUserId : id;
            isEnabledAccess = enabledAccess ? enabledAccess === "true" : false;
          }
          //todo khusus untuk USER
          if (role === 2) {
            idUser = id;
          }
          if (!(!getFromReq.query || !getFromReq.query.hasOwnProperty("request_id"))) {
            _context3.next = 15;
            break;
          }
          return _context3.abrupt("return", (0, _responseApi.result)({
            statusCode: _responseApi.statusHttpCode.badRequest400,
            message: "Request not allowed: parameter request_id not found"
          }));
        case 15:
          // todo menambahkan validasi disini get environment by access
          getEnvironmentAccess = (0, _query.sql_getEnvironmentAccess)({
            userId: idUser,
            envName: getFromReq.url
          });
          _context3.next = 18;
          return (0, _callQuery.queryExecute)({
            query: getEnvironmentAccess,
            getOneObject: true
          });
        case 18:
          execGetEnvAccess = _context3.sent;
          if (execGetEnvAccess) {
            _context3.next = 21;
            break;
          }
          return _context3.abrupt("return", (0, _responseApi.result)({
            statusCode: _responseApi.statusHttpCode.badRequest400,
            message: "Environment cant access, please call administrator !"
          }));
        case 21:
          if (execGetEnvAccess.status) {
            _context3.next = 23;
            break;
          }
          return _context3.abrupt("return", (0, _responseApi.result)({
            statusCode: _responseApi.statusHttpCode.badRequest400,
            message: "Environment status cant active, please call administrator !"
          }));
        case 23:
          _context3.next = 25;
          return (0, _repository2["default"])().getEnvByNameUrl({
            envName: getFromReq.url
          });
        case 25:
          getEnv = _context3.sent;
          if (!(getEnv.code !== _responseApi.statusHttpCode.ok200.code)) {
            _context3.next = 28;
            break;
          }
          return _context3.abrupt("return", (0, _responseApi.result)({
            statusCode: _responseApi.statusHttpCode.badRequest400,
            message: getEnv.message
          }));
        case 28:
          // todo menambahkan validasi disini get endpoint by access
          getEndpointByAccess = (0, _query.sql_getEndpointByAccess)({
            userId: idUser,
            method: getFromReq.method,
            requestId: getFromReq.query.request_id,
            envId: execGetEnvAccess.idEnv
          });
          _context3.next = 31;
          return (0, _callQuery.queryExecute)({
            query: getEndpointByAccess,
            getOneObject: true
          });
        case 31:
          execEndpointByAccess = _context3.sent;
          if (execEndpointByAccess) {
            _context3.next = 34;
            break;
          }
          return _context3.abrupt("return", (0, _responseApi.result)({
            statusCode: _responseApi.statusHttpCode.badRequest400,
            message: "API not found !, please check environment, request_id, and method",
            data: {
              time: (0, _helpers["default"])().getDateTimeNowFormattedIndonesianDateTime(),
              requestDetail: getFromReq,
              body: req.body
            }
          }));
        case 34:
          if (!(execEndpointByAccess.isDisabled && !isEnabledAccess)) {
            _context3.next = 36;
            break;
          }
          return _context3.abrupt("return", (0, _responseApi.result)({
            statusCode: _responseApi.statusHttpCode.badRequest400,
            message: "API unable to access !, please call administrator !"
          }));
        case 36:
          _context3.next = 38;
          return (0, _A_get_integration_bypath.getIntegrationByPath)({
            path: getFromReq.path,
            method: getFromReq.method,
            idEnv: getEnv.results.id,
            originalUrl: getFromReq.fullUrl,
            requestId: getFromReq.query.request_id
          });
        case 38:
          getPathIntegration = _context3.sent;
          if (!(getPathIntegration.code !== _responseApi.statusHttpCode.ok200.code)) {
            _context3.next = 41;
            break;
          }
          return _context3.abrupt("return", (0, _responseApi.result)({
            statusCode: _responseApi.statusHttpCode.badRequest400,
            message: getPathIntegration.message
          }));
        case 41:
          /**
           * SetNew getPathIntegration
           * */
          getPathIntegration = getPathIntegration.results;
          _getPathIntegration = getPathIntegration, bodyRequest = _getPathIntegration.bodyRequest, mapClientResources = _getPathIntegration.mapClientResources, mappingTemplate = _getPathIntegration.mappingTemplate, request = _getPathIntegration.request;
          /**
           * Get Params Path FROM DB
           * */
          if (!(getPathIntegration.params.variable.length > 0)) {
            _context3.next = 48;
            break;
          }
          // let varQuery = getPathIntegration.path.split(':');
          // digantikan ke sini karna ada issue path panjang tidak kena
          varQuery = getPathIntegration.path.split("/").filter(function (item) {
            return item !== "";
          });
          if (!(req.path.split("/").slice(5).length !== varQuery.length)) {
            _context3.next = 47;
            break;
          }
          return _context3.abrupt("return", (0, _responseApi.result)({
            statusCode: _responseApi.statusHttpCode.methodNotAllowed405,
            message: "Request not allowed: Params Path invalid !"
          }));
        case 47:
          //NewLogic Get Params
          varQuery.map(function (el, idx) {
            var pattern = new RegExp(":");
            if (pattern.test(el)) {
              var resPath = req.path.split("/").slice(5)[idx];
              var newEL = _helpers["default"].removeSpecialCase(el);
              getFromReq["params"] = _objectSpread(_objectSpread({}, getFromReq["params"]), {}, _defineProperty({}, "".concat(newEL), resPath));
            }
          });
        case 48:
          if (!(getFromReq.params && getPathIntegration.params.variable.length > 0)) {
            _context3.next = 61;
            break;
          }
          _getPathIntegration2 = getPathIntegration, params = _getPathIntegration2.params;
          fromParams = params.variable;
          _loop = /*#__PURE__*/_regeneratorRuntime().mark(function _loop(key) {
            var queryIdx;
            return _regeneratorRuntime().wrap(function _loop$(_context) {
              while (1) switch (_context.prev = _context.next) {
                case 0:
                  queryIdx = fromParams.findIndex(function (ql) {
                    return ql.key === key;
                  });
                  if (!(queryIdx < 0)) {
                    _context.next = 3;
                    break;
                  }
                  return _context.abrupt("return", {
                    v: (0, _responseApi.result)({
                      statusCode: _responseApi.statusHttpCode.methodNotAllowed405,
                      message: "Request not allowed: Path parameter are different"
                    })
                  });
                case 3:
                  getPathIntegration.params.variable[queryIdx].value = (0, _helpers["default"])().replacingValue("#".concat(params.variable[queryIdx].value, "#"), getFromReq.params[key]);
                case 4:
                case "end":
                  return _context.stop();
              }
            }, _loop);
          });
          _context3.t0 = _regeneratorRuntime().keys(getFromReq.params);
        case 53:
          if ((_context3.t1 = _context3.t0()).done) {
            _context3.next = 61;
            break;
          }
          key = _context3.t1.value;
          return _context3.delegateYield(_loop(key), "t2", 56);
        case 56:
          _ret = _context3.t2;
          if (!_ret) {
            _context3.next = 59;
            break;
          }
          return _context3.abrupt("return", _ret.v);
        case 59:
          _context3.next = 53;
          break;
        case 61:
          if (!getFromReq.query) {
            _context3.next = 78;
            break;
          }
          queryParams = Object.keys(getFromReq.query).filter(function (el) {
            return el !== "variable" && el !== "request_id" && el !== "test_request" && el !== "overrideResponse" && el !== "accessUserId" && el !== "enabledAccess";
          });
          if (!(queryParams.length > 0)) {
            _context3.next = 78;
            break;
          }
          if (!(getPathIntegration.params.query.length === 0 || queryParams.length !== getPathIntegration.params.query.length)) {
            _context3.next = 68;
            break;
          }
          return _context3.abrupt("return", (0, _responseApi.result)({
            statusCode: _responseApi.statusHttpCode.methodNotAllowed405,
            message: "Request not allowed: Params Query not found"
          }));
        case 68:
          _loop2 = /*#__PURE__*/_regeneratorRuntime().mark(function _loop2(queryKey) {
            var getFilter, checkTgr, requestQueryUrl;
            return _regeneratorRuntime().wrap(function _loop2$(_context2) {
              while (1) switch (_context2.prev = _context2.next) {
                case 0:
                  getFilter = getPathIntegration.params.query.find(function (queryFill) {
                    return queryFill.key === queryParams[queryKey];
                  });
                  if (getFilter) {
                    _context2.next = 3;
                    break;
                  }
                  return _context2.abrupt("return", {
                    v: (0, _responseApi.result)({
                      statusCode: _responseApi.statusHttpCode.methodNotAllowed405,
                      message: "Request not allowed: Params Queries are different"
                    })
                  });
                case 3:
                  checkTgr = new RegExp("#");
                  if (getPathIntegration.params.hasOwnProperty("triggerParamsQuery") && getPathIntegration.params.triggerParamsQuery.length > 0) {
                    /** replace list trigger */
                    requestQueryUrl = getPathIntegration.params.triggerParamsQuery.find(function (ql) {
                      return queryParams[queryKey] === ql.key;
                    });
                    if (requestQueryUrl) {
                      getPathIntegration.params.query.map(function (el) {
                        if (el.key === requestQueryUrl.key) {
                          el.value = (0, _helpers["default"])().replacingValue(requestQueryUrl.value, getFromReq.query[queryParams[queryKey]]);
                        }
                      });
                    }
                  } else {
                    /** Jika tidak ada di list trigger */
                    getPathIntegration.params.query[queryKey].value = getFromReq.query[queryParams[queryKey]];
                  }
                case 5:
                case "end":
                  return _context2.stop();
              }
            }, _loop2);
          });
          _context3.t3 = _regeneratorRuntime().keys(getPathIntegration.params.query);
        case 70:
          if ((_context3.t4 = _context3.t3()).done) {
            _context3.next = 78;
            break;
          }
          queryKey = _context3.t4.value;
          return _context3.delegateYield(_loop2(queryKey), "t5", 73);
        case 73:
          _ret2 = _context3.t5;
          if (!_ret2) {
            _context3.next = 76;
            break;
          }
          return _context3.abrupt("return", _ret2.v);
        case 76:
          _context3.next = 70;
          break;
        case 78:
          _context3.next = 80;
          return (0, _repository3["default"])().showList({
            query: {
              endpointIntegrationId: getFromReq.query["request_id"],
              noPagination: true,
              resourceType: "request"
            }
          });
        case 80:
          getListMappingClientRsc = _context3.sent;
          _context3.next = 83;
          return (0, _repository3["default"])().showList({
            query: {
              endpointIntegrationId: getFromReq.query["request_id"],
              noPagination: true,
              resourceType: "response"
            }
          });
        case 83:
          getListMappingClientRscResponse = _context3.sent;
          if (getListMappingClientRsc.code === 200 && getListMappingClientRsc.results.rows.length > 0) {
            getListMappingClientRsc = getListMappingClientRsc.results.rows;
          } else {
            getListMappingClientRsc = [];
          }

          /**
           * This process mapping body request: add new parameter or changes value
           * */
          mode = null;
          request.header = [];
          originBodyRequest = bodyRequest;
          getPathIntegration["originalBodyRequest"] = _objectSpread(_objectSpread({}, originBodyRequest), {}, {
            resChanges: originBodyRequest
          });
          originalBody = _objectSpread({}, req.body); // Body Request Ovveride and Mapping
          _context3.t6 = bodyRequest.mode;
          _context3.next = _context3.t6 === "raw" ? 93 : _context3.t6 === "urlencoded" ? 99 : 105;
          break;
        case 93:
          request.header.push({
            key: "Content-Type",
            value: "application/json",
            type: "text"
          });
          mode = "rawJson";
          _context3.next = 97;
          return (0, _helpers_maprequest_body.bodyRequestRaw)(req.body, getListMappingClientRsc);
        case 97:
          bodyRequest["rawJson"] = _context3.sent;
          return _context3.abrupt("break", 107);
        case 99:
          request.header.push({
            key: "Content-Type",
            value: "application/x-www-form-urlencoded"
          });
          mode = "formUrlEncoded";
          _context3.next = 103;
          return (0, _helpers_maprequest_body.bodyRequestRaw)(req.body, getListMappingClientRsc);
        case 103:
          bodyRequest["formUrlEncoded"] = _context3.sent;
          return _context3.abrupt("break", 107);
        case 105:
          request.header.push({
            key: "Content-Type",
            value: "application/json",
            type: "text"
          });
          return _context3.abrupt("break", 107);
        case 107:
          /**
           * Replace MapTemplateValue (Proses Mapping Trigger Request)
           * */
          if (mappingTemplate && bodyRequest.mode !== "none") {
            if (Array.isArray(bodyRequest[mode]) && bodyRequest[mode].length > 0) {
              getPathIntegration.mappingTemplate = bodyRequest[mode].map(function (el) {
                return (0, _helpers_maprequest_body.changesValueByPropertyKey)(mappingTemplate, el);
              });
            } else {
              getPathIntegration.mappingTemplate = (0, _helpers_maprequest_body.changesValueByPropertyKey)(mappingTemplate, bodyRequest[mode]);
            }
          }

          //todo added getFromReq with Environment
          execGetEnvAccess.variable = JSON.parse(execGetEnvAccess.variable);
          getFromReq["environmentAccess"] = execGetEnvAccess;
          _context3.next = 112;
          return (0, _A_mapping_params_access.mappingParameterSendAccess)(getPathIntegration, getFromReq, getListMappingClientRsc);
        case 112:
          resultParamSend = _context3.sent;
          if (getListMappingClientRscResponse.code === 200 && getListMappingClientRscResponse.results.rows.length > 0) {
            getListMappingClientRscResponse = getListMappingClientRscResponse.results.rows;
          } else {
            getListMappingClientRscResponse = [];
          }
          originalResponse = _objectSpread({}, resultParamSend.resultCall.results);
          /**
           * Trigger Response By Value Manipulation
           * */
          if (!(resultParamSend.resultCall.code === 200 && (!getFromReq.query.test_request || getFromReq.query.test_request === "false"))) {
            _context3.next = 119;
            break;
          }
          _context3.next = 118;
          return (0, _A_trigger.triggerResponseMapClientResource)(resultParamSend.resultCall.results, getListMappingClientRscResponse);
        case 118:
          resultParamSend.resultCall.results = _context3.sent;
        case 119:
          if (!((!getFromReq.query.test_request || getFromReq.query.test_request === "false") && getPathIntegration.responseTrigger && Object.keys(getPathIntegration.responseTrigger).length > 0 && !!getFromReq.query["overrideResponse"] && getFromReq.query["overrideResponse"] === "true")) {
            _context3.next = 125;
            break;
          }
          if (!(Object.keys(getPathIntegration.responseTrigger.newMappingResponse).length > 0 && resultParamSend.resultCall.code === 200)) {
            _context3.next = 125;
            break;
          }
          _context3.next = 123;
          return (0, _A_trigger.triggerResponse)(getPathIntegration.responseTrigger.newMappingResponse, resultParamSend.resultCall.results);
        case 123:
          resultParamSend.resultCall.results = _context3.sent;
          if (getPathIntegration.responseTrigger.getIndexByArray.value !== "none") {
            resultParamSend.resultCall.results = resultParamSend.resultCall.results[0];
          }
        case 125:
          requestHistory = {
            request: originalBody,
            response: resultParamSend.resultCall.results,
            overideRequest: getPathIntegration.mappingTemplate,
            overideResponse: originalResponse
          }; //for history request access api
          if (!(process.env.npm_lifecycle_event == "dev")) {
            _context3.next = 130;
            break;
          }
          return _context3.abrupt("return", resultParamSend.resultCall);
        case 130:
          if (!(getFromReq.query.hasOwnProperty("test_request") && getFromReq.query.test_request === "true")) {
            _context3.next = 134;
            break;
          }
          return _context3.abrupt("return", resultParamSend.resultCall);
        case 134:
          _getPathIntegration3 = getPathIntegration, name = _getPathIntegration3.name, groupName = _getPathIntegration3.groupName, version = _getPathIntegration3.version, idEnv = _getPathIntegration3.idEnv;
          _id = getFromReq.environmentAccess.id;
          req.body = {
            idEnvAccess: _id,
            requestId: getFromReq.query.request_id,
            path: getFromReq.fullUrl,
            method: getFromReq.method,
            resourceName: name,
            groupName: groupName,
            idEnv: idEnv,
            version: version,
            message: resultParamSend.resultCall.message,
            httpCode: resultParamSend.resultCall.code,
            request: requestHistory.request,
            response: requestHistory.response,
            overideRequest: requestHistory.overideRequest,
            overideResponse: requestHistory.overideResponse,
            variableName: getFromReq.env.toUpperCase()
          };
          /** for save history */
          _context3.next = 139;
          return (0, _repository4["default"])().create(req);
        case 139:
          return _context3.abrupt("return", resultParamSend.resultCall);
        case 140:
        case "end":
          return _context3.stop();
      }
    }, _callee);
  }));
  return function RequestAccessAPI(_x) {
    return _ref.apply(this, arguments);
  };
}();