"use strict";

require("core-js/modules/es.symbol.js");
require("core-js/modules/es.symbol.description.js");
require("core-js/modules/es.symbol.async-iterator.js");
require("core-js/modules/es.symbol.iterator.js");
require("core-js/modules/es.symbol.to-primitive.js");
require("core-js/modules/es.symbol.to-string-tag.js");
require("core-js/modules/es.array.reverse.js");
require("core-js/modules/es.date.to-primitive.js");
require("core-js/modules/es.json.to-string-tag.js");
require("core-js/modules/es.math.to-string-tag.js");
require("core-js/modules/es.number.constructor.js");
require("core-js/modules/es.object.create.js");
require("core-js/modules/es.object.define-properties.js");
require("core-js/modules/es.object.define-property.js");
require("core-js/modules/es.object.get-own-property-descriptor.js");
require("core-js/modules/es.object.get-own-property-descriptors.js");
require("core-js/modules/es.object.get-prototype-of.js");
require("core-js/modules/es.object.set-prototype-of.js");
require("core-js/modules/es.weak-map.js");
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = sendApiRepository;
require("core-js/modules/es.array.concat.js");
require("core-js/modules/es.array.filter.js");
require("core-js/modules/es.array.find.js");
require("core-js/modules/es.array.find-index.js");
require("core-js/modules/es.array.for-each.js");
require("core-js/modules/es.array.is-array.js");
require("core-js/modules/es.array.iterator.js");
require("core-js/modules/es.array.join.js");
require("core-js/modules/es.array.map.js");
require("core-js/modules/es.array.slice.js");
require("core-js/modules/es.date.to-json.js");
require("core-js/modules/es.date.to-string.js");
require("core-js/modules/es.function.name.js");
require("core-js/modules/es.object.keys.js");
require("core-js/modules/es.object.to-string.js");
require("core-js/modules/es.parse-int.js");
require("core-js/modules/es.promise.js");
require("core-js/modules/es.regexp.constructor.js");
require("core-js/modules/es.regexp.exec.js");
require("core-js/modules/es.regexp.to-string.js");
require("core-js/modules/es.string.iterator.js");
require("core-js/modules/es.string.replace.js");
require("core-js/modules/web.dom-collections.for-each.js");
require("core-js/modules/web.dom-collections.iterator.js");
require("core-js/modules/web.timers.js");
require("core-js/modules/web.url.js");
require("core-js/modules/web.url.to-json.js");
require("core-js/modules/web.url-search-params.js");
var _repository = _interopRequireDefault(require("../environtment/repository"));
var _repository2 = _interopRequireDefault(require("../callApi/repository"));
var _repository3 = _interopRequireDefault(require("../mapClientResource/repository"));
var _repository4 = _interopRequireDefault(require("../requestResponse/repository"));
var _models = require("../../models");
var _responseApi = require("../../libs/utils/responseApi");
var _helpers = _interopRequireDefault(require("../../libs/utils/helpers"));
var _helpers_maprequest_body = require("../../libs/utils/helpers_maprequest_body");
var _axios = _interopRequireDefault(require("axios"));
var _path = _interopRequireDefault(require("path"));
var _query = require("./query");
var _callQuery = require("../../libs/utils/callQuery");
var https = _interopRequireWildcard(require("https"));
var http = _interopRequireWildcard(require("http"));
var _repository5 = _interopRequireDefault(require("../environmentAccess/repository"));
var _nodeFetch = _interopRequireDefault(require("node-fetch"));
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != _typeof(e) && "function" != typeof e) return { "default": e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && {}.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n["default"] = e, t && t.set(e, n), n; }
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
function _typeof(o) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) { return typeof o; } : function (o) { return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o; }, _typeof(o); }
function _regeneratorRuntime() { "use strict"; /*! regenerator-runtime -- Copyright (c) 2014-present, Facebook, Inc. -- license (MIT): https://github.com/facebook/regenerator/blob/main/LICENSE */ _regeneratorRuntime = function _regeneratorRuntime() { return e; }; var t, e = {}, r = Object.prototype, n = r.hasOwnProperty, o = Object.defineProperty || function (t, e, r) { t[e] = r.value; }, i = "function" == typeof Symbol ? Symbol : {}, a = i.iterator || "@@iterator", c = i.asyncIterator || "@@asyncIterator", u = i.toStringTag || "@@toStringTag"; function define(t, e, r) { return Object.defineProperty(t, e, { value: r, enumerable: !0, configurable: !0, writable: !0 }), t[e]; } try { define({}, ""); } catch (t) { define = function define(t, e, r) { return t[e] = r; }; } function wrap(t, e, r, n) { var i = e && e.prototype instanceof Generator ? e : Generator, a = Object.create(i.prototype), c = new Context(n || []); return o(a, "_invoke", { value: makeInvokeMethod(t, r, c) }), a; } function tryCatch(t, e, r) { try { return { type: "normal", arg: t.call(e, r) }; } catch (t) { return { type: "throw", arg: t }; } } e.wrap = wrap; var h = "suspendedStart", l = "suspendedYield", f = "executing", s = "completed", y = {}; function Generator() {} function GeneratorFunction() {} function GeneratorFunctionPrototype() {} var p = {}; define(p, a, function () { return this; }); var d = Object.getPrototypeOf, v = d && d(d(values([]))); v && v !== r && n.call(v, a) && (p = v); var g = GeneratorFunctionPrototype.prototype = Generator.prototype = Object.create(p); function defineIteratorMethods(t) { ["next", "throw", "return"].forEach(function (e) { define(t, e, function (t) { return this._invoke(e, t); }); }); } function AsyncIterator(t, e) { function invoke(r, o, i, a) { var c = tryCatch(t[r], t, o); if ("throw" !== c.type) { var u = c.arg, h = u.value; return h && "object" == _typeof(h) && n.call(h, "__await") ? e.resolve(h.__await).then(function (t) { invoke("next", t, i, a); }, function (t) { invoke("throw", t, i, a); }) : e.resolve(h).then(function (t) { u.value = t, i(u); }, function (t) { return invoke("throw", t, i, a); }); } a(c.arg); } var r; o(this, "_invoke", { value: function value(t, n) { function callInvokeWithMethodAndArg() { return new e(function (e, r) { invoke(t, n, e, r); }); } return r = r ? r.then(callInvokeWithMethodAndArg, callInvokeWithMethodAndArg) : callInvokeWithMethodAndArg(); } }); } function makeInvokeMethod(e, r, n) { var o = h; return function (i, a) { if (o === f) throw Error("Generator is already running"); if (o === s) { if ("throw" === i) throw a; return { value: t, done: !0 }; } for (n.method = i, n.arg = a;;) { var c = n.delegate; if (c) { var u = maybeInvokeDelegate(c, n); if (u) { if (u === y) continue; return u; } } if ("next" === n.method) n.sent = n._sent = n.arg;else if ("throw" === n.method) { if (o === h) throw o = s, n.arg; n.dispatchException(n.arg); } else "return" === n.method && n.abrupt("return", n.arg); o = f; var p = tryCatch(e, r, n); if ("normal" === p.type) { if (o = n.done ? s : l, p.arg === y) continue; return { value: p.arg, done: n.done }; } "throw" === p.type && (o = s, n.method = "throw", n.arg = p.arg); } }; } function maybeInvokeDelegate(e, r) { var n = r.method, o = e.iterator[n]; if (o === t) return r.delegate = null, "throw" === n && e.iterator["return"] && (r.method = "return", r.arg = t, maybeInvokeDelegate(e, r), "throw" === r.method) || "return" !== n && (r.method = "throw", r.arg = new TypeError("The iterator does not provide a '" + n + "' method")), y; var i = tryCatch(o, e.iterator, r.arg); if ("throw" === i.type) return r.method = "throw", r.arg = i.arg, r.delegate = null, y; var a = i.arg; return a ? a.done ? (r[e.resultName] = a.value, r.next = e.nextLoc, "return" !== r.method && (r.method = "next", r.arg = t), r.delegate = null, y) : a : (r.method = "throw", r.arg = new TypeError("iterator result is not an object"), r.delegate = null, y); } function pushTryEntry(t) { var e = { tryLoc: t[0] }; 1 in t && (e.catchLoc = t[1]), 2 in t && (e.finallyLoc = t[2], e.afterLoc = t[3]), this.tryEntries.push(e); } function resetTryEntry(t) { var e = t.completion || {}; e.type = "normal", delete e.arg, t.completion = e; } function Context(t) { this.tryEntries = [{ tryLoc: "root" }], t.forEach(pushTryEntry, this), this.reset(!0); } function values(e) { if (e || "" === e) { var r = e[a]; if (r) return r.call(e); if ("function" == typeof e.next) return e; if (!isNaN(e.length)) { var o = -1, i = function next() { for (; ++o < e.length;) if (n.call(e, o)) return next.value = e[o], next.done = !1, next; return next.value = t, next.done = !0, next; }; return i.next = i; } } throw new TypeError(_typeof(e) + " is not iterable"); } return GeneratorFunction.prototype = GeneratorFunctionPrototype, o(g, "constructor", { value: GeneratorFunctionPrototype, configurable: !0 }), o(GeneratorFunctionPrototype, "constructor", { value: GeneratorFunction, configurable: !0 }), GeneratorFunction.displayName = define(GeneratorFunctionPrototype, u, "GeneratorFunction"), e.isGeneratorFunction = function (t) { var e = "function" == typeof t && t.constructor; return !!e && (e === GeneratorFunction || "GeneratorFunction" === (e.displayName || e.name)); }, e.mark = function (t) { return Object.setPrototypeOf ? Object.setPrototypeOf(t, GeneratorFunctionPrototype) : (t.__proto__ = GeneratorFunctionPrototype, define(t, u, "GeneratorFunction")), t.prototype = Object.create(g), t; }, e.awrap = function (t) { return { __await: t }; }, defineIteratorMethods(AsyncIterator.prototype), define(AsyncIterator.prototype, c, function () { return this; }), e.AsyncIterator = AsyncIterator, e.async = function (t, r, n, o, i) { void 0 === i && (i = Promise); var a = new AsyncIterator(wrap(t, r, n, o), i); return e.isGeneratorFunction(r) ? a : a.next().then(function (t) { return t.done ? t.value : a.next(); }); }, defineIteratorMethods(g), define(g, u, "Generator"), define(g, a, function () { return this; }), define(g, "toString", function () { return "[object Generator]"; }), e.keys = function (t) { var e = Object(t), r = []; for (var n in e) r.push(n); return r.reverse(), function next() { for (; r.length;) { var t = r.pop(); if (t in e) return next.value = t, next.done = !1, next; } return next.done = !0, next; }; }, e.values = values, Context.prototype = { constructor: Context, reset: function reset(e) { if (this.prev = 0, this.next = 0, this.sent = this._sent = t, this.done = !1, this.delegate = null, this.method = "next", this.arg = t, this.tryEntries.forEach(resetTryEntry), !e) for (var r in this) "t" === r.charAt(0) && n.call(this, r) && !isNaN(+r.slice(1)) && (this[r] = t); }, stop: function stop() { this.done = !0; var t = this.tryEntries[0].completion; if ("throw" === t.type) throw t.arg; return this.rval; }, dispatchException: function dispatchException(e) { if (this.done) throw e; var r = this; function handle(n, o) { return a.type = "throw", a.arg = e, r.next = n, o && (r.method = "next", r.arg = t), !!o; } for (var o = this.tryEntries.length - 1; o >= 0; --o) { var i = this.tryEntries[o], a = i.completion; if ("root" === i.tryLoc) return handle("end"); if (i.tryLoc <= this.prev) { var c = n.call(i, "catchLoc"), u = n.call(i, "finallyLoc"); if (c && u) { if (this.prev < i.catchLoc) return handle(i.catchLoc, !0); if (this.prev < i.finallyLoc) return handle(i.finallyLoc); } else if (c) { if (this.prev < i.catchLoc) return handle(i.catchLoc, !0); } else { if (!u) throw Error("try statement without catch or finally"); if (this.prev < i.finallyLoc) return handle(i.finallyLoc); } } } }, abrupt: function abrupt(t, e) { for (var r = this.tryEntries.length - 1; r >= 0; --r) { var o = this.tryEntries[r]; if (o.tryLoc <= this.prev && n.call(o, "finallyLoc") && this.prev < o.finallyLoc) { var i = o; break; } } i && ("break" === t || "continue" === t) && i.tryLoc <= e && e <= i.finallyLoc && (i = null); var a = i ? i.completion : {}; return a.type = t, a.arg = e, i ? (this.method = "next", this.next = i.finallyLoc, y) : this.complete(a); }, complete: function complete(t, e) { if ("throw" === t.type) throw t.arg; return "break" === t.type || "continue" === t.type ? this.next = t.arg : "return" === t.type ? (this.rval = this.arg = t.arg, this.method = "return", this.next = "end") : "normal" === t.type && e && (this.next = e), y; }, finish: function finish(t) { for (var e = this.tryEntries.length - 1; e >= 0; --e) { var r = this.tryEntries[e]; if (r.finallyLoc === t) return this.complete(r.completion, r.afterLoc), resetTryEntry(r), y; } }, "catch": function _catch(t) { for (var e = this.tryEntries.length - 1; e >= 0; --e) { var r = this.tryEntries[e]; if (r.tryLoc === t) { var n = r.completion; if ("throw" === n.type) { var o = n.arg; resetTryEntry(r); } return o; } } throw Error("illegal catch attempt"); }, delegateYield: function delegateYield(e, r, n) { return this.delegate = { iterator: values(e), resultName: r, nextLoc: n }, "next" === this.method && (this.arg = t), y; } }, e; }
function ownKeys(e, r) { var t = Object.keys(e); if (Object.getOwnPropertySymbols) { var o = Object.getOwnPropertySymbols(e); r && (o = o.filter(function (r) { return Object.getOwnPropertyDescriptor(e, r).enumerable; })), t.push.apply(t, o); } return t; }
function _objectSpread(e) { for (var r = 1; r < arguments.length; r++) { var t = null != arguments[r] ? arguments[r] : {}; r % 2 ? ownKeys(Object(t), !0).forEach(function (r) { _defineProperty(e, r, t[r]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys(Object(t)).forEach(function (r) { Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r)); }); } return e; }
function _defineProperty(e, r, t) { return (r = _toPropertyKey(r)) in e ? Object.defineProperty(e, r, { value: t, enumerable: !0, configurable: !0, writable: !0 }) : e[r] = t, e; }
function _toPropertyKey(t) { var i = _toPrimitive(t, "string"); return "symbol" == _typeof(i) ? i : i + ""; }
function _toPrimitive(t, r) { if ("object" != _typeof(t) || !t) return t; var e = t[Symbol.toPrimitive]; if (void 0 !== e) { var i = e.call(t, r || "default"); if ("object" != _typeof(i)) return i; throw new TypeError("@@toPrimitive must return a primitive value."); } return ("string" === r ? String : Number)(t); }
function asyncGeneratorStep(n, t, e, r, o, a, c) { try { var i = n[a](c), u = i.value; } catch (n) { return void e(n); } i.done ? t(u) : Promise.resolve(u).then(r, o); }
function _asyncToGenerator(n) { return function () { var t = this, e = arguments; return new Promise(function (r, o) { var a = n.apply(t, e); function _next(n) { asyncGeneratorStep(a, r, o, _next, _throw, "next", n); } function _throw(n) { asyncGeneratorStep(a, r, o, _next, _throw, "throw", n); } _next(void 0); }); }; }
var helpers = (0, _helpers["default"])();
var envRepository = (0, _repository["default"])();
var environmentAccessRepository = (0, _repository5["default"])();
var callApiRepository = (0, _repository2["default"])();
var mapClientResourceRepository = (0, _repository3["default"])();
var requestResponseRepo = (0, _repository4["default"])();
function sendApiRepository() {
  /**
   * Mapping from request http to API (with access validation)
   * */
  // --> Diganti ke RequestAccessAPI
  var mappingRequestApiAccessValidation = /*#__PURE__*/function () {
    var _ref = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee(req) {
      var getFromReq, idUser, isEnabledAccess, _req$decoded, id, role, _req$query, accessUserId, enabledAccess, getEnvironmentAccess, execGetEnvAccess, getEnv, getEndpointByAccess, execEndpointByAccess, getPathIntegration, _getPathIntegration, bodyRequest, mapClientResources, mappingTemplate, request, varQuery, _getPathIntegration2, params, fromParams, _loop, _ret, key, queryParams, _loop2, _ret2, queryKey, getListMappingClientRsc, getListMappingClientRscResponse, mode, originBodyRequest, resultParamSend, _getPathIntegration3, name, groupName, version, idEnv, _id;
      return _regeneratorRuntime().wrap(function _callee$(_context3) {
        while (1) switch (_context3.prev = _context3.next) {
          case 0:
            getFromReq = {};
            idUser = "";
            isEnabledAccess = false; // todo get user login
            _req$decoded = req.decoded, id = _req$decoded.id, role = _req$decoded.role;
            getFromReq["path"] = "/" + req.path.split("/").slice(4).join("/");
            getFromReq["method"] = req.method;
            getFromReq["url"] = req.originalUrl.split("/")[4];
            getFromReq["query"] = Object.keys(req.query).length > 0 ? req.query : null;
            getFromReq["fullUrl"] = req.protocol + "://" + req.get("host") + req.originalUrl;
            getFromReq["env"] = req.originalUrl.split("/")[5];

            // todo untuk admin dapat memilih menggunakan user lain untuk execute endpoint
            _req$query = req.query, accessUserId = _req$query.accessUserId, enabledAccess = _req$query.enabledAccess; //todo khusus untuk ADMIN
            if (role === 1) {
              idUser = accessUserId ? accessUserId : id;
              isEnabledAccess = enabledAccess ? enabledAccess === "true" : false;
            }
            //todo khusus untuk USER
            if (role === 2) {
              idUser = id;
            }
            if (!(!getFromReq.query || !getFromReq.query.hasOwnProperty("request_id"))) {
              _context3.next = 15;
              break;
            }
            return _context3.abrupt("return", (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.methodNotAllowed405,
              message: "Request not allowed: parameter request_id not found"
            }));
          case 15:
            // todo menambahkan validasi disini get environment by access
            getEnvironmentAccess = (0, _query.sql_getEnvironmentAccess)({
              userId: idUser,
              envName: getFromReq.url
            });
            _context3.next = 18;
            return (0, _callQuery.queryExecute)({
              query: getEnvironmentAccess,
              getOneObject: true
            });
          case 18:
            execGetEnvAccess = _context3.sent;
            if (execGetEnvAccess) {
              _context3.next = 21;
              break;
            }
            return _context3.abrupt("return", (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.badRequest400,
              message: "Environment cant access, please call administrator !"
            }));
          case 21:
            if (execGetEnvAccess.status) {
              _context3.next = 23;
              break;
            }
            return _context3.abrupt("return", (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.badRequest400,
              message: "Environment status cant active, please call administrator !"
            }));
          case 23:
            _context3.next = 25;
            return envRepository.getEnvByNameUrl({
              envName: getFromReq.url
            });
          case 25:
            getEnv = _context3.sent;
            if (!(getEnv.code !== _responseApi.statusHttpCode.ok200.code)) {
              _context3.next = 28;
              break;
            }
            return _context3.abrupt("return", (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.badRequest400,
              message: getEnv.message
            }));
          case 28:
            // todo menambahkan validasi disini get endpoint by access
            getEndpointByAccess = (0, _query.sql_getEndpointByAccess)({
              userId: idUser,
              method: getFromReq.method,
              requestId: getFromReq.query.request_id,
              envId: execGetEnvAccess.idEnv
            });
            _context3.next = 31;
            return (0, _callQuery.queryExecute)({
              query: getEndpointByAccess,
              getOneObject: true
            });
          case 31:
            execEndpointByAccess = _context3.sent;
            if (execEndpointByAccess) {
              _context3.next = 34;
              break;
            }
            return _context3.abrupt("return", (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.badRequest400,
              message: "API not found !, please check environment, request_id, and method",
              data: {
                time: helpers.getDateTimeNowFormattedIndonesianDateTime(),
                requestDetail: getFromReq,
                body: req.body
              }
            }));
          case 34:
            if (!(execEndpointByAccess.isDisabled && !isEnabledAccess)) {
              _context3.next = 36;
              break;
            }
            return _context3.abrupt("return", (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.badRequest400,
              message: "API unable to access !, please call administrator !"
            }));
          case 36:
            _context3.next = 38;
            return getIntegrationByPath({
              path: getFromReq.path,
              method: getFromReq.method,
              idEnv: getEnv.results.id,
              originalUrl: getFromReq.fullUrl,
              requestId: getFromReq.query.request_id
            });
          case 38:
            getPathIntegration = _context3.sent;
            if (!(getPathIntegration.code !== _responseApi.statusHttpCode.ok200.code)) {
              _context3.next = 41;
              break;
            }
            return _context3.abrupt("return", (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.badRequest400,
              message: getPathIntegration.message
            }));
          case 41:
            /**
             * SetNew getPathIntegration
             * */
            getPathIntegration = getPathIntegration.results;
            _getPathIntegration = getPathIntegration, bodyRequest = _getPathIntegration.bodyRequest, mapClientResources = _getPathIntegration.mapClientResources, mappingTemplate = _getPathIntegration.mappingTemplate, request = _getPathIntegration.request;
            /**
             * Get Params Path FROM DB
             * */
            if (!(getPathIntegration.params.variable.length > 0)) {
              _context3.next = 48;
              break;
            }
            // let varQuery = getPathIntegration.path.split(':');
            // digantikan ke sini karna ada issue path panjang tidak kena
            varQuery = getPathIntegration.path.split("/").filter(function (item) {
              return item !== "";
            });
            if (!(req.path.split("/").slice(4).length !== varQuery.length)) {
              _context3.next = 47;
              break;
            }
            return _context3.abrupt("return", (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.methodNotAllowed405,
              message: "Request not allowed: Params Path invalid !"
            }));
          case 47:
            //NewLogic Get Params
            varQuery.map(function (el, idx) {
              var pattern = new RegExp(":");
              if (pattern.test(el)) {
                var resPath = req.path.split("/").slice(4)[idx];
                var newEL = helpers.removeSpecialCase(el);
                getFromReq["params"] = _objectSpread(_objectSpread({}, getFromReq["params"]), {}, _defineProperty({}, "".concat(newEL), req.path.split("/").slice(4)[idx]));
              }
            });
          case 48:
            if (!(getFromReq.params && getPathIntegration.params.variable.length > 0)) {
              _context3.next = 61;
              break;
            }
            _getPathIntegration2 = getPathIntegration, params = _getPathIntegration2.params;
            fromParams = params.variable;
            _loop = /*#__PURE__*/_regeneratorRuntime().mark(function _loop(key) {
              var queryIdx;
              return _regeneratorRuntime().wrap(function _loop$(_context) {
                while (1) switch (_context.prev = _context.next) {
                  case 0:
                    queryIdx = fromParams.findIndex(function (ql) {
                      return ql.key === key;
                    });
                    if (!(queryIdx < 0)) {
                      _context.next = 3;
                      break;
                    }
                    return _context.abrupt("return", {
                      v: (0, _responseApi.result)({
                        statusCode: _responseApi.statusHttpCode.methodNotAllowed405,
                        message: "Request not allowed: Path parameter are different"
                      })
                    });
                  case 3:
                    getPathIntegration.params.variable[queryIdx].value = helpers.replacingValue("#".concat(params.variable[queryIdx].value, "#"), getFromReq.params[key]);
                  case 4:
                  case "end":
                    return _context.stop();
                }
              }, _loop);
            });
            _context3.t0 = _regeneratorRuntime().keys(getFromReq.params);
          case 53:
            if ((_context3.t1 = _context3.t0()).done) {
              _context3.next = 61;
              break;
            }
            key = _context3.t1.value;
            return _context3.delegateYield(_loop(key), "t2", 56);
          case 56:
            _ret = _context3.t2;
            if (!_ret) {
              _context3.next = 59;
              break;
            }
            return _context3.abrupt("return", _ret.v);
          case 59:
            _context3.next = 53;
            break;
          case 61:
            if (!getFromReq.query) {
              _context3.next = 78;
              break;
            }
            queryParams = Object.keys(getFromReq.query).filter(function (el) {
              return el !== "variable" && el !== "request_id" && el !== "test_request" && el !== "overrideResponse" && el !== "accessUserId" && el !== "enabledAccess";
            });
            if (!(queryParams.length > 0)) {
              _context3.next = 78;
              break;
            }
            if (!(getPathIntegration.params.query.length === 0 || queryParams.length !== getPathIntegration.params.query.length)) {
              _context3.next = 68;
              break;
            }
            return _context3.abrupt("return", (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.methodNotAllowed405,
              message: "Request not allowed: Params Query not found"
            }));
          case 68:
            _loop2 = /*#__PURE__*/_regeneratorRuntime().mark(function _loop2(queryKey) {
              var getFilter, checkTgr, requestQueryUrl;
              return _regeneratorRuntime().wrap(function _loop2$(_context2) {
                while (1) switch (_context2.prev = _context2.next) {
                  case 0:
                    getFilter = getPathIntegration.params.query.find(function (queryFill) {
                      return queryFill.key === queryParams[queryKey];
                    });
                    if (getFilter) {
                      _context2.next = 3;
                      break;
                    }
                    return _context2.abrupt("return", {
                      v: (0, _responseApi.result)({
                        statusCode: _responseApi.statusHttpCode.methodNotAllowed405,
                        message: "Request not allowed: Params Queries are different"
                      })
                    });
                  case 3:
                    checkTgr = new RegExp("#");
                    if (getPathIntegration.params.hasOwnProperty("triggerParamsQuery") && getPathIntegration.params.triggerParamsQuery.length > 0) {
                      /** replace list trigger */
                      requestQueryUrl = getPathIntegration.params.triggerParamsQuery.find(function (ql) {
                        return queryParams[queryKey] === ql.key;
                      });
                      if (requestQueryUrl) {
                        getPathIntegration.params.query.map(function (el) {
                          if (el.key === requestQueryUrl.key) {
                            el.value = helpers.replacingValue(requestQueryUrl.value, getFromReq.query[queryParams[queryKey]]);
                          }
                        });
                      }
                    } else {
                      /** Jika tidak ada di list trigger */
                      getPathIntegration.params.query[queryKey].value = getFromReq.query[queryParams[queryKey]];
                    }
                  case 5:
                  case "end":
                    return _context2.stop();
                }
              }, _loop2);
            });
            _context3.t3 = _regeneratorRuntime().keys(getPathIntegration.params.query);
          case 70:
            if ((_context3.t4 = _context3.t3()).done) {
              _context3.next = 78;
              break;
            }
            queryKey = _context3.t4.value;
            return _context3.delegateYield(_loop2(queryKey), "t5", 73);
          case 73:
            _ret2 = _context3.t5;
            if (!_ret2) {
              _context3.next = 76;
              break;
            }
            return _context3.abrupt("return", _ret2.v);
          case 76:
            _context3.next = 70;
            break;
          case 78:
            _context3.next = 80;
            return mapClientResourceRepository.showList({
              query: {
                endpointIntegrationId: getFromReq.query["request_id"],
                noPagination: true,
                resourceType: "request"
              }
            });
          case 80:
            getListMappingClientRsc = _context3.sent;
            _context3.next = 83;
            return mapClientResourceRepository.showList({
              query: {
                endpointIntegrationId: getFromReq.query["request_id"],
                noPagination: true,
                resourceType: "response"
              }
            });
          case 83:
            getListMappingClientRscResponse = _context3.sent;
            if (getListMappingClientRsc.code === 200 && getListMappingClientRsc.results.rows.length > 0) {
              getListMappingClientRsc = getListMappingClientRsc.results.rows;
            } else {
              getListMappingClientRsc = [];
            }

            /**
             * This process mapping body request: add new parameter or changes value
             * */
            mode = null;
            request.header = [];
            originBodyRequest = bodyRequest;
            getPathIntegration["originalBodyRequest"] = _objectSpread(_objectSpread({}, originBodyRequest), {}, {
              resChanges: originBodyRequest
            });
            _context3.t6 = bodyRequest.mode;
            _context3.next = _context3.t6 === "raw" ? 92 : _context3.t6 === "urlencoded" ? 98 : 104;
            break;
          case 92:
            request.header.push({
              key: "Content-Type",
              value: "application/json",
              type: "text"
            });
            mode = "rawJson";
            _context3.next = 96;
            return (0, _helpers_maprequest_body.bodyRequestRaw)(req.body, getListMappingClientRsc);
          case 96:
            bodyRequest["rawJson"] = _context3.sent;
            return _context3.abrupt("break", 106);
          case 98:
            request.header.push({
              key: "Content-Type",
              value: "application/x-www-form-urlencoded"
            });
            mode = "formUrlEncoded";
            _context3.next = 102;
            return (0, _helpers_maprequest_body.bodyRequestRaw)(req.body, getListMappingClientRsc);
          case 102:
            bodyRequest["formUrlEncoded"] = _context3.sent;
            return _context3.abrupt("break", 106);
          case 104:
            request.header.push({
              key: "Content-Type",
              value: "application/json",
              type: "text"
            });
            return _context3.abrupt("break", 106);
          case 106:
            /**
             * Replace MapTemplateValue
             * */
            if (mappingTemplate && bodyRequest.mode !== "none") {
              (0, _helpers_maprequest_body.changesValueByPropertyKey)(mappingTemplate, bodyRequest[mode]);
            }

            //todo added getFromReq with Environment
            execGetEnvAccess.variable = JSON.parse(execGetEnvAccess.variable);
            getFromReq["environmentAccess"] = execGetEnvAccess;
            _context3.next = 111;
            return _mappingParameterSendAccess(getPathIntegration, getFromReq, getListMappingClientRsc);
          case 111:
            resultParamSend = _context3.sent;
            if (getListMappingClientRscResponse.code === 200 && getListMappingClientRscResponse.results.rows.length > 0) {
              getListMappingClientRscResponse = getListMappingClientRscResponse.results.rows;
            } else {
              getListMappingClientRscResponse = [];
            }
            /**
             * Trigger Response By Value Manipulation
             * */
            if (!(resultParamSend.resultCall.code === 200 && getFromReq.query.test_request === "false")) {
              _context3.next = 117;
              break;
            }
            _context3.next = 116;
            return triggerResponseMapClientResource(resultParamSend.resultCall.results, getListMappingClientRscResponse);
          case 116:
            resultParamSend.resultCall.results = _context3.sent;
          case 117:
            if (!(getPathIntegration.responseTrigger && Object.keys(getPathIntegration.responseTrigger).length > 0 && !!getFromReq.query["overrideResponse"] && getFromReq.query["overrideResponse"] === "true" && getFromReq.query.test_request === "false")) {
              _context3.next = 123;
              break;
            }
            if (!(Object.keys(getPathIntegration.responseTrigger.newMappingResponse).length > 0 && resultParamSend.resultCall.code === 200)) {
              _context3.next = 123;
              break;
            }
            _context3.next = 121;
            return triggerResponse(getPathIntegration.responseTrigger.newMappingResponse, resultParamSend.resultCall.results);
          case 121:
            resultParamSend.resultCall.results = _context3.sent;
            if (getPathIntegration.responseTrigger.getIndexByArray !== "none") {
              resultParamSend.resultCall.results = resultParamSend.resultCall.results[0];
            }
          case 123:
            if (!(process.env.NODE_ENV === "development")) {
              _context3.next = 127;
              break;
            }
            return _context3.abrupt("return", resultParamSend.resultCall);
          case 127:
            if (!(getFromReq.query.hasOwnProperty("test_request") && getFromReq.query.test_request === "true")) {
              _context3.next = 131;
              break;
            }
            return _context3.abrupt("return", resultParamSend.resultCall);
          case 131:
            _getPathIntegration3 = getPathIntegration, name = _getPathIntegration3.name, groupName = _getPathIntegration3.groupName, version = _getPathIntegration3.version, idEnv = _getPathIntegration3.idEnv;
            _id = getFromReq.environmentAccess.id;
            req.body = {
              idEnvAccess: _id,
              requestId: getFromReq.query.request_id,
              path: getFromReq.fullUrl,
              method: getFromReq.method,
              resourceName: name,
              groupName: groupName,
              idEnv: idEnv,
              version: version,
              message: resultParamSend.resultCall.message,
              httpCode: resultParamSend.resultCall.code,
              request: bodyRequest,
              response: resultParamSend.resultCall.results && resultParamSend.resultCall.results.length > 0 ? resultParamSend.resultCall.results : resultParamSend.resultCall
            };
            /** for save history */
            _context3.next = 136;
            return requestResponseRepo.create(req);
          case 136:
            return _context3.abrupt("return", resultParamSend.resultCall);
          case 137:
          case "end":
            return _context3.stop();
        }
      }, _callee);
    }));
    return function mappingRequestApiAccessValidation(_x) {
      return _ref.apply(this, arguments);
    };
  }();
  /**
   * Mapping from request http to API
   * */
  var mappingRequestApi = /*#__PURE__*/function () {
    var _ref2 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee2(req) {
      var getFromReq, getEnv, getPathIntegration, _getPathIntegration4, bodyRequest, mapClientResources, mappingTemplate, request, varQuery, _getPathIntegration5, params, fromParams, _loop3, _ret3, key, queryParams, _loop4, _ret4, queryKey, getListMappingClientRsc, getListMappingClientRscResponse, mode, originBodyRequest, resultParamSend, _getPathIntegration6, name, groupName, version, idEnv;
      return _regeneratorRuntime().wrap(function _callee2$(_context6) {
        while (1) switch (_context6.prev = _context6.next) {
          case 0:
            getFromReq = {};
            getFromReq["path"] = "/" + req.path.split("/").slice(3).join("/");
            getFromReq["method"] = req.method;
            getFromReq["url"] = req.originalUrl.split("/")[3];
            getFromReq["query"] = Object.keys(req.query).length > 0 ? req.query : null;
            getFromReq["fullUrl"] = req.protocol + "://" + req.get("host") + req.originalUrl;
            getFromReq["env"] = req.originalUrl.split("/")[4];
            if (!(!getFromReq.query || !getFromReq.query.hasOwnProperty("request_id"))) {
              _context6.next = 9;
              break;
            }
            return _context6.abrupt("return", (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.methodNotAllowed405,
              message: "Request not allowed: parameter request_id not found"
            }));
          case 9:
            _context6.next = 11;
            return envRepository.getEnvByNameUrl({
              envName: getFromReq.url
            });
          case 11:
            getEnv = _context6.sent;
            console.log('getEnv ?? ');
            if (!(getEnv.code !== _responseApi.statusHttpCode.ok200.code)) {
              _context6.next = 15;
              break;
            }
            return _context6.abrupt("return", getEnv);
          case 15:
            _context6.next = 17;
            return getIntegrationByPath({
              path: getFromReq.path,
              method: getFromReq.method,
              idEnv: getEnv.results.id,
              originalUrl: getFromReq.fullUrl,
              requestId: getFromReq.query.request_id
            });
          case 17:
            getPathIntegration = _context6.sent;
            if (!(getPathIntegration.code !== _responseApi.statusHttpCode.ok200.code)) {
              _context6.next = 20;
              break;
            }
            return _context6.abrupt("return", getPathIntegration);
          case 20:
            /**
             * SetNew getPathIntegration
             * */
            getPathIntegration = getPathIntegration.results;
            _getPathIntegration4 = getPathIntegration, bodyRequest = _getPathIntegration4.bodyRequest, mapClientResources = _getPathIntegration4.mapClientResources, mappingTemplate = _getPathIntegration4.mappingTemplate, request = _getPathIntegration4.request;
            /**
             * Get Params Path FROM DB
             * */
            if (!(getPathIntegration.params.variable.length > 0)) {
              _context6.next = 27;
              break;
            }
            // let varQuery = getPathIntegration.path.split(':');
            // digantikan ke sini karna ada issue path panjang tidak kena
            varQuery = getPathIntegration.path.split("/").filter(function (item) {
              return item !== "";
            });
            if (!(req.path.split("/").slice(3).length !== varQuery.length)) {
              _context6.next = 26;
              break;
            }
            return _context6.abrupt("return", (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.methodNotAllowed405,
              message: "Request not allowed: Params Path invalid !"
            }));
          case 26:
            //NewLogic Get Params
            varQuery.map(function (el, idx) {
              var pattern = new RegExp(":");
              if (pattern.test(el)) {
                var resPath = req.path.split("/").slice(3)[idx];
                var newEL = helpers.removeSpecialCase(el);
                getFromReq["params"] = _objectSpread(_objectSpread({}, getFromReq["params"]), {}, _defineProperty({}, "".concat(newEL), req.path.split("/").slice(3)[idx]));
              }
            });

            // varQuery.map((el, idx) => {
            //     if (idx > 0) {
            //         let newEL = helpers.removeSpecialCase(el)
            //         getFromReq['params'] = {
            //             ...getFromReq['params'], [`${newEL}`]: req.path.split('/').slice(3)[idx]
            //         }
            //     }
            // })
          case 27:
            if (!(getFromReq.params && getPathIntegration.params.variable.length > 0)) {
              _context6.next = 40;
              break;
            }
            _getPathIntegration5 = getPathIntegration, params = _getPathIntegration5.params;
            fromParams = params.variable;
            _loop3 = /*#__PURE__*/_regeneratorRuntime().mark(function _loop3(key) {
              var queryIdx;
              return _regeneratorRuntime().wrap(function _loop3$(_context4) {
                while (1) switch (_context4.prev = _context4.next) {
                  case 0:
                    queryIdx = fromParams.findIndex(function (ql) {
                      return ql.key === key;
                    });
                    if (!(queryIdx < 0)) {
                      _context4.next = 3;
                      break;
                    }
                    return _context4.abrupt("return", {
                      v: (0, _responseApi.result)({
                        statusCode: _responseApi.statusHttpCode.methodNotAllowed405,
                        message: "Request not allowed: Path parameter are different"
                      })
                    });
                  case 3:
                    getPathIntegration.params.variable[queryIdx].value = helpers.replacingValue("#".concat(params.variable[queryIdx].value, "#"), getFromReq.params[key]);
                  case 4:
                  case "end":
                    return _context4.stop();
                }
              }, _loop3);
            });
            _context6.t0 = _regeneratorRuntime().keys(getFromReq.params);
          case 32:
            if ((_context6.t1 = _context6.t0()).done) {
              _context6.next = 40;
              break;
            }
            key = _context6.t1.value;
            return _context6.delegateYield(_loop3(key), "t2", 35);
          case 35:
            _ret3 = _context6.t2;
            if (!_ret3) {
              _context6.next = 38;
              break;
            }
            return _context6.abrupt("return", _ret3.v);
          case 38:
            _context6.next = 32;
            break;
          case 40:
            if (!getFromReq.query) {
              _context6.next = 57;
              break;
            }
            queryParams = Object.keys(getFromReq.query).filter(function (el) {
              return el !== "variable" && el !== "request_id" && el !== "test_request" && el !== "overrideResponse";
            });
            if (!(queryParams.length > 0)) {
              _context6.next = 57;
              break;
            }
            if (!(getPathIntegration.params.query.length === 0 || queryParams.length !== getPathIntegration.params.query.length)) {
              _context6.next = 47;
              break;
            }
            return _context6.abrupt("return", (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.methodNotAllowed405,
              message: "Request not allowed: Params Query not found"
            }));
          case 47:
            _loop4 = /*#__PURE__*/_regeneratorRuntime().mark(function _loop4(queryKey) {
              var getFilter, checkTgr, requestQueryUrl;
              return _regeneratorRuntime().wrap(function _loop4$(_context5) {
                while (1) switch (_context5.prev = _context5.next) {
                  case 0:
                    getFilter = getPathIntegration.params.query.find(function (queryFill) {
                      return queryFill.key === queryParams[queryKey];
                    });
                    if (getFilter) {
                      _context5.next = 3;
                      break;
                    }
                    return _context5.abrupt("return", {
                      v: (0, _responseApi.result)({
                        statusCode: _responseApi.statusHttpCode.methodNotAllowed405,
                        message: "Request not allowed: Params Queries are different"
                      })
                    });
                  case 3:
                    checkTgr = new RegExp("#");
                    if (getPathIntegration.params.hasOwnProperty("triggerParamsQuery") && getPathIntegration.params.triggerParamsQuery.length > 0) {
                      /** replace list trigger */
                      requestQueryUrl = getPathIntegration.params.triggerParamsQuery.find(function (ql) {
                        return queryParams[queryKey] === ql.key;
                      });
                      if (requestQueryUrl) {
                        getPathIntegration.params.query.map(function (el) {
                          if (el.key === requestQueryUrl.key) {
                            el.value = helpers.replacingValue(requestQueryUrl.value, getFromReq.query[queryParams[queryKey]]);
                          }
                        });
                      }
                    } else {
                      /** Jika tidak ada di list trigger */
                      getPathIntegration.params.query[queryKey].value = getFromReq.query[queryParams[queryKey]];
                    }
                  case 5:
                  case "end":
                    return _context5.stop();
                }
              }, _loop4);
            });
            _context6.t3 = _regeneratorRuntime().keys(getPathIntegration.params.query);
          case 49:
            if ((_context6.t4 = _context6.t3()).done) {
              _context6.next = 57;
              break;
            }
            queryKey = _context6.t4.value;
            return _context6.delegateYield(_loop4(queryKey), "t5", 52);
          case 52:
            _ret4 = _context6.t5;
            if (!_ret4) {
              _context6.next = 55;
              break;
            }
            return _context6.abrupt("return", _ret4.v);
          case 55:
            _context6.next = 49;
            break;
          case 57:
            _context6.next = 59;
            return mapClientResourceRepository.showList({
              query: {
                endpointIntegrationId: getFromReq.query["request_id"],
                noPagination: true,
                resourceType: "request"
              }
            });
          case 59:
            getListMappingClientRsc = _context6.sent;
            _context6.next = 62;
            return mapClientResourceRepository.showList({
              query: {
                endpointIntegrationId: getFromReq.query["request_id"],
                noPagination: true,
                resourceType: "response"
              }
            });
          case 62:
            getListMappingClientRscResponse = _context6.sent;
            if (getListMappingClientRsc.code === 200 && getListMappingClientRsc.results.rows.length > 0) {
              getListMappingClientRsc = getListMappingClientRsc.results.rows;
            } else {
              getListMappingClientRsc = [];
            }

            /**
             * This process mapping body request: add new parameter or changes value
             * */
            mode = null;
            request.header = [];
            originBodyRequest = bodyRequest;
            getPathIntegration["originalBodyRequest"] = _objectSpread(_objectSpread({}, originBodyRequest), {}, {
              resChanges: originBodyRequest
            });
            _context6.t6 = bodyRequest.mode;
            _context6.next = _context6.t6 === "raw" ? 71 : _context6.t6 === "urlencoded" ? 77 : 83;
            break;
          case 71:
            request.header.push({
              key: "Content-Type",
              value: "application/json",
              type: "text"
            });
            mode = "rawJson";
            _context6.next = 75;
            return (0, _helpers_maprequest_body.bodyRequestRaw)(req.body, getListMappingClientRsc);
          case 75:
            bodyRequest["rawJson"] = _context6.sent;
            return _context6.abrupt("break", 85);
          case 77:
            request.header.push({
              key: "Content-Type",
              value: "application/x-www-form-urlencoded"
            });
            mode = "formUrlEncoded";
            _context6.next = 81;
            return (0, _helpers_maprequest_body.bodyRequestRaw)(req.body, getListMappingClientRsc);
          case 81:
            bodyRequest["formUrlEncoded"] = _context6.sent;
            return _context6.abrupt("break", 85);
          case 83:
            request.header.push({
              key: "Content-Type",
              value: "application/json",
              type: "text"
            });
            return _context6.abrupt("break", 85);
          case 85:
            /**
             * Replace MapTemplateValue
             * */
            if (mappingTemplate && bodyRequest.mode !== "none") {
              (0, _helpers_maprequest_body.changesValueByPropertyKey)(mappingTemplate, bodyRequest[mode]);
            }
            _context6.next = 88;
            return _mappingParameterSend(getPathIntegration, getFromReq, getListMappingClientRsc);
          case 88:
            resultParamSend = _context6.sent;
            //todo testing master api
            // return result({
            //     message: "master api",
            //     data: {
            //         getPathIntegration: getPathIntegration,
            //         getFromReq: getFromReq,
            //         getListMappingClientRsc: getListMappingClientRsc,
            //         resultParamSend: resultParamSend
            //     }
            // })

            if (getListMappingClientRscResponse.code === 200 && getListMappingClientRscResponse.results.rows.length > 0) {
              getListMappingClientRscResponse = getListMappingClientRscResponse.results.rows;
            } else {
              getListMappingClientRscResponse = [];
            }
            /**
             * Trigger Response By Value Manipulation
             * */
            if (!(resultParamSend.resultCall.code === 200 && getFromReq.query.test_request === "false")) {
              _context6.next = 94;
              break;
            }
            _context6.next = 93;
            return triggerResponseMapClientResource(resultParamSend.resultCall.results, getListMappingClientRscResponse);
          case 93:
            resultParamSend.resultCall.results = _context6.sent;
          case 94:
            if (!(getPathIntegration.responseTrigger && Object.keys(getPathIntegration.responseTrigger).length > 0 && !!getFromReq.query["overrideResponse"] && getFromReq.query["overrideResponse"] === "true" && getFromReq.query.test_request === "false")) {
              _context6.next = 100;
              break;
            }
            if (!(Object.keys(getPathIntegration.responseTrigger.newMappingResponse).length > 0 && resultParamSend.resultCall.code === 200)) {
              _context6.next = 100;
              break;
            }
            _context6.next = 98;
            return triggerResponse(getPathIntegration.responseTrigger.newMappingResponse, resultParamSend.resultCall.results);
          case 98:
            resultParamSend.resultCall.results = _context6.sent;
            if (getPathIntegration.responseTrigger.getIndexByArray !== "none") {
              resultParamSend.resultCall.results = resultParamSend.resultCall.results[0];
            }
          case 100:
            if (!(process.env.NODE_ENV === "development")) {
              _context6.next = 104;
              break;
            }
            return _context6.abrupt("return", resultParamSend.resultCall);
          case 104:
            if (!(getFromReq.query.hasOwnProperty("test_request") && getFromReq.query.test_request === "true")) {
              _context6.next = 108;
              break;
            }
            return _context6.abrupt("return", resultParamSend.resultCall);
          case 108:
            _getPathIntegration6 = getPathIntegration, name = _getPathIntegration6.name, groupName = _getPathIntegration6.groupName, version = _getPathIntegration6.version, idEnv = _getPathIntegration6.idEnv;
            req.body = {
              requestId: getFromReq.query.request_id,
              path: getFromReq.fullUrl,
              method: getFromReq.method,
              resourceName: name,
              groupName: groupName,
              idEnv: idEnv,
              version: version,
              message: resultParamSend.resultCall.message,
              httpCode: resultParamSend.resultCall.code,
              request: resultParamSend.resultTest ? resultParamSend.resultTest : {},
              response: resultParamSend.resultCall.results && resultParamSend.resultCall.results.length > 0 ? resultParamSend.resultCall.results : resultParamSend.resultCall
            };
            /** for save history */
            _context6.next = 112;
            return requestResponseRepo.create(req);
          case 112:
            return _context6.abrupt("return", resultParamSend.resultCall);
          case 113:
          case "end":
            return _context6.stop();
        }
      }, _callee2);
    }));
    return function mappingRequestApi(_x2) {
      return _ref2.apply(this, arguments);
    };
  }();
  /**
   * Mapping from request http to API From Transporter
   * */
  var mappingRequestApiFromTransporter = /*#__PURE__*/function () {
    var _ref3 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee3(req) {
      var getFromReq, getEnv, getPathIntegration, _getPathIntegration7, bodyRequest, mapClientResources, mappingTemplate, request, varQuery, _getPathIntegration8, params, fromParams, _loop5, _ret5, key, queryParams, _loop6, _ret6, queryKey, getListMappingClientRsc, getListMappingClientRscResponse, mode, originBodyRequest, resultParamSend, _getPathIntegration9, name, groupName, version, idEnv;
      return _regeneratorRuntime().wrap(function _callee3$(_context9) {
        while (1) switch (_context9.prev = _context9.next) {
          case 0:
            getFromReq = req; // todo
            // getFromReq['path'] = "/" + req.path.split('/').slice(3).join('/');
            // getFromReq['method'] = req.method;
            // getFromReq['url'] = req.fullUrl.split('/')[3];
            // getFromReq['query'] = Object.keys(req.query).length > 0 ? req.query : null;
            // getFromReq['fullUrl'] = req.protocol + '://' + req.get('host') + req.originalUrl;
            // getFromReq['env'] = req.originalUrl.split('/')[4]
            // return untuk testing
            // return result({
            //     data: getFromReq
            // })
            if (!(!getFromReq.query || !getFromReq.query.hasOwnProperty("request_id"))) {
              _context9.next = 3;
              break;
            }
            return _context9.abrupt("return", (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.methodNotAllowed405,
              message: "Request not allowed: parameter request_id not found"
            }));
          case 3:
            _context9.next = 5;
            return envRepository.getEnvByNameUrl({
              envName: getFromReq.url
            });
          case 5:
            getEnv = _context9.sent;
            if (!(getEnv.code !== _responseApi.statusHttpCode.ok200.code)) {
              _context9.next = 8;
              break;
            }
            return _context9.abrupt("return", getEnv);
          case 8:
            _context9.next = 10;
            return getIntegrationByPath({
              path: getFromReq.path,
              method: getFromReq.method,
              idEnv: getEnv.results.id,
              originalUrl: getFromReq.fullUrl,
              requestId: getFromReq.query.request_id
            });
          case 10:
            getPathIntegration = _context9.sent;
            if (!(getPathIntegration.code !== _responseApi.statusHttpCode.ok200.code)) {
              _context9.next = 13;
              break;
            }
            return _context9.abrupt("return", getPathIntegration);
          case 13:
            /**
             * SetNew getPathIntegration
             * */
            getPathIntegration = getPathIntegration.results;
            _getPathIntegration7 = getPathIntegration, bodyRequest = _getPathIntegration7.bodyRequest, mapClientResources = _getPathIntegration7.mapClientResources, mappingTemplate = _getPathIntegration7.mappingTemplate, request = _getPathIntegration7.request;
            /**
             * Get Params Path FROM DB
             * */
            if (!(getPathIntegration.params.variable.length > 0)) {
              _context9.next = 20;
              break;
            }
            varQuery = getPathIntegration.path.split("/").filter(function (item) {
              return item !== "";
            });
            if (!(getFromReq.path.split("/").slice(1).length !== varQuery.length)) {
              _context9.next = 19;
              break;
            }
            return _context9.abrupt("return", (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.methodNotAllowed405,
              message: "Request not allowed: Params Path invalid !"
            }));
          case 19:
            //NewLogic Get Params
            varQuery.map(function (el, idx) {
              var pattern = new RegExp(":");
              if (pattern.test(el)) {
                var newEL = helpers.removeSpecialCase(el);
                getFromReq["params"] = _objectSpread(_objectSpread({}, getFromReq["params"]), {}, _defineProperty({}, "".concat(newEL), getFromReq.path.split("/").slice(1)[idx]));
              }
            });
          case 20:
            if (!(getFromReq.params && getPathIntegration.params.variable.length > 0)) {
              _context9.next = 33;
              break;
            }
            _getPathIntegration8 = getPathIntegration, params = _getPathIntegration8.params;
            fromParams = params.variable;
            _loop5 = /*#__PURE__*/_regeneratorRuntime().mark(function _loop5(key) {
              var queryIdx;
              return _regeneratorRuntime().wrap(function _loop5$(_context7) {
                while (1) switch (_context7.prev = _context7.next) {
                  case 0:
                    queryIdx = fromParams.findIndex(function (ql) {
                      return ql.key === key;
                    });
                    if (!(queryIdx < 0)) {
                      _context7.next = 3;
                      break;
                    }
                    return _context7.abrupt("return", {
                      v: (0, _responseApi.result)({
                        statusCode: _responseApi.statusHttpCode.methodNotAllowed405,
                        message: "Request not allowed: Path parameter are different"
                      })
                    });
                  case 3:
                    getPathIntegration.params.variable[queryIdx].value = helpers.replacingValue("#".concat(params.variable[queryIdx].value, "#"), getFromReq.params[key]);
                  case 4:
                  case "end":
                    return _context7.stop();
                }
              }, _loop5);
            });
            _context9.t0 = _regeneratorRuntime().keys(getFromReq.params);
          case 25:
            if ((_context9.t1 = _context9.t0()).done) {
              _context9.next = 33;
              break;
            }
            key = _context9.t1.value;
            return _context9.delegateYield(_loop5(key), "t2", 28);
          case 28:
            _ret5 = _context9.t2;
            if (!_ret5) {
              _context9.next = 31;
              break;
            }
            return _context9.abrupt("return", _ret5.v);
          case 31:
            _context9.next = 25;
            break;
          case 33:
            if (!getFromReq.query) {
              _context9.next = 50;
              break;
            }
            queryParams = Object.keys(getFromReq.query).filter(function (el) {
              return el !== "variable" && el !== "request_id" && el !== "test_request" && el !== "overrideResponse";
            });
            if (!(queryParams.length > 0)) {
              _context9.next = 50;
              break;
            }
            if (!(getPathIntegration.params.query.length === 0 || queryParams.length !== getPathIntegration.params.query.length)) {
              _context9.next = 40;
              break;
            }
            return _context9.abrupt("return", (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.methodNotAllowed405,
              message: "Request not allowed: Params Query not found"
            }));
          case 40:
            _loop6 = /*#__PURE__*/_regeneratorRuntime().mark(function _loop6(queryKey) {
              var getFilter, checkTgr, requestQueryUrl;
              return _regeneratorRuntime().wrap(function _loop6$(_context8) {
                while (1) switch (_context8.prev = _context8.next) {
                  case 0:
                    getFilter = getPathIntegration.params.query.find(function (queryFill) {
                      return queryFill.key === queryParams[queryKey];
                    });
                    if (getFilter) {
                      _context8.next = 3;
                      break;
                    }
                    return _context8.abrupt("return", {
                      v: (0, _responseApi.result)({
                        statusCode: _responseApi.statusHttpCode.methodNotAllowed405,
                        message: "Request not allowed: Params Queries are different"
                      })
                    });
                  case 3:
                    checkTgr = new RegExp("#");
                    if (getPathIntegration.params.hasOwnProperty("triggerParamsQuery") && getPathIntegration.params.triggerParamsQuery.length > 0) {
                      /** replace list trigger */
                      requestQueryUrl = getPathIntegration.params.triggerParamsQuery.find(function (ql) {
                        return queryParams[queryKey] === ql.key;
                      });
                      if (requestQueryUrl) {
                        getPathIntegration.params.query.map(function (el) {
                          if (el.key === requestQueryUrl.key) {
                            el.value = helpers.replacingValue(requestQueryUrl.value, getFromReq.query[queryParams[queryKey]]);
                          }
                        });
                      }
                    } else {
                      /** Jika tidak ada di list trigger */
                      getPathIntegration.params.query[queryKey].value = getFromReq.query[queryParams[queryKey]];
                    }
                  case 5:
                  case "end":
                    return _context8.stop();
                }
              }, _loop6);
            });
            _context9.t3 = _regeneratorRuntime().keys(getPathIntegration.params.query);
          case 42:
            if ((_context9.t4 = _context9.t3()).done) {
              _context9.next = 50;
              break;
            }
            queryKey = _context9.t4.value;
            return _context9.delegateYield(_loop6(queryKey), "t5", 45);
          case 45:
            _ret6 = _context9.t5;
            if (!_ret6) {
              _context9.next = 48;
              break;
            }
            return _context9.abrupt("return", _ret6.v);
          case 48:
            _context9.next = 42;
            break;
          case 50:
            _context9.next = 52;
            return mapClientResourceRepository.showList({
              query: {
                endpointIntegrationId: getFromReq.query["request_id"],
                noPagination: true,
                resourceType: "request"
              }
            });
          case 52:
            getListMappingClientRsc = _context9.sent;
            _context9.next = 55;
            return mapClientResourceRepository.showList({
              query: {
                endpointIntegrationId: getFromReq.query["request_id"],
                noPagination: true,
                resourceType: "response"
              }
            });
          case 55:
            getListMappingClientRscResponse = _context9.sent;
            if (getListMappingClientRsc.code === 200 && getListMappingClientRsc.results.rows.length > 0) {
              getListMappingClientRsc = getListMappingClientRsc.results.rows;
            } else {
              getListMappingClientRsc = [];
            }

            /**
             * This process mapping body request: add new parameter or changes value
             * */
            mode = null;
            request.header = [];
            originBodyRequest = bodyRequest;
            getPathIntegration["originalBodyRequest"] = _objectSpread(_objectSpread({}, originBodyRequest), {}, {
              resChanges: originBodyRequest
            });
            _context9.t6 = bodyRequest.mode;
            _context9.next = _context9.t6 === "raw" ? 64 : _context9.t6 === "urlencoded" ? 70 : 76;
            break;
          case 64:
            request.header.push({
              key: "Content-Type",
              value: "application/json",
              type: "text"
            });
            mode = "rawJson";
            _context9.next = 68;
            return (0, _helpers_maprequest_body.bodyRequestRaw)(getFromReq.body.rawJson, getListMappingClientRsc);
          case 68:
            bodyRequest["rawJson"] = _context9.sent;
            return _context9.abrupt("break", 78);
          case 70:
            request.header.push({
              key: "Content-Type",
              value: "application/x-www-form-urlencoded"
            });
            mode = "formUrlEncoded";
            _context9.next = 74;
            return (0, _helpers_maprequest_body.bodyRequestRaw)(getFromReq.body.formUrlEncoded, getListMappingClientRsc);
          case 74:
            bodyRequest["formUrlEncoded"] = _context9.sent;
            return _context9.abrupt("break", 78);
          case 76:
            request.header.push({
              key: "Content-Type",
              value: "application/json",
              type: "text"
            });
            return _context9.abrupt("break", 78);
          case 78:
            /**
             * Replace MapTemplateValue
             * */
            if (mappingTemplate && bodyRequest.mode !== "none") {
              (0, _helpers_maprequest_body.changesValueByPropertyKey)(mappingTemplate, bodyRequest[mode]);
            }
            _context9.next = 81;
            return _mappingParameterSend(getPathIntegration, getFromReq, getListMappingClientRsc);
          case 81:
            resultParamSend = _context9.sent;
            //todo testing access API
            // return result({
            //     message: "transporter",
            //     data: {
            //         bodyRequest: bodyRequest,
            //         getPathIntegration: getPathIntegration,
            //         getFromReq: getFromReq,
            //         getListMappingClientRsc: getListMappingClientRsc,
            //         resultParamSend: resultParamSend
            //     }
            // })

            if (getListMappingClientRscResponse.code === 200 && getListMappingClientRscResponse.results.rows.length > 0) {
              getListMappingClientRscResponse = getListMappingClientRscResponse.results.rows;
            } else {
              getListMappingClientRscResponse = [];
            }
            /**
             * Trigger Response By Value Manipulation
             * */
            if (!(resultParamSend.resultCall.code === 200 && getFromReq.query.test_request === "false")) {
              _context9.next = 87;
              break;
            }
            _context9.next = 86;
            return triggerResponseMapClientResource(resultParamSend.resultCall.results, getListMappingClientRscResponse);
          case 86:
            resultParamSend.resultCall.results = _context9.sent;
          case 87:
            if (!(getPathIntegration.responseTrigger && Object.keys(getPathIntegration.responseTrigger).length > 0 && !!getFromReq.query["overrideResponse"] && getFromReq.query["overrideResponse"] === "true" && getFromReq.query.test_request === "false")) {
              _context9.next = 93;
              break;
            }
            if (!(Object.keys(getPathIntegration.responseTrigger.newMappingResponse).length > 0 && resultParamSend.resultCall.code === 200)) {
              _context9.next = 93;
              break;
            }
            _context9.next = 91;
            return triggerResponse(getPathIntegration.responseTrigger.newMappingResponse, resultParamSend.resultCall.results);
          case 91:
            resultParamSend.resultCall.results = _context9.sent;
            if (getPathIntegration.responseTrigger.getIndexByArray !== "none") {
              resultParamSend.resultCall.results = resultParamSend.resultCall.results[0];
            }
          case 93:
            if (!(process.env.NODE_ENV === "development")) {
              _context9.next = 97;
              break;
            }
            return _context9.abrupt("return", resultParamSend.resultCall);
          case 97:
            if (getFromReq.query.test_request === "false") {
              _context9.next = 101;
              break;
            }
            return _context9.abrupt("return", resultParamSend.resultCall);
          case 101:
            _getPathIntegration9 = getPathIntegration, name = _getPathIntegration9.name, groupName = _getPathIntegration9.groupName, version = _getPathIntegration9.version, idEnv = _getPathIntegration9.idEnv;
            req.body = {
              requestId: getFromReq.query.request_id,
              path: getFromReq.fullUrl,
              method: getFromReq.method,
              resourceName: name,
              groupName: groupName,
              idEnv: idEnv,
              version: version,
              message: resultParamSend.resultCall.message,
              httpCode: resultParamSend.resultCall.code,
              request: resultParamSend.resultTest ? resultParamSend.resultTest : {},
              response: resultParamSend.resultCall.results && resultParamSend.resultCall.results.length > 0 ? resultParamSend.resultCall.results : resultParamSend.resultCall
            };
            /** for save history */
            _context9.next = 105;
            return requestResponseRepo.create(req);
          case 105:
            return _context9.abrupt("return", resultParamSend.resultCall);
          case 106:
          case "end":
            return _context9.stop();
        }
      }, _callee3);
    }));
    return function mappingRequestApiFromTransporter(_x3) {
      return _ref3.apply(this, arguments);
    };
  }();

  /**
   * Use for mapping from request to send with access users
   * */
  var countMappingParameterSendAccess = 0;
  var _mappingParameterSendAccess = /*#__PURE__*/function () {
    var _ref4 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee4(getPathIntegration, getFromReq, getListMappingClientRsc) {
      var resultsIntegration, paramsForAuth, getEnvLogin, envByParams, variable, header, getVariable, headers, option, urlApi, paramsQuery, params, bodyRequest, mappingTemplate, request, method, data, getTokenVariable, hostUrl, newOption, checkTestRequest, getEnvProcess, getCallAuth, getEnvDetailError, _resultCall, resultCall, codeResult, _getCallAuth;
      return _regeneratorRuntime().wrap(function _callee4$(_context10) {
        while (1) switch (_context10.prev = _context10.next) {
          case 0:
            _context10.prev = 0;
            /**
             * Get Headers ContentType From List send integration API
             * */
            resultsIntegration = getPathIntegration;
            paramsForAuth = {
              params: {
                id: resultsIntegration.idEnv
              }
            };
            delete resultsIntegration.originalBodyRequest.resChanges;

            //todo Environment Access
            _context10.next = 6;
            return environmentAccessRepository.getIsLoginEnvAccess({
              userId: getFromReq.environmentAccess.idUser,
              idEnv: resultsIntegration.idEnv
            });
          case 6:
            getEnvLogin = _context10.sent.results;
            if (!(getEnvLogin.variable.length === 0)) {
              _context10.next = 9;
              break;
            }
            return _context10.abrupt("return", {
              resultTest: (0, _responseApi.result)({
                statusCode: _responseApi.statusHttpCode.notFound404,
                message: "SEND_API: Variable not set"
              }),
              resultCall: (0, _responseApi.result)({
                statusCode: _responseApi.statusHttpCode.notFound404,
                message: "SEND_API: Variable not set"
              })
            });
          case 9:
            /** SET UP ENV BY QUERY */
            // let envByParams = null
            envByParams = getEnvLogin.variable.findIndex(function (el) {
              return el.varName.toLowerCase() === getFromReq.env && el.status === 1;
            });
            if (!(envByParams < 0)) {
              _context10.next = 12;
              break;
            }
            return _context10.abrupt("return", {
              resultTest: (0, _responseApi.result)({
                statusCode: _responseApi.statusHttpCode.badRequest400,
                message: "Variable " + "'".concat(getFromReq.env.toUpperCase(), "'") + " is not found / IsNotActive"
              }),
              resultCall: (0, _responseApi.result)({
                statusCode: _responseApi.statusHttpCode.badRequest400,
                message: "Variable " + "'".concat(getFromReq.env.toUpperCase(), "'") + " is not found / IsNotActive"
              })
            });
          case 12:
            paramsForAuth = _objectSpread(_objectSpread({}, paramsForAuth), {}, _defineProperty({}, "query", {
              env: getEnvLogin.variable[envByParams].varName,
              idUser: getFromReq.environmentAccess.idUser
            }));
            variable = getEnvLogin.variable, header = getEnvLogin.header;
            getVariable = variable[envByParams];
            /**
             * GET Api Resource SEND
             * */
            headers = {};
            option = {};
            urlApi = "";
            paramsQuery = null;
            params = resultsIntegration.params, bodyRequest = resultsIntegration.bodyRequest, mappingTemplate = resultsIntegration.mappingTemplate, request = resultsIntegration.request, method = resultsIntegration.method;
            data = null;
            /**
             * Get Header Form ENV
             * */
            getTokenVariable = {};
            if (getVariable.header.length > 0) {
              getVariable.header.map(function (headersEnv) {
                getTokenVariable = getVariable.code.find(function (el) {
                  return el.key === headersEnv.value;
                });
                if (getTokenVariable) {
                  headers["".concat(headersEnv.key)] = getTokenVariable.value;
                }
              });
            }

            /**
             * Get Header Form Resource SendAPI
             * */
            request.header.map(function (elHeader) {
              headers["".concat(elHeader.key)] = elHeader.value;
            });
            hostUrl = getVariable.code.find(function (el) {
              return el.key === request.url.host.replace(/#/g, "");
            }); // is host url not found please check your env host
            if (hostUrl) {
              _context10.next = 27;
              break;
            }
            return _context10.abrupt("return", {
              resultTest: (0, _responseApi.result)({
                statusCode: _responseApi.statusHttpCode.notFound404,
                message: "Host url not found please check your env host",
                data: request.url
              }),
              resultCall: (0, _responseApi.result)({
                statusCode: _responseApi.statusHttpCode.notFound404,
                message: "Host url not found please check your env host",
                data: request.url
              })
            });
          case 27:
            /**
             * SET URL API
             * */
            urlApi = hostUrl.value + getFromReq.path;
            /**
             * SET Params Query
             * */
            if (params.query.length > 0) {
              paramsQuery = {};
              params.query.map(function (elParamsQuery) {
                paramsQuery["".concat(elParamsQuery.key)] = elParamsQuery.value;
              });
            }

            /**
             * SET BODY PARAMETER
             * */
            if (Object.keys(mappingTemplate).length !== 0) {
              data = mappingTemplate;
            } else if (bodyRequest.mode === "raw" && Object.keys(mappingTemplate).length === 0 && Object.keys(bodyRequest.rawJson).length !== 0) {
              data = bodyRequest.rawJson;
            } else if (bodyRequest.mode === "urlencoded" && Object.keys(mappingTemplate).length === 0 && Object.keys(bodyRequest.formUrlEncoded).length !== 0) {
              data = bodyRequest.formUrlEncoded;
            }

            /**
             * SET OPTION AXIOS
             * */
            option["method"] = method;
            option["url"] = urlApi;
            option["headers"] = headers;
            option["params"] = paramsQuery;
            option["timeout"] = 5000; //ms
            option["variable"] = getVariable.varName;
            option["httpsAgent"] = new https.Agent({
              rejectUnauthorized: false
            });
            if (data) {
              option["data"] = data;
            }
            newOption = {};
            /** Show Header BY ENV PROCESS NODE_ENV : development */
            checkTestRequest = Object.keys(getFromReq.query).length > 0 ? Object.keys(getFromReq.query).filter(function (el) {
              return el === "test_request";
            }) : [];
            /** Test Request Return */
            if (!(checkTestRequest.length > 0 && getFromReq.query[checkTestRequest] === "true")) {
              _context10.next = 45;
              break;
            }
            getEnvProcess = process.env.NODE_ENV;
            Object.keys(option).map(function (el) {
              if (getEnvProcess === "development") {
                return newOption = _objectSpread(_objectSpread({}, newOption), {}, _defineProperty({}, el, option[el]));
              } else if (el !== "headers") {
                return newOption = _objectSpread(_objectSpread({}, newOption), {}, _defineProperty({}, el, option[el]));
              }
            });
            if (bodyRequest.mode.toLowerCase() !== "none" && getListMappingClientRsc.length > 0 || Object.keys(mappingTemplate).length !== 0) {
              newOption["originalRequest"] = resultsIntegration.originalBodyRequest;
            }
            return _context10.abrupt("return", {
              resultTest: newOption,
              resultCall: (0, _responseApi.result)({
                isSuccess: false,
                message: "Test Request Success",
                data: newOption
              })
            });
          case 45:
            if (!(getTokenVariable && getVariable.hasOwnProperty("useLogin") && getVariable.useLogin && getTokenVariable.value === "null")) {
              _context10.next = 59;
              break;
            }
            _context10.next = 48;
            return callTheAuthAccess(paramsForAuth);
          case 48:
            getCallAuth = _context10.sent;
            if (!(getCallAuth.resultCall.code >= 400)) {
              _context10.next = 55;
              break;
            }
            getEnvDetailError = {
              varName: getEnvLogin.variable[envByParams].varName,
              code: getEnvLogin.variable[envByParams].code,
              useLogin: getEnvLogin.variable[envByParams].useLogin
            };
            getCallAuth.resultCall.results = getEnvDetailError;
            return _context10.abrupt("return", {
              resultTest: getCallAuth.resultCall,
              resultCall: getCallAuth.resultCall
            });
          case 55:
            _context10.next = 57;
            return _mappingParameterSendAccess(resultsIntegration, getFromReq, getListMappingClientRsc);
          case 57:
            _resultCall = _context10.sent;
            return _context10.abrupt("return", {
              resultTest: _resultCall.resultTest,
              resultCall: _resultCall.resultCall
            });
          case 59:
            _context10.next = 61;
            return callAxiosIntegration(option);
          case 61:
            resultCall = _context10.sent;
            //todo path is login run this `DON'T Recursively Authentication`
            codeResult = null;
            if (!(Object.keys(getVariable.authentication).length > 0)) {
              _context10.next = 79;
              break;
            }
            if (!(getPathIntegration.path === getVariable.authentication.path)) {
              _context10.next = 79;
              break;
            }
            _context10.t0 = resultCall.code;
            _context10.next = _context10.t0 === _responseApi.statusHttpCode.badRequest400.code ? 68 : _context10.t0 === _responseApi.statusHttpCode.unauthorized401.code ? 70 : _context10.t0 === _responseApi.statusHttpCode.notFound404.code ? 72 : _context10.t0 === _responseApi.statusHttpCode.methodNotAllowed405.code ? 74 : 76;
            break;
          case 68:
            codeResult = _responseApi.statusHttpCode.badRequest400;
            return _context10.abrupt("break", 78);
          case 70:
            codeResult = _responseApi.statusHttpCode.unauthorized401;
            return _context10.abrupt("break", 78);
          case 72:
            codeResult = _responseApi.statusHttpCode.notFound404;
            return _context10.abrupt("break", 78);
          case 74:
            codeResult = _responseApi.statusHttpCode.methodNotAllowed405;
            return _context10.abrupt("break", 78);
          case 76:
            codeResult = _responseApi.statusHttpCode.ok200;
            return _context10.abrupt("break", 78);
          case 78:
            return _context10.abrupt("return", {
              resultTest: newOption,
              resultCall: (0, _responseApi.result)({
                message: resultCall.message,
                statusCode: codeResult,
                data: resultCall.results
              })
            });
          case 79:
            _context10.t1 = resultCall.code;
            _context10.next = _context10.t1 === _responseApi.statusHttpCode.badRequest400.code ? 82 : _context10.t1 === _responseApi.statusHttpCode.unauthorized401.code ? 84 : _context10.t1 === _responseApi.statusHttpCode.notFound404.code ? 86 : _context10.t1 === _responseApi.statusHttpCode.methodNotAllowed405.code ? 88 : 90;
            break;
          case 82:
            codeResult = _responseApi.statusHttpCode.badRequest400;
            return _context10.abrupt("break", 92);
          case 84:
            codeResult = _responseApi.statusHttpCode.unauthorized401;
            return _context10.abrupt("break", 92);
          case 86:
            codeResult = _responseApi.statusHttpCode.notFound404;
            return _context10.abrupt("break", 92);
          case 88:
            codeResult = _responseApi.statusHttpCode.methodNotAllowed405;
            return _context10.abrupt("break", 92);
          case 90:
            codeResult = _responseApi.statusHttpCode.ok200;
            return _context10.abrupt("break", 92);
          case 92:
            if (!(resultCall.code === _responseApi.statusHttpCode.unauthorized401.code && getVariable.hasOwnProperty("useLogin") && getVariable.useLogin)) {
              _context10.next = 116;
              break;
            }
            _context10.prev = 93;
            _context10.next = 96;
            return callTheAuthAccess(paramsForAuth);
          case 96:
            _getCallAuth = _context10.sent;
            if (!(_getCallAuth.resultCall.code >= 400)) {
              _context10.next = 101;
              break;
            }
            return _context10.abrupt("return", _getCallAuth);
          case 101:
            // to stop recursive if error
            countMappingParameterSendAccess = countMappingParameterSendAccess + 1;
            if (!(countMappingParameterSendAccess == 3)) {
              _context10.next = 105;
              break;
            }
            countMappingParameterSendAccess = 0;
            return _context10.abrupt("return", {
              resultTest: newOption,
              resultCall: (0, _responseApi.result)({
                isSuccess: true,
                message: resultCall.message,
                statusCode: codeResult,
                data: resultCall.results
              })
            });
          case 105:
            _context10.next = 107;
            return _mappingParameterSendAccess(getPathIntegration, getFromReq, getListMappingClientRsc);
          case 107:
            resultCall = _context10.sent;
            return _context10.abrupt("return", resultCall);
          case 109:
            _context10.next = 114;
            break;
          case 111:
            _context10.prev = 111;
            _context10.t2 = _context10["catch"](93);
            throw _context10.t2;
          case 114:
            _context10.next = 117;
            break;
          case 116:
            return _context10.abrupt("return", {
              resultTest: newOption,
              resultCall: resultCall
            });
          case 117:
            _context10.next = 122;
            break;
          case 119:
            _context10.prev = 119;
            _context10.t3 = _context10["catch"](0);
            throw _context10.t3;
          case 122:
          case "end":
            return _context10.stop();
        }
      }, _callee4, null, [[0, 119], [93, 111]]);
    }));
    return function mappingParameterSendAccess(_x4, _x5, _x6) {
      return _ref4.apply(this, arguments);
    };
  }();

  /**
   * Use for mapping from request to send
   * */
  var _mappingParameterSend = /*#__PURE__*/function () {
    var _ref5 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee5(getPathIntegration, getFromReq, getListMappingClientRsc) {
      var resultsIntegration, paramsForAuth, getEnvLogin, envByParams, _getEnvLogin$results, variable, header, getVariable, headers, option, urlApi, paramsQuery, params, bodyRequest, mappingTemplate, request, method, data, getTokenVariable, hostUrl, newOption, checkTestRequest, getEnvProcess, getCallAuth, _resultCall2, resultCall, codeResult, _getCallAuth2;
      return _regeneratorRuntime().wrap(function _callee5$(_context11) {
        while (1) switch (_context11.prev = _context11.next) {
          case 0:
            _context11.prev = 0;
            /**
             * Get Headers ContentType From List send integration API
             * */
            resultsIntegration = getPathIntegration;
            paramsForAuth = {
              params: {
                id: resultsIntegration.idEnv
              }
            };
            delete resultsIntegration.originalBodyRequest.resChanges;
            _context11.next = 6;
            return envRepository.getIsLoginEnv(paramsForAuth);
          case 6:
            getEnvLogin = _context11.sent;
            if (!(getEnvLogin.results.variable.length === 0)) {
              _context11.next = 9;
              break;
            }
            return _context11.abrupt("return", {
              resultTest: (0, _responseApi.result)({
                statusCode: _responseApi.statusHttpCode.notFound404,
                message: "SEND_API: Variable not set"
              }),
              resultCall: (0, _responseApi.result)({
                statusCode: _responseApi.statusHttpCode.notFound404,
                message: "SEND_API: Variable not set"
              })
            });
          case 9:
            /** SET UP ENV BY QUERY */
            // let envByParams = null
            envByParams = getEnvLogin.results.variable.findIndex(function (el) {
              return el.varName.toLowerCase() === getFromReq.env && el.status === 1;
            });
            if (!(envByParams < 0)) {
              _context11.next = 12;
              break;
            }
            return _context11.abrupt("return", {
              resultTest: (0, _responseApi.result)({
                statusCode: _responseApi.statusHttpCode.notFound404,
                message: "Variable " + "'".concat(getFromReq.env.toUpperCase(), "'") + " is not found"
              }),
              resultCall: (0, _responseApi.result)({
                statusCode: _responseApi.statusHttpCode.notFound404,
                message: "Variable " + "'".concat(getFromReq.env.toUpperCase(), "'") + " is not found"
              })
            });
          case 12:
            paramsForAuth = _objectSpread(_objectSpread({}, paramsForAuth), {}, _defineProperty({}, "query", {
              env: getEnvLogin.results.variable[envByParams].varName
            }));
            _getEnvLogin$results = getEnvLogin.results, variable = _getEnvLogin$results.variable, header = _getEnvLogin$results.header;
            getVariable = variable[envByParams];
            /**
             * GET Api Resource SEND
             * */
            headers = {};
            option = {};
            urlApi = "";
            paramsQuery = null;
            params = resultsIntegration.params, bodyRequest = resultsIntegration.bodyRequest, mappingTemplate = resultsIntegration.mappingTemplate, request = resultsIntegration.request, method = resultsIntegration.method;
            data = null;
            /**
             * Get Header Form ENV
             * */
            getTokenVariable = {};
            if (getVariable.header.length > 0) {
              getVariable.header.map(function (headersEnv) {
                getTokenVariable = getVariable.code.find(function (el) {
                  return el.key === headersEnv.value;
                });
                if (getTokenVariable) {
                  headers["".concat(headersEnv.key)] = getTokenVariable.value;
                }
              });
            }

            /**
             * Get Header Form Resource SendAPI
             * */
            request.header.map(function (elHeader) {
              headers["".concat(elHeader.key)] = elHeader.value;
            });
            hostUrl = getVariable.code.find(function (el) {
              return el.key === request.url.host.replace(/#/g, "");
            }); // is host url not found please check your env host
            if (hostUrl) {
              _context11.next = 27;
              break;
            }
            return _context11.abrupt("return", {
              resultTest: (0, _responseApi.result)({
                statusCode: _responseApi.statusHttpCode.notFound404,
                message: "Host url not found please check your env host",
                data: request.url
              }),
              resultCall: (0, _responseApi.result)({
                statusCode: _responseApi.statusHttpCode.notFound404,
                message: "Host url not found please check your env host",
                data: request.url
              })
            });
          case 27:
            /**
             * SET URL API
             * */
            urlApi = hostUrl.value + getFromReq.path;
            /**
             * SET Params Query
             * */
            if (params.query.length > 0) {
              paramsQuery = {};
              params.query.map(function (elParamsQuery) {
                paramsQuery["".concat(elParamsQuery.key)] = elParamsQuery.value;
              });
            }

            /**
             * SET BODY PARAMETER
             * */
            if (Object.keys(mappingTemplate).length !== 0) {
              data = mappingTemplate;
            } else if (bodyRequest.mode === "raw" && Object.keys(mappingTemplate).length === 0 && Object.keys(bodyRequest.rawJson).length !== 0) {
              data = bodyRequest.rawJson;
            } else if (bodyRequest.mode === "urlencoded" && Object.keys(mappingTemplate).length === 0 && Object.keys(bodyRequest.formUrlEncoded).length !== 0) {
              data = bodyRequest.formUrlEncoded;
            }

            /**
             * SET OPTION AXIOS
             * */
            option["method"] = method;
            option["url"] = urlApi;
            option["headers"] = headers;
            option["params"] = paramsQuery;
            option["timeout"] = 15000; //ms
            option["variable"] = getVariable.varName;
            option["httpsAgent"] = new https.Agent({
              rejectUnauthorized: false
            });
            if (data) {
              option["data"] = data;
            }
            newOption = {};
            /** Show Header BY ENV PROCESS NODE_ENV : development */
            checkTestRequest = Object.keys(getFromReq.query).length > 0 ? Object.keys(getFromReq.query).filter(function (el) {
              return el === "test_request";
            }) : [];
            /** Test Request Return */
            if (!(checkTestRequest.length > 0 && getFromReq.query[checkTestRequest] === "true")) {
              _context11.next = 45;
              break;
            }
            getEnvProcess = process.env.NODE_ENV;
            Object.keys(option).map(function (el) {
              if (getEnvProcess === "development") {
                return newOption = _objectSpread(_objectSpread({}, newOption), {}, _defineProperty({}, el, option[el]));
              } else if (el !== "headers") {
                return newOption = _objectSpread(_objectSpread({}, newOption), {}, _defineProperty({}, el, option[el]));
              }
            });
            if (bodyRequest.mode.toLowerCase() !== "none" && getListMappingClientRsc.length > 0 || Object.keys(mappingTemplate).length !== 0) {
              newOption["originalRequest"] = resultsIntegration.originalBodyRequest;
            }
            return _context11.abrupt("return", {
              resultTest: newOption,
              resultCall: (0, _responseApi.result)({
                message: "Test Request Success",
                data: newOption
              })
            });
          case 45:
            if (!(getTokenVariable && getVariable.hasOwnProperty("useLogin") && getVariable.useLogin && getTokenVariable.value === "null")) {
              _context11.next = 57;
              break;
            }
            _context11.next = 48;
            return callTheAuth(resultsIntegration, paramsForAuth, newOption);
          case 48:
            getCallAuth = _context11.sent;
            if (!(getCallAuth.resultCall.code !== _responseApi.statusHttpCode.ok200.code)) {
              _context11.next = 53;
              break;
            }
            return _context11.abrupt("return", {
              resultTest: getCallAuth.resultCall,
              resultCall: getCallAuth.resultCall
            });
          case 53:
            _context11.next = 55;
            return _mappingParameterSend(resultsIntegration, getFromReq, getListMappingClientRsc);
          case 55:
            _resultCall2 = _context11.sent;
            return _context11.abrupt("return", {
              resultTest: _resultCall2.resultTest,
              resultCall: _resultCall2.resultCall
            });
          case 57:
            _context11.next = 59;
            return callAxiosIntegration(option);
          case 59:
            resultCall = _context11.sent;
            if (!(Object.keys(getVariable.authentication).length > 0)) {
              _context11.next = 77;
              break;
            }
            if (!(getPathIntegration.path === getVariable.authentication.path)) {
              _context11.next = 77;
              break;
            }
            codeResult = null;
            _context11.t0 = resultCall.code;
            _context11.next = _context11.t0 === _responseApi.statusHttpCode.badRequest400.code ? 66 : _context11.t0 === _responseApi.statusHttpCode.unauthorized401.code ? 68 : _context11.t0 === _responseApi.statusHttpCode.notFound404.code ? 70 : _context11.t0 === _responseApi.statusHttpCode.methodNotAllowed405.code ? 72 : 74;
            break;
          case 66:
            codeResult = _responseApi.statusHttpCode.badRequest400;
            return _context11.abrupt("break", 76);
          case 68:
            codeResult = _responseApi.statusHttpCode.unauthorized401;
            return _context11.abrupt("break", 76);
          case 70:
            codeResult = _responseApi.statusHttpCode.notFound404;
            return _context11.abrupt("break", 76);
          case 72:
            codeResult = _responseApi.statusHttpCode.methodNotAllowed405;
            return _context11.abrupt("break", 76);
          case 74:
            codeResult = _responseApi.statusHttpCode.ok200;
            return _context11.abrupt("break", 76);
          case 76:
            return _context11.abrupt("return", {
              resultTest: newOption,
              resultCall: (0, _responseApi.result)({
                isSuccess: true,
                message: resultCall.message,
                statusCode: codeResult,
                data: resultCall.results
              })
            });
          case 77:
            if (!(resultCall.code === _responseApi.statusHttpCode.unauthorized401.code && getVariable.hasOwnProperty("useLogin") && getVariable.useLogin)) {
              _context11.next = 91;
              break;
            }
            _context11.next = 80;
            return callTheAuth(getPathIntegration, paramsForAuth, newOption);
          case 80:
            _getCallAuth2 = _context11.sent;
            if (!(_getCallAuth2.resultCall.code !== _responseApi.statusHttpCode.ok200.code)) {
              _context11.next = 85;
              break;
            }
            return _context11.abrupt("return", _getCallAuth2);
          case 85:
            _context11.next = 87;
            return _mappingParameterSend(getPathIntegration, getFromReq, getListMappingClientRsc);
          case 87:
            resultCall = _context11.sent;
            return _context11.abrupt("return", resultCall);
          case 89:
            _context11.next = 92;
            break;
          case 91:
            return _context11.abrupt("return", {
              resultTest: newOption,
              resultCall: resultCall
            });
          case 92:
            _context11.next = 98;
            break;
          case 94:
            _context11.prev = 94;
            _context11.t1 = _context11["catch"](0);
            _context11.t1.message = _context11.t1;
            throw _context11.t1;
          case 98:
          case "end":
            return _context11.stop();
        }
      }, _callee5, null, [[0, 94]]);
    }));
    return function mappingParameterSend(_x7, _x8, _x9) {
      return _ref5.apply(this, arguments);
    };
  }();

  /**
   * CALL AXIOS INTEGRATION
   * */

  var callAxiosIntegration = /*#__PURE__*/function () {
    var _ref6 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee8(options) {
      var method, url, headers, params, timeout, _options$httpsAgent, httpsAgent, _options$body, body, urlWithParams, isHttps, agent, fetchOptions, resultSend;
      return _regeneratorRuntime().wrap(function _callee8$(_context14) {
        while (1) switch (_context14.prev = _context14.next) {
          case 0:
            method = options.method, url = options.url, headers = options.headers, params = options.params, timeout = options.timeout, _options$httpsAgent = options.httpsAgent, httpsAgent = _options$httpsAgent === void 0 ? new https.Agent({
              rejectUnauthorized: false
            }) : _options$httpsAgent, _options$body = options.body, body = _options$body === void 0 ? options.data : _options$body; //new fetch area
            // Construct URL with query parameters
            urlWithParams = new URL(url);
            if (params && _typeof(params) === "object") {
              Object.keys(params).forEach(function (key) {
                return urlWithParams.searchParams.append(key, params[key]);
              });
            }

            // Determine the protocol and set the appropriate agent
            isHttps = urlWithParams.protocol === "https:";
            agent = isHttps ? new https.Agent({
              rejectUnauthorized: false
            }) : new http.Agent(); // Set up the fetch options
            fetchOptions = _objectSpread({
              method: method,
              headers: headers,
              agent: agent,
              timeout: timeout
            }, body && method !== "GET" && {
              body: JSON.stringify(body)
            }); // Fetch with timeout handling
            _context14.next = 8;
            return fetchWithTimeout(urlWithParams, fetchOptions, timeout).then(/*#__PURE__*/function () {
              var _ref7 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee6(response) {
                var resultData, statusCode, key, status, resData;
                return _regeneratorRuntime().wrap(function _callee6$(_context12) {
                  while (1) switch (_context12.prev = _context12.next) {
                    case 0:
                      resultData = {}; // Iterate over keys in statusHttpCode object
                      statusCode = null;
                      _context12.t0 = _regeneratorRuntime().keys(_responseApi.statusHttpCode);
                    case 3:
                      if ((_context12.t1 = _context12.t0()).done) {
                        _context12.next = 12;
                        break;
                      }
                      key = _context12.t1.value;
                      if (!_responseApi.statusHttpCode.hasOwnProperty(key)) {
                        _context12.next = 10;
                        break;
                      }
                      status = _responseApi.statusHttpCode[key];
                      if (!(response.status === status.code)) {
                        _context12.next = 10;
                        break;
                      }
                      statusCode = status;
                      return _context12.abrupt("break", 12);
                    case 10:
                      _context12.next = 3;
                      break;
                    case 12:
                      // Now statusCode holds the matched status object
                      if (statusCode) {
                        resultData["statusCode"] = statusCode;
                      } else {
                        // Handle unknown status code
                        resultData["statusCode"] = {
                          code: response.status,
                          message: "Unknown Status"
                        };
                      }
                      _context12.t2 = _responseApi.result;
                      _context12.t3 = response.statusText;
                      _context12.next = 17;
                      return response.json();
                    case 17:
                      _context12.t4 = _context12.sent;
                      _context12.t5 = eval(resultData.statusCode);
                      _context12.t6 = {
                        isSuccess: true,
                        message: _context12.t3,
                        data: _context12.t4,
                        statusCode: _context12.t5
                      };
                      resData = (0, _context12.t2)(_context12.t6);
                      return _context12.abrupt("return", resData);
                    case 22:
                    case "end":
                      return _context12.stop();
                  }
                }, _callee6);
              }));
              return function (_x11) {
                return _ref7.apply(this, arguments);
              };
            }()).then(/*#__PURE__*/function () {
              var _ref8 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee7(data) {
                return _regeneratorRuntime().wrap(function _callee7$(_context13) {
                  while (1) switch (_context13.prev = _context13.next) {
                    case 0:
                      return _context13.abrupt("return", data);
                    case 1:
                    case "end":
                      return _context13.stop();
                  }
                }, _callee7);
              }));
              return function (_x12) {
                return _ref8.apply(this, arguments);
              };
            }())["catch"](function (error) {
              var changesOption = {
                method: options.method,
                url: options.url,
                headers: {
                  "Content-Type": options.headers["Content-Type"]
                },
                params: options.params,
                variable: options.variable
              };
              if (error.code && error.code.toString() == "ECONNREFUSED") {
                return (0, _responseApi.result)({
                  message: error.message,
                  statusCode: _responseApi.statusHttpCode.badGateway502,
                  data: changesOption
                });
              }
              return (0, _responseApi.result)({
                message: error.message,
                statusCode: _responseApi.statusHttpCode.badRequest400,
                data: changesOption
              });
            });
          case 8:
            resultSend = _context14.sent;
            return _context14.abrupt("return", resultSend);
          case 10:
          case "end":
            return _context14.stop();
        }
      }, _callee8);
    }));
    return function callAxiosIntegration(_x10) {
      return _ref6.apply(this, arguments);
    };
  }();
  function fetchWithTimeout(url, options) {
    var timeout = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 1000;
    return Promise.race([(0, _nodeFetch["default"])(url, options), new Promise(function (_, reject) {
      return setTimeout(function () {
        return reject(new Error("Timeout"));
      }, timeout);
    })]);
  }
  var callAxiosIntegration_new = /*#__PURE__*/function () {
    var _ref9 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee11(options) {
      var method, url, headers, params, timeout, _options$httpsAgent2, httpsAgent, _options$body2, body, resultData, urlWithParams, isHttps, agent, fetchOptions;
      return _regeneratorRuntime().wrap(function _callee11$(_context17) {
        while (1) switch (_context17.prev = _context17.next) {
          case 0:
            _context17.prev = 0;
            method = options.method, url = options.url, headers = options.headers, params = options.params, timeout = options.timeout, _options$httpsAgent2 = options.httpsAgent, httpsAgent = _options$httpsAgent2 === void 0 ? new https.Agent({
              rejectUnauthorized: false
            }) : _options$httpsAgent2, _options$body2 = options.body, body = _options$body2 === void 0 ? options.data : _options$body2;
            resultData = {}; //new fetch area
            // Construct URL with query parameters
            urlWithParams = new URL(url);
            if (params && _typeof(params) === "object") {
              Object.keys(params).forEach(function (key) {
                return urlWithParams.searchParams.append(key, params[key]);
              });
            }

            // Determine the protocol and set the appropriate agent
            isHttps = urlWithParams.protocol === "https:";
            agent = isHttps ? new https.Agent({
              rejectUnauthorized: false
            }) : new http.Agent(); // Set up the fetch options
            fetchOptions = _objectSpread({
              method: method,
              headers: headers,
              agent: agent,
              timeout: timeout
            }, body && method !== "GET" && {
              body: JSON.stringify(body)
            }); // Fetch with timeout handling
            return _context17.abrupt("return", new Promise(function (resolve, reject) {
              var fetchTimeout = setTimeout(function () {
                // throw new Error("Timeout");
                reject(new Error("Request timed out"));
                // return result({
                //     message: "Request timed out or was aborted.",
                //     statusCode: statusHttpCode.badRequest400,
                //     data: options,
                // });
              }, timeout);
              (0, _nodeFetch["default"])(urlWithParams, fetchOptions).then(/*#__PURE__*/function () {
                var _ref10 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee9(response) {
                  var resultData;
                  return _regeneratorRuntime().wrap(function _callee9$(_context15) {
                    while (1) switch (_context15.prev = _context15.next) {
                      case 0:
                        clearTimeout(fetchTimeout);
                        resultData = {};
                        _context15.t0 = response.status;
                        _context15.next = _context15.t0 === _responseApi.statusHttpCode.ok200.code ? 5 : _context15.t0 === _responseApi.statusHttpCode.badRequest400.code ? 7 : _context15.t0 === _responseApi.statusHttpCode.unauthorized401.code ? 9 : _context15.t0 === _responseApi.statusHttpCode.notFound404.code ? 11 : _context15.t0 === _responseApi.statusHttpCode.methodNotAllowed405.code ? 13 : _context15.t0 === _responseApi.statusHttpCode.internalServerError500.code ? 15 : 17;
                        break;
                      case 5:
                        resultData["statusCode"] = _responseApi.statusHttpCode.ok200;
                        return _context15.abrupt("break", 17);
                      case 7:
                        resultData["statusCode"] = _responseApi.statusHttpCode.badRequest400;
                        return _context15.abrupt("break", 17);
                      case 9:
                        resultData["statusCode"] = _responseApi.statusHttpCode.unauthorized401;
                        return _context15.abrupt("break", 17);
                      case 11:
                        resultData["statusCode"] = _responseApi.statusHttpCode.notFound404;
                        return _context15.abrupt("break", 17);
                      case 13:
                        resultData["statusCode"] = _responseApi.statusHttpCode.methodNotAllowed405;
                        return _context15.abrupt("break", 17);
                      case 15:
                        resultData["statusCode"] = _responseApi.statusHttpCode.internalServerError500;
                        return _context15.abrupt("break", 17);
                      case 17:
                        _context15.t1 = _responseApi.result;
                        _context15.t2 = response.statusText;
                        _context15.next = 21;
                        return response.json();
                      case 21:
                        _context15.t3 = _context15.sent;
                        _context15.t4 = eval(resultData.statusCode);
                        _context15.t5 = {
                          message: _context15.t2,
                          data: _context15.t3,
                          statusCode: _context15.t4
                        };
                        return _context15.abrupt("return", (0, _context15.t1)(_context15.t5));
                      case 25:
                      case "end":
                        return _context15.stop();
                    }
                  }, _callee9);
                }));
                return function (_x14) {
                  return _ref10.apply(this, arguments);
                };
              }()).then(/*#__PURE__*/function () {
                var _ref11 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee10(data) {
                  var getErrorFromResponseSuccess, resData;
                  return _regeneratorRuntime().wrap(function _callee10$(_context16) {
                    while (1) switch (_context16.prev = _context16.next) {
                      case 0:
                        _context16.next = 2;
                        return helpers.findNestedErrorHttp(data);
                      case 2:
                        getErrorFromResponseSuccess = _context16.sent;
                        if (!(Object.keys(getErrorFromResponseSuccess).length > 0)) {
                          _context16.next = 8;
                          break;
                        }
                        if (getErrorFromResponseSuccess.obj.hasOwnProperty("results")) {
                          getErrorFromResponseSuccess.result = getErrorFromResponseSuccess.obj.results;
                        }
                        data = (0, _responseApi.result)({
                          message: getErrorFromResponseSuccess.message,
                          statusCode: (0, _responseApi.getStatusHttpByCode)(parseInt(getErrorFromResponseSuccess.code)),
                          data: getErrorFromResponseSuccess.result
                        });
                        resolve(data);
                        return _context16.abrupt("return");
                      case 8:
                        if (data.hasOwnProperty("results")) {
                          data.result = data.results;
                        }
                        resData = (0, _responseApi.result)({
                          data: data.result
                        });
                        resolve(resData);
                      case 11:
                      case "end":
                        return _context16.stop();
                    }
                  }, _callee10);
                }));
                return function (_x15) {
                  return _ref11.apply(this, arguments);
                };
              }())["catch"](function (error) {
                clearTimeout(fetchTimeout);
                return (0, _responseApi.result)({
                  message: "Cant connect client server :" + urlWithParams.host,
                  statusCode: _responseApi.statusHttpCode.badRequest400
                });
              });
            }));
          case 11:
            _context17.prev = 11;
            _context17.t0 = _context17["catch"](0);
            e.message = e;
            throw e;
          case 15:
          case "end":
            return _context17.stop();
        }
      }, _callee11, null, [[0, 11]]);
    }));
    return function callAxiosIntegration_new(_x13) {
      return _ref9.apply(this, arguments);
    };
  }();
  var triggerResponse = /*#__PURE__*/function () {
    var _ref12 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee12(target, source) {
      var result, idxSrc;
      return _regeneratorRuntime().wrap(function _callee12$(_context18) {
        while (1) switch (_context18.prev = _context18.next) {
          case 0:
            result = {};
            if (Array.isArray(source)) {
              result = [];
              for (idxSrc in source) {
                result.push((0, _helpers_maprequest_body.changesValueByPropertyKey)(target, source[idxSrc]));
              }
            } else {
              result = (0, _helpers_maprequest_body.changesValueByPropertyKey)(target, source);
            }
            return _context18.abrupt("return", result);
          case 3:
          case "end":
            return _context18.stop();
        }
      }, _callee12);
    }));
    return function triggerResponse(_x16, _x17) {
      return _ref12.apply(this, arguments);
    };
  }();
  var triggerResponseMapClientResource = /*#__PURE__*/function () {
    var _ref13 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee13(target, source) {
      var result, dtSource, i;
      return _regeneratorRuntime().wrap(function _callee13$(_context19) {
        while (1) switch (_context19.prev = _context19.next) {
          case 0:
            result = {};
            if (!(source.length === 0)) {
              _context19.next = 3;
              break;
            }
            return _context19.abrupt("return", target);
          case 3:
            dtSource = source.map(function (el) {
              el.clientResourceExtend.value = JSON.parse(el.clientResourceExtend.value);
              return el;
            });
            if (!Array.isArray(target)) {
              _context19.next = 18;
              break;
            }
            result = [];
            _context19.t0 = _regeneratorRuntime().keys(target);
          case 7:
            if ((_context19.t1 = _context19.t0()).done) {
              _context19.next = 16;
              break;
            }
            i = _context19.t1.value;
            _context19.t2 = result;
            _context19.next = 12;
            return helpers.getObjectWithChanges(target[i], dtSource);
          case 12:
            _context19.t3 = _context19.sent;
            _context19.t2.push.call(_context19.t2, _context19.t3);
            _context19.next = 7;
            break;
          case 16:
            _context19.next = 21;
            break;
          case 18:
            _context19.next = 20;
            return helpers.getObjectWithChanges(target, dtSource);
          case 20:
            result = _context19.sent;
          case 21:
            return _context19.abrupt("return", result);
          case 22:
          case "end":
            return _context19.stop();
        }
      }, _callee13);
    }));
    return function triggerResponseMapClientResource(_x18, _x19) {
      return _ref13.apply(this, arguments);
    };
  }();
  var delay = function delay(ms) {
    return new Promise(function (res) {
      return setTimeout(res, ms);
    });
  };
  /** Call Auth */
  var countErr = 0;

  // todo callTheAuth Deprecated please use >> callTheAuthAccess
  function callTheAuth(_x20, _x21, _x22) {
    return _callTheAuth.apply(this, arguments);
  }
  function _callTheAuth() {
    _callTheAuth = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee15(req, paramsForAuth, newOption) {
      var resultCall, authCall;
      return _regeneratorRuntime().wrap(function _callee15$(_context21) {
        while (1) switch (_context21.prev = _context21.next) {
          case 0:
            resultCall = {}; //scope callApiRepository please use endpoint /api/call/auth for testing
            _context21.next = 3;
            return callApiRepository.callAuth(paramsForAuth);
          case 3:
            authCall = _context21.sent;
            if (!(authCall.code !== 200)) {
              _context21.next = 15;
              break;
            }
            if (!(countErr === 3)) {
              _context21.next = 10;
              break;
            }
            // reset count
            countErr = 0;
            return _context21.abrupt("return", {
              resultTest: paramsForAuth,
              resultCall: resultCall
            });
          case 10:
            _context21.next = 12;
            return delay(1000);
          case 12:
            countErr = countErr + 1;
            _context21.next = 15;
            return callTheAuth(req, paramsForAuth, newOption);
          case 15:
            return _context21.abrupt("return", {
              resultTest: paramsForAuth,
              resultCall: authCall
            });
          case 16:
          case "end":
            return _context21.stop();
        }
      }, _callee15);
    }));
    return _callTheAuth.apply(this, arguments);
  }
  function callTheAuthAccess(_x23) {
    return _callTheAuthAccess.apply(this, arguments);
  }
  function _callTheAuthAccess() {
    _callTheAuthAccess = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee16(paramsForAuth) {
      var resultCall, authCall;
      return _regeneratorRuntime().wrap(function _callee16$(_context22) {
        while (1) switch (_context22.prev = _context22.next) {
          case 0:
            resultCall = {}; //scope callApiRepository please use endpoint /api/call/access/auth for testing
            _context22.prev = 1;
            _context22.next = 4;
            return callApiRepository.callAuthAccess(paramsForAuth);
          case 4:
            authCall = _context22.sent;
            if (!(authCall.code >= 400)) {
              _context22.next = 16;
              break;
            }
            if (!(countErr === 2)) {
              _context22.next = 11;
              break;
            }
            // reset count
            countErr = 0;
            return _context22.abrupt("return", {
              resultTest: paramsForAuth,
              resultCall: resultCall
            });
          case 11:
            _context22.next = 13;
            return delay(500);
          case 13:
            countErr = countErr + 1;
            _context22.next = 16;
            return callTheAuthAccess(paramsForAuth);
          case 16:
            return _context22.abrupt("return", {
              resultTest: paramsForAuth,
              resultCall: authCall
            });
          case 19:
            _context22.prev = 19;
            _context22.t0 = _context22["catch"](1);
            throw _context22.t0;
          case 22:
          case "end":
            return _context22.stop();
        }
      }, _callee16, null, [[1, 19]]);
    }));
    return _callTheAuthAccess.apply(this, arguments);
  }
  var getIntegrationByPath = /*#__PURE__*/function () {
    var _ref15 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee14(_ref14) {
      var _ref14$method, method, _ref14$idEnv, idEnv, _ref14$originalUrl, originalUrl, _ref14$requestId, requestId, getDetail;
      return _regeneratorRuntime().wrap(function _callee14$(_context20) {
        while (1) switch (_context20.prev = _context20.next) {
          case 0:
            _ref14$method = _ref14.method, method = _ref14$method === void 0 ? null : _ref14$method, _ref14$idEnv = _ref14.idEnv, idEnv = _ref14$idEnv === void 0 ? null : _ref14$idEnv, _ref14$originalUrl = _ref14.originalUrl, originalUrl = _ref14$originalUrl === void 0 ? null : _ref14$originalUrl, _ref14$requestId = _ref14.requestId, requestId = _ref14$requestId === void 0 ? null : _ref14$requestId;
            _context20.prev = 1;
            _context20.next = 4;
            return _models.endpointResourceIntegration.findOne({
              where: {
                method: method,
                idEnv: idEnv,
                id: requestId
              },
              include: [{
                model: _models.mapClientResource,
                attributes: ["id", "idClientResourceExtend"],
                include: _models.clientResourceExtends
              }]
            });
          case 4:
            getDetail = _context20.sent;
            if (getDetail) {
              _context20.next = 9;
              break;
            }
            _context20.next = 8;
            return (0, _responseApi.result)({
              message: "Request ".concat(originalUrl, " with Method: ").concat(method, " Not Found"),
              statusCode: _responseApi.statusHttpCode.methodNotAllowed405,
              data: {
                requestUrl: originalUrl,
                Method: method,
                RequestId: requestId
              }
            });
          case 8:
            return _context20.abrupt("return", _context20.sent);
          case 9:
            getDetail.requestExample = getDetail.requestExample ? JSON.parse(getDetail.requestExample) : getDetail.requestExample;
            getDetail.responseExample = getDetail.responseExample ? JSON.parse(getDetail.responseExample) : getDetail.responseExample;
            getDetail.params = getDetail.params ? JSON.parse(getDetail.params) : getDetail.params;
            getDetail.bodyRequest = getDetail.bodyRequest ? JSON.parse(getDetail.bodyRequest) : getDetail.bodyRequest;
            getDetail.mappingTemplate = getDetail.mappingTemplate ? JSON.parse(getDetail.mappingTemplate) : getDetail.mappingTemplate;
            getDetail.request = getDetail.request ? JSON.parse(getDetail.request) : getDetail.request;
            getDetail.responseTrigger = getDetail.responseTrigger ? JSON.parse(getDetail.responseTrigger) : getDetail.responseTrigger;
            _context20.next = 18;
            return (0, _responseApi.result)({
              data: getDetail
            });
          case 18:
            return _context20.abrupt("return", _context20.sent);
          case 21:
            _context20.prev = 21;
            _context20.t0 = _context20["catch"](1);
            _context20.t0.message = "integrationResourceRepository.getIntegrationByPath: " + _context20.t0;
            throw _context20.t0;
          case 25:
          case "end":
            return _context20.stop();
        }
      }, _callee14, null, [[1, 21]]);
    }));
    return function getIntegrationByPath(_x24) {
      return _ref15.apply(this, arguments);
    };
  }();
  return {
    mappingRequestApi: mappingRequestApi,
    getIntegrationByPath: getIntegrationByPath,
    mappingRequestApiFromTransporter: mappingRequestApiFromTransporter,
    mappingRequestApiAccessValidation: mappingRequestApiAccessValidation
  };
}