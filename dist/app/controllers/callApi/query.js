"use strict";

require("core-js/modules/es.object.define-property.js");
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.sql_getEnvironmentAccess = exports.sql_getEndpointByAccess = void 0;
require("core-js/modules/es.array.concat.js");
var sql_getEnvironmentAccess = exports.sql_getEnvironmentAccess = function sql_getEnvironmentAccess(_ref) {
  var _ref$userId = _ref.userId,
    userId = _ref$userId === void 0 ? null : _ref$userId,
    _ref$envName = _ref.envName,
    envName = _ref$envName === void 0 ? null : _ref$envName,
    _ref$idEnv = _ref.idEnv,
    idEnv = _ref$idEnv === void 0 ? null : _ref$idEnv;
  var select = "SELECT TOP 1 a.id,idEnv,a.status, env.envName, a.idUser, a.variable";
  var from = " FROM environmentAccess a WITH (NOLOCK)\n                INNER JOIN environtment env WITH (NOLOCK) ON env.id=a.idEnv AND env.deletedAt IS NULL";
  var where = " WHERE a.deletedAt IS NULL\n                 AND a.idUser = '".concat(userId, "' AND a.status=1 ");
  if (envName) {
    where += "AND env.envName = '".concat(envName, "'");
  }
  if (idEnv) {
    where += "AND a.idEnv = '".concat(idEnv, "'");
  }
  return select + from + where;
};
var sql_getEndpointByAccess = exports.sql_getEndpointByAccess = function sql_getEndpointByAccess(_ref2) {
  var _ref2$userId = _ref2.userId,
    userId = _ref2$userId === void 0 ? null : _ref2$userId,
    _ref2$envId = _ref2.envId,
    envId = _ref2$envId === void 0 ? null : _ref2$envId,
    _ref2$requestId = _ref2.requestId,
    requestId = _ref2$requestId === void 0 ? null : _ref2$requestId,
    _ref2$method = _ref2.method,
    method = _ref2$method === void 0 ? null : _ref2$method;
  var select = "\n        SELECT TOP 1\n        Eri.id, Eri.idEnv, Eri.createdBy, Eri.updateBy, Eri.deletedBy, \n        Eri.createdAt, Eri.updatedAt, Eri.deletedAt, Eri.status, \n        Eri.groupName, Eri.name, Eri.description, Eri.version, \n        Eri.path, Eri.method, Eri.responseTrigger, \n        environtment.envName AS envName,\n        apiAccess.id as apiAccessId, apiAccess.isDisabled\n    ";
  var from = "\n         FROM endpointResourceIntegration AS Eri WITH (NOLOCK)\n        INNER JOIN environtment AS environtment WITH (NOLOCK) \n        ON Eri.idEnv = environtment.id\n        AND (environtment.deletedAt IS NULL \n        AND (environtment.id = '".concat(envId, "')) \n        LEFT OUTER JOIN apiAccess WITH (NOLOCK) ON apiAccess.idEnv=Eri.idEnv\n        AND (\n             apiAccess.deletedAt IS NULL\n             AND apiAccess.idEndpoint=Eri.id\n             AND apiAccess.idUser = '").concat(userId, "'\n        )\n    ");
  var where = "\n         WHERE Eri.deletedAt IS NULL\n        AND Eri.id = '".concat(requestId, "'\n        AND Eri.method = '").concat(method, "'\n    ");
  return select + from + where;
};