"use strict";

require("core-js/modules/es.object.define-property.js");
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
require("core-js/modules/es.array.concat.js");
var _express = _interopRequireDefault(require("express"));
var _controllers = _interopRequireDefault(require("./controllers.js"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
var basePath = "/users";
var checkTokenPath = "/users/check";
var checkPasswordPath = "/users/checkpassword";
var changesPassword = "/users/changespassword";
var listAllUsers = basePath + "/list/all";
var testApiPath = "/users/testing";
var resetPasswordPath = "/users/resetpassword";
var setPath = function setPath(path) {
  return "".concat(basePath, "/").concat(path);
};
var controller = (0, _controllers["default"])();
var routes = (0, _express["default"])();
routes.route(basePath).get(controller.getUser).post(controller.createUsers);
routes.route(setPath(':id')).get(controller.getDetailUser).put(controller.updateUsers)["delete"](controller.deleteUser);
routes.route(checkTokenPath).post(controller.checkToken);
routes.route(listAllUsers).get(controller.getListAllUsers);
routes.route(checkPasswordPath).post(controller.checkPassword);
routes.route(changesPassword).post(controller.changesPassword);
routes.route(testApiPath).post(controller.testApi);
routes.route(resetPasswordPath).post(controller.resetPassword);
var _default = exports["default"] = routes;