"use strict";

function _typeof(o) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) { return typeof o; } : function (o) { return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o; }, _typeof(o); }
require("core-js/modules/es.symbol.js");
require("core-js/modules/es.symbol.description.js");
require("core-js/modules/es.symbol.async-iterator.js");
require("core-js/modules/es.symbol.iterator.js");
require("core-js/modules/es.symbol.to-primitive.js");
require("core-js/modules/es.symbol.to-string-tag.js");
require("core-js/modules/es.array.for-each.js");
require("core-js/modules/es.array.iterator.js");
require("core-js/modules/es.array.reverse.js");
require("core-js/modules/es.array.slice.js");
require("core-js/modules/es.date.to-primitive.js");
require("core-js/modules/es.function.name.js");
require("core-js/modules/es.json.to-string-tag.js");
require("core-js/modules/es.math.to-string-tag.js");
require("core-js/modules/es.number.constructor.js");
require("core-js/modules/es.object.create.js");
require("core-js/modules/es.object.define-properties.js");
require("core-js/modules/es.object.define-property.js");
require("core-js/modules/es.object.get-own-property-descriptor.js");
require("core-js/modules/es.object.get-own-property-descriptors.js");
require("core-js/modules/es.object.get-prototype-of.js");
require("core-js/modules/es.object.keys.js");
require("core-js/modules/es.object.set-prototype-of.js");
require("core-js/modules/es.string.iterator.js");
require("core-js/modules/web.dom-collections.for-each.js");
require("core-js/modules/web.dom-collections.iterator.js");
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = userRepository;
require("core-js/modules/es.array.filter.js");
require("core-js/modules/es.array.find.js");
require("core-js/modules/es.date.now.js");
require("core-js/modules/es.object.to-string.js");
require("core-js/modules/es.promise.js");
require("core-js/modules/es.regexp.exec.js");
require("core-js/modules/es.string.search.js");
var _helpers = _interopRequireDefault(require("../../libs/utils/helpers.js"));
var _models = require("../../models");
var _sequelize = require("sequelize");
var _responseApi = require("../../libs/utils/responseApi");
var _query = require("./query");
var _callQuery = require("../../libs/utils/callQuery");
var _bcryptjs = _interopRequireDefault(require("bcryptjs"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
function ownKeys(e, r) { var t = Object.keys(e); if (Object.getOwnPropertySymbols) { var o = Object.getOwnPropertySymbols(e); r && (o = o.filter(function (r) { return Object.getOwnPropertyDescriptor(e, r).enumerable; })), t.push.apply(t, o); } return t; }
function _objectSpread(e) { for (var r = 1; r < arguments.length; r++) { var t = null != arguments[r] ? arguments[r] : {}; r % 2 ? ownKeys(Object(t), !0).forEach(function (r) { _defineProperty(e, r, t[r]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys(Object(t)).forEach(function (r) { Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r)); }); } return e; }
function _defineProperty(e, r, t) { return (r = _toPropertyKey(r)) in e ? Object.defineProperty(e, r, { value: t, enumerable: !0, configurable: !0, writable: !0 }) : e[r] = t, e; }
function _toPropertyKey(t) { var i = _toPrimitive(t, "string"); return "symbol" == _typeof(i) ? i : i + ""; }
function _toPrimitive(t, r) { if ("object" != _typeof(t) || !t) return t; var e = t[Symbol.toPrimitive]; if (void 0 !== e) { var i = e.call(t, r || "default"); if ("object" != _typeof(i)) return i; throw new TypeError("@@toPrimitive must return a primitive value."); } return ("string" === r ? String : Number)(t); }
function _regeneratorRuntime() { "use strict"; /*! regenerator-runtime -- Copyright (c) 2014-present, Facebook, Inc. -- license (MIT): https://github.com/facebook/regenerator/blob/main/LICENSE */ _regeneratorRuntime = function _regeneratorRuntime() { return e; }; var t, e = {}, r = Object.prototype, n = r.hasOwnProperty, o = Object.defineProperty || function (t, e, r) { t[e] = r.value; }, i = "function" == typeof Symbol ? Symbol : {}, a = i.iterator || "@@iterator", c = i.asyncIterator || "@@asyncIterator", u = i.toStringTag || "@@toStringTag"; function define(t, e, r) { return Object.defineProperty(t, e, { value: r, enumerable: !0, configurable: !0, writable: !0 }), t[e]; } try { define({}, ""); } catch (t) { define = function define(t, e, r) { return t[e] = r; }; } function wrap(t, e, r, n) { var i = e && e.prototype instanceof Generator ? e : Generator, a = Object.create(i.prototype), c = new Context(n || []); return o(a, "_invoke", { value: makeInvokeMethod(t, r, c) }), a; } function tryCatch(t, e, r) { try { return { type: "normal", arg: t.call(e, r) }; } catch (t) { return { type: "throw", arg: t }; } } e.wrap = wrap; var h = "suspendedStart", l = "suspendedYield", f = "executing", s = "completed", y = {}; function Generator() {} function GeneratorFunction() {} function GeneratorFunctionPrototype() {} var p = {}; define(p, a, function () { return this; }); var d = Object.getPrototypeOf, v = d && d(d(values([]))); v && v !== r && n.call(v, a) && (p = v); var g = GeneratorFunctionPrototype.prototype = Generator.prototype = Object.create(p); function defineIteratorMethods(t) { ["next", "throw", "return"].forEach(function (e) { define(t, e, function (t) { return this._invoke(e, t); }); }); } function AsyncIterator(t, e) { function invoke(r, o, i, a) { var c = tryCatch(t[r], t, o); if ("throw" !== c.type) { var u = c.arg, h = u.value; return h && "object" == _typeof(h) && n.call(h, "__await") ? e.resolve(h.__await).then(function (t) { invoke("next", t, i, a); }, function (t) { invoke("throw", t, i, a); }) : e.resolve(h).then(function (t) { u.value = t, i(u); }, function (t) { return invoke("throw", t, i, a); }); } a(c.arg); } var r; o(this, "_invoke", { value: function value(t, n) { function callInvokeWithMethodAndArg() { return new e(function (e, r) { invoke(t, n, e, r); }); } return r = r ? r.then(callInvokeWithMethodAndArg, callInvokeWithMethodAndArg) : callInvokeWithMethodAndArg(); } }); } function makeInvokeMethod(e, r, n) { var o = h; return function (i, a) { if (o === f) throw Error("Generator is already running"); if (o === s) { if ("throw" === i) throw a; return { value: t, done: !0 }; } for (n.method = i, n.arg = a;;) { var c = n.delegate; if (c) { var u = maybeInvokeDelegate(c, n); if (u) { if (u === y) continue; return u; } } if ("next" === n.method) n.sent = n._sent = n.arg;else if ("throw" === n.method) { if (o === h) throw o = s, n.arg; n.dispatchException(n.arg); } else "return" === n.method && n.abrupt("return", n.arg); o = f; var p = tryCatch(e, r, n); if ("normal" === p.type) { if (o = n.done ? s : l, p.arg === y) continue; return { value: p.arg, done: n.done }; } "throw" === p.type && (o = s, n.method = "throw", n.arg = p.arg); } }; } function maybeInvokeDelegate(e, r) { var n = r.method, o = e.iterator[n]; if (o === t) return r.delegate = null, "throw" === n && e.iterator["return"] && (r.method = "return", r.arg = t, maybeInvokeDelegate(e, r), "throw" === r.method) || "return" !== n && (r.method = "throw", r.arg = new TypeError("The iterator does not provide a '" + n + "' method")), y; var i = tryCatch(o, e.iterator, r.arg); if ("throw" === i.type) return r.method = "throw", r.arg = i.arg, r.delegate = null, y; var a = i.arg; return a ? a.done ? (r[e.resultName] = a.value, r.next = e.nextLoc, "return" !== r.method && (r.method = "next", r.arg = t), r.delegate = null, y) : a : (r.method = "throw", r.arg = new TypeError("iterator result is not an object"), r.delegate = null, y); } function pushTryEntry(t) { var e = { tryLoc: t[0] }; 1 in t && (e.catchLoc = t[1]), 2 in t && (e.finallyLoc = t[2], e.afterLoc = t[3]), this.tryEntries.push(e); } function resetTryEntry(t) { var e = t.completion || {}; e.type = "normal", delete e.arg, t.completion = e; } function Context(t) { this.tryEntries = [{ tryLoc: "root" }], t.forEach(pushTryEntry, this), this.reset(!0); } function values(e) { if (e || "" === e) { var r = e[a]; if (r) return r.call(e); if ("function" == typeof e.next) return e; if (!isNaN(e.length)) { var o = -1, i = function next() { for (; ++o < e.length;) if (n.call(e, o)) return next.value = e[o], next.done = !1, next; return next.value = t, next.done = !0, next; }; return i.next = i; } } throw new TypeError(_typeof(e) + " is not iterable"); } return GeneratorFunction.prototype = GeneratorFunctionPrototype, o(g, "constructor", { value: GeneratorFunctionPrototype, configurable: !0 }), o(GeneratorFunctionPrototype, "constructor", { value: GeneratorFunction, configurable: !0 }), GeneratorFunction.displayName = define(GeneratorFunctionPrototype, u, "GeneratorFunction"), e.isGeneratorFunction = function (t) { var e = "function" == typeof t && t.constructor; return !!e && (e === GeneratorFunction || "GeneratorFunction" === (e.displayName || e.name)); }, e.mark = function (t) { return Object.setPrototypeOf ? Object.setPrototypeOf(t, GeneratorFunctionPrototype) : (t.__proto__ = GeneratorFunctionPrototype, define(t, u, "GeneratorFunction")), t.prototype = Object.create(g), t; }, e.awrap = function (t) { return { __await: t }; }, defineIteratorMethods(AsyncIterator.prototype), define(AsyncIterator.prototype, c, function () { return this; }), e.AsyncIterator = AsyncIterator, e.async = function (t, r, n, o, i) { void 0 === i && (i = Promise); var a = new AsyncIterator(wrap(t, r, n, o), i); return e.isGeneratorFunction(r) ? a : a.next().then(function (t) { return t.done ? t.value : a.next(); }); }, defineIteratorMethods(g), define(g, u, "Generator"), define(g, a, function () { return this; }), define(g, "toString", function () { return "[object Generator]"; }), e.keys = function (t) { var e = Object(t), r = []; for (var n in e) r.push(n); return r.reverse(), function next() { for (; r.length;) { var t = r.pop(); if (t in e) return next.value = t, next.done = !1, next; } return next.done = !0, next; }; }, e.values = values, Context.prototype = { constructor: Context, reset: function reset(e) { if (this.prev = 0, this.next = 0, this.sent = this._sent = t, this.done = !1, this.delegate = null, this.method = "next", this.arg = t, this.tryEntries.forEach(resetTryEntry), !e) for (var r in this) "t" === r.charAt(0) && n.call(this, r) && !isNaN(+r.slice(1)) && (this[r] = t); }, stop: function stop() { this.done = !0; var t = this.tryEntries[0].completion; if ("throw" === t.type) throw t.arg; return this.rval; }, dispatchException: function dispatchException(e) { if (this.done) throw e; var r = this; function handle(n, o) { return a.type = "throw", a.arg = e, r.next = n, o && (r.method = "next", r.arg = t), !!o; } for (var o = this.tryEntries.length - 1; o >= 0; --o) { var i = this.tryEntries[o], a = i.completion; if ("root" === i.tryLoc) return handle("end"); if (i.tryLoc <= this.prev) { var c = n.call(i, "catchLoc"), u = n.call(i, "finallyLoc"); if (c && u) { if (this.prev < i.catchLoc) return handle(i.catchLoc, !0); if (this.prev < i.finallyLoc) return handle(i.finallyLoc); } else if (c) { if (this.prev < i.catchLoc) return handle(i.catchLoc, !0); } else { if (!u) throw Error("try statement without catch or finally"); if (this.prev < i.finallyLoc) return handle(i.finallyLoc); } } } }, abrupt: function abrupt(t, e) { for (var r = this.tryEntries.length - 1; r >= 0; --r) { var o = this.tryEntries[r]; if (o.tryLoc <= this.prev && n.call(o, "finallyLoc") && this.prev < o.finallyLoc) { var i = o; break; } } i && ("break" === t || "continue" === t) && i.tryLoc <= e && e <= i.finallyLoc && (i = null); var a = i ? i.completion : {}; return a.type = t, a.arg = e, i ? (this.method = "next", this.next = i.finallyLoc, y) : this.complete(a); }, complete: function complete(t, e) { if ("throw" === t.type) throw t.arg; return "break" === t.type || "continue" === t.type ? this.next = t.arg : "return" === t.type ? (this.rval = this.arg = t.arg, this.method = "return", this.next = "end") : "normal" === t.type && e && (this.next = e), y; }, finish: function finish(t) { for (var e = this.tryEntries.length - 1; e >= 0; --e) { var r = this.tryEntries[e]; if (r.finallyLoc === t) return this.complete(r.completion, r.afterLoc), resetTryEntry(r), y; } }, "catch": function _catch(t) { for (var e = this.tryEntries.length - 1; e >= 0; --e) { var r = this.tryEntries[e]; if (r.tryLoc === t) { var n = r.completion; if ("throw" === n.type) { var o = n.arg; resetTryEntry(r); } return o; } } throw Error("illegal catch attempt"); }, delegateYield: function delegateYield(e, r, n) { return this.delegate = { iterator: values(e), resultName: r, nextLoc: n }, "next" === this.method && (this.arg = t), y; } }, e; }
function asyncGeneratorStep(n, t, e, r, o, a, c) { try { var i = n[a](c), u = i.value; } catch (n) { return void e(n); } i.done ? t(u) : Promise.resolve(u).then(r, o); }
function _asyncToGenerator(n) { return function () { var t = this, e = arguments; return new Promise(function (r, o) { var a = n.apply(t, e); function _next(n) { asyncGeneratorStep(a, r, o, _next, _throw, "next", n); } function _throw(n) { asyncGeneratorStep(a, r, o, _next, _throw, "throw", n); } _next(void 0); }); }; }
function userRepository() {
  //todo list all user without admin
  var getUser = /*#__PURE__*/function () {
    var _ref = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee(req) {
      var _req$query, page, size, search, createAt, _queryListUsers, select, from, where, orderByRowNumber, getData;
      return _regeneratorRuntime().wrap(function _callee$(_context) {
        while (1) switch (_context.prev = _context.next) {
          case 0:
            _req$query = req.query, page = _req$query.page, size = _req$query.size, search = _req$query.search, createAt = _req$query.createAt;
            _queryListUsers = (0, _query.queryListUsers)({
              search: search,
              desc: createAt.length > 0 ? createAt.toUpperCase() : "DESC"
            }), select = _queryListUsers.select, from = _queryListUsers.from, where = _queryListUsers.where, orderByRowNumber = _queryListUsers.orderByRowNumber;
            _context.next = 4;
            return (0, _callQuery.queryWithPagination)({
              select: select,
              from: from,
              where: where,
              orderByRowNumber: orderByRowNumber,
              pageNumber: page,
              pageSize: size
            });
          case 4:
            getData = _context.sent;
            return _context.abrupt("return", (0, _responseApi.result)({
              data: getData
            }));
          case 6:
          case "end":
            return _context.stop();
        }
      }, _callee);
    }));
    return function getUser(_x) {
      return _ref.apply(this, arguments);
    };
  }();
  var getUserDetail = /*#__PURE__*/function () {
    var _ref2 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee2(req) {
      var id, _getUser;
      return _regeneratorRuntime().wrap(function _callee2$(_context2) {
        while (1) switch (_context2.prev = _context2.next) {
          case 0:
            id = req.params.id;
            _context2.prev = 1;
            _context2.next = 4;
            return _models.users.findByPk(id, {
              attributes: {
                exclude: ["deletedAt", "deletedBy", "createdBy", "password", "token"]
              }
            });
          case 4:
            _getUser = _context2.sent;
            if (_getUser) {
              _context2.next = 9;
              break;
            }
            _context2.next = 8;
            return (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.badRequest400,
              message: "user id ".concat(id, " not found")
            });
          case 8:
            return _context2.abrupt("return", _context2.sent);
          case 9:
            _context2.next = 11;
            return (0, _responseApi.result)({
              data: _getUser
            });
          case 11:
            return _context2.abrupt("return", _context2.sent);
          case 14:
            _context2.prev = 14;
            _context2.t0 = _context2["catch"](1);
            _context2.t0.message = "UserRepository.getUserDetail: " + _context2.t0;
            throw _context2.t0;
          case 18:
          case "end":
            return _context2.stop();
        }
      }, _callee2, null, [[1, 14]]);
    }));
    return function getUserDetail(_x2) {
      return _ref2.apply(this, arguments);
    };
  }();
  var updateUser = /*#__PURE__*/function () {
    var _ref3 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee3(req) {
      var id, _req$body, status, fullname, email, password, companyName, role, validateUpdate, _getUser2, getUserFrom;
      return _regeneratorRuntime().wrap(function _callee3$(_context3) {
        while (1) switch (_context3.prev = _context3.next) {
          case 0:
            id = req.params.id;
            _req$body = req.body, status = _req$body.status, fullname = _req$body.fullname, email = _req$body.email, password = _req$body.password, companyName = _req$body.companyName, role = _req$body.role;
            _context3.prev = 2;
            req.body = _objectSpread(_objectSpread({}, req.body), {}, {
              id: id
            });
            _context3.next = 6;
            return userRepository().validationUserCreate(req);
          case 6:
            validateUpdate = _context3.sent;
            if (!(validateUpdate.code === _responseApi.statusHttpCode.badRequest400.code)) {
              _context3.next = 9;
              break;
            }
            return _context3.abrupt("return", validateUpdate);
          case 9:
            _context3.next = 11;
            return _models.users.findByPk(id);
          case 11:
            _getUser2 = _context3.sent;
            if (_getUser2) {
              _context3.next = 14;
              break;
            }
            return _context3.abrupt("return", (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.badRequest400,
              message: "user id ".concat(id, " not found")
            }));
          case 14:
            _context3.next = 16;
            return _models.users.update({
              status: status,
              fullname: fullname,
              email: email,
              password: password,
              companyName: companyName,
              role: role
            }, {
              where: {
                id: id
              }
            });
          case 16:
            _context3.next = 18;
            return userRepository().getUserDetail(req);
          case 18:
            getUserFrom = _context3.sent;
            return _context3.abrupt("return", (0, _responseApi.result)({
              data: getUserFrom.results
            }));
          case 22:
            _context3.prev = 22;
            _context3.t0 = _context3["catch"](2);
            _context3.t0.message = "UserRepository.UpdateUser: " + _context3.t0;
            throw _context3.t0;
          case 26:
          case "end":
            return _context3.stop();
        }
      }, _callee3, null, [[2, 22]]);
    }));
    return function updateUser(_x3) {
      return _ref3.apply(this, arguments);
    };
  }();
  var createUser = /*#__PURE__*/function () {
    var _ref4 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee4(req) {
      var registrationToken, _req$body2, fullname, username, password, email, companyName, role, status, _getUser3, _createUser, getUserCreate;
      return _regeneratorRuntime().wrap(function _callee4$(_context4) {
        while (1) switch (_context4.prev = _context4.next) {
          case 0:
            _context4.next = 2;
            return (0, _helpers["default"])().getUniqueRegistrationToken(_models.users);
          case 2:
            registrationToken = _context4.sent;
            _req$body2 = req.body, fullname = _req$body2.fullname, username = _req$body2.username, password = _req$body2.password, email = _req$body2.email, companyName = _req$body2.companyName, role = _req$body2.role, status = _req$body2.status;
            _context4.prev = 4;
            _context4.next = 7;
            return userRepository().validationUserCreate(req);
          case 7:
            _getUser3 = _context4.sent;
            if (!(_getUser3.code === _responseApi.statusHttpCode.badRequest400.code)) {
              _context4.next = 10;
              break;
            }
            return _context4.abrupt("return", _getUser3);
          case 10:
            _context4.next = 12;
            return _models.users.create({
              id: Date.now(),
              fullname: fullname,
              username: username,
              password: password,
              email: email,
              token: registrationToken,
              companyName: companyName,
              role: role,
              status: status
            });
          case 12:
            _createUser = _context4.sent;
            req.params = {
              id: _createUser.id
            };
            _context4.next = 16;
            return userRepository().getUserDetail(req);
          case 16:
            getUserCreate = _context4.sent;
            return _context4.abrupt("return", (0, _responseApi.result)({
              data: getUserCreate.results
            }));
          case 20:
            _context4.prev = 20;
            _context4.t0 = _context4["catch"](4);
            _context4.t0.message = "UserRepository.CreateUser: " + _context4.t0;
            throw _context4.t0;
          case 24:
          case "end":
            return _context4.stop();
        }
      }, _callee4, null, [[4, 20]]);
    }));
    return function createUser(_x4) {
      return _ref4.apply(this, arguments);
    };
  }();
  var deleteUser = /*#__PURE__*/function () {
    var _ref5 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee5(req) {
      var id, checkId;
      return _regeneratorRuntime().wrap(function _callee5$(_context5) {
        while (1) switch (_context5.prev = _context5.next) {
          case 0:
            id = req.params.id;
            _context5.prev = 1;
            _context5.next = 4;
            return userRepository().getUserDetail(req);
          case 4:
            checkId = _context5.sent;
            if (!(checkId.code === _responseApi.statusHttpCode.badRequest400.code)) {
              _context5.next = 7;
              break;
            }
            return _context5.abrupt("return", checkId);
          case 7:
            _context5.next = 9;
            return _models.users.destroy({
              where: {
                id: id
              }
            });
          case 9:
            return _context5.abrupt("return", (0, _responseApi.result)({
              message: "DELETED UserID ".concat(id)
            }));
          case 12:
            _context5.prev = 12;
            _context5.t0 = _context5["catch"](1);
            _context5.t0.message = "UserRepository.deleteUser: " + _context5.t0;
            throw _context5.t0;
          case 16:
          case "end":
            return _context5.stop();
        }
      }, _callee5, null, [[1, 12]]);
    }));
    return function deleteUser(_x5) {
      return _ref5.apply(this, arguments);
    };
  }();
  var validationUserCreate = /*#__PURE__*/function () {
    var _ref6 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee6(req) {
      var _req$body3, id, username, email, checkUserName, checkEmail;
      return _regeneratorRuntime().wrap(function _callee6$(_context6) {
        while (1) switch (_context6.prev = _context6.next) {
          case 0:
            _req$body3 = req.body, id = _req$body3.id, username = _req$body3.username, email = _req$body3.email;
            _context6.prev = 1;
            _context6.next = 4;
            return _models.users.findOne({
              where: {
                username: username
              }
            });
          case 4:
            checkUserName = _context6.sent;
            _context6.next = 7;
            return _models.users.findOne({
              where: {
                email: email
              }
            });
          case 7:
            checkEmail = _context6.sent;
            if (!(id && checkUserName && id !== checkUserName.id || !id && checkUserName)) {
              _context6.next = 10;
              break;
            }
            return _context6.abrupt("return", (0, _responseApi.result)({
              message: "username already exist",
              statusCode: _responseApi.statusHttpCode.badRequest400
            }));
          case 10:
            if (!(id && checkEmail && id !== checkEmail.id || !id && checkEmail)) {
              _context6.next = 12;
              break;
            }
            return _context6.abrupt("return", (0, _responseApi.result)({
              message: "email already exist",
              statusCode: _responseApi.statusHttpCode.badRequest400
            }));
          case 12:
            return _context6.abrupt("return", (0, _responseApi.result)({}));
          case 15:
            _context6.prev = 15;
            _context6.t0 = _context6["catch"](1);
            _context6.t0.message = "UserRepository.checkUsernameEmail :" + _context6.t0;
            throw _context6.t0;
          case 19:
          case "end":
            return _context6.stop();
        }
      }, _callee6, null, [[1, 15]]);
    }));
    return function validationUserCreate(_x6) {
      return _ref6.apply(this, arguments);
    };
  }();
  var checkToken = /*#__PURE__*/function () {
    var _ref7 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee7(req) {
      return _regeneratorRuntime().wrap(function _callee7$(_context7) {
        while (1) switch (_context7.prev = _context7.next) {
          case 0:
            return _context7.abrupt("return", (0, _responseApi.result)({
              data: req.decoded
            }));
          case 1:
          case "end":
            return _context7.stop();
        }
      }, _callee7);
    }));
    return function checkToken(_x7) {
      return _ref7.apply(this, arguments);
    };
  }();
  //todo list all user include admin (tanpa menampilkan admin yang login)
  var getListAllUsers = /*#__PURE__*/function () {
    var _ref8 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee8(req) {
      var id, _req$query2, page, size, filter, userRole, orderByCreatedAt, condition, _helpers$getPaginatio, limit, offset, order, model;
      return _regeneratorRuntime().wrap(function _callee8$(_context8) {
        while (1) switch (_context8.prev = _context8.next) {
          case 0:
            id = req.decoded.id;
            _req$query2 = req.query, page = _req$query2.page, size = _req$query2.size, filter = _req$query2.filter, userRole = _req$query2.userRole, orderByCreatedAt = _req$query2.orderByCreatedAt;
            condition = _defineProperty({}, _sequelize.Op.and, [{
              id: _defineProperty({}, _sequelize.Op.ne, id)
            }]);
            if (userRole) {
              condition = _objectSpread(_objectSpread({}, condition), {}, {
                role: _defineProperty({}, _sequelize.Op.eq, userRole)
              });
            }
            if (filter) {
              condition = _objectSpread(_objectSpread({}, condition), {}, _defineProperty({}, _sequelize.Op.or, [{
                id: _defineProperty({}, _sequelize.Op.like, "%".concat(filter, "%"))
              }, {
                fullname: _defineProperty({}, _sequelize.Op.like, "%".concat(filter, "%"))
              }, {
                username: _defineProperty({}, _sequelize.Op.like, "%".concat(filter, "%"))
              }, {
                email: _defineProperty({}, _sequelize.Op.like, "%".concat(filter, "%"))
              }, {
                companyName: _defineProperty({}, _sequelize.Op.like, "%".concat(filter, "%"))
              }]));
            }
            _helpers$getPaginatio = (0, _helpers["default"])().getPagination(page, size, orderByCreatedAt), limit = _helpers$getPaginatio.limit, offset = _helpers$getPaginatio.offset, order = _helpers$getPaginatio.order;
            _context8.next = 8;
            return _models.users.findAndCountAll({
              attributes: ["id", "updateBy", "createdAt", "updatedAt", "status", "fullname", "username", "email", "companyName", "role"],
              where: condition,
              order: order,
              limit: limit,
              offset: offset
            });
          case 8:
            model = _context8.sent;
            return _context8.abrupt("return", (0, _responseApi.result)({
              data: (0, _helpers["default"])().getPagingData(model, page, limit)
            }));
          case 10:
          case "end":
            return _context8.stop();
        }
      }, _callee8);
    }));
    return function getListAllUsers(_x8) {
      return _ref8.apply(this, arguments);
    };
  }();
  var checkPassword = /*#__PURE__*/function () {
    var _ref9 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee9(req) {
      var _req$decoded, id, role, userId, idUser, password, model, passwordOk;
      return _regeneratorRuntime().wrap(function _callee9$(_context9) {
        while (1) switch (_context9.prev = _context9.next) {
          case 0:
            _req$decoded = req.decoded, id = _req$decoded.id, role = _req$decoded.role;
            userId = req.query.userId;
            if (!(role !== 1 && userId)) {
              _context9.next = 4;
              break;
            }
            return _context9.abrupt("return", (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.unauthorized401,
              message: "Cant access API"
            }));
          case 4:
            idUser = userId ? userId : id;
            password = req.body.password;
            _context9.prev = 6;
            _context9.next = 9;
            return _models.users.findOne({
              where: {
                id: idUser
              }
            });
          case 9:
            model = _context9.sent;
            if (!model) {
              _context9.next = 19;
              break;
            }
            _context9.next = 13;
            return model.comparePassword(password, function (error, response) {
              if (error) {
                var err = new Error(error);
                err.status = 500;
                throw err;
              }
              return response;
            });
          case 13:
            passwordOk = _context9.sent;
            if (passwordOk) {
              _context9.next = 16;
              break;
            }
            return _context9.abrupt("return", (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.badRequest400,
              message: "Password wrong"
            }));
          case 16:
            return _context9.abrupt("return", (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.ok200
            }));
          case 19:
            return _context9.abrupt("return", (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.unauthorized401,
              message: "User not found"
            }));
          case 20:
            _context9.next = 26;
            break;
          case 22:
            _context9.prev = 22;
            _context9.t0 = _context9["catch"](6);
            _context9.t0.message = "userRepository.checkPassword: " + _context9.t0;
            throw _context9.t0;
          case 26:
          case "end":
            return _context9.stop();
        }
      }, _callee9, null, [[6, 22]]);
    }));
    return function checkPassword(_x9) {
      return _ref9.apply(this, arguments);
    };
  }();
  var changesPassword = /*#__PURE__*/function () {
    var _ref10 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee10(req) {
      var _req$decoded2, id, role, userId, idUser, _req$body4, oldPassword, newPassword, model, passwordOk;
      return _regeneratorRuntime().wrap(function _callee10$(_context10) {
        while (1) switch (_context10.prev = _context10.next) {
          case 0:
            _req$decoded2 = req.decoded, id = _req$decoded2.id, role = _req$decoded2.role;
            userId = req.query.userId;
            if (!(role !== 1 && userId)) {
              _context10.next = 4;
              break;
            }
            return _context10.abrupt("return", (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.unauthorized401,
              message: "Cant access API"
            }));
          case 4:
            idUser = userId ? userId : id;
            _req$body4 = req.body, oldPassword = _req$body4.oldPassword, newPassword = _req$body4.newPassword;
            _context10.prev = 6;
            _context10.next = 9;
            return _models.users.findOne({
              where: {
                id: idUser
              }
            });
          case 9:
            model = _context10.sent;
            if (!model) {
              _context10.next = 21;
              break;
            }
            _context10.next = 13;
            return model.comparePassword(oldPassword, function (error, response) {
              if (error) {
                var err = new Error(error);
                err.status = 500;
                throw err;
              }
              return response;
            });
          case 13:
            passwordOk = _context10.sent;
            if (passwordOk) {
              _context10.next = 16;
              break;
            }
            return _context10.abrupt("return", (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.badRequest400,
              message: "Password wrong"
            }));
          case 16:
            _context10.next = 18;
            return _models.users.update({
              password: newPassword
            }, {
              where: {
                id: idUser
              },
              individualHooks: true //for hook beforeCreate into model
            });
          case 18:
            return _context10.abrupt("return", (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.ok200,
              message: "changes password success"
            }));
          case 21:
            return _context10.abrupt("return", (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.unauthorized401,
              message: "User not found"
            }));
          case 22:
            _context10.next = 28;
            break;
          case 24:
            _context10.prev = 24;
            _context10.t0 = _context10["catch"](6);
            _context10.t0.message = "userRepository.changesPassword: " + _context10.t0;
            throw _context10.t0;
          case 28:
          case "end":
            return _context10.stop();
        }
      }, _callee10, null, [[6, 24]]);
    }));
    return function changesPassword(_x10) {
      return _ref10.apply(this, arguments);
    };
  }();
  var testApi = /*#__PURE__*/function () {
    var _ref11 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee11(req) {
      var responseName, resultListErrorName, token, findErrorName, statusCode;
      return _regeneratorRuntime().wrap(function _callee11$(_context11) {
        while (1) switch (_context11.prev = _context11.next) {
          case 0:
            responseName = req.query.responseName;
            resultListErrorName = [{
              code: 200,
              value: "success"
            }, {
              code: 400,
              value: "bad_request"
            }, {
              code: 401,
              value: "unauthorize"
            }, {
              code: 404,
              value: "not_found"
            }, {
              code: 500,
              value: "server_internal_error"
            }];
            token = req.headers["authorization"]; //useDefaultHeader
            if (token) {
              _context11.next = 5;
              break;
            }
            return _context11.abrupt("return", (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.unauthorized401,
              message: "No token provided"
            }));
          case 5:
            if (responseName) {
              _context11.next = 7;
              break;
            }
            return _context11.abrupt("return", (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.ok200,
              message: "ErrorName List: you can use value parameter query responseName by value",
              data: resultListErrorName
            }));
          case 7:
            findErrorName = resultListErrorName.find(function (el) {
              return el.value === responseName.toLowerCase();
            }); // todo is error name not found
            if (findErrorName) {
              _context11.next = 10;
              break;
            }
            return _context11.abrupt("return", (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.badRequest400,
              message: "Error name not found, please check in list :",
              data: resultListErrorName
            }));
          case 10:
            _context11.t0 = findErrorName.code;
            _context11.next = _context11.t0 === _responseApi.statusHttpCode.ok200.code ? 13 : _context11.t0 === _responseApi.statusHttpCode.badRequest400.code ? 15 : _context11.t0 === _responseApi.statusHttpCode.unauthorized401.code ? 17 : _context11.t0 === _responseApi.statusHttpCode.notFound404.code ? 19 : 21;
            break;
          case 13:
            statusCode = _responseApi.statusHttpCode.ok200;
            return _context11.abrupt("break", 23);
          case 15:
            statusCode = _responseApi.statusHttpCode.badRequest400;
            return _context11.abrupt("break", 23);
          case 17:
            statusCode = _responseApi.statusHttpCode.unauthorized401;
            return _context11.abrupt("break", 23);
          case 19:
            statusCode = _responseApi.statusHttpCode.notFound404;
            return _context11.abrupt("break", 23);
          case 21:
            statusCode = _responseApi.statusHttpCode.internalServerError500;
            return _context11.abrupt("break", 23);
          case 23:
            return _context11.abrupt("return", (0, _responseApi.result)({
              statusCode: statusCode,
              message: "API Success Test : " + findErrorName.value,
              data: {
                test: "response data"
              }
            }));
          case 24:
          case "end":
            return _context11.stop();
        }
      }, _callee11);
    }));
    return function testApi(_x11) {
      return _ref11.apply(this, arguments);
    };
  }();
  var resetPassword = /*#__PURE__*/function () {
    var _ref12 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee12(req, res, next) {
      var _req$decoded3, id, role, userId, idUser, generator, password, hash, model, updateUser;
      return _regeneratorRuntime().wrap(function _callee12$(_context12) {
        while (1) switch (_context12.prev = _context12.next) {
          case 0:
            _req$decoded3 = req.decoded, id = _req$decoded3.id, role = _req$decoded3.role;
            userId = req.query.userId;
            if (!(role !== 1 && userId)) {
              _context12.next = 4;
              break;
            }
            return _context12.abrupt("return", (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.unauthorized401,
              message: "Cant access API"
            }));
          case 4:
            idUser = userId ? userId : id; //sendmailer
            generator = require("generate-password");
            password = generator.generate({
              length: 5,
              numbers: true
            });
            _context12.next = 9;
            return _bcryptjs["default"].hash(password, 10);
          case 9:
            hash = _context12.sent;
            _context12.next = 12;
            return _models.users.findOne({
              where: {
                id: idUser
              }
            });
          case 12:
            model = _context12.sent;
            if (model) {
              _context12.next = 15;
              break;
            }
            return _context12.abrupt("return", (0, _responseApi.result)({
              message: "User not found",
              statusCode: _responseApi.statusHttpCode.notFound404
            }));
          case 15:
            _context12.next = 17;
            return _models.users.update({
              password: hash
            }, {
              where: {
                id: userId
              }
            });
          case 17:
            updateUser = _context12.sent;
            if (updateUser) {
              _context12.next = 20;
              break;
            }
            return _context12.abrupt("return", (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.badRequest400,
              message: "Error Update Password",
              data: updateUser
            }));
          case 20:
            return _context12.abrupt("return", (0, _responseApi.result)({
              message: "Reset Password success, save your password",
              data: password
            }));
          case 21:
          case "end":
            return _context12.stop();
        }
      }, _callee12);
    }));
    return function resetPassword(_x12, _x13, _x14) {
      return _ref12.apply(this, arguments);
    };
  }();
  return {
    getUser: getUser,
    getUserDetail: getUserDetail,
    updateUser: updateUser,
    createUser: createUser,
    deleteUser: deleteUser,
    validationUserCreate: validationUserCreate,
    checkToken: checkToken,
    getListAllUsers: getListAllUsers,
    checkPassword: checkPassword,
    changesPassword: changesPassword,
    testApi: testApi,
    resetPassword: resetPassword
  };
}