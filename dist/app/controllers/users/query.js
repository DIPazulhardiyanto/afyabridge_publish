"use strict";

require("core-js/modules/es.object.define-property.js");
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.queryListUsers = void 0;
require("core-js/modules/es.array.concat.js");
require("core-js/modules/es.regexp.exec.js");
require("core-js/modules/es.string.search.js");
var queryListUsers = exports.queryListUsers = function queryListUsers(_ref) {
  var _ref$desc = _ref.desc,
    desc = _ref$desc === void 0 ? 'DESC' : _ref$desc,
    _ref$search = _ref.search,
    search = _ref$search === void 0 ? '' : _ref$search;
  var select = "SELECT [id], [updateBy], [createdAt], [updatedAt], [status], [fullname], [username], [email], [companyName], [role] ";
  var from = "FROM [users] AS [users] WITH (NOLOCK)";
  var where = "WHERE [users].[deletedAt] IS NULL AND [users].[role] != N'1'";
  var orderByRowNumber = "[users].[createdAt] ".concat(desc);

  //todo add where clause
  if (search.length > 0) {
    where += " AND ( \n            [users].[id] LIKE '%".concat(search, "%' \n            OR [users].[username] LIKE '%").concat(search, "%'\n            OR [users].[email] LIKE '%").concat(search, "%' \n            OR [users].[fullname] LIKE '%").concat(search, "%' \n            OR [users].[companyName] LIKE '%").concat(search, "%' \n        )");
  }
  return {
    select: select,
    from: from,
    where: where,
    orderByRowNumber: orderByRowNumber
  };
};