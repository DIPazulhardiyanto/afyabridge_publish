"use strict";

require("core-js/modules/es.object.define-property.js");
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.sql_showListApiAccess = exports.sql_checkItemAlreadyAdded = exports.sql_checkEndpointResourceIntegration = void 0;
require("core-js/modules/es.array.concat.js");
var sql_showListApiAccess = function sql_showListApiAccess() {
  var select = "SELECT *";
  var from = "FROM apiAccess WITH (NOLOCK)";
  var where = "WHERE apiAccess.deletedAt IS NULL ";
  var orderByRowNumber = "apiAccess.createdAt DESC";
  return {
    select: select,
    from: from,
    where: where,
    orderByRowNumber: orderByRowNumber
  };
};
exports.sql_showListApiAccess = sql_showListApiAccess;
var sql_checkEndpointResourceIntegration = function sql_checkEndpointResourceIntegration(_ref) {
  var _ref$idUser = _ref.idUser,
    idUser = _ref$idUser === void 0 ? null : _ref$idUser,
    _ref$idEnv = _ref.idEnv,
    idEnv = _ref$idEnv === void 0 ? null : _ref$idEnv,
    _ref$idEndpoint = _ref.idEndpoint,
    idEndpoint = _ref$idEndpoint === void 0 ? null : _ref$idEndpoint;
  return "SELECT eri.id, eri.idEnv, eri.name, eri.groupName, eri.version, \n            eri.path, eri.method, era.createdBy, era.createdAt\n            FROM endpointResourceIntegration as eri WITH (NOLOCK)\n            left join environmentAccess as era WITH (NOLOCK) on eri.idEnv=era.idEnv\n            and era.idUser = '".concat(idUser, "'\n            and era.deletedAt IS NULL\n            WHERE eri.deletedAt IS NULL \n            and era.idEnv='").concat(idEnv, "'\n            and eri.id='").concat(idEndpoint, "'");
};
exports.sql_checkEndpointResourceIntegration = sql_checkEndpointResourceIntegration;
var sql_checkItemAlreadyAdded = function sql_checkItemAlreadyAdded(_ref2) {
  var _ref2$userId = _ref2.userId,
    userId = _ref2$userId === void 0 ? null : _ref2$userId,
    _ref2$idEndpoint = _ref2.idEndpoint,
    idEndpoint = _ref2$idEndpoint === void 0 ? null : _ref2$idEndpoint;
  return "SELECT TOP 1 *\n            FROM apiAccess WITH (NOLOCK)\n            WHERE apiAccess.deletedAt IS NULL \n            AND apiAccess.idUser='".concat(userId, "'\n            AND apiAccess.idEndpoint='").concat(idEndpoint, "'");
};
exports.sql_checkItemAlreadyAdded = sql_checkItemAlreadyAdded;