"use strict";

require("core-js/modules/es.object.define-property.js");
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.queryListTransporter = void 0;
require("core-js/modules/es.array.concat.js");
require("core-js/modules/es.array.filter.js");
require("core-js/modules/es.array.map.js");
require("core-js/modules/es.object.to-string.js");
require("core-js/modules/es.regexp.exec.js");
require("core-js/modules/es.string.replace.js");
var queryListTransporter = exports.queryListTransporter = function queryListTransporter(_ref) {
  var _ref$method = _ref.method,
    method = _ref$method === void 0 ? '' : _ref$method,
    _ref$env = _ref.env,
    env = _ref$env === void 0 ? '' : _ref$env,
    _ref$filter = _ref.filter,
    filter = _ref$filter === void 0 ? '' : _ref$filter,
    _ref$desc = _ref.desc,
    desc = _ref$desc === void 0 ? '' : _ref$desc,
    _ref$category = _ref.category,
    category = _ref$category === void 0 ? '' : _ref$category;
  var select = "SELECT [id], [environment], [createdBy], [updateBy], [deletedBy], [createdAt], \n                  [updatedAt], [deletedAt], [status], [groupName], [name], [description], [version], [path], [method], \n                  [responseTrigger] ";
  var from = "FROM [transporter] AS [transporter]";
  var where = "WHERE [transporter].[deletedAt] IS NULL";
  var orderByRowNumber = "[transporter].[createdAt] ".concat(desc);

  // todo add where clause
  if (method.length > 0) {
    where += " AND [transporter].[method] LIKE '%".concat(method, "%'");
  }
  if (env.length > 0) {
    where += " AND [transporter].[environment] LIKE '%".concat(env, "%'");
  }
  if (filter.length > 0) {
    where += " AND (\n            [transporter].[id] LIKE '%".concat(filter, "%'\n            OR\n            [transporter].[path] LIKE '%").concat(filter, "%'\n            OR\n            [transporter].[name] LIKE '%").concat(filter, "%'\n        )");
  }
  if (category.length > 0) {
    var categoryString = '';
    category.split(',').map(function (el) {
      el = el.replace(/\s/g, '');
      if (categoryString.length === 0) {
        categoryString += "[transporter].[groupName] = N'".concat(el, "'");
      } else {
        categoryString += " OR [transporter].[groupName] = N'".concat(el, "'");
      }
    });
    where += " AND ( ".concat(categoryString, " )");
  }
  return {
    select: select,
    from: from,
    where: where,
    orderByRowNumber: orderByRowNumber
  };
};