"use strict";

require("core-js/modules/es.object.define-property.js");
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
require("core-js/modules/es.array.concat.js");
var _express = _interopRequireDefault(require("express"));
var _controllers = _interopRequireDefault(require("./controllers"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
var basePath = "/users/accessapi";
var accessDisabledPath = "".concat(basePath, "/disabled");
var enabledAccessPath = "".concat(basePath, "/enabled");
var setPath = function setPath(path) {
  return "".concat(basePath, "/").concat(path);
};
var setPathEnabledAccess = function setPathEnabledAccess(path) {
  return "".concat(enabledAccessPath, "/").concat(path);
};
var controller = (0, _controllers["default"])();
var routes = (0, _express["default"])();
routes.route(basePath).get(controller.showList);
routes.route(setPath(":id")).get(controller.detail);
routes.route(accessDisabledPath).post(controller.create);
routes.route(setPathEnabledAccess(':id')).post(controller.deleted);
var _default = exports["default"] = routes;