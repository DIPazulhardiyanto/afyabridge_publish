"use strict";

require("core-js/modules/es.object.define-property.js");
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _express = require("express");
var _controllers = _interopRequireDefault(require("./controllers.js"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
var routes = (0, _express.Router)();
var _Controller = (0, _controllers["default"])(),
  index = _Controller.index;
var basePath = "/app-config";
routes.route(basePath).get(index);
var _default = exports["default"] = routes;