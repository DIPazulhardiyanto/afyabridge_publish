"use strict";

function _typeof(o) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) { return typeof o; } : function (o) { return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o; }, _typeof(o); }
require("core-js/modules/es.symbol.async-iterator.js");
require("core-js/modules/es.symbol.iterator.js");
require("core-js/modules/es.symbol.to-primitive.js");
require("core-js/modules/es.symbol.to-string-tag.js");
require("core-js/modules/es.array.for-each.js");
require("core-js/modules/es.array.iterator.js");
require("core-js/modules/es.array.reverse.js");
require("core-js/modules/es.array.slice.js");
require("core-js/modules/es.date.to-primitive.js");
require("core-js/modules/es.json.to-string-tag.js");
require("core-js/modules/es.math.to-string-tag.js");
require("core-js/modules/es.number.constructor.js");
require("core-js/modules/es.object.create.js");
require("core-js/modules/es.object.define-property.js");
require("core-js/modules/es.object.get-own-property-descriptor.js");
require("core-js/modules/es.object.get-prototype-of.js");
require("core-js/modules/es.object.set-prototype-of.js");
require("core-js/modules/es.string.iterator.js");
require("core-js/modules/es.weak-map.js");
require("core-js/modules/web.dom-collections.for-each.js");
require("core-js/modules/web.dom-collections.iterator.js");
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = endpointResourceIntegrationRepository;
require("core-js/modules/es.symbol.js");
require("core-js/modules/es.symbol.description.js");
require("core-js/modules/es.array.filter.js");
require("core-js/modules/es.array.map.js");
require("core-js/modules/es.date.now.js");
require("core-js/modules/es.date.to-json.js");
require("core-js/modules/es.function.name.js");
require("core-js/modules/es.object.keys.js");
require("core-js/modules/es.object.to-string.js");
require("core-js/modules/es.promise.js");
require("core-js/modules/es.regexp.exec.js");
require("core-js/modules/es.string.replace.js");
var _models = _interopRequireWildcard(require("../../models"));
var _helpers = _interopRequireDefault(require("../../libs/utils/helpers"));
var _responseApi = require("../../libs/utils/responseApi");
var _sequelize = require("sequelize");
var _callQuery = require("../../libs/utils/callQuery");
var _query = require("./query");
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != _typeof(e) && "function" != typeof e) return { "default": e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && {}.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n["default"] = e, t && t.set(e, n), n; }
function _defineProperty(e, r, t) { return (r = _toPropertyKey(r)) in e ? Object.defineProperty(e, r, { value: t, enumerable: !0, configurable: !0, writable: !0 }) : e[r] = t, e; }
function _toPropertyKey(t) { var i = _toPrimitive(t, "string"); return "symbol" == _typeof(i) ? i : i + ""; }
function _toPrimitive(t, r) { if ("object" != _typeof(t) || !t) return t; var e = t[Symbol.toPrimitive]; if (void 0 !== e) { var i = e.call(t, r || "default"); if ("object" != _typeof(i)) return i; throw new TypeError("@@toPrimitive must return a primitive value."); } return ("string" === r ? String : Number)(t); }
function _regeneratorRuntime() { "use strict"; /*! regenerator-runtime -- Copyright (c) 2014-present, Facebook, Inc. -- license (MIT): https://github.com/facebook/regenerator/blob/main/LICENSE */ _regeneratorRuntime = function _regeneratorRuntime() { return e; }; var t, e = {}, r = Object.prototype, n = r.hasOwnProperty, o = Object.defineProperty || function (t, e, r) { t[e] = r.value; }, i = "function" == typeof Symbol ? Symbol : {}, a = i.iterator || "@@iterator", c = i.asyncIterator || "@@asyncIterator", u = i.toStringTag || "@@toStringTag"; function define(t, e, r) { return Object.defineProperty(t, e, { value: r, enumerable: !0, configurable: !0, writable: !0 }), t[e]; } try { define({}, ""); } catch (t) { define = function define(t, e, r) { return t[e] = r; }; } function wrap(t, e, r, n) { var i = e && e.prototype instanceof Generator ? e : Generator, a = Object.create(i.prototype), c = new Context(n || []); return o(a, "_invoke", { value: makeInvokeMethod(t, r, c) }), a; } function tryCatch(t, e, r) { try { return { type: "normal", arg: t.call(e, r) }; } catch (t) { return { type: "throw", arg: t }; } } e.wrap = wrap; var h = "suspendedStart", l = "suspendedYield", f = "executing", s = "completed", y = {}; function Generator() {} function GeneratorFunction() {} function GeneratorFunctionPrototype() {} var p = {}; define(p, a, function () { return this; }); var d = Object.getPrototypeOf, v = d && d(d(values([]))); v && v !== r && n.call(v, a) && (p = v); var g = GeneratorFunctionPrototype.prototype = Generator.prototype = Object.create(p); function defineIteratorMethods(t) { ["next", "throw", "return"].forEach(function (e) { define(t, e, function (t) { return this._invoke(e, t); }); }); } function AsyncIterator(t, e) { function invoke(r, o, i, a) { var c = tryCatch(t[r], t, o); if ("throw" !== c.type) { var u = c.arg, h = u.value; return h && "object" == _typeof(h) && n.call(h, "__await") ? e.resolve(h.__await).then(function (t) { invoke("next", t, i, a); }, function (t) { invoke("throw", t, i, a); }) : e.resolve(h).then(function (t) { u.value = t, i(u); }, function (t) { return invoke("throw", t, i, a); }); } a(c.arg); } var r; o(this, "_invoke", { value: function value(t, n) { function callInvokeWithMethodAndArg() { return new e(function (e, r) { invoke(t, n, e, r); }); } return r = r ? r.then(callInvokeWithMethodAndArg, callInvokeWithMethodAndArg) : callInvokeWithMethodAndArg(); } }); } function makeInvokeMethod(e, r, n) { var o = h; return function (i, a) { if (o === f) throw Error("Generator is already running"); if (o === s) { if ("throw" === i) throw a; return { value: t, done: !0 }; } for (n.method = i, n.arg = a;;) { var c = n.delegate; if (c) { var u = maybeInvokeDelegate(c, n); if (u) { if (u === y) continue; return u; } } if ("next" === n.method) n.sent = n._sent = n.arg;else if ("throw" === n.method) { if (o === h) throw o = s, n.arg; n.dispatchException(n.arg); } else "return" === n.method && n.abrupt("return", n.arg); o = f; var p = tryCatch(e, r, n); if ("normal" === p.type) { if (o = n.done ? s : l, p.arg === y) continue; return { value: p.arg, done: n.done }; } "throw" === p.type && (o = s, n.method = "throw", n.arg = p.arg); } }; } function maybeInvokeDelegate(e, r) { var n = r.method, o = e.iterator[n]; if (o === t) return r.delegate = null, "throw" === n && e.iterator["return"] && (r.method = "return", r.arg = t, maybeInvokeDelegate(e, r), "throw" === r.method) || "return" !== n && (r.method = "throw", r.arg = new TypeError("The iterator does not provide a '" + n + "' method")), y; var i = tryCatch(o, e.iterator, r.arg); if ("throw" === i.type) return r.method = "throw", r.arg = i.arg, r.delegate = null, y; var a = i.arg; return a ? a.done ? (r[e.resultName] = a.value, r.next = e.nextLoc, "return" !== r.method && (r.method = "next", r.arg = t), r.delegate = null, y) : a : (r.method = "throw", r.arg = new TypeError("iterator result is not an object"), r.delegate = null, y); } function pushTryEntry(t) { var e = { tryLoc: t[0] }; 1 in t && (e.catchLoc = t[1]), 2 in t && (e.finallyLoc = t[2], e.afterLoc = t[3]), this.tryEntries.push(e); } function resetTryEntry(t) { var e = t.completion || {}; e.type = "normal", delete e.arg, t.completion = e; } function Context(t) { this.tryEntries = [{ tryLoc: "root" }], t.forEach(pushTryEntry, this), this.reset(!0); } function values(e) { if (e || "" === e) { var r = e[a]; if (r) return r.call(e); if ("function" == typeof e.next) return e; if (!isNaN(e.length)) { var o = -1, i = function next() { for (; ++o < e.length;) if (n.call(e, o)) return next.value = e[o], next.done = !1, next; return next.value = t, next.done = !0, next; }; return i.next = i; } } throw new TypeError(_typeof(e) + " is not iterable"); } return GeneratorFunction.prototype = GeneratorFunctionPrototype, o(g, "constructor", { value: GeneratorFunctionPrototype, configurable: !0 }), o(GeneratorFunctionPrototype, "constructor", { value: GeneratorFunction, configurable: !0 }), GeneratorFunction.displayName = define(GeneratorFunctionPrototype, u, "GeneratorFunction"), e.isGeneratorFunction = function (t) { var e = "function" == typeof t && t.constructor; return !!e && (e === GeneratorFunction || "GeneratorFunction" === (e.displayName || e.name)); }, e.mark = function (t) { return Object.setPrototypeOf ? Object.setPrototypeOf(t, GeneratorFunctionPrototype) : (t.__proto__ = GeneratorFunctionPrototype, define(t, u, "GeneratorFunction")), t.prototype = Object.create(g), t; }, e.awrap = function (t) { return { __await: t }; }, defineIteratorMethods(AsyncIterator.prototype), define(AsyncIterator.prototype, c, function () { return this; }), e.AsyncIterator = AsyncIterator, e.async = function (t, r, n, o, i) { void 0 === i && (i = Promise); var a = new AsyncIterator(wrap(t, r, n, o), i); return e.isGeneratorFunction(r) ? a : a.next().then(function (t) { return t.done ? t.value : a.next(); }); }, defineIteratorMethods(g), define(g, u, "Generator"), define(g, a, function () { return this; }), define(g, "toString", function () { return "[object Generator]"; }), e.keys = function (t) { var e = Object(t), r = []; for (var n in e) r.push(n); return r.reverse(), function next() { for (; r.length;) { var t = r.pop(); if (t in e) return next.value = t, next.done = !1, next; } return next.done = !0, next; }; }, e.values = values, Context.prototype = { constructor: Context, reset: function reset(e) { if (this.prev = 0, this.next = 0, this.sent = this._sent = t, this.done = !1, this.delegate = null, this.method = "next", this.arg = t, this.tryEntries.forEach(resetTryEntry), !e) for (var r in this) "t" === r.charAt(0) && n.call(this, r) && !isNaN(+r.slice(1)) && (this[r] = t); }, stop: function stop() { this.done = !0; var t = this.tryEntries[0].completion; if ("throw" === t.type) throw t.arg; return this.rval; }, dispatchException: function dispatchException(e) { if (this.done) throw e; var r = this; function handle(n, o) { return a.type = "throw", a.arg = e, r.next = n, o && (r.method = "next", r.arg = t), !!o; } for (var o = this.tryEntries.length - 1; o >= 0; --o) { var i = this.tryEntries[o], a = i.completion; if ("root" === i.tryLoc) return handle("end"); if (i.tryLoc <= this.prev) { var c = n.call(i, "catchLoc"), u = n.call(i, "finallyLoc"); if (c && u) { if (this.prev < i.catchLoc) return handle(i.catchLoc, !0); if (this.prev < i.finallyLoc) return handle(i.finallyLoc); } else if (c) { if (this.prev < i.catchLoc) return handle(i.catchLoc, !0); } else { if (!u) throw Error("try statement without catch or finally"); if (this.prev < i.finallyLoc) return handle(i.finallyLoc); } } } }, abrupt: function abrupt(t, e) { for (var r = this.tryEntries.length - 1; r >= 0; --r) { var o = this.tryEntries[r]; if (o.tryLoc <= this.prev && n.call(o, "finallyLoc") && this.prev < o.finallyLoc) { var i = o; break; } } i && ("break" === t || "continue" === t) && i.tryLoc <= e && e <= i.finallyLoc && (i = null); var a = i ? i.completion : {}; return a.type = t, a.arg = e, i ? (this.method = "next", this.next = i.finallyLoc, y) : this.complete(a); }, complete: function complete(t, e) { if ("throw" === t.type) throw t.arg; return "break" === t.type || "continue" === t.type ? this.next = t.arg : "return" === t.type ? (this.rval = this.arg = t.arg, this.method = "return", this.next = "end") : "normal" === t.type && e && (this.next = e), y; }, finish: function finish(t) { for (var e = this.tryEntries.length - 1; e >= 0; --e) { var r = this.tryEntries[e]; if (r.finallyLoc === t) return this.complete(r.completion, r.afterLoc), resetTryEntry(r), y; } }, "catch": function _catch(t) { for (var e = this.tryEntries.length - 1; e >= 0; --e) { var r = this.tryEntries[e]; if (r.tryLoc === t) { var n = r.completion; if ("throw" === n.type) { var o = n.arg; resetTryEntry(r); } return o; } } throw Error("illegal catch attempt"); }, delegateYield: function delegateYield(e, r, n) { return this.delegate = { iterator: values(e), resultName: r, nextLoc: n }, "next" === this.method && (this.arg = t), y; } }, e; }
function asyncGeneratorStep(n, t, e, r, o, a, c) { try { var i = n[a](c), u = i.value; } catch (n) { return void e(n); } i.done ? t(u) : Promise.resolve(u).then(r, o); }
function _asyncToGenerator(n) { return function () { var t = this, e = arguments; return new Promise(function (r, o) { var a = n.apply(t, e); function _next(n) { asyncGeneratorStep(a, r, o, _next, _throw, "next", n); } function _throw(n) { asyncGeneratorStep(a, r, o, _next, _throw, "throw", n); } _next(void 0); }); }; }
var helpers = (0, _helpers["default"])();
function endpointResourceIntegrationRepository() {
  var create = /*#__PURE__*/function () {
    var _ref = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee(req) {
      var _req$body, idEnv, status, groupName, name, description, version, path, method, params, bodyRequest, requestExample, responseExample, request, mappingTemplate, responseTrigger, resCreate, getDetail;
      return _regeneratorRuntime().wrap(function _callee$(_context) {
        while (1) switch (_context.prev = _context.next) {
          case 0:
            _req$body = req.body, idEnv = _req$body.idEnv, status = _req$body.status, groupName = _req$body.groupName, name = _req$body.name, description = _req$body.description, version = _req$body.version, path = _req$body.path, method = _req$body.method, params = _req$body.params, bodyRequest = _req$body.bodyRequest, requestExample = _req$body.requestExample, responseExample = _req$body.responseExample, request = _req$body.request, mappingTemplate = _req$body.mappingTemplate, responseTrigger = _req$body.responseTrigger;
            _context.prev = 1;
            _context.next = 4;
            return _models.endpointResourceIntegration.create({
              id: Date.now(),
              idEnv: idEnv,
              status: status,
              groupName: groupName,
              name: name,
              description: description,
              version: version,
              path: path,
              method: method,
              params: params ? JSON.stringify(params) : "{}",
              bodyRequest: bodyRequest ? JSON.stringify(bodyRequest) : "{}",
              requestExample: requestExample ? JSON.stringify(requestExample) : "{}",
              responseExample: responseExample ? JSON.stringify(responseExample) : "{}",
              request: request ? JSON.stringify(request) : "{}",
              mappingTemplate: mappingTemplate ? JSON.stringify(mappingTemplate) : "{}",
              responseTrigger: responseTrigger ? JSON.stringify(responseTrigger) : "{}"
            });
          case 4:
            resCreate = _context.sent;
            req.params = {
              id: resCreate.id
            };
            _context.next = 8;
            return endpointResourceIntegrationRepository().detail(req);
          case 8:
            getDetail = _context.sent;
            if (!(getDetail.code === _responseApi.statusHttpCode.badRequest400.code)) {
              _context.next = 11;
              break;
            }
            return _context.abrupt("return", getDetail);
          case 11:
            return _context.abrupt("return", (0, _responseApi.result)({
              data: getDetail.results
            }));
          case 14:
            _context.prev = 14;
            _context.t0 = _context["catch"](1);
            _context.t0.message = "endpointResourceIntegrationRepository.create: " + _context.t0;
            throw _context.t0;
          case 18:
          case "end":
            return _context.stop();
        }
      }, _callee, null, [[1, 14]]);
    }));
    return function create(_x) {
      return _ref.apply(this, arguments);
    };
  }();
  var update = /*#__PURE__*/function () {
    var _ref2 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee2(req) {
      var id, _req$body2, idEnv, status, groupName, name, description, version, path, method, params, bodyRequest, requestExample, responseExample, request, mappingTemplate, responseTrigger, getDetail;
      return _regeneratorRuntime().wrap(function _callee2$(_context2) {
        while (1) switch (_context2.prev = _context2.next) {
          case 0:
            id = req.params.id;
            _req$body2 = req.body, idEnv = _req$body2.idEnv, status = _req$body2.status, groupName = _req$body2.groupName, name = _req$body2.name, description = _req$body2.description, version = _req$body2.version, path = _req$body2.path, method = _req$body2.method, params = _req$body2.params, bodyRequest = _req$body2.bodyRequest, requestExample = _req$body2.requestExample, responseExample = _req$body2.responseExample, request = _req$body2.request, mappingTemplate = _req$body2.mappingTemplate, responseTrigger = _req$body2.responseTrigger;
            _context2.prev = 2;
            _context2.next = 5;
            return endpointResourceIntegrationRepository().detail(req);
          case 5:
            getDetail = _context2.sent;
            if (!(getDetail.code === _responseApi.statusHttpCode.badRequest400.code)) {
              _context2.next = 8;
              break;
            }
            return _context2.abrupt("return", getDetail);
          case 8:
            _context2.next = 10;
            return _models.endpointResourceIntegration.update({
              idEnv: idEnv,
              status: status,
              groupName: groupName,
              name: name,
              description: description,
              version: version,
              path: path,
              method: method,
              params: params ? JSON.stringify(params) : "{}",
              bodyRequest: bodyRequest ? JSON.stringify(bodyRequest) : "{}",
              requestExample: requestExample ? JSON.stringify(requestExample) : "{}",
              responseExample: responseExample ? JSON.stringify(responseExample) : "{}",
              request: request ? JSON.stringify(request) : "{}",
              mappingTemplate: mappingTemplate ? JSON.stringify(mappingTemplate) : "{}",
              responseTrigger: responseTrigger ? JSON.stringify(responseTrigger) : "{}"
            }, {
              where: {
                id: id
              }
            });
          case 10:
            _context2.next = 12;
            return endpointResourceIntegrationRepository().detail(req);
          case 12:
            getDetail = _context2.sent;
            return _context2.abrupt("return", (0, _responseApi.result)({
              data: getDetail.results
            }));
          case 16:
            _context2.prev = 16;
            _context2.t0 = _context2["catch"](2);
            _context2.t0.message = "endpointResourceIntegrationRepository.update: " + _context2.t0;
            throw _context2.t0;
          case 20:
          case "end":
            return _context2.stop();
        }
      }, _callee2, null, [[2, 16]]);
    }));
    return function update(_x2) {
      return _ref2.apply(this, arguments);
    };
  }();
  var detail = /*#__PURE__*/function () {
    var _ref3 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee3(req) {
      var id, getDetail;
      return _regeneratorRuntime().wrap(function _callee3$(_context3) {
        while (1) switch (_context3.prev = _context3.next) {
          case 0:
            id = req.params.id;
            _context3.prev = 1;
            _context3.next = 4;
            return _models.endpointResourceIntegration.findOne({
              where: {
                id: id
              },
              include: [{
                model: _models.environtment,
                attributes: ['id', 'envName']
              }]
            });
          case 4:
            getDetail = _context3.sent;
            if (getDetail) {
              _context3.next = 9;
              break;
            }
            _context3.next = 8;
            return (0, _responseApi.result)({
              message: "ID ".concat(id, " Not Found"),
              statusCode: _responseApi.statusHttpCode.badRequest400
            });
          case 8:
            return _context3.abrupt("return", _context3.sent);
          case 9:
            getDetail.responseTrigger = getDetail.responseTrigger ? JSON.parse(getDetail.responseTrigger) : getDetail.responseTrigger;
            getDetail.params = getDetail.params ? JSON.parse(getDetail.params) : getDetail.params;
            getDetail.bodyRequest = getDetail.bodyRequest ? JSON.parse(getDetail.bodyRequest) : getDetail.bodyRequest;
            getDetail.mappingTemplate = getDetail.mappingTemplate ? JSON.parse(getDetail.mappingTemplate) : getDetail.mappingTemplate;
            getDetail.request = getDetail.request ? JSON.parse(getDetail.request) : getDetail.request;
            getDetail.requestExample = getDetail.requestExample ? JSON.parse(getDetail.requestExample) : getDetail.requestExample;
            getDetail.responseExample = getDetail.responseExample ? JSON.parse(getDetail.responseExample) : getDetail.responseExample;
            getDetail.dataValues.pathName = getDetail.environtment.envName + getDetail.path;
            _context3.next = 19;
            return (0, _responseApi.result)({
              data: getDetail
            });
          case 19:
            return _context3.abrupt("return", _context3.sent);
          case 22:
            _context3.prev = 22;
            _context3.t0 = _context3["catch"](1);
            _context3.t0.message = "endpointResourceIntegrationRepository.detail: " + _context3.t0;
            throw _context3.t0;
          case 26:
          case "end":
            return _context3.stop();
        }
      }, _callee3, null, [[1, 22]]);
    }));
    return function detail(_x3) {
      return _ref3.apply(this, arguments);
    };
  }();
  var showList = /*#__PURE__*/function () {
    var _ref4 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee4(req) {
      var _req$query, page, size, descending, category, name, method, envName, _sql_showlist_mastera, select, from, where, orderByRowNumber, getData;
      return _regeneratorRuntime().wrap(function _callee4$(_context4) {
        while (1) switch (_context4.prev = _context4.next) {
          case 0:
            _req$query = req.query, page = _req$query.page, size = _req$query.size, descending = _req$query.descending, category = _req$query.category, name = _req$query.name, method = _req$query.method, envName = _req$query.envName;
            _sql_showlist_mastera = (0, _query.sql_showlist_masterapi)({
              envName: envName,
              category: category,
              desc: descending.length === 0 ? 'DESC' : descending,
              method: method,
              name: name
            }), select = _sql_showlist_mastera.select, from = _sql_showlist_mastera.from, where = _sql_showlist_mastera.where, orderByRowNumber = _sql_showlist_mastera.orderByRowNumber;
            _context4.next = 4;
            return (0, _callQuery.queryWithPagination)({
              select: select,
              from: from,
              where: where,
              orderByRowNumber: orderByRowNumber,
              pageNumber: page,
              pageSize: size
            });
          case 4:
            getData = _context4.sent;
            return _context4.abrupt("return", (0, _responseApi.result)({
              data: getData
            }));
          case 6:
          case "end":
            return _context4.stop();
        }
      }, _callee4);
    }));
    return function showList(_x4) {
      return _ref4.apply(this, arguments);
    };
  }();
  var showListAccessApi = /*#__PURE__*/function () {
    var _ref5 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee5(req) {
      var _req$query2, page, size, idEnv, userId, showDisabledList, orderByCreatedAt, category, filter, method, hideDisabled, orderByCreateDisable, _req$decoded, role, id, onHideDisabled, onShowDisabledList, idUser, getEnv, _sql_showListMasterAp, select, from, where, orderByRowNumber, getData;
      return _regeneratorRuntime().wrap(function _callee5$(_context5) {
        while (1) switch (_context5.prev = _context5.next) {
          case 0:
            _req$query2 = req.query, page = _req$query2.page, size = _req$query2.size, idEnv = _req$query2.idEnv, userId = _req$query2.userId, showDisabledList = _req$query2.showDisabledList, orderByCreatedAt = _req$query2.orderByCreatedAt, category = _req$query2.category, filter = _req$query2.filter, method = _req$query2.method, hideDisabled = _req$query2.hideDisabled, orderByCreateDisable = _req$query2.orderByCreateDisable;
            _req$decoded = req.decoded, role = _req$decoded.role, id = _req$decoded.id;
            onHideDisabled = false;
            onShowDisabledList = false;
            if (idEnv) {
              _context5.next = 6;
              break;
            }
            return _context5.abrupt("return", (0, _responseApi.result)({
              statusCode: _responseApi.statusHttpCode.badRequest400,
              message: "idEnv cant be empty"
            }));
          case 6:
            // todo if USER action
            if (role > 1) {
              onHideDisabled = true;
              idUser = id;
            }

            // todo if ADMIN action
            if (role === 1) {
              onShowDisabledList = showDisabledList;
              idUser = userId ? userId : id;
              onHideDisabled = hideDisabled;
            }

            //todo check environment access (apakah memiliki access)
            _context5.next = 10;
            return _models.environmentAccess.findOne({
              attributes: ["id", "idEnv", "idUser"],
              where: {
                idEnv: idEnv,
                idUser: idUser
              }
            });
          case 10:
            getEnv = _context5.sent;
            if (!(!getEnv && role > 1)) {
              _context5.next = 13;
              break;
            }
            return _context5.abrupt("return", (0, _responseApi.result)({
              message: "Env ID ".concat(idEnv, " Not Found or users cannot access"),
              statusCode: _responseApi.statusHttpCode.badRequest400
            }));
          case 13:
            _sql_showListMasterAp = (0, _query.sql_showListMasterApiWithAccess)({
              showDisabledOnly: onShowDisabledList,
              hideDisabled: onHideDisabled,
              category: category,
              userId: idUser,
              idEnv: idEnv,
              filter: filter,
              orderByCreatedAt: orderByCreatedAt,
              method: method,
              orderByCreateDisable: orderByCreateDisable
            }), select = _sql_showListMasterAp.select, from = _sql_showListMasterAp.from, where = _sql_showListMasterAp.where, orderByRowNumber = _sql_showListMasterAp.orderByRowNumber;
            _context5.next = 16;
            return (0, _callQuery.queryWithPagination)({
              select: select,
              from: from,
              where: where,
              orderByRowNumber: orderByRowNumber,
              pageNumber: page,
              pageSize: size
            });
          case 16:
            getData = _context5.sent;
            return _context5.abrupt("return", (0, _responseApi.result)({
              data: getData
            }));
          case 18:
          case "end":
            return _context5.stop();
        }
      }, _callee5);
    }));
    return function showListAccessApi(_x5) {
      return _ref5.apply(this, arguments);
    };
  }();
  var deleted = /*#__PURE__*/function () {
    var _ref6 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee6(req) {
      var id, checkId;
      return _regeneratorRuntime().wrap(function _callee6$(_context6) {
        while (1) switch (_context6.prev = _context6.next) {
          case 0:
            id = req.params.id;
            _context6.prev = 1;
            _context6.next = 4;
            return endpointResourceIntegrationRepository().detail(req);
          case 4:
            checkId = _context6.sent;
            if (!(checkId.code === _responseApi.statusHttpCode.badRequest400.code)) {
              _context6.next = 7;
              break;
            }
            return _context6.abrupt("return", checkId);
          case 7:
            _models.endpointResourceIntegration.destroy({
              where: {
                id: id
              }
            });
            return _context6.abrupt("return", (0, _responseApi.result)({
              message: "DELETED ID " + id
            }));
          case 11:
            _context6.prev = 11;
            _context6.t0 = _context6["catch"](1);
            _context6.t0.message = "endpointResourceIntegrationRepository.deleted: " + _context6.t0;
            throw _context6.t0;
          case 15:
          case "end":
            return _context6.stop();
        }
      }, _callee6, null, [[1, 11]]);
    }));
    return function deleted(_x6) {
      return _ref6.apply(this, arguments);
    };
  }();
  var groupByCategory = /*#__PURE__*/function () {
    var _ref7 = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee7(req) {
      var envName, conditionEnv, model, newModel, initial;
      return _regeneratorRuntime().wrap(function _callee7$(_context7) {
        while (1) switch (_context7.prev = _context7.next) {
          case 0:
            _context7.prev = 0;
            envName = req.query.envName;
            conditionEnv = _defineProperty({}, _sequelize.Op.and, [{
              envName: _defineProperty({}, _sequelize.Op.eq, "".concat(envName))
            }]);
            _context7.next = 5;
            return _models.endpointResourceIntegration.findAll({
              attributes: ['groupName'],
              group: ['groupName'],
              include: [{
                model: _models.environtment,
                attributes: [],
                where: conditionEnv
              }]
            });
          case 5:
            model = _context7.sent;
            newModel = []; //string to Pascalcase
            if (model.length > 0) {
              newModel = model.map(function (el) {
                var toPascalString = el.groupName.replace(/(\w)(\w*)/g, function (g0, g1, g2) {
                  return g1.toUpperCase() + g2.toLowerCase();
                });
                return {
                  'label': toPascalString,
                  'value': el.groupName
                };
              });
              initial = {
                'label': 'All',
                'value': ''
              }; //add fist element
              newModel.unshift(initial);
            }
            return _context7.abrupt("return", (0, _responseApi.result)({
              data: newModel
            }));
          case 11:
            _context7.prev = 11;
            _context7.t0 = _context7["catch"](0);
            _context7.t0.message = "endpointResourceIntegrationRepository.groupByCategory: " + _context7.t0;
            throw _context7.t0;
          case 15:
          case "end":
            return _context7.stop();
        }
      }, _callee7, null, [[0, 11]]);
    }));
    return function groupByCategory(_x7) {
      return _ref7.apply(this, arguments);
    };
  }();
  return {
    create: create,
    detail: detail,
    update: update,
    showList: showList,
    deleted: deleted,
    groupByCategory: groupByCategory,
    showListAccessApi: showListAccessApi
  };
}