"use strict";

require("core-js/modules/es.object.define-property.js");
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.sql_showlist_masterapi = exports.sql_showListMasterApiWithAccess = void 0;
require("core-js/modules/es.array.concat.js");
require("core-js/modules/es.array.filter.js");
require("core-js/modules/es.array.map.js");
require("core-js/modules/es.function.name.js");
require("core-js/modules/es.object.to-string.js");
require("core-js/modules/es.regexp.exec.js");
require("core-js/modules/es.string.replace.js");
var _sequelize = require("sequelize");
var sql_showlist_masterapi = exports.sql_showlist_masterapi = function sql_showlist_masterapi(_ref) {
  var _ref$category = _ref.category,
    category = _ref$category === void 0 ? '' : _ref$category,
    _ref$envName = _ref.envName,
    envName = _ref$envName === void 0 ? '' : _ref$envName,
    _ref$desc = _ref.desc,
    desc = _ref$desc === void 0 ? 'DESC' : _ref$desc,
    _ref$method = _ref.method,
    method = _ref$method === void 0 ? '' : _ref$method,
    _ref$name = _ref.name,
    name = _ref$name === void 0 ? '' : _ref$name;
  var categoryString = "";
  if (category.length > 0) {
    category.split(',').map(function (el) {
      el = el.replace(/\s/g, '');
      if (categoryString.length === 0) {
        categoryString += "[endpointResourceIntegration].[groupName] = N'".concat(el, "'");
      } else {
        categoryString += " OR [endpointResourceIntegration].[groupName] = N'".concat(el, "'");
      }
    });
  }
  var select = "SELECT [endpointResourceIntegration].[id], [endpointResourceIntegration].[idEnv], " + "[endpointResourceIntegration].[createdBy], [endpointResourceIntegration].[updateBy], " + "[endpointResourceIntegration].[deletedBy], [endpointResourceIntegration].[createdAt], " + "[endpointResourceIntegration].[updatedAt], [endpointResourceIntegration].[deletedAt], " + "[endpointResourceIntegration].[status], [endpointResourceIntegration].[groupName], " + "[endpointResourceIntegration].[name], [endpointResourceIntegration].[description], " + "[endpointResourceIntegration].[version], [endpointResourceIntegration].[path], " + "[endpointResourceIntegration].[method], " + "[endpointResourceIntegration].[responseTrigger], [environtment].[id] AS [environtment.id], " + "[environtment].[envName] AS [environtment.envName]";
  var from = "FROM [endpointResourceIntegration] AS [endpointResourceIntegration] \n    INNER JOIN [environtment] AS [environtment] ON [endpointResourceIntegration].[idEnv] = [environtment].[id] \n    AND ([environtment].[deletedAt] IS NULL AND ([environtment].[envName] = N'".concat(envName, "'))");
  var where = "WHERE [endpointResourceIntegration].[deletedAt] IS NULL";
  var orderByRowNumber = "[endpointResourceIntegration].[createdAt] ".concat(desc);

  //todo menambahkan where clause
  // todo pencarian berdasarkan category
  if (categoryString.length > 0) {
    where += " AND ( ".concat(categoryString, " )");
  }

  //todo pencarian berdasarkan method
  if (method.length > 0) {
    where += " AND [endpointResourceIntegration].[method] LIKE N'%".concat(method, "%'");
  }

  //todo pencarian berdasarkan id, name, path hanya dari parameter `name`
  if (name.length > 0) {
    where += " AND ( \n            [endpointResourceIntegration].[id] LIKE '%".concat(name, "%' \n            OR [endpointResourceIntegration].[name] LIKE '%").concat(name, "%'\n            OR [endpointResourceIntegration].[path] LIKE '%").concat(name, "%' \n        )");
  }
  return {
    select: select,
    from: from,
    where: where,
    orderByRowNumber: orderByRowNumber
  };
};
var sql_showListMasterApiWithAccess = exports.sql_showListMasterApiWithAccess = function sql_showListMasterApiWithAccess(_ref2) {
  var _ref2$userId = _ref2.userId,
    userId = _ref2$userId === void 0 ? null : _ref2$userId,
    _ref2$category = _ref2.category,
    category = _ref2$category === void 0 ? null : _ref2$category,
    _ref2$idEnv = _ref2.idEnv,
    idEnv = _ref2$idEnv === void 0 ? null : _ref2$idEnv,
    _ref2$orderByCreatedA = _ref2.orderByCreatedAt,
    orderByCreatedAt = _ref2$orderByCreatedA === void 0 ? 'DESC' : _ref2$orderByCreatedA,
    _ref2$method = _ref2.method,
    method = _ref2$method === void 0 ? null : _ref2$method,
    _ref2$filter = _ref2.filter,
    filter = _ref2$filter === void 0 ? null : _ref2$filter,
    _ref2$showDisabledOnl = _ref2.showDisabledOnly,
    showDisabledOnly = _ref2$showDisabledOnl === void 0 ? false : _ref2$showDisabledOnl,
    _ref2$hideDisabled = _ref2.hideDisabled,
    hideDisabled = _ref2$hideDisabled === void 0 ? false : _ref2$hideDisabled,
    _ref2$orderByCreateDi = _ref2.orderByCreateDisable,
    orderByCreateDisable = _ref2$orderByCreateDi === void 0 ? null : _ref2$orderByCreateDi;
  var select = "SELECT Eri.id, Eri.idEnv, Eri.createdBy, Eri.updateBy, Eri.deletedBy, Eri.createdAt, Eri.updatedAt, Eri.deletedAt, Eri.status, \n                    Eri.groupName, Eri.name, Eri.description, Eri.version, Eri.path, Eri.method, Eri.responseTrigger, \n                    environtment.envName AS envName,\n                    apiAccess.id as apiAccessId, apiAccess.isDisabled";
  var from = "\n                FROM endpointResourceIntegration AS Eri WITH (NOLOCK)\n                INNER JOIN environtment AS environtment WITH (NOLOCK) ON Eri.idEnv = environtment.id\n                AND (environtment.deletedAt IS NULL \n                AND (environtment.id = '".concat(idEnv, "')) \n                LEFT OUTER JOIN apiAccess WITH (NOLOCK) ON apiAccess.idEnv=Eri.idEnv\n                AND (\n                     apiAccess.deletedAt IS NULL\n                     AND apiAccess.idEndpoint=Eri.id\n                     AND apiAccess.idUser = '").concat(userId, "'\n                )");
  var where = "WHERE Eri.deletedAt IS NULL ";
  var orderByRowNumber = "Eri.createdAt ".concat(orderByCreatedAt);
  if (orderByCreateDisable) {
    orderByRowNumber = "apiAccess.createdAt ".concat(orderByCreateDisable);
  }
  if (category) {
    var categoryString = "";
    category.split(',').map(function (el) {
      el = el.replace(/\s/g, '');
      if (categoryString.length === 0) {
        categoryString += " Eri.groupName = N'".concat(el, "'");
      } else {
        categoryString += " OR Eri.groupName = N'".concat(el, "'");
      }
    });
    where += "AND ( ".concat(categoryString, " )");
  }
  if (filter) {
    where += " AND ( \n            Eri.id LIKE '%".concat(filter, "%' \n            OR Eri.name LIKE '%").concat(filter, "%'\n            OR Eri.path LIKE '%").concat(filter, "%' \n        )");
  }
  if (method) {
    where += "AND Eri.method LIKE N'%".concat(method, "%'");
  }
  if (showDisabledOnly === 'true') {
    where += "AND apiAccess.isDisabled=1";
  }
  if (hideDisabled === 'true') {
    where += "AND apiAccess.id IS NULL";
  }
  return {
    select: select,
    from: from,
    where: where,
    orderByRowNumber: orderByRowNumber
  };
};