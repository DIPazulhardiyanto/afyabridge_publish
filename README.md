# Afya Bridge

API Third Party For Bridging Health System

## Install dependencies

```
npm install
```
## Requirement

```
Node V16+ (Current V18)
```

## SETUP PRODUCTION

```
Periksa package.json
tambahkan ignore untuk file berikut:
# Mengabaikan folder config di dalam dist/app
dist/app/config/

```
## Run the server

```
npm start
```